@extends('sup_admin.index')

@section('content')




<!-- <div class="wrapper"> -->

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('sup_admin/suplier_dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Add Video</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-1"></div>
                <div class="col-10">

                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Add Video</strong></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        @include('admin.partials.messages')
                        <form name="addVideoForm" id="addVideoForm" class="form-horizontal" method="post" action="{{route('Suplier.Add.Video')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                 <input type="hidden" name="id" value="{{auth()->user()->id}}" />
                                 <div class="form-group row">
                               <label for="Description" class="col-sm-2 col-form-label">Description</label>
                               <div class="col-sm-10">
                               <textarea type="text" class="form-control textarea" id="description" name="description" value="{{old('description')}}" placeholder="Description"  rows="8"></textarea>
                            </div>
                           </div>
                             <div class="form-group row">
                        <label for="Price" class="col-sm-2 col-form-label">Company Video </label>
                                    <div class="col-sm-10">
                                        <input type="file" accept="video/mp4,video/x-m4v,video/*"  class="form-control" id="size" name="video_url">
                  
                                    </div>
                            </div>
                                <div class="form-group row">
                                    <label for="Status" class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10">
                                        <select class="form-control select2" style="width: 100%;" name="status" required>
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                           
                                        </select>
                                    </div>
                                </div>

                               
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-info add_video">Add Video</button>
                                <a href="{{url('Suplier/Video.Lists')}}" class="btn btn-default custom-btn">Cancel</a>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>

                </div>
                <div class="col-1"></div>
            </div>
        </div>
    </section>

</div>

<!-- </div> -->
<script type="text/javascript">
  $('.add_video').on('click',function(){
   
    var video  = $('#size').val();
    
    
    var error_message = "";
    if(video=='')
    {
      error_message = "Please Select Video.";
    }
    
    
    
    
    //alert(error_message);
    if(error_message!='')
    {
      $('.custom_alert').css('display','block');
      $('.alert_message').html(error_message);
      close_alert();
      return false;
    }
    else
    {
      $('#addVideoForm').submit();
    }
  });
</script>
@endsection


