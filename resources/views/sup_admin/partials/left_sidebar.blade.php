

<style>
.side-active{
    background:#9e3058;
}
</style>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->

    <a href="{{url('sup_admin/suplier_dashboard')}}" class="brand-link">
        <img src="{{asset('public/images/app_logo.png')}}" alt="Sanitization" class="brand-image img-circle elevation-3"
             style="opacity: .8;">  

        <span class="brand-text font-weight-light">{{env('APP_NAME')}}</span>
    </a>
   <p>{{Session::get('previoususer')}}</p>
    @if(!empty(Session::get('previoususer')))
    <a href="{{url('sup_admin/admin_redirect')}}" class="brand-link"><button class="btn btn-info" style="margin-left: 14px;">Back</button></a>
    @endif

    

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <!-- <div class="image">
                <img src="{{auth()->user()->profile_image ?? asset('public/images/user-logo.png')}}" class="img-circle elevation-2" alt="User Image">
            </div> -->
            <div class="image">
                <img src="{{asset('public/images/user-logo.png')}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="{{url('sup_admin/suplier_dashboard')}}" class="d-block">{{auth()->user()->name ?? ""}}</a>
            </div>
            
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item {{ (request()->is('sup_admin/profile')) || (request()->is('sup_admin/change-password')) || (request()->is('sup_admin/get-profile')) ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-info"></i>
                        <p>Manage Account
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                     <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('Suplier.Profile')}}" class="nav-link {{ (request()->is('sup_admin/profile')) ? 'active-child' : '' }}">
                                <i class="fas fa-address-card nav-icon"></i>
                                <p>Profile</p>
                            </a>
                        </li>
                       <li class="nav-item">
                            <a href="{{route('Suplier.Change.Password.Form')}}" class="nav-link {{ (request()->is('sup_admin/change-password')) ? 'active-child' : '' }}">
                                <i class="fas fa-key nav-icon"></i>
                                <p>Change Password</p>
                            </a>
                        </li>
                        
                    </ul>
                </li>

               <!--  <li class="nav-item">
                    <a href="{{route('Suplier.Users.List')}}" class="nav-link {{ (request()->is('sup_admin/user-list')) || (request()->is('sup_admin/add_user')) || (request()->is('sup_admin/edit_user/*'))? 'active' : '' }}">
                        <i class="nav-icon fas fa-users-cog"></i>
                        <p>
                            Manage User
                        </p>
                    </a>
                </li>   -->

             <li class="nav-item">
                    <a href="{{route('Suplier.Products.Lists')}}" class="nav-link {{ (request()->is('sup_admin/products_lists')) || (request()->is('sup_admin/add_products_lists')) || (request()->is('sup_admin/edit_products_lists/*'))? 'active' : '' }}">
                        <i class="nav-icon fas fa-users-cog"></i>
                        <p>
                            Manage Products
                        </p>
                    </a>
                </li>  




                 <li class="nav-item {{ (request()->is('sup_admin/banner_list')) || (request()->is('sup_admin/about_list')) || (request()->is('sup_admin/blog_list')) || (request()->is('sup_admin/testimonial_list')) || (request()->is('sup_admin/gallery_list')) || (request()->is('sup_admin/network_list')) || (request()->is('sup_admin/certificate_list'))  ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-info"></i>
                        <p>Manage Profile
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                    <li class="nav-item">
                    <a href="{{route('Suplier.Banner.Lists')}}" class="nav-link {{ (request()->is('sup_admin/banner_list')) || (request()->is('sup_admin/add_banner')) || (request()->is('sup_admin/edit_banner/*'))? 'active-child' : '' }}">
                     <i class="nav-icon fa fa-circle-o"></i>
                        <p>Contact Details</p>
                    </a>
                </li> 

                <li class="nav-item">
                    <a href="{{route('Suplier.About.Lists')}}" class="nav-link {{ (request()->is('sup_admin/about_list')) || (request()->is('sup_admin/add_about')) || (request()->is('sup_admin/edit_about/*'))? 'active-child' : '' }}">
                     <i class="nav-icon fa fa-circle-o"></i>
                        <p>About</p>
                    </a>
                </li> 

                 <li class="nav-item">
                    <a href="{{route('Suplier.Gallery.Lists')}}" class="nav-link {{ (request()->is('sup_admin/gallery_list')) || (request()->is('sup_admin/add_gallery')) || (request()->is('sup_admin/edit_gallery/*'))? 'active-child' : '' }}">
                     <i class="nav-icon fa fa-circle-o"></i>
                        <p>Gallery</p>
                    </a>
                </li> 

                <li class="nav-item">
                    <a href="{{route('Suplier.Network.Lists')}}" class="nav-link {{ (request()->is('sup_admin/network_list')) || (request()->is('sup_admin/add_network')) || (request()->is('sup_admin/edit_network/*'))? 'active-child' : '' }}">
                     <i class="nav-icon fa fa-circle-o"></i>
                        <p>Our Network</p>
                    </a>
                </li>


                <li class="nav-item">
                    <a href="{{route('Suplier.Testimonial.Lists')}}" class="nav-link {{ (request()->is('sup_admin/testimonial_list')) || (request()->is('sup_admin/add_testimonial')) || (request()->is('sup_admin/edit_testimonialt/*'))? 'active-child' : '' }}">
                     <i class="nav-icon fa fa-circle-o"></i>
                        <p>Testimonial</p>
                    </a>
                </li> 

                <li class="nav-item">
                    <a href="{{route('Suplier.Certificate.Lists')}}" class="nav-link {{ (request()->is('sup_admin/certificate_list')) || (request()->is('sup_admin/add_certificate')) || (request()->is('sup_admin/edit_certificate/*'))? 'active-child' : '' }}">
                     <i class="nav-icon fa fa-circle-o"></i>
                        <p>Certificate</p>
                    </a>
                </li> 

                 <li class="nav-item">
                    <a href="{{route('Suplier.Blog.Lists')}}" class="nav-link {{ (request()->is('sup_admin/blog_list')) || (request()->is('sup_admin/add_blog')) || (request()->is('sup_admin/edit_blog/*'))? 'active-child' : '' }}">
                     <i class="nav-icon fa fa-circle-o"></i>
                        <p>Blog</p>
                    </a>
                </li> 
                        
                    </ul>
                </li>

                <li class="nav-item">
                    <a href="{{route('Suplier.Enquiry.Lists')}}" class="nav-link {{ (request()->is('sup_admin/enquiry_list')) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-users-cog"></i>
                        <p>
                            Manage Business Enquiry
                        </p>
                    </a>
                </li>  

                <li class="nav-item">
                    <a href="{{route('SupAdmin.Logout')}}" class="nav-link">
                        <i class="fas fa-sign-out-alt nav-icon"></i>
                        <p>Logout</p>
                    </a>
                </li>

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>