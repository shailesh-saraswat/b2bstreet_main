@extends('sup_admin.index')

@section('content')

<!-- <div class="wrapper"> -->

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <!--  -->
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('sup_admin/suplier_dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Update Blog</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-1"></div>
                <div class="col-10">

                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Update Blog <strong></strong></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        @include('admin.partials.messages')
                        <form class="form-horizontal" method="post" action="{{route('Suplier.Update.Blog')}} " enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id" value="{{$blog->id}}" />
                            <div class="card-body">
                          
                          <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                <div class="form-group row">
                                    <label for="Title" class="col-sm-2 col-form-label">Title</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="title" name="title" value="{{$blog->title}}" placeholder="Title" required />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Date" class="col-sm-2 col-form-label">Date</label>
                                    <div class="col-sm-10">
                                        <input type="date" class="form-control" id="date" name="date" value="{{$blog->date}}" placeholder="Date" required />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="User Name" class="col-sm-2 col-form-label">User Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="user_name" name="user_name" value="{{$blog->user_name}}" placeholder="User Name" required />
                                    </div>
                                </div>

            <div class="form-group row">
                    <label for="Description" class="col-sm-2 col-form-label">Description</label>
                    <div class="col-sm-10">
                    <textarea type="text" class="form-control textarea" id="description" name="description" value="" placeholder="Description"  rows="10">{{$blog->description}}</textarea>
                </div>
            </div>

            <div class="form-group row">
            <label for="Image" class="col-sm-2 col-form-label">Image</label>
            <div class="col-sm-10">
            <input type="file" class="" id="image" name="image" value="">
            <img src="{{ asset($blog['image']) }}" style="width:150px;" />
                </div>
            </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-info">Update Blog</button>
                                <a href="{{route('Suplier.Blog.Lists')}}" class="btn btn-default float-right">Cancel</a>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>

                </div>
                <div class="col-1"></div>
            </div>
        </div>
    </section>

</div>

<!-- </div> -->
@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script>
@endsection
@section('after-style')
<link rel="stylesheet" href="{{asset('public/plugins/summernote/summernote-bs4.css')}}">
@endsection 