@extends('sup_admin.index')

@section('after-style')

<link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endsection

@section('content')

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('sup_admin/suplier_dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Manage Categories</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Categories List</h3>

                            <a href="{{route('Suplier.Add.Products.Form')}}" style="float: right;"><i class="fas fa-plus-square" style="font-size:25px;"></i></a>
                        </div>

                        
                        @if(session('message'))
                            <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
                        @endif
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="subcategory-list" class="table table-bordered table-striped " data-order='[[ 0, "asc" ]]'>


                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Menu Category Name</th>
                                        <th>Category Name</th>
                                        <th>Sub Category Name</th>
                                        <th>Product Name</th>
                                        <th>Price</th>
                                        <th>Quantity Available</th>
                                        <th>Packaging Type</th>
                                        <th>Storage Tips</th>
                                        <th>Packaging Size</th>
                                        <th>Image</th>
                                       <!--  <th>Description</th> -->
                                        <th>Status</th>
                                    <th data-orderable="false">Action</th>
                                    </tr>
                                </thead>
                                <tbody>


                        @if(isset($products) && $products != null)
                        @foreach($products as $product)
                                            
                        <tr>
                            <td>{{$product->id}}</td>
                            <td>@if($product->main_category) {{$product->main_category->name}} @endif</td>
                            <td>@if($product->category) {{$product->category->name}} @endif</td>
                            <td>@if($product->subcategory) {{$product->subcategory->name}} @endif</td>                     
                            <td>{{$product->title}}</td>
                            <td>{{$product->price}}</td>
                            <td>{{$product->quality}}</td>
                            <td>{{$product->packaging_type}}</td>
                            <td>{{$product->storage_tips}}</td>
                            <td>{{$product->packaging}}</td>
                           <!--  <td>{{$product->description}}</td> -->
                            @foreach($product->product_images as $prod)                          
                            <td><img src="{{ asset($prod['product_image']) }}" width="60px"></td>
                            @endforeach 
                
           
       <td style="text-align:center">
        @if($product->status == 1)
        <p style="color:green">Active</p>  
        @elseif($product->status == 0)
        <p style="color:red">In-active</p>  
            @endif
        </td>

            <td class="w--100" style="text-align: center;">
             
                <a href="{{route('Suplier.Edit.Products',[$product->id])}}"><i class="fas fa-edit"></i>&nbsp;&nbsp;</a>
                
                <a href="{{route('Suplier.Delete.Products',[$product->id])}}" onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></a>
            </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->



@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<script>
    $(function() {
        $("#subcategory-list").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });
</script>

<script type="text/javascript">
    $(".pasination ul li").click(function() {
    if ($(".pasination ul li").removeClass("active")) {
        $(this).removeClass("active");
    }
    $(this).addClass("active");
});
</script>

@endsection