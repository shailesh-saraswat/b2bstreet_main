@extends('sup_admin.index')

@section('content')




<!-- <div class="wrapper"> -->

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('sup_admin/suplier_dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Add Gallery</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
         <div class="col-1"></div>
         <div class="col-10">
         <div class="card card-info">
            <div class="card-header">
        <h3 class="card-title">Add Gallery</strong></h3>
                </div>
                        <!-- /.card-header -->
                        <!-- form start -->
         @include('admin.partials.messages')
        <form class="form-horizontal" method="post" action="{{route('Suplier.Add.Gallery')}}" enctype="multipart/form-data">
            @csrf
            
            <div class="card-body">
                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                <div class="form-group row">
                    <label for="Title" class="col-sm-2 col-form-label">Title</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="title" name="title" value="{{old('title')}}" placeholder="Title" />
                     </div>
                </div>

                 <div class="form-group row">
                    <label for="Description" class="col-sm-2 col-form-label">Description</label>
                    <div class="col-sm-10">
                    <textarea type="text" class="form-control textarea" id="description" name="description" value="{{old('description')}}" placeholder="Description"  rows="10"></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="Image" class="col-sm-2 col-form-label">Image</label>
                    <div class="col-sm-10">
                    <input type="file" class="" id="image" name="image">
                    <img src="" style="width:150px;" />
                </div>
            </div>
                       
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-info">Add Gallery</button>
                <a href="{{url('sup_admin/gallery_list')}}" class="btn btn-default custom-btn">Cancel</a>
            </div>
             <!-- /.card-footer -->
        </form>
    </div>
  </div>
<div class="col-1"></div>
</div>
</div>
</section>

</div>

<!-- </div> -->

@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script>
@endsection
@section('after-style')
<link rel="stylesheet" href="{{asset('public/plugins/summernote/summernote-bs4.css')}}">
@endsection 
