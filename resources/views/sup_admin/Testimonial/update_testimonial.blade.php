@extends('sup_admin.index')

@section('content')

<!-- <div class="wrapper"> -->

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <!--  -->
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('sup_admin/suplier_dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Testimonial Banner</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-1"></div>
                <div class="col-10">

                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Testimonial  <strong></strong></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        @include('admin.partials.messages')
                        <form class="form-horizontal" method="post" action="{{route('Suplier.Update.Testimonial')}} " enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id" value="{{$testimonial->id}}" />
                            <div class="card-body">
                          
                        <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                <div class="form-group row">
                                    <label for="Name" class="col-sm-2 col-form-label">Title</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="title" name="title" value="{{$testimonial->title}}" placeholder="Title" />
                                    </div>
                                </div>

            <div class="form-group row">
                    <label for="Description" class="col-sm-2 col-form-label">Description</label>
                    <div class="col-sm-10">
                    <textarea type="text" class="form-control textarea" id="description" name="description" placeholder="Description"  rows="10">{{$testimonial->description}}</textarea>
                </div>
            </div>

                                <div class="form-group row">
                                <label for="Image" class="col-sm-2 col-form-label">Image</label>
                                <div class="col-sm-10">
                                    <input type="file" class="" id="image" name="image" value="">
                                    <img src="{{ asset($testimonial['image']) }}" style="width:150px;" />
                                </div>
                            </div>
                                
                                <div class="form-group row">
                                    <label for="Status" class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10">
                                        <select class="form-control select2" style="width: 100%;" name="status" id="status" required>
                                            <option value="0" {{$testimonial->status == 0  ? 'selected' : ''}}>Inactive</option>
                                            <option value="1" {{$testimonial->status == 1  ? 'selected' : ''}}>Active</option>
                                        </select>
                                    </div>
                                </div>                                

                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-info">Update Testimonial</button>
                                <a href="{{route('Suplier.Testimonial.Lists')}}" class="btn btn-default float-right">Cancel</a>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>

                </div>
                <div class="col-1"></div>
            </div>
        </div>
    </section>

</div>

<!-- </div> -->
@endsection


@section('after-scripts')
<script src="{{asset('public/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script>
@endsection
@section('after-style')
<link rel="stylesheet" href="{{asset('public/plugins/summernote/summernote-bs4.css')}}">
@endsection 