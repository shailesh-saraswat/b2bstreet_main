@extends('sup_admin.index')

@section('content')




<!-- <div class="wrapper"> -->

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('sup_admin/suplier_dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Add Banner</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
         <div class="col-1"></div>
         <div class="col-10">
         <div class="card card-info">
            <div class="card-header">
        <h3 class="card-title">Add Banner</strong></h3>
                </div>
                        <!-- /.card-header -->
                        <!-- form start -->
         @include('admin.partials.messages')
        <form class="form-horizontal" method="post" action="{{url('sup_admin/add_banner')}}" enctype="multipart/form-data">
            @csrf
            
            <div class="card-body">
                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                <div class="form-group row">
                    <label for="Company Name" class="col-sm-2 col-form-label">Company Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="title" name="title" value="{{old('title')}}" placeholder="Company Name" />
                     </div>
                </div>

                <div class="form-group row">
                    <label for="Supplier Name" class="col-sm-2 col-form-label">Supplier Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}" placeholder="Supplier Name" />
                     </div>
                </div>

                <div class="form-group row">
                    <label for="Address" class="col-sm-2 col-form-label">Address</label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" id="address" name="address" value="{{old('address')}}" placeholder="Address" />
                </div>
            </div>

                <div class="form-group row">
                    <label for="Phone" class="col-sm-2 col-form-label">Phone</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="mobile" name="mobile" value="{{old('mobile')}}" placeholder="Phone" />
                     </div>
                </div>

                <div class="form-group row">
                    <label for="Email" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                   <input type="text" class="form-control" id="email" name="email" value="{{old('email')}}" placeholder="Email" required />
                </div>
            </div>

             <div class="form-group row">
                    <label for="Website" class="col-sm-2 col-form-label">Website</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="website" name="website" value="{{old('website')}}" placeholder="Website" />
                     </div>
                </div>
 

            <div class="form-group row">
                <label for="Logo" class="col-sm-2 col-form-label">Logo</label>
                    <div class="col-sm-10">
                    <input type="file" class="" id="image" name="image">
                    <img src="" style="width:150px;" />
                </div>
            </div>

            <div class="form-group row">
                <label for="Video" class="col-sm-2 col-form-label">Video</label>
                    <div class="col-sm-10">
                    <input type="file" class="" id="video" name="video">
                    <img src="" style="width:150px;" />
                </div>
            </div>

                             
            <div class="form-group row">
                <label for="Status" class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-10">
                        <select class="form-control select2" style="width: 100%;" name="status" required>
                      <option value="1">Active</option>
                      <option value="0">Inactive</option>
                        </select>
                    </div>
            </div>

                               
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-info">Add Banner</button>
                <a href="{{route('Suplier.Banner.Lists')}}" class="btn btn-default custom-btn">Cancel</a>
            </div>
             <!-- /.card-footer -->
        </form>
    </div>
  </div>
<div class="col-1"></div>
</div>
</div>
</section>

</div>

<!-- </div> -->

@endsection
