@extends('sup_admin.index')

@section('content')

<!-- <div class="wrapper"> -->

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <!--  -->
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('sup_admin/suplier_dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Update Banner</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-1"></div>
                <div class="col-10">

                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Update Banner <strong></strong></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        @include('admin.partials.messages')
                        <form class="form-horizontal" method="post" action="{{url('sup_admin/update_banner')}} " enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                            <input type="hidden" name="id" value="{{$banner->id}}" />
                            <div class="card-body">
                          
                                <div class="form-group row">
                                    <label for="Company Name" class="col-sm-2 col-form-label">Company Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="title" name="title" value="{{$banner->title}}" placeholder="Company Name"/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="Supplier Name" class="col-sm-2 col-form-label">Supplier Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="name" name="name" value="{{$banner->name}}" placeholder="Supplier Name" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Address" class="col-sm-2 col-form-label">Address</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="address" name="address" value="{{$banner->address}}" placeholder="Address"/>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Email" class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="email" name="email" value="{{$banner->email}}" placeholder="Email" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Phone" class="col-sm-2 col-form-label">Phone</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="mobile" name="mobile" value="{{$banner->mobile}}" placeholder="Phone"  />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Website" class="col-sm-2 col-form-label">Website</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="website" name="website" value="{{$banner->website}}" placeholder="Website" />
                                    </div>
                                </div>

                                
                                <div class="form-group row">
                                <label for="Logo" class="col-sm-2 col-form-label">Logo</label>
                                <div class="col-sm-10">
                                    <input type="file" class="" id="image" name="image" value="">
                                    <img src="{{ asset($banner['image']) }}" style="width:150px;" />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="Video" class="col-sm-2 col-form-label">Video</label>
                                <div class="col-sm-10">
                                    <input type="file" class="" id="video" name="video" value="">
                                    <video width="150" height="150" controls>
                                    <source src="{{ asset($banner['video']) }}" type="video/mp4">
                                    <source src="{{ asset($banner['video']) }}" type="video/ogg">
                               </video>
                                
                                </div>
                            </div>
                                
                                <div class="form-group row">
                                    <label for="Status" class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10">
                                        <select class="form-control select2" style="width: 100%;" name="status" id="status" required>
                                            <option value="1" {{$banner->status == 1  ? 'selected' : ''}}>Active</option>
                                            <option value="0" {{$banner->status == 0  ? 'selected' : ''}}>Inactive</option>
                                            
                                        </select>
                                    </div>
                                </div>                                

                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-info">Update Banner</button>
                                <a href="{{route('Suplier.Banner.Lists')}}" class="btn btn-default float-right">Cancel</a>

                              
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>

                </div>
                <div class="col-1"></div>
            </div>
        </div>
    </section>

</div>

<!-- </div> -->
@endsection