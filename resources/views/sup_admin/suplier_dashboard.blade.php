@extends('sup_admin.index')

@section('after-style')
<style>

    a{

        color:inherit;
    }
</style>

@endsection

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Supplier Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('sup_admin/suplier_dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Supplier Admin Dashboard</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Info boxes -->
            <div class="row">
                <div class="col-12 col-sm-6 col-md-3">
                    <a href="{{url('sup_admin/enquiry_list')}}">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Total Enquiry</span>
                                <span class="info-box-number">{{$countAnus ?? 0}}</span>
                            </div>
                           
                        </div>
                    </a>
                   
                </div>

                <div class="col-12 col-sm-6 col-md-3 ">
                    <a href="#">
                        <div class="info-box">
                        <div class="span4">
                            <div class="plan prefered" style="padding: 30px;">
                               
                                <div class="plan-name">
                                    <h4>{{auth()->user()->package_name}}</h4>
                                </div>
                                <div class="plan-price">
                                    <b style="margin-right: 12px;">Amount:</b>{{auth()->user()->amount}}
                                </div>
                                <div class="plan-details">
                                    <div>
                                        <b style="margin-right: 12px;">Date:</b>{{auth()->user()->date}}
                                    </div>
                                </div>
                                <div class="plan-action" style="margin-top: 20px;">
                                    <a href="#" class="btn btn-success btn-block btn-large">Renewal</a>
                                </div>
                               
                            </div>
                        </div>
                        </div>
                    </a>                   
                </div>

                

                <!-- fix for small devices only -->
                <div class="clearfix hidden-md-up"></div>

            </div>
            <!-- /.row -->
        </div><!--/. container-fluid -->
    </section>

    <!-- /.content -->
</div>
@endsection
