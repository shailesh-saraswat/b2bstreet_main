<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Meta -->
      <title>B2B Streets | Indias First Visual B2B Search Engine </title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
      <meta name="description"
         content="Indias First Visual B2B Search Engine, Get Unlimited Business Deals with Indias Best Mart Business Directory, For More Details Hit The Call Button.">
      <meta name="author" content="">
      <meta name="keywords" content="B2B Streets, Indias First Visual B2B Search Engine">
      <meta name="robots" content="all">
      <meta name="csrf-token" content="{{ csrf_token() }}" />
      <!-- Bootstrap Core CSS -->
      <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
      <!-- Customizable CSS -->
      <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/blue.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/simple-line-icons.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/owl.transitions.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/animate.min.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/rateit.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-select.min.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/customhj.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/anuj.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/pratima.css') }}">
       <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
      <!-- Icons/Glyphs -->
      <link rel="stylesheet" href=" {{ asset('assets/css/font-awesome.css') }}">
      <script src="{{ asset('assets/js/jquery-3.2.1.min.js') }}"></script>
      <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
      {{-- <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
      <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> --}}
      <!-- Fonts -->
      <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
      <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,600italic,700,700italic,800'
         rel='stylesheet' type='text/css'>
      <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,500i,600,700,800,900" rel="stylesheet">
      <!-- Google Tag Manager -->
      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
         new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
         j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
         'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
         })(window,document,'script','dataLayer','GTM-MP2TZBJ');
      </script>
      <!-- End Google Tag Manager -->
      <style>
         @media only and (min-width:768px) {
         #myCarousel img {
         height: 430px;
         }
         }
         @media only and (max-width:767px) {}
         .imgs {
         width: 250px;
         height: 150px;
         }
         .footer .footer-bottom {
         background: #f3f4f6;
         }
         .pro-nme {
         width: 25px;
         display: block;
         margin: 0 auto;
         margin-bottom: 6px;
         }
         @media only screen and (max-width: 991px) and (min-width: 320px) {
         .dropdown-menu>.dropdown {
         position: relative;
         }
         .dropdown-menu>.dropdown a::after {
         transform: rotate(-90deg);
         position: absolute;
         right: .9rem;
         top: .9rem;
         }
         .dropdown-menu>.dropdown .dropdown-menu {
         top: -.7rem;
         left: 100%;
         border-radius: 0 .25rem .25rem .25rem;
         }
         .new-navig {
         position: fixed;
         top: 12px;
         right: 10px;
         }
         .mobile-menu-show.new-navig button {
         background: none;
         border: none;
         position: absolute;
         right: 13px;
         top: -33px;
         }
         .mobile-menu-show.new-navig .navbar-collapse.collapse.in {
         width: 100%;
         margin-top: 28px;
         padding: 0;
         }
         .mobile-menu-show.new-navig .navbar-collapse.collapse.in ul {
         width: 100%;
         }
         .mobile-menu-show.new-navig .navbar-collapse.collapse.in ul li {
         width: 100%;
         border-bottom: 1px solid #ccc;
         /* padding: 8px; */
         /* padding-left: 15px; */
         }
         .mobile-menu-show.new-navig nav {
         width: 100%;
         position: relative;
         top: 11px;
         z-index: 999999;
         background: #efefef;
         margin-top: 36px;
         }
         .logo {
         width: 100%;
         justify-content: space-between;
         }
         .mobile-menu-show.new-navig {
         position: absolute;
         right: 9px;
         width: 100%;
         left: 0;
         }
         div#navbarNavDropdown {
         margin-top: 0;
         padding: 0;
         }
         .navbar-nav {
         margin: 0;
         }
         .navbar-nav .open a {
         background: none !important;
         text-decoration: none;
         }
         .navbar-nav .open .dropdown-menu>li>a,
         .navbar-nav .open .dropdown-menu .dropdown-header {
         padding: 0;
         }
         .mobile-menu-show.new-navig .navbar-nav li:first-child {
         background: none;
         }
         .mobile-menu-show.new-navig li.nav-item {
         text-align: left;
         padding: 8px;
         padding-left: 15px;
         }
         .mobile-menu-show.new-navig a.dropdown-item.dropdown-toggle {
         padding: 8px;
         padding-left: 0px;
         border-bottom: 1px solid #acacac;
         }
         li.dropdown {}
         .mobile-menu-show.new-navig li.dropdown {
         border: none !important;
         }
         ul.dropdown-menu {
         background: #b0adad;
         }
         li.nav-item.dropdown.open {
         background: #e7e7e7;
         }
         .mobile-menu-show.new-navig a {
         font-size: 14px;
         }
         .mobile-menu-show.new-navig ul.dropdown-menu.show {
         padding: 8px;
         padding-left: 25px;
         }
         .mobile-menu-show.new-navig ul.dropdown-menu.show li {
         padding: 8px;
         padding-left: 25px;
         border-bottom: 1px solid #ccc;
         }
         .mobile-menu-show.new-navig ul.dropdown-menu.show li a {
         /* border-bottom: 1px
         solid #ccc; */
         }
         ul.dropdown-menu {
         padding-left: 15px;
         }
         .mobile-menu-show.new-navig .dropdown-menu>li>a {
         font-weight: 400;
         color: #333 !important;
         }
         a#navbarDropdownMenuLink {
         color: #333 !important;
         font-weight: 600;
         }
         ul.dropdown-menu.show {
         padding: 0 !important;
         padding-left: 15px !important;
         }
         ul.dropdown-menu.show li {
         padding: 0 !important;
         border-bottom: 1px solid #b9b9b9 !important;
         }
         ul.dropdown-menu.show li a {
         padding: 8px !important;
         padding-left: 0px !important;
         }
         ul.dropdown-menu {
         /* background: white !important; */
         }
         ul.dropdown-menu.show {
         padding-bottom: 12px !important;
         }
         .log-s {
         display: flex;
         justify-content: center;
         gap: 10px;
         padding-top: 15px;
         }
         .log-s a {
         background: #556495;
         padding: 10px 20px;
         color: #fff;
         border-radius: 5px;
         }
         }
      </style>
   </head>
   <body class="cnt-home">
      <!-- ============================================== HEADER ============================================== -->
      <style>
         .main-header.new-header {
         grid-template-columns: 100%;
         }
      </style>
      <link
         href="https://fonts.googleapis.com/css2?family=Public+Sans:wght@100;200;300;400;500;600;700;800;900&display=swap"
         rel="stylesheet">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
         integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
      <header class="header-style-1">
         <div class="container">
            <div class="main-header new-header">
               <div class="logo-holder">
                  <div class="logo">
                     <a href="{{ route('Front.Index') }}"> <img src="{{ asset('assets/images/B2B-logo.svg') }}"
                        alt="logo"> </a>
                     <div class="mobile-menu-show new-navig">
                        <nav class="navbar navbar-expand-lg navbar-dark bg-dark ">
                           <button class="navbar-toggler" type="button" data-toggle="collapse"
                              data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
                              aria-expanded="false" aria-label="Toggle navigation">
                              <span class="open-menu">
                                 <svg xmlns="http://www.w3.org/2000/svg" width="24" height="16">
                                    <g fill="#252a32" fill-rule="evenodd">
                                       <path d="M0 0h24v2H0zM0 7h24v2H0zM0 14h24v2H0z"></path>
                                    </g>
                                 </svg>
                              </span>
                           </button>
                           <div class="collapse navbar-collapse" id="navbarNavDropdown">
                              <ul class="navbar-nav">
                                 <div class="log-s">
                                    <a href="http://3.6.178.204/devb2b/b2b/login">Login</a>
                                    <a href="http://3.6.178.204/devb2b/b2b/sign_up">Sign up</a>
                                 </div>
                                 <li class="nav-item active">
                                    <a class="nav-link" href="#">Home</a>
                                 </li>
                                 @foreach ($maincategories as $menu)
                                 <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="http://example.com"
                                       id="navbarDropdownMenuLink" data-toggle="dropdown"
                                       aria-haspopup="true" aria-expanded="false">{{ $menu->name }}</a>
                                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                       @php
                                       $category = \App\Models\Category::where('maincategory_id', $menu->id)
                                       ->where('status', 1)
                                       ->where('name', '!=', 'null')
                                       ->limit(8)
                                       ->get();
                                       @endphp
                                       @foreach ($category as $cat)
                                       <li class="dropdown">
                                          <a class="dropdown-item dropdown-toggle"
                                             href="{{ URL::to('/category/' . $cat->slug) }}">{{ $cat->name }}</a>
                                          <ul class="dropdown-menu">
                                             @php
                                             //dd($cat->id);
                                             $subcategory = \App\Models\SubCategory::where('categories_id', $cat->id)
                                             ->limit(5)
                                             ->get();
                                             @endphp
                                             @foreach ($subcategory as $sub)
                                             <li><a class="dropdown-item"
                                                href="{{ URL::to('/products/' . $sub->slug) }}">{{ $sub->name }}</a>
                                             </li>
                                             @endforeach
                                             <li>                                   
                                                <a href="{{ URL::to('/category/' . $cat->slug) }}" style="color: blue!important;font-weight: 700;">View All</a>   
                                             </li>
                                          </ul>
                                       </li>
                                       @endforeach
                                    </ul>
                                 </li>
                                 @endforeach
                                 </li>
                                 <li><a href="{{route('Front.AllCategory')}}">All Category</a></li>
                              </ul>
                           </div>
                        </nav>
                     </div>
                  </div>
                  <div class="menu-essential">
                     <ul>
                        <li><a href="{{ route('Front.Covid-19') }}"><img
                           src="{{ asset('assets/images/verified.svg') }}"> Covid-19 Supplies</a></li>
                        <li id="myBtn4"><a href="#"><img src="{{ asset('assets/images/buyer-1.svg') }}">Post Requirement</a>
                        </li>
                        <!--  <li><a href=""><img src="{{ asset('assets/images/delivery-man.svg') }}"> For Suppliers</a></li> -->
                        <li><a href="{{ route('Front.need_help') }}"><img
                           src="{{ asset('assets/images/information-1.svg') }}"> Need Help</a></li>
                        @if (Auth::check() && Auth::user()->type == 3)
                        <a href="{{ route('Front.UserProfile') }}"> <img class="pro-nme"
                           src="{{ asset('assets/images/add-user.svg') }}">
                        {{ Auth::user()->name }}</a>
                        <!--  <li><a href="{{ route('SupAdmin.Logout') }}">Sign out</a></li> -->
                        @else
                        <li class="mr-20"><a href="{{ route('Front.login') }}"><img
                           src="{{ asset('assets/images/user-1.svg') }}"> Login</a></li>
                        <li><a href="{{ route('Front.Signup') }}"><img
                           src="{{ asset('assets/images/add-user.svg') }}"> Register</a></li>
                        @endif
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </header>
      <div class="header-style-1 new-cate">
      <div class="container">
      <div class="">
         <div class="logo-holder">
            <div class="header-nav animate-dropdown red-menu">
               <div class="">
                  <div class="yamm navbar navbar-default" role="navigation">
                     <div class="navbar-header">
                        <button data-target="#mc-horizontal-menu-collapse" data-toggle="collapse"
                           class="navbar-toggle collapsed" type="button">
                        <span class="sr-only">Toggle navigation</span> <span
                           class="icon-bar"></span> <span class="icon-bar"></span> <span
                           class="icon-bar"></span> </button>
                     </div>
                     <div class="nav-bg-class container">
                        <div class="navbar-collapse collapse" id="mc-horizontal-menu-collapse">
                           <div class="nav-outer">
                              <ul class="nav navbar-nav">
                                 <nav>
                                    <ul class="ul-reset">
                                       <li><a href="{{ route('Front.Index') }}">Home</a></li>
                                       @php
                                       $maincategories = \App\Models\Maincategory::where('status', 1)            
                                       ->limit(5)
                                       ->get();
                                       @endphp
                                       @foreach ($maincategories as $menu)
                                       <li class="droppable">
                                          <a href="#">{{ $menu->name }}</a>
                                          <div class="mega-menu12">
                                             <div class="mega-menu container">
                                                <div class="mega-menu-grid">
                                                   @php
                                                   $category = \App\Models\Category::where('maincategory_id', $menu->id)
                                                   ->where('status', 1)
                                                   ->where('name', '!=', 'null')
                                                   ->limit(5)
                                                   ->get();
                                                   @endphp
                                                   @foreach ($category as $cat)
                                                   <div class="mega-menu-content">
                                                      <h2><a class="dropdown-item dropdown-toggle"href="{{ URL::to('/category/' . $cat->slug) }}">{{ $cat->name }}</a>
                                                      </h2>
                                                      <ul class="links list-unstyled">
                                                         @php
                                                         //dd($cat->id);
                                                         $subcategory = \App\Models\SubCategory::where('categories_id', $cat->id)
                                                         ->limit(5)
                                                         ->get();
                                                         @endphp
                                                         @foreach ($subcategory as $sub)
                                                         <li>
                                                            <a href="{{ URL::to('/products/' . $sub->slug) }}"
                                                               title="{{ $sub->name }} ">{{ $sub->name }}</a>
                                                         </li>
                                                         @endforeach
                                                         <li>                                   
                                                            <a href="{{ URL::to('/category/' . $cat->slug) }}" style="color: blue!important;font-weight: 700;">View All</a>     
                                                         </li>
                                                      </ul>
                                                   </div>
                                                   @endforeach
                                                </div>
                                                <!-- .mega-menu-->
                                       </li>
                                       <!-- .droppable -->
                                       @endforeach
                                       <li><a href="{{route('Front.AllCategory')}}">All Category</a></li>
                                       </li>
                                    </ul>
                                    <!-- .container .ul-reset -->
                                 </nav>
                              </ul>
                              <!-- /.navbar-nav -->
                              </div>
                              <!-- /.nav-outer -->
                              </div>
                              <!-- /.navbar-collapse -->
                           </div>
                           <!-- /.nav-bg-class -->
                        </div>
                        <!-- /.navbar-default -->
                     </div>
                     <!-- /.container-class -->
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="menu-zindex">
      <div class="company-video-play" id="lookBuy">
         <div id="myModal4" class="modal">
            <div class="modal-content">
               <div class="modal-header">
                  <span class="close4">&times;</span>
               </div>
               <div class="modal-body">
                  <form method="post"  action="{{ url('add_buyer_requirment') }}">
                  @if(Session::has('message'))
                <div class="alert alert-danger"  role="alert">{{Session::get('message')}}</div>
                @elseif(Session::has('success'))
                <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                @endif  
                     @csrf
                     <div class="tellHeader"> Tell us more about your requirement </div>
                     <div class="form-group inputWrap">
                        <label>Product name <span class="mandt"> * </span></label>
                        <input type="text" name="product_name1" id="product_name1"
                           placeholder="Please type what you are looking for..." value="{{old('product_name1')}}">
                            @if($errors->has('product_name1'))
                                <div class="error">{{ $errors->first('product_name1') }}</div>
                                @endif
                     </div>
                     <div class="form-group inputWrap">
                     <label>Describe in few words (minimum 20 characters)<span class="mandt"> *</span></label>
                     <textarea id="description" name="description" value="description" rows="3"
                           placeholder="Please include product name, order quantity, usage, special requests if any in your inquiry."
                           >{{old('description')}}</textarea>
                         @if($errors->has('description'))
                                <div class="error">{{ $errors->first('description') }}</div>
                                @endif   
                     </div>
                     <div class="form-group inputWrap tGridS">
                        <div>
                           <label>Quantity <span class="mandt"> * </span></label>
                           <div class="tGrid">
                              <input type="number" name="quantity" value="{{old('quantity')}}" id="quantity" placeholder="Quantity">
                              
                              <select name="quantity" id="quantity_unit">
                                 <option value="Kilogram">Kilogram</option>
                                 <option value="Nos">Nos</option>
                                 <option value="Pieces">Pieces</option>
                                 <option value="Tons">Tons</option>
                              </select>
                           @if($errors->has('quantity'))
                                <div class="error">{{ $errors->first('quantity') }}</div>
                                @endif 
                           </div>
                        </div>
                        <div>
                           <label>Approximate order value <span class="mandt"> * </span></label>
                           <div class="apprSel">
                              <select name="order" id="order">
                                 <option value="5000 to 10000">5000 to 10000</option>
                                 <option value="10001 to 20000">10001 to 20000</option>
                                 <option value="20001 to 50000">20001 to 50000</option>
                                 <option value="Upto 1 Lakh">Upto 1 Lakh</option>
                              </select>
                               @if($errors->has('order'))
                                <div class="error">{{ $errors->first('order') }}</div>
                                @endif
                           </div>
                        </div>
                     </div>
                     <div class="form-group inputWrap tGridS">
                        <div>
                           <label>Name <span class="mandt"> * </span></label>
                           <div class="">
                              <input type="text" name="name1"  id="name1" placeholder="Name" value="{{old('name1')}}"> 
                              @if($errors->has('name1'))
                                <div class="error">{{ $errors->first('name1') }}</div>
                                @endif                      
                           </div>
                        </div>
                        <div>
                           <label>Email ID <span class="mandt"> * </span></label>
                           <div class="">
                              <input type="text" name="email1" id="email1" placeholder="Email Id" value="{{old('email1')}}">
                              @if($errors->has('email1'))
                                <div class="error">{{ $errors->first('email1') }}</div>
                                @endif 
                           </div>
                        </div>
                        <div>
                           <label>Phone number <span class="mandt"> * </span></label>
                           <div class="ccphoneIptWrap">
                              <div class="cCode">+91</div>
                              <input type="text" name="mobile" id="mobile" placeholder="Phone Number" value="{{old('mobile')}}">
                              
                           </div>
                           @if($errors->has('mobile'))
                                <div class="error">{{ $errors->first('mobile') }}</div>
                                @endif 
                        </div>
                        <div>
                           <label>Address<span class="mandt"> * </span></label>
                           <div class="">
                              <input type="text" name="addresss"  id="addresss" placeholder="Address" value="{{old('addresss')}}"> 
                              @if($errors->has('addresss'))
                                <div class="error">{{ $errors->first('addresss') }}</div>
                                @endif                      
                           </div>
                        </div>
                     </div>
                     <div class="form-group pbrWrap">
                        <button type="submit" name="submit" class="new-button save_btn">Post Buy Requirement</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
      <script>
         const openMenu = document.querySelector(".open-menu");
         const closeMenu = document.querySelector(".close-menu");
         const menuWrapper = document.querySelector(".menu-wrapper");
         const hasCollapsible = document.querySelectorAll(".has-collapsible");
         
         // Sidenav Toggle
         openMenu.addEventListener("click", function() {
             menuWrapper.classList.add("offcanvas");
         });
         
         closeMenu.addEventListener("click", function() {
             menuWrapper.classList.remove("offcanvas");
         });
         
         // Collapsible Menu
         hasCollapsible.forEach(function(collapsible) {
             collapsible.addEventListener("click", function() {
                 collapsible.classList.toggle("active");
         
                 // Close Other Collapsible
                 hasCollapsible.forEach(function(otherCollapsible) {
                     if (otherCollapsible !== collapsible) {
                         otherCollapsible.classList.remove("active");
                     }
                 });
             });
         });
      </script>
      <script>
         // Get the modal
         var modal4 = document.getElementById("myModal4");
         
         // Get the button that opens the modal
         var btn4 = document.getElementById("myBtn4");
         
         // Get the <span> element that closes the modal
         var span4 = document.getElementsByClassName("close4")[0];
         
         // When the user clicks the button, open the modal 
         btn4.onclick = function() {
             modal4.style.display = "block";
         }
         
         // When the user clicks on <span> (x), close the modal
         span4.onclick = function() {
             modal4.style.display = "none";
         }
      </script>
      <script>
         (function() {
             var oldVal2;
             var checkLength = function(val2) {
                 $('#nearx2').html(val2);
             }
             $('#nearu2').bind('DOMAttrModified textInput input change keypress paste', function() {
                 var val2 = this.value;
                 if (val2 !== oldVal2) {
                     oldVal2 = val2;
                     checkLength(val2);
                 }
                 $.ajax({
                     type: 'POST',
                     url: 'nearyou2.php',
                     data: 'nearFrmSubmit=1&oldVal2=' + oldVal2,
                     beforeSend: function() {},
                     success: function(msg) {
                         $('#near2').html(msg);
                     }
                 });
             });
         }());
      </script>
      <script type="text/javascript">
         $(document).ready(function() {
             // dropdown-toggle class not added for submenus by current WP Bootstrap Navwalker as of November 15, 2017.
             $('.dropdown-menu > .dropdown > a').addClass('dropdown-toggle');
         
             $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
                 if (!$(this).next().hasClass('show')) {
                     $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
                 }
                 var $subMenu = $(this).next(".dropdown-menu");
                 $subMenu.toggleClass('show');
                 $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
                     $('.dropdown-menu > .dropdown .show').removeClass("show");
                 });
                 return false;
             });
         
         });
      </script>
     