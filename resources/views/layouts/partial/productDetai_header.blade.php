<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Meta -->
      <title>B2B Streets | Indias First Visual B2B Search Engine </title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
      <meta name="description" content="Indias First Visual B2B Search Engine, Get Unlimited Business Deals with Indias Best Mart Business Directory, For More Details Hit The Call Button.">
      <meta name="author" content="">
      <meta name="keywords" content="B2B Streets, Indias First Visual B2B Search Engine">
      <meta name="robots" content="all">
      <!-- Bootstrap Core CSS -->
      <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
      <!-- Customizable CSS -->

     

      <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/blue.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/simple-line-icons.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/owl.transitions.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/animate.min.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/rateit.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-select.min.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/customhj.css') }}">
       <link rel="stylesheet" href="{{ asset('assets/css/anuj.css') }}">
      
      <!-- Icons/Glyphs -->
      <link rel="stylesheet" href=" {{ asset('assets/css/font-awesome.css') }}">
      <script src="{{ asset('assets/js/jquery-3.2.1.min.js') }}"></script>     
      <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script> 
      <!-- Fonts -->
      <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
      <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,600italic,700,700italic,800' rel='stylesheet' type='text/css'>
      <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,500i,600,700,800,900" rel="stylesheet">
      <style>
         @media only and (min-width:768px){
         #myCarousel img{
         height:430px;
         } 
         }
         @media only and (max-width:767px){
         }
         .imgs{width: 250px;height:150px;}
         .footer .footer-bottom {
         background: #f3f4f6; 
         }
         .pro-nme
         {
                width: 25px;
    display: block;
    margin: 0 auto;
    margin-bottom: 6px;
         }
      </style>
   </head>
   <body class="cnt-home">
      <!-- ============================================== HEADER ============================================== -->
      <style>
.main-header.new-header {
    grid-template-columns: 100%;
}
</style>
  <link href="https://fonts.googleapis.com/css2?family=Public+Sans:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
 

  

  <div class="header-style-1 new-cate"> 
  <div class="container">
    <div class=""> 
            <div class="logo-holder"> 
            
                 
 <div class="header-nav animate-dropdown red-menu">
  <div class=""> 
      <div class="yamm navbar navbar-default" role="navigation">
        <div class="navbar-header">
       <button data-target="#mc-horizontal-menu-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button"> 
       <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <div class="nav-bg-class container">
          <div class="navbar-collapse collapse" id="mc-horizontal-menu-collapse">
            <div class="nav-outer">
              <ul class="nav navbar-nav">
               
                
                
                 

                 
              </ul>
              
              <!-- /.navbar-nav -->
           
            </div>
            <!-- /.nav-outer --> 
          </div>
          <!-- /.navbar-collapse --> 
          
        </div>
        <!-- /.nav-bg-class --> 
      </div>
      <!-- /.navbar-default --> 
    </div>
    <!-- /.container-class --> 
     
  </div>
</div> 
 </div>
  </div>
  </div>
  <div class="menu-zindex">


      <div class="company-video-play" id="lookBuy">
        <div id="myModal4" class="modal">
            <div class="modal-content">
                <div class="modal-header">
                    <span class="close4">&times;</span> 
                </div>
                <div class="modal-body">
                    <form method="post" action="{{url('add_buyer_requirment')}}">
                        @csrf
                    <div class="tellHeader"> Tell us about your requirement </div>
                    <div class="form-group inputWrap">

                        <label>Product name <span class="mandt"> * </span></label>
                        <input type="text" name="product_name"  id="product_name" placeholder="Please type what you are looking for...">
                    </div>
                    <div class="form-group inputWrap">
                        <label>Describe in few words (minimum 20 characters)<span class="mandt"> * </span></label>
                        <textarea id="description" name="description" value="description" rows="3" placeholder="Please include product name, order quantity, usage, special requests if any in your inquiry.">
                        </textarea>
                    </div>
                    <div class="form-group inputWrap tGridS">
                        <div>
                            <label>Quantity <span class="mandt"> * </span></label>
                            <div class="tGrid">
                                <input type="text" name="quantity"  id="quantity" placeholder="Quantity">
                                <select name="quantity" id="quantity">
                                    <option value="Kilogram">Kilogram</option>
                                    <option value="Nos">Nos</option>
                                    <option value="Pieces">Pieces</option>
                                    <option value="Tons">Tons</option>
                                </select>                           
                            </div>
                        </div>
                        <div>


                            <label>Approximate order value <span class="mandt"> * </span></label>
                            <div class="apprSel">
                                <select name="order" id="order">
                                    <option value="5000 to 10000">5000 to 10000</option>
                                    <option value="10001 to 20000">10001 to 20000</option>
                                    <option value="20001 to 50000">20001 to 50000</option>
                                    <option value="Upto 1 Lakh">Upto 1 Lakh</option>
                                </select>                           
                            </div>
                        </div>
                    </div>  
                    <div class="form-group inputWrap tGridS">
                        <div>
                            <label>Email ID <span class="mandt"> * </span></label>
                            <div class="">
                                <input type="text" name="email"  id="email" placeholder="Email Id">                      
                            </div>
                        </div>
                        <div>
                            <label>Phone number <span class="mandt"> * </span></label>
                            <div class="ccphoneIptWrap">
                                <div class="cCode">
                                    +91
                                </div>
                                <input type="text" name="phone" id="phone" placeholder="Phone Number">
                            </div>
                        </div>
                    </div>  
                    <div class="form-group">
                        <div class="checkWrap">
                            <input type="checkbox" id="terms_conditions" name="terms_conditions">
                            <label for="whatsapp"> I agree to <a href="#"> terms and conditions </a></label>
                        </div>
                    </div>      
                    <div class="form-group">
                        <div class="checkWrap">
                            <input type="checkbox" id="whatsapp" name="whatsapp">
                            <label for="whatsapp"> <img src="assets/images/whatsapp.svg"> Would you like to get updates over Whatsapp </label>
                        </div>
                    </div>  
                    <div class="form-group pbrWrap">
                        <button type="submit" name="submit" value="submit" id="submit" class="new-button">Post Buy Requirement</button>
                    </div>
                    </form>                      
                </div>
            </div>
        </div>
        </div>
  
 <script>
const openMenu = document.querySelector(".open-menu");
const closeMenu = document.querySelector(".close-menu");
const menuWrapper = document.querySelector(".menu-wrapper");
const hasCollapsible = document.querySelectorAll(".has-collapsible");

// Sidenav Toggle
openMenu.addEventListener("click", function () {
    menuWrapper.classList.add("offcanvas");
});

closeMenu.addEventListener("click", function () {
    menuWrapper.classList.remove("offcanvas");
});

// Collapsible Menu
hasCollapsible.forEach(function (collapsible) {
    collapsible.addEventListener("click", function () {
        collapsible.classList.toggle("active");

        // Close Other Collapsible
        hasCollapsible.forEach(function (otherCollapsible) {
            if (otherCollapsible !== collapsible) {
                otherCollapsible.classList.remove("active");
            }
        });
    });
});

</script> 
  
  
 <script>
// Get the modal
var modal4 = document.getElementById("myModal4");

// Get the button that opens the modal
var btn4 = document.getElementById("myBtn4");

// Get the <span> element that closes the modal
var span4 = document.getElementsByClassName("close4")[0];

// When the user clicks the button, open the modal 
btn4.onclick = function() {
  modal4.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span4.onclick = function() {
  modal4.style.display = "none";
}
</script> 
  

<script>
(function () {
    var oldVal2;
    var checkLength = function (val2) {
        $('#nearx2').html(val2);
    }
    $('#nearu2').bind('DOMAttrModified textInput input change keypress paste', function () {
        var val2 = this.value;
        if (val2 !== oldVal2) {
            oldVal2 = val2;
            checkLength(val2);
        }
         $.ajax({
            type:'POST',
            url:'nearyou2.php',
             data:'nearFrmSubmit=1&oldVal2='+oldVal2,
            beforeSend: function () {
              },
            success:function(msg){
                $('#near2').html(msg);
                }
        });
    });
}());
 </script>    


       
   @yield('content')
@include('layouts.partial.footer')
@yield('js')