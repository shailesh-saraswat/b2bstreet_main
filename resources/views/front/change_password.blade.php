@extends('layouts.front')
@section('content')
      
      <div class="breadcrumb">
         <div class="container">
            <div class="breadcrumb-inner">
               <ul class="list-inline list-unstyled">
                  <li><a href="{{route('Front.Index')}}">Home</a></li>
                  <li class='active'>Create New Password</li>
               </ul>
            </div>
            <!-- /.breadcrumb-inner -->
         </div>
         <!-- /.container -->
      </div>
      <!-- /.breadcrumb -->
      <div class="body-content">
         <div class="container">
            <div class="row login">
               <div class="col-lg-6 offset-lg-3">
                     <div class="contact-form profile-form">
                        <div class="cont-f-grid">
                           <form method="post" action="{{route('Front.changePassword')}}" id="forgotPassForm" name="forgotPassForm">
                               @if(Session::has('message'))
                        <p class="alert alert-danger" id="alert_box">{{ Session::get('message') }}</p>
                      @endif
                      @if(Session::has('log_message'))
                        <p class="alert alert-success" id="alert_box">{{ Session::get('log_message') }}</p>
                      @endif
                      @if ($errors->any())
                         <div class="alert alert-danger alert-dismissible page_alert">
                           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                             <ul>
                                 @foreach ($errors->all() as $error)
                                     <li>{{ $error }}</li>
                                 @endforeach
                             </ul>
                         </div>
                       @endif
                        <div class="alert alert-danger alert-dismissible custom_alert" style="display: none;">
                           <span class="alert_message"></span>
                        </div> 
                              @csrf
                               <div class="form-heading">
                                <h1>Create New Password</h1>
                                   <p>Enter Registered Email-Id</p>
                               </div>
                              
                              <div class="cont-sec-cont">

                                 <div class="form-group">
                                    <label class="info-title" for="exampleInputName">Email Id<span>*</span></label>
                                    <input type="text" id="email" name="email" class="form-control unicase-form-control text-input" placeholder="">
                                 </div>

                                 <div class="form-group">
                                    <label class="info-title" for="exampleInputName">New Password <span>*</span></label>
                                    <input type="password" id="password" name="password" class="form-control unicase-form-control text-input" placeholder="">
                                 </div>
                                 <div class="form-group">
                                    <label class="info-title" for="exampleInputName">Re-Type Password <span>*</span></label>
                                    <input type="password" id="password" name="password" class="form-control unicase-form-control text-input" placeholder="">
                                 </div>
                              </div>
                              <div class="cont-sec-cont linkss">
                                  <ul>
                                    <button type="submit" class="reset_btn" id="submit" name="submit">Submit</button>
                                  </ul>
                                  <ul class="text-right">
                                    <li><a href="{{route('Front.login')}}">Cancel</a></li>
                                  </ul>
                              </div>
                           </form>
                        </div>
                        <p id="trimb" style="clear:both;color:red;"></p>
                     </div>
               </div>
            </div>
            <!-- /.row -->
            <!-- ============================================== BRANDS CAROUSEL ============================================== -->
         </div>
      </div>
      
 @endsection 
 @section('js')
<script type="text/javascript">
   $('.reset_btn').on('click',function(){
    var password         = $('#password').val();
    var error_message = "";
    if(password=="")
    {
      error_message = "Please Enter password.";
    }
  
    if(error_message!='')
    {
      $('.custom_alert').css('display','block');
      $('.alert_message').text(error_message);
      close_alert();
      return false;
    }
    else
    {
      $('#forgotPassForm').submit();
    }
  });
</script>
@endsection