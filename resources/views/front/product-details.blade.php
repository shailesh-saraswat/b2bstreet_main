
 <!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Meta -->
      <title>B2B Streets | Indias First Visual B2B Search Engine </title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
      <meta name="description" content="Indias First Visual B2B Search Engine, Get Unlimited Business Deals with Indias Best Mart Business Directory, For More Details Hit The Call Button.">
      <meta name="author" content="">
      <meta name="keywords" content="B2B Streets, Indias First Visual B2B Search Engine">
      <meta name="robots" content="all">
      <!-- Bootstrap Core CSS -->
      <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
      <!-- Customizable CSS -->
      
      <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/blue.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/simple-line-icons.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/owl.transitions.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/animate.min.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/rateit.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/anuj.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-select.min.css') }}">
      <!-- Icons/Glyphs -->
      <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.css') }}">
      <!-- Fonts -->
      <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
      <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,600italic,700,700italic,800' rel='stylesheet' type='text/css'>
      <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,500i,600,700,800,900" rel="stylesheet">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
   <body class="cnt-home inside-header">
      @foreach($banners as $banner)
      
       <div class="cl-header-fixed">
       <div class="cl-header container">
         <div class="cl-grid">
            <div class="cl-logo">
               <img src="{{asset($banner['image'])}}">
            </div>
            <div class="cl-menu">
               <div class="nav">
                  <a href="{{url('/'.$products->user['slug'])}}#s1" class="">Intro</a>
                  <a href="{{url('/'.$products->user['slug'])}}#s2">About </a>
                  <a href="{{url('/'.$products->user['slug'])}}#s3" class="active">Products</a>
                  <a href="{{url('/'.$products->user['slug'])}}#s4" class="">Gallery</a>
                  <a href="{{url('/'.$products->user['slug'])}}#s1" class="">Contact Us</a>
               </div>
            </div>
         </div>
      </div>
      </div>
      @endforeach
      <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js'></script>
<style>
   .cl-logo img {
   width: 67px;
}
</style>
      <script>
function scrollNav() {
  $('.nav a').click(function(){
    $(".active").removeClass("active");     
    $(this).addClass("active");
    
    $('html, body').stop().animate({
      scrollTop: $($(this).attr('href')).offset().top - 160
    }, 300);
    return false;
  });
}
scrollNav();
      </script>

  
 <script>
const openMenu = document.querySelector(".open-menu");
const closeMenu = document.querySelector(".close-menu");
const menuWrapper = document.querySelector(".menu-wrapper");
const hasCollapsible = document.querySelectorAll(".has-collapsible");

// Sidenav Toggle
openMenu.addEventListener("click", function () {
   menuWrapper.classList.add("offcanvas");
});

closeMenu.addEventListener("click", function () {
   menuWrapper.classList.remove("offcanvas");
});

// Collapsible Menu
hasCollapsible.forEach(function (collapsible) {
   collapsible.addEventListener("click", function () {
      collapsible.classList.toggle("active");

      // Close Other Collapsible
      hasCollapsible.forEach(function (otherCollapsible) {
         if (otherCollapsible !== collapsible) {
            otherCollapsible.classList.remove("active");
         }
      });
   });
});

</script>
      <!-- ============================================== HEADER : END ============================================== -->

      @include('front.seo-keywords')
      <div class="breadcrumb mt-89">

         <div class="container">
                        <div class="breadcrumb-inner">
               <ul class="list-inline list-unstyled chevron">
                  <li><a href="{{route('Front.Index')}}">Home</a><i class="fa fa-chevron-right"></i></li>
                  <li class='active'>{{$products->main_category['name']}}<i class="fa fa-chevron-right"></i></li>
                  <li class='active'>{{$products->category['name']}} <i class="fa fa-chevron-right"></i></li>
                   <li class='active'>{{$products->subcategory['name']}} <i class="fa fa-chevron-right"></i></li>
                    <li class='active'>{{$products->user['company_name']}}</li>
               </ul>
            </div>
         </div>
         <!-- /.container -->
      </div>
      <!-- /.breadcrumb -->
      <div class="body-content outer-top-xs">
         <div class="container">
            <div class="details-top-grid">
               <div class="details-top-grid-left" >
                  @if($products->product_images != null)
                     @foreach($products->product_images as $image)
                  <div class="mySlides">
                     <div class="numbertext">1 / 6</div>
                     <img src="{{ asset($image['product_image']) }}" style="width:100%">
                  </div>
                  @endforeach
                  @endif

                  <a class="prev" onclick="plusSlides(-1)">❮</a>
                  <a class="next" onclick="plusSlides(1)">❯</a>
                  <div class="caption-container">
                     <p id="caption"></p>
                  </div>
                  <div class=" thumb-grids">
                       @if($products->product_images != null)
                       @php $i=1; @endphp
                     @foreach($products->product_images as $image)

                     <div class="column">
                        <img class="demo cursor" src="{{ asset($image['product_image']) }}" style="width:100%" onclick="currentSlide({{$i}})" alt="The Woods">
                     </div>
                      @php $i++; @endphp
                     @endforeach
                    @endif
                 </div>
               </div>



               <div class="details-top-grid-right">
                  <h2> {{$products->title}} </h2>
                  <p>{!! $products->description !!}</p>
              <ul>
                                 <li><i class="fas fa-angle-right"></i><span>Quality Available:</span>{{$products->quality}}</li>
                                 <li><i class="fas fa-angle-right"></i><span>Packaging Type:</span> {{$products->packaging_type}}</li>
                                 <li><i class="fas fa-angle-right"></i><span>Storage Tips:</span> {{$products->storage_tips}}</li>
                                 <li><i class="fas fa-angle-right"></i><span>Packaging Size:</span> {{$products->packaging}}</li>
                              </ul>
                       <div class="list-cont-no">
                                 <div class="mob-no">
                                    <i class="fas fa-mobile-alt"></i>
                                 </div>
                                 <!-- {{$products->user['mobile']}} -->
                               <!--   <div id="myBtn3" class="cont-no-button">
                                   <a href="#"> Contact Supplier</a>
                                 </div> -->
                              </div>
                  <div class=" profile-form">
                     <div id="popup" class="overlay">
                        <div class="popup">
                           <div class="dott-pattern  form-pat pd-details"></div>
                           <div class="">                   
                        <form action="{{url('/add-supplier-contact')}}" method="post">

                        @csrf
                       @if(Session::has('message'))
                       <div class="alert alert-danger"  role="alert">{{Session::get('message')}}</div>
                       @elseif(Session::has('success'))
                       <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                       @endif
                        <div class="alert alert-danger alert-dismissible custom_alert" style="display: none;">
                           <span class="alert_message"></span>
                        </div> 
                        <div class="det-two-grid">
                        <input type="hidden" name="user_id" id="" value="{{$products->user_id}}">

                        <input type="hidden" name="supplier_email"  value="{{$banner['email']}}">

                        <div>
                         <label class="info-title" for="exampleInputName">Your Name <span>*</span></label>
                                       <input type="text" id="name" name="name" class="form-control unicase-form-control text-input cal" placeholder="" value="{{old('name')}}">
                                       @if($errors->has('name'))
                                       <div class="error">{{ $errors->first('name') }}</div>
                                       @endif
                                    </div>
                                     <div>
                                       <label class="info-title" for="exampleInputEmail1">Contact <span>*</span></label>
                                       <input type="text" class="form-control unicase-form-control text-input cal"  id="mobile" name="mobile" placeholder="" value="{{old('mobile')}}">
                                       @if($errors->has('mobile'))
                                      <div class="error">{{ $errors->first('mobile') }}</div>
                                     @endif
                                    </div>
                                   
                                 </div>
                                 <div class="det-two-grid">
                                    <div>
                                       <label class="info-title" for="exampleInputEmail1">Email Address <span>(Not Mandatory)</span></label>
                                       <input class="form-control unicase-form-control text-input cal" type="email" id="email_data" name="email" placeholder="" value="{{old('email')}}">
                                    @if($errors->has('email'))
                                <div class="error">{{ $errors->first('email') }}</div>
                                @endif
                                    </div>
                                    <div>
                                       <label class="info-title" for="exampleInputTitle">Location<span>*</span></label>
                                       <input type="text" id="location" name="location" class="form-control unicase-form-control text-input cal" placeholder=""  value="{{old('location')}}">
                                       @if($errors->has('location'))
                                <div class="error">{{ $errors->first('location') }}</div>
                                @endif
                                    </div>
                                 </div>
                                 <div>
                              <label class="info-title" for="exampleInputComments">Your Requirements <span>*</span></label>  
                              <textarea class="form-control unicase-form-control" name="requirements" id="requirements" >{{old('requirements')}}</textarea>
                              @if($errors->has('requirements'))
                                <div class="error">{{ $errors->first('requirements') }}</div>
                                @endif
                                 </div>
                                  <button type="submit" id="submit" name="submit">Submit</button>
                               
                              </form>
                              <p id="trimbn" style="color:red;"></p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <script>
            var slideIndex = 1;
            showSlides(slideIndex);
            
            function plusSlides(n) {
              showSlides(slideIndex += n);
            }
            
            function currentSlide(n) {
              showSlides(slideIndex = n);
            }
            
            function showSlides(n) {
              var i;
              var slides = document.getElementsByClassName("mySlides");
              var dots = document.getElementsByClassName("demo");
              //var captionText = document.getElementById("caption");
              if (n > slides.length) {slideIndex = 1}
              if (n < 1) {slideIndex = slides.length}
              for (i = 0; i < slides.length; i++) {
                  slides[i].style.display = "none";
              }
              for (i = 0; i < dots.length; i++) {
                  dots[i].className = dots[i].className.replace(" active", "");
              }
              slides[slideIndex-1].style.display = "block";
              dots[slideIndex-1].className += " active";
              captionText.innerHTML = dots[slideIndex-1].alt;
            }
         </script>   
      </div>
      <!-- /.body-content -->
</div>




      <script>
         // Get the modal
         var modal = document.getElementById("myModal");
         
         // Get the button that opens the modal
         var btn = document.getElementById("myBtn");
         
         // Get the <span> element that closes the modal
         var span = document.getElementsByClassName("close")[0];
         
         // When the user clicks the button, open the modal 
         btn.onclick = function() {
           modal.style.display = "block";
         }
         
         // When the user clicks on <span> (x), close the modal
         span.onclick = function() {
           modal.style.display = "none";
         }
         
         // When the user clicks anywhere outside of the modal, close it
         window.onclick = function(event) {
           if (event.target == modal) {
             modal.style.display = "none";
           }
         }
      </script>
  <div class="copyright-bar1">
               <div class="container">
                  <div class="row align-items-center">
                        <div class="col-md-6 text-left">
                            <p>All right reserved @IBEM  Solutions LLP.</p>
                        </div>
                      <div class="col-md-6 text-right">
                           
                           <a href="https://b2bstreets.com/"><img src="{{ asset('assets/images/footer-logo2.png') }}" style="width: 127px;"></a>
                           <a href="https://www.ibem.co.in/"><img src="{{asset ('assets/images/slider/footer-logo1.png') }}" width="10%"></a>
                        </div>
                   </div>
               </div>
            </div>



<style>
.copyright-bar1 p {
    margin-bottom: 0;
    text-align: left;
    padding-top: 15px;
    font-size: 14px;
    color: #000;
}    
    .copyright-bar1 {
    background: none;
    padding: 10px 0px;
    z-index: 99;
    position: relative;
    border-top: 1px solid #eeee;
    margin-top: -20px;
    background-color: #ededed;
}
.error{
   color: red;
}
 
</style>
      


  
      <!-- JavaScripts placed at the end of the document so the pages load faster --> 
      <script src="https://b2bstreets.com/assets/js/bootstrap-hover-dropdown.min.js"></script> 
      <script src="https://b2bstreets.com/assets/js/owl.carousel.min.js"></script> 
      <script src="https://b2bstreets.com/assets/js/echo.min.js"></script> 
      <script src="https://b2bstreets.com/assets/js/jquery.easing-1.3.min.js"></script> 
      <script src="https://b2bstreets.com/assets/js/bootstrap-slider.min.js"></script> 
      <script src="https://b2bstreets.com/assets/js/jquery.rateit.min.js"></script> 
      <script type="text/javascript" src="https://b2bstreets.com/assets/js/lightbox.min.js"></script> 
      <script src="https://b2bstreets.com/assets/js/bootstrap-select.min.js"></script> 
      <script src="https://b2bstreets.com/assets/js/wow.min.js"></script> 
      <script src="https://b2bstreets.com/assets/js/scripts.js"></script>
   </body>
</html
