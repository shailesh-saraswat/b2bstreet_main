
@extends('layouts.front')
@section('content')
@include('front.seo-keywords') 
 

    <div class="breadcrumb">
            <div class="container">
               <div class="breadcrumb-inner">
                  <ul class="list-inline list-unstyled">
                     <li><a href="{{route('Front.Index')}}">Home</a></li>
                     <li class='active'>Package</li>
                  </ul>
               </div>
               <!-- /.breadcrumb-inner -->
            </div>
            <!-- /.container -->
    </div>
         <!-- /.breadcrumb -->   

    <section id="package-banner" style="background-image: url({{ asset('public/images/blog/package-banner.jpg') }});background-size: cover; position: relative;height: 400px;">
    <div class="container">
        <div class="row">
            <div class="headings">
                <h2>B2B Streets Package</h2>
                <h4>Membership Details</h4>
            </div>
        </div>
    </div>
</section>
          
          

          
     
<section id="package">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="pack-head text-center">
                    <h3>Our Packages</h3>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-3">
                <div class="pack-1">
                    <div class="top-1">
                        <img src="{{ asset('public/images/blog/bg.jpg') }}" class="img-fluid" width="100%">
                        <h4>Visual Initiator</h4>
                    </div>
                    <div class="body text-center">
                        <ul>
                            <li>Corporate Film</li>
                            <li>MSME Assistance</li>
                            <li>Banner Add</li>
                            <li>Social Media Package</li>
                            <li>Google Package</li>
                            <li>Other</li> 
                             
                        </ul>
                    </div>
                    <div class="bottom text-center">
                        <a href="#" class="btn btn-primary-1" data-toggle="modal" data-target="#myModal">Get Started</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 text-center">
                <div class="pack-1">
                    <div class="top-2">
                         <img src="{{ asset('public/images/blog/bg2.jpg') }}" class="img-fluid" width="100%">
                        <h4>Visual Certified Supplier</h4>
                    </div>
                    <div class="body">
                        <ul>
                            <li>Sub Domain</li>
                            <li>Standard fix Template</li>
                            <li>Certificate of Trust</li>
                            <li>HD Video Bio Presentation</li>
                            <li>MSME Video Interview</li>
                            <li>20 keyword search on b2bstreets.com</li>
                        </ul>
                    </div>
                    <div class="bottom text-center">
                        <a href="#" class="btn btn-primary-2" data-toggle="modal" data-target="#myModal">Get Started</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="pack-1">
                    <div class="top-4">
                        <img src="{{ asset('public/images/blog/bg4.jpg') }}" class="img-fluid" width="100%">
                        <h4>Visual Sliver Supplier</h4>
                    </div>
                    <div class="body text-center">
                        <ul>
                            <li>Sub Domain</li>
                            <li>Standard fix Template</li>
                            <li>Certificate of Trust</li>
                            <li>HD Video Bio Presentation</li>
                            <li>HD MSME Video Interview</li>
                            <li>30 keyword search on b2bstreets.com</li>
                        </ul>
                    </div>
                    <div class="bottom text-center">
                         <a href="#" class="btn btn-primary-3" data-toggle="modal" data-target="#myModal">Get Started</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="pack-1">
                    <div class="top-3">
                        <img src="{{ asset('public/images/blog/bg3.jpg') }}" class="img-fluid" width="100%">
                        <h4>Visual Gold Supplier</h4>
                    </div>
                    <div class="body text-center">
                        <ul>
                            <li>Sub Domain</li>
                            <li>Standard fix Template</li>
                            <li>Certificate of Trust</li>
                            <li>HD Video Bio Presentation</li>
                            <li>HD MSME Video Interview</li>
                            <li>30 keyword search on b2bstreets.com</li>
                        </ul>
                    </div>
                    <div class="bottom text-center">
                        <a href="#" class="btn btn-primary-4" data-toggle="modal" data-target="#myModal">Get Started</a>
                    </div>
                </div>
            </div>
            
        </div>
        
         <div class="row">
             <div class="col-md-3"></div>
            <div class="col-md-3">
                <div class="pack-1">
                    <div class="top-5">
                        <img src="{{ asset('public/images/blog/bg5.jpg') }}" class="img-fluid" width="100%">
                        <h4>Visual Platinum Supplier</h4>
                    </div>
                    <div class="body text-center">
                        <ul>
                            <li>Sub Domain</li>
                            <li>Standard fix Template</li>
                            <li>Certificate of Trust</li>
                            <li>HD Video Bio Presentation</li>
                            <li>HD MSME Video Interview</li>
                            <li>60 keyword search on b2bstreets.com</li>
                        </ul>
                    </div>
                    <div class="bottom text-center">
                         <a href="#" class="btn btn-primary-5" data-toggle="modal" data-target="#myModal">Get Started</a>
                    </div>
                </div>
            </div>
           
            <div class="col-md-3">
                <div class="pack-1">
                    <div class="top-6">
                        <img src="{{ asset('public/images/blog/bg6.jpg') }}" class="img-fluid" width="100%">
                        <h4>Visual Customized</h4>
                    </div>
                    <div class="body text-center">
                        <ul>
                            <li>Sub Domain</li>
                            <li>Standard fix Template</li>
                            <li>Up to 50 products</li>
                            <li>Video Bio Presentation</li>
                            <li>Published on MSMENEWS.NET</li>
                            <li>10 keyword search on b2bstreets.com</li>
                        </ul>
                    </div>
                    <div class="bottom text-center">
                         <a href="#" class="btn btn-primary-6" data-toggle="modal" data-target="#myModal">Get Started</a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section> 

<section id="package-modal">
    <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="myModalLabel">Get the Best Package</h4>
      </div>
     @include('admin.partials.messages')
      <div class="modal-body">
       <form class="form-horizontal" method="post" action="{{route('Front.Add.Package')}}">
        @csrf

                        <div class="form-group">
                            <label for="inputEmail3" class="col-md-offset-2 col-sm-2 control-label">Name:</label>
                            <div class="col-md-6">
                              <input type="text" class="form-control" id="name" name="name" placeholder="ABC" value="{{old('name')}}">
                              <span class="text-danger error-text name_err"></span>
                            </div>
                          </div>
                           
                          <div class="form-group">
                            <label for="inputEmail3" class="col-md-offset-2 col-sm-2 control-label">Email:</label>
                            <div class="col-md-6">
                              <input type="email" class="form-control" id="email" name="email" placeholder="abc@gmail.com" value="{{old('email')}}">
                             <span class="text-danger error-text email_err"></span>
                            </div>
                          </div>

                           <div class="form-group">
                            <label for="inputEmail3" class="col-md-offset-2 col-sm-2 control-label">Phone No.:</label>
                            <div class="col-md-6">
                              <input type="text" class="form-control" id="phone" name="phone" placeholder="12345679" value="{{old('phone')}}">
                              <span class="text-danger error-text phone_err"></span>
                            </div>
                          </div>
                            <div class="form-group">
                            <label for="inputPassword3" class="col-md-offset-2 col-sm-2 control-label">Company Name:</label>
                            <div class="col-md-6">
                              <input type="text" class="form-control" id="company_name" name="company_name" placeholder="abc" value="{{old('company_name')}}">
                             <span class="text-danger error-text company_name_err"></span>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="inputPassword3" class="col-md-offset-2 col-sm-2 control-label">Address:</label>
                            <div class="col-md-6">
                              <input type="text" class="form-control" id="address" name="address" placeholder="India" value="{{old('address')}}">
                             <span class="text-danger error-text address_err"></span>
                            </div>
                          </div>
                          <div class="modal-footer">
                        <button type="submit" value="submit" class="btn btn-form btn-submit">Submit</button>
                       </div>
            </form>                            
      </div>
    </div>
  </div>
</div>
</section>

<script type="text/javascript">
   $(document).ready(function() {
       $(".btn-submit").click(function(e){
           e.preventDefault();
           var name = $("#name").val();
           var email = $("#email").val();
           var phone = $("#phone").val();
           var company_name = $("#company_name").val();
           var address = $("#address").val();
           
           $.ajax({
               url: "{{ url('front/package') }}",
               type:'POST',
               data: {_token:_token,name:name,email:email,phone:phone, company_name:company_name, address:address},
               success: function(data) {
                  console.log(data.error)
                   if($.isEmptyObject(data.error)){
                     $('#success').show();
                     $('#success').html(data.success);
                     $('#msg_div').removeClass('d-none');

                     document.getElementById("package").reset(); 

                     //alert(data.success);
                     // setTimeout(function(){
                     // $('#success').hide();
                     // $('#msg_div').hide();
                     // },10000);
                      
                      // alert(data.success);
                   }else{
                       printErrorMsg(data.error);
                   }
               }
           });
       }); 

       function printErrorMsg (msg) {
           $.each( msg, function( key, value ) {
           console.log(key);
             $('.'+key+'_err').text(value);
           });
       }
   });
</script>
<style>
   .text-danger {
    font-size: 13px !important;
}
</style>

<style>
    .pack-1 {
    border: 1px solid #eeee;
    margin-top: 30px;
}
.pack-1{
    padding: 0px;
    position: relative;
}

.pack-1 h4 {
    font-size: 18px;
    font-weight: 600;
        position: absolute;
    top: 12%;
    left: 15%;
    justify-content: center;
    display: inline-block;
}
.error{
    color: red;
}
</style>
 
          
@endsection