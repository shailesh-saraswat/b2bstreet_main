
@extends('layouts.front')
@section('content')
@include('front.seo-keywords')
       
        <div class="breadcrumb">
            <div class="container">
               <div class="breadcrumb-inner">
                  <ul class="list-inline list-unstyled">
                     <li><a href="https://b2bstreets.com">Home</a></li>
                     <li class='active'>Career</li>
                  </ul>
               </div>
               <!-- /.breadcrumb-inner -->
            </div>
            <!-- /.container -->
         </div>
         <!-- /.breadcrumb -->
      

<section id="career" style="background-image: url({{ asset('public/images/blog/career-b2b.png') }});background-size: cover; position: relative;height: 400px;">
    <div class="container">
        <div class="row">
             <div class="headings">
                <h2>B2B Streets Careers</h2>
                <h4>Interested in joining </h4>
            </div>
        </div>
    </div>
</section>
 <style>
     .error{
    color: red;
}
 </style>            
          
<section id="career-sec">
    <div class="container">
       @foreach($careers as $career) 
        <div class="row align">
            <div class="col-md-7">
                <div class="career-cont">
                    <h3>{{$career->title}}</h3>
                    <p>{!!$career->description!!}</p>
                   
                
                </div>
            </div>
            <div class="col-md-5">
                <div class="row">
                    <div class="career-box">
                        <div class="complaint-heading">
                            <h2 class="text-center">Interested in joining!!</h2>
                            <p class="text-center">Get in touch with us to discover the right career opportunities for you.</p>
                        @if(Session::has('message'))
                        <div class="alert alert-danger"  role="alert">{{Session::get('message')}}</div>
                        @elseif(Session::has('success'))
                        <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                        @endif
                            <form method="post" action="{{route('Front.Add.Career')}}">
                                @csrf
                                <div class="form-group">
                                <label for="exampleInputtext">Name*</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="name" value="{{old('name')}}">
                                @if($errors->has('name'))
                                <div class="error">{{ $errors->first('name') }}</div>
                                @endif
                              </div>
                                <div class="form-group">
                                <label for="exampleInputEmail1">Email*</label>
                                <input type="text" class="form-control" id="email" name="email" placeholder="abc@gmail.com" value="{{old('email')}}">
                                @if($errors->has('email'))
                                <div class="error">{{ $errors->first('email') }}</div>
                                @endif
                                </div>
                                <div class="form-group">
                                <label for="exampleInputtext">Mobile Number*</label>
                                <input type="text" class="form-control" id="phone" name="phone" placeholder="Mobile / Cell Phone" value="{{old('phone')}}">
                                @if($errors->has('phone'))
                                <div class="error">{{ $errors->first('phone') }}</div>
                                @endif
                              </div>
                               
                                <div class="form-group">
                                <label for="exampleInputtext">Preferred Job Location*</label>
                                <input type="text" class="form-control" id="location" name="location" placeholder="Provide Short Title To Your Complaint" value="{{old('location')}}">
                                @if($errors->has('location'))
                                <div class="error">{{ $errors->first('location') }}</div>
                                @endif
                              </div>
                              
                                
                             <div class="form-group">
                                <label for="exampleInputtext">Message*</label>
                                 <textarea class="form-control" id="message" name="message" rows="3" placeholder="abc......." value="{{old('message')}}"></textarea>
                                 @if($errors->has('message'))
                                <div class="error">{{ $errors->first('message') }}</div>
                                @endif
                              </div>
                               
                              <button type="submit" class="btn btn-default">Submit</button>
                            </form>
                        </div>
                    </div>
              </div>
            </div>            
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="waiting-sec">
                    <h4>Why Choose B2B Streets?</h4>
                    <hr>
                    <p>{!!$career->content!!}</p>
                    
                    
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="waiting-sec">
                    <h4>Why are you waiting?</h4>
                    <hr>
                    <p>{!!$career->short_content!!}</p>
                   
                    
                   
                </div>
            </div>
        </div>
        @endforeach
    </div>
</section>
          

@endsection