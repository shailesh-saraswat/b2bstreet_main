@extends('layouts.front')
@section('content')
@include('front.seo-keywords')
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="{{route('Front.Index')}}">Home</a></li>
				
				<li class='active'><a href="{{route('Front.TrendingProducts')}}">Trending Products</li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->

<div class="body-content outer-top-xs">
	<section class="section featured-product wow fadeInUp   pt-50 pb-50 mb-50 hot-seller" style=" ">
<div class="container">
<h2 class="section-title1  "><span>Hot </span> Products</h2>
<div class="products-gridss">
@if (count($product) > 0 && $product !== null)
         @foreach ($product as $prod)
<div class="products">
<div class="product">
@if ($prod->firstImage)
<div class="product-image">
<div class="image"> <a
   href="#"><img
   src="{{ asset($prod->firstImage->product_image) }}" alt=""
   style="height:200px;"></a> </div>
</div>
@endif
<div class="product-info text-left cate-headi">
<h3 class="name"><a
   href="{{ URL::to('/products/' . $prod->slug) }}">{!! Str::limit(strip_tags($prod->title), 20) !!}</a>
</h3>
</div>
</div>
</div>
@endforeach
    @else
   @endif
</div>
<!-- /.home-owl-carousel -->
</div>
</section>
<!-- /.section -->
</div>
@endsection






