@extends('layouts.front')
@section('content') 
@include('front.seo-keywords')

<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="{{route('Front.Index')}}">Home</a></li>
				
				<li class='active'>Covid-19</li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->
   <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="covid-box">
                        <h2>COVID-19 Supplies</h2>
                        <p>Follow the latest figures on the COVID-19 supplies B2BSTREETS has shipped since the start of the outbreak.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="second">
        <div class="container">
            <div class="row">
			  <div class="col-md-12">
                <h2>Corona Covid-19 Supplies : </h2>
            </div>
            </div>
            <div class="row">
                @foreach($category as $cat)
                <div class="col-md-3">
                    <div class="box1 img-wrapper">
                        <img src="{{ asset($cat['image']) }}" class="img-responsive inner-img">
                        <h6><a href="{{ URL::to('/category/' . $cat->slug) }}">{{$cat->name}}</a></h6>
                    </div>
                </div>
                <!-- <div class="col-md-3">
                    <div class="box1 img-wrapper">
                        <img src="{{ asset('public/images/medical-treatment.jpg') }}" class="img-responsive inner-img">
                        <h6>Medical & Hospital Consumables</h6>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box1 img-wrapper">
                        <img src="{{ asset('public/images/mask.jpg') }}" class="img-responsive inner-img">
                        <h6>Masks, Sanitizers & Raw Material</h6>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box1 img-wrapper">
                        <img src="{{ asset('public/images/hospital-furniture.jpg') }}" class="img-responsive inner-img">
                        <h6>Hospital and Medical Furniture</h6>
                    </div>
                </div> -->
              @endforeach  
            </div>            
        </div>
    </section>

 

    <section id="fourth">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Coronavirus Disease (COVID-19) Technical Guidance: Maintaining Essential Health Services and Systems - </h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h4>COVID-19: Operational guidance for maintaining essential health services during an outbreak : </h4>
                    <p>When health systems are overwhelmed, both direct mortality from an outbreak and indirect mortality from vaccine-preventable and treatable conditions increase dramatically. Countries will need to make difficult decisions to balance the demands of responding directly to COVID-19, while simultaneously engaging in strategic planning and coordinated action to maintain essential health service delivery, mitigating the risk of system collapse. This document expands on the content of the Operational planning guidelines to support country preparedness and response, and provides guidance on a set of targeted immediate actions that countries should consider at national, regional, and local level to reorganize and maintain access to high-quality essential health services for all.</p>
                </div>
                <div class="col-md-12">
                    <h4>Pulse survey on continuity of essential health services during the COVID-19 pandemic : </h4>
                    <p>The <em>Pulse survey on continuity of essential health services during the COVID-19 pandemic</em> aimed to gain initial insight from country key informants into the impact of the COVID-19 pandemic on essential health services across the life course. The survey results in this interim report can improve our understanding of the extent of disruptions across all services, the reasons for disruptions, and the mitigation strategies countries are using to maintain service delivery.</p>
                </div>
                <div class="col-md-12">
                    <h4>Considerations for the provision of essential oral health services in the context of COVID-19 : </h4>
                    <p>The purpose of this document is to address specific needs and considerations for essential oral health services in the context of COVID-19 in accordance with WHO operational guidance on maintaining essential health services.</p>
                </div>
                <div class="col-md-12">
                    <h4>Harmonized health service capacity assessments in the context of the COVID-19 pandemic : </h4>
                    <p>Countries are facing a multitude of questions that must be addressed to prepare for and respond to COVID-19 while maintaining other essential health services across the life course. Key decisions and actions must be informed by accurate and timely data on health service delivery and utilization throughout all phases of the COVID-19 pandemic.</p>
                </div>
                <div class="col-md-12">
                    <h4>Recommendations to Member States to improve hand hygiene practices to help prevent the transmission of the COVID-19 virus : </h4>
                    <p>WHO recommends member states provide universal access to public hand hygiene stations and making their use obligatory on entering and leaving any public or private commercial building and any public transport facility. It is also recommended that healthcare facilities improve access to and practice of hand hygiene.</p>
                </div>
                <div class="col-md-12">
                    <h4>WHO SAVES LIVES: Clean your hands in the context of COVID-19 : </h4>
                    <p>Hand Hygiene is one of the most effective actions to reduce the spread of pathogens and prevent infections, including the COVID-19 virus. This document promotes the WHO global hand hygiene campaign SAVE LIVES: Clean Your Hands in the context of other hand hygiene initiatives launched by WHO for COVID-19, and provides rapid technical guidance. </p>
                </div>
            </div>
        </div>
    </section>


@endsection 