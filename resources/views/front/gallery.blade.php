@extends('layouts.front')
@section('content')
@include('front.seo-keywords')

<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="{{route('Front.Index')}}">Home</a></li>
				
				<li class='active'>Gallery</li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->
<div class="body-content outer-top-xs">
	<div class='container'>
		 <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.14/css/lightgallery.css'>
      
      

      <div class="cont">
         
         <div class="container">
            @foreach($galleries as $gallery)
         <div class="inside-top-heading mb-30">
            <h2>{{$gallery->title}}</h2>
			<p>{!! $gallery->content !!}</p>
			</div>
         @endforeach
         </div>
         <div class="demo-gallery">
            <ul id="lightgallery">
             @foreach($galleries as $gallery)
               <li data-responsive="{{ asset($gallery['image']) }}"data-src="{{ asset($gallery['image']) }}">
	            <a href=""><img class="img-responsive" src="{{ asset($gallery['image']) }}"></a>
               </li>
            @endforeach
            </ul>
         </div>
      </div>







      <script>$(document).ready(() => {  $("#lightgallery").lightGallery({    pager: true  });});</script><script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script><script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script><script src='https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js'></script><script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js'></script><script src='https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.14/js/lightgallery-all.min.js'></script><script  src="./script.js"></script> 
			       </div>
			</div><!-- /.col -->
			<div class="clearfix"></div>
		</div><!-- /.row -->
		</div><!-- /.container -->
</div><!-- /.body-content -->




 


@endsection