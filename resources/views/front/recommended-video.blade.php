@extends('layouts.front')
@section('content') 
@include('front.seo-keywords')

<div class="breadcrumb">
    <div class="container">
        <div class="breadcrumb-inner">
            <ul class="list-inline list-unstyled">
                <li><a href="{{route('Front.Index')}}">Home</a></li>
                
                <li class='active'>Recommended Video Bio</li>
            </ul>
        </div><!-- /.breadcrumb-inner -->
    </div><!-- /.container -->
</div><!-- /.breadcrumb -->


<div class="body-content outer-top-xs">
  <div class='container'>
      <div class="inside-top-heading mb-30">
            <h2>Recommended Video Bio</h2>
      </div>
    <section class=" "  >
    <div class="  two-slide  msme-int">     
    <div class="two-grid">    
        <div class="products-grids">

         @foreach($video_recommended as $video)

            <div class="product">
               <div class="product-image">
                  <video style="height:150px;width:100%;" controls="">
                     <source src="{{asset($video['video'])}}#t=15" type="video/mp4">
                  </video>
                  <div class="logo-part">
                  <img src="{{asset($video['image'])}}" style="height: 60px!important;width: 60px!important;">
                  </div> 
               </div>
               <div class="product-info text-left">
                 <h3 class="name"><a href="{{url('/'.$video->user['slug'])}}">{{$video->user['company_name'] }}</a></h3>
                 <!--  <p>{{$video['description']}}</p> -->
               </div>
            </div>
      @endforeach

                 </div>
                 </div>
                 </div> 
    </section>
      </div><!-- /.container -->
</div><!-- /.body-content -->

@endsection

