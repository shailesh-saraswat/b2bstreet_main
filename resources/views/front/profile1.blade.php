
<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Meta -->
      <title>B2B Streets | Indias First Visual B2B Search Engine </title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
      <meta name="description" content="Indias First Visual B2B Search Engine, Get Unlimited Business Deals with Indias Best Mart Business Directory, For More Details Hit The Call Button.">
      <meta name="author" content="">
      <meta name="keywords" content="B2B Streets, Indias First Visual B2B Search Engine">
      <meta name="robots" content="all">
      <!-- Bootstrap Core CSS -->
      <link rel="stylesheet" href="assets/css/bootstrap.min.css">
      <!-- Customizable CSS -->
      <link rel="stylesheet" href="assets/css/main.css">
      <link rel="stylesheet" href="assets/css/blue.css">
      <link rel="stylesheet" href="assets/css/simple-line-icons.css">
      <link rel="stylesheet" href="assets/css/owl.carousel.css">
      <link rel="stylesheet" href="assets/css/owl.transitions.css">
      <link rel="stylesheet" href="assets/css/animate.min.css">
      <link rel="stylesheet" href="assets/css/rateit.css">
      <link rel="stylesheet" href="assets/css/anuj.css">
      <link rel="stylesheet" href="assets/css/bootstrap-select.min.css">
      <!-- Icons/Glyphs -->
      <link rel="stylesheet" href="assets/css/font-awesome.css">
      <!-- Fonts -->
      <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
      <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,600italic,700,700italic,800' rel='stylesheet' type='text/css'>
      <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,500i,600,700,800,900" rel="stylesheet">
	   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
   <body class="cnt-home inside-header">




       <div class="cl-header-fixed">
       <div class="cl-header container">
			<div class="cl-grid">
				<div class="cl-logo">
					<img src="images/swasthi.png">
				</div>
				<div class="cl-menu">
					<div class="nav">
						<a href="#s1" class="">Intro</a>
						<a href="#s2">About </a>
						<a href="#s3" class="active">Products</a>
						<a href="#s4" class="">Gallery</a>
						<a href="#s6" class="">Packages</a>
						<a href="#s5" class="">Contact Us</a>
					</div>
				</div>
			</div>
	   </div>
	   </div>
	   
	   
	   
	   
 <div class="mobile-menu-show client-Profile">
<header class="headerq">
	<nav class="navbar">
		<span class="open-menu">
			<svg xmlns="http://www.w3.org/2000/svg" width="24" height="16">
				<g fill="#252a32" fill-rule="evenodd">
					<path d="M0 0h24v2H0zM0 7h24v2H0zM0 14h24v2H0z" />
				</g>
			</svg>
		</span>
		 
		<div class="menu-wrapper">
			<ul class="menu">
				<li class="menu-block">
					<span class="close-menu">
						<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20">
							<path fill="#252a32" fill-rule="evenodd" d="M17.778.808l1.414 1.414L11.414 10l7.778 7.778-1.414 1.414L10 11.414l-7.778 7.778-1.414-1.414L8.586 10 .808 2.222 2.222.808 10 8.586 17.778.808z" />
						</svg>
					</span>
				</li>
				<li class="menu-item"><a href="#" class="menu-link">Intro</a></li>
				 
				<li class="menu-item"><a href="#" class="menu-link">About</a></li>
				<li class="menu-item"><a href="#" class="menu-link">Products</a></li>
				<li class="menu-item"><a href="#" class="menu-link">Gallery</a></li>
				<li class="menu-item"><a href="#" class="menu-link">Packages</a></li>
				<li class="menu-item"><a href="#" class="menu-link">Contact Us</a></li>
			</ul>
		</div>
	</nav>
</header>
</div> 
  
  
  
 <script>
const openMenu = document.querySelector(".open-menu");
const closeMenu = document.querySelector(".close-menu");
const menuWrapper = document.querySelector(".menu-wrapper");
const hasCollapsible = document.querySelectorAll(".has-collapsible");

// Sidenav Toggle
openMenu.addEventListener("click", function () {
	menuWrapper.classList.add("offcanvas");
});

closeMenu.addEventListener("click", function () {
	menuWrapper.classList.remove("offcanvas");
});

// Collapsible Menu
hasCollapsible.forEach(function (collapsible) {
	collapsible.addEventListener("click", function () {
		collapsible.classList.toggle("active");

		// Close Other Collapsible
		hasCollapsible.forEach(function (otherCollapsible) {
			if (otherCollapsible !== collapsible) {
				otherCollapsible.classList.remove("active");
			}
		});
	});
});

</script>
	   
	   
	   
	   
	   
	   
	   
	   <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js'></script>
	   <script>
function scrollNav() {
  $('.nav a').click(function(){
    $(".active").removeClass("active");     
    $(this).addClass("active");
    
    $('html, body').stop().animate({
      scrollTop: $($(this).attr('href')).offset().top - 160
    }, 300);
    return false;
  });
}
scrollNav();
	   </script>
	   
	   
      <!-- ============================================== HEADER : END ============================================== -->
      <div class="profile-container" id="s1">
         <div class="breadcrumb bg-none">
            <div class="container">
               <div class="breadcrumb-inner">
                  <ul class="list-inline list-unstyled">
                     <li><a href="index.php">Home</a></li>
                     <li class="active">Video Bio</li>
                  </ul>
               </div>
               <!-- /.breadcrumb-inner -->	
            </div>
            <!-- /.container -->
         </div>
         <div class="container">
            <div class="pro-grid">
               <div class="video-right">
                  <div class="video-frame">
                     <div class="video-pro-content">
                        <video controls="">
                           <source src="images/video/5d9ed5834e625.mp4#t=1" type="video/mp4">
                           Your browser doesnot support the video tag.						
                        </video>
                     </div>
                  </div>
               </div>
               <div class="profile-flex ">
                  <div class="profile-img">
                     <div class="profile-cont">
                        <div class="profile-logo-div">
                           <img class="profile-logo" src="images/swasthi.png">
                           <h2>        Swasthi Earthing Solutions</h2>
                        </div>
                        <div class="info-lable"><span>Name: </span> Neeraj Kumar</div>
                        <div class="info-lable"><span>Address:</span> A-53, Shop No. 2, GT Karnal Road, Azadpur Ind. Area, Near Hans Cinema, Delhi, 110033.</div>
                        <div class="info-lable">          <span>Contact Number:</span> 080 4805 4263</div>
                        <div class="info-lable">     <span>Email Id:</span> neerajkumarses91@gmail.com </div>
                        <div class="info-lable">            <span>Website:</span> www.earthingelectrodes.org        </div>
                        <a href="" class="new-button">Enquiry Now</a>                                        	    
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="dott-pattern"></div>
      </div>
      <div class="abt-comp pt-50 pb-50" id="s2">
         <div class="container">
            <h2>About  Swasthi Earthing Solutions</h2>
            <p>Swasthi Earthing Solutions established in 2007, Swasthi is a famous manufacturer and trader specialist in GI, Copper, Bounding, Earthing Electrodes, Rods, Pit Cover, etc. Immensely acclaimed in the industry owing to their preciseness, these are presented by us in standard and modified forms to our clients. To add, only optimum class material is utilized in their production. Under the administration of Mr. Neeraj Kumar, we have garnered a reputed position in this highly competitive industry.</p>
            <h3>Here is the list of Swasthi Earthing Solutions Services</h3>
            <ul>
               <li><i class="fas fa-angle-left"></i> Lorem Ipsum is simply dummy text of the printing</li>
               <li><i class="fas fa-angle-left"></i> Lorem Ipsum has been the industry's standard</li>
               <li><i class="fas fa-angle-left"></i> When an unknown printer took a galley of type</li>
               <li><i class="fas fa-angle-left"></i> Type and scrambled it to make a type specimen book</li>
            </ul>
<!--
			<div class="cost">
				<div class="cost-grid">
					<div class="cost-left" style="background: #ffe9fb;">
    <div>
						<h2>1 Year Subscription</h2>
						<h3>MRP: Rs 30,000</h3>
					</div></div>
					<div class="cost-left" style=" background: #e0f5ea;">
    <div>
						<h2>3 Year Subscription</h2>
						<h3>MRP: Rs 30,000 + 35,000 = 65,000 MAP: Rs 55,000</h3>
					</div></div>
				</div>
			</div>
-->
         </div>
      </div>
      <section id="s3" class="section featured-product wow fadeInUp   pt-50 pb-50 mb-50 hot-seller animated profile-product" style="background: rgb(244, 241, 249); visibility: visible; animation-name: fadeInUp;">
         <div class="container">
            <h2 class="section-title1	">Our   <span>Products/Services</span></h2>
            <div class="products-gridss">
               <div class="products">
                  <div class="product">
                     <div class="product-image">
                        <div class="image"> <a href="product-details.php"><img src="images/product/5d7f9fae06ac0.jpg" alt=""  ></a> </div>
                     </div>
                     <div class="product-info text-left cate-headi">
                        <h3 class="name"><a href="product-details.php">Domestic Relocation Service</a></h3>
                     </div>
                  </div>
               </div>
               <div class="products">
                  <div class="product">
                     <div class="product-image">
                        <div class="image"> <a href="product-details.php"><img src="images/product/5e8482ad3426f.jpg" alt=""  ></a> </div>
                     </div>
                     <div class="product-info text-left cate-headi">
                        <h3 class="name"><a href="product-details.php">Conveyor Components</a></h3>
                     </div>
                  </div>
               </div>
               <div class="products">
                  <div class="product">
                     <div class="product-image">
                        <div class="image"> <a href="product-details.php"><img src="images/product/5e848312ab255.jpg" alt=""  ></a> </div>
                     </div>
                     <div class="product-info text-left cate-headi">
                        <h3 class="name"><a href="product-details.php">Industrial Plants &amp; Machines</a></h3>
                     </div>
                  </div>
               </div>
               <div class="products">
                  <div class="product">
                     <div class="product-image">
                        <div class="image"> <a href="product-details.php"><img src="images/product/5e8484bf8257b.jpg" alt=""  ></a> </div>
                     </div>
                     <div class="product-info text-left cate-headi">
                        <h3 class="name"><a href="product-details.php">Plastic Scrap</a></h3>
                     </div>
                  </div>
               </div>
               <div class="products">
                  <div class="product">
                     <div class="product-image">
                        <div class="image"> <a href="product-details.php"><img src="images/product/5e84853dd2311.jpg" alt=""  ></a> </div>
                     </div>
                     <div class="product-info text-left cate-headi">
                        <h3 class="name"><a href="product-details.php">Plastic Scrap</a></h3>
                     </div>
                  </div>
               </div>
               <div class="products">
                  <div class="product">
                     <div class="product-image">
                        <div class="image"> <a href="product-details.php"><img src="images/product/5e8485d242372.jpg" alt=""  ></a> </div>
                     </div>
                     <div class="product-info text-left cate-headi">
                        <h3 class="name"><a href="product-details.php">Plastic Scrap</a></h3>
                     </div>
                  </div>
               </div>
               <div class="products">
                  <div class="product">
                     <div class="product-image">
                        <div class="image"> <a href="product-details.php"><img src="images/product/5e8486f83a4ae.jpg" alt=""  ></a> </div>
                     </div>
                     <div class="product-info text-left cate-headi">
                        <h3 class="name"><a href="product-details.php">Plastic Scrap</a></h3>
                     </div>
                  </div>
               </div>
               <div class="products">
                  <div class="product">
                     <div class="product-image">
                        <div class="image"> <a href="product-details.php"><img src="images/product/5e8487505a161.jpg" alt=""  ></a> </div>
                     </div>
                     <div class="product-info text-left cate-headi">
                        <h3 class="name"><a href="product-details.php">Plastic Scrap</a></h3>
                     </div>
                  </div>
               </div>
               <div class="products">
                  <div class="product">
                     <div class="product-image">
                        <div class="image"> <a href="product-details.php"><img src="images/product/5e8487a4c3856.jpg" alt=""  ></a> </div>
                     </div>
                     <div class="product-info text-left cate-headi">
                        <h3 class="name"><a href="product-details.php">Plastic Scrap</a></h3>
                     </div>
                  </div>
               </div>
               <div class="products">
                  <div class="product">
                     <div class="product-image">
                        <div class="image"> <a href="product-details.php"><img src="images/product/5e8487f665a03.png" alt=""  ></a> </div>
                     </div>
                     <div class="product-info text-left cate-headi">
                        <h3 class="name"><a href="product-details.php">Plastic Scrap</a></h3>
                     </div>
                  </div>
               </div>
               <div class="products">
                  <div class="product">
                     <div class="product-image">
                        <div class="image"> <a href="product-details.php"><img src="images/product/5e848859a3f2f.jpg" alt=""  ></a> </div>
                     </div>
                     <div class="product-info text-left cate-headi">
                        <h3 class="name"><a href="product-details.php">Plastic Scrap</a></h3>
                     </div>
                  </div>
               </div>
               <div class="products">
                  <div class="product">
                     <div class="product-image">
                        <div class="image"> <a href="product-details.php"><img src="images/product/5e8488cad9700.jpg" alt=""  ></a> </div>
                     </div>
                     <div class="product-info text-left cate-headi">
                        <h3 class="name"><a href="product-details.php">Plastic Scrap</a></h3>
                     </div>
                  </div>
               </div>
            </div>
            <!-- /.home-owl-carousel --> 		  
         </div>
      </section>
      <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.14/css/lightgallery.css'>
      <div class="cont " id="s4">
         <div class="container">
            <h2 class="section-title1	">Our   <span>Gallery</span></h2>
         </div>
         <div class="demo-gallery">
            <ul id="lightgallery">
               <li data-responsive="https://images.pexels.com/photos/163726/belgium-antwerp-shipping-container-163726.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260 375, https://images.pexels.com/photos/163726/belgium-antwerp-shipping-container-163726.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260 480, https://images.pexels.com/photos/163726/belgium-antwerp-shipping-container-163726.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260 800" 	  data-src="https://images.pexels.com/photos/163726/belgium-antwerp-shipping-container-163726.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260">        <a href="">          <img class="img-responsive" src="https://images.pexels.com/photos/163726/belgium-antwerp-shipping-container-163726.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260">                  </a>      </li>
               <li data-responsive="https://images.pexels.com/photos/1267338/pexels-photo-1267338.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260 375, https://images.pexels.com/photos/1267338/pexels-photo-1267338.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260 480, https://images.pexels.com/photos/1267338/pexels-photo-1267338.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260 800" data-src="https://images.pexels.com/photos/1267338/pexels-photo-1267338.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260">        <a href="">          <img class="img-responsive" src="https://images.pexels.com/photos/1267338/pexels-photo-1267338.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260">                  </a>      </li>
               <li data-responsive="https://www.b2bstreets.com/development/images/destination/5d6cdbe7122be.jpg 375, https://www.b2bstreets.com/development/images/destination/5d6cdbe7122be.jpg 480, https://www.b2bstreets.com/development/images/destination/5d6cdbe7122be.jpg 800" data-src="https://www.b2bstreets.com/development/images/destination/5d6cdbe7122be.jpg"      >        <a href="">          <img class="img-responsive" src="https://www.b2bstreets.com/development/images/destination/5d6cdbe7122be.jpg">                   </a>      </li>
               <li data-responsive="https://images.pexels.com/photos/4481531/pexels-photo-4481531.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260 375, https://images.pexels.com/photos/4481531/pexels-photo-4481531.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260 480, https://images.pexels.com/photos/4481531/pexels-photo-4481531.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260 800" data-src="https://images.pexels.com/photos/4481531/pexels-photo-4481531.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"      >        <a href="">          <img class="img-responsive" src="https://images.pexels.com/photos/4481531/pexels-photo-4481531.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260">                  </a>      </li>
               <li data-responsive="https://www.b2bstreets.com/development/images/destination/5d6cdb73a1e2c.jpg 375, https://www.b2bstreets.com/development/images/destination/5d6cdb73a1e2c.jpg 480, https://www.b2bstreets.com/development/images/destination/5d6cdb73a1e2c.jpg 800" data-src="https://www.b2bstreets.com/development/images/destination/5d6cdb73a1e2c.jpg"      >        <a href="">          <img class="img-responsive" src="https://www.b2bstreets.com/development/images/destination/5d6cdb73a1e2c.jpg">                  </a>      </li>
               <li data-responsive="https://images.pexels.com/photos/1267329/pexels-photo-1267329.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260 375, https://images.pexels.com/photos/1267329/pexels-photo-1267329.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260 480, https://images.pexels.com/photos/1267329/pexels-photo-1267329.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260 800" data-src="https://images.pexels.com/photos/1267329/pexels-photo-1267329.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"      >        <a href="">          <img class="img-responsive" src="https://images.pexels.com/photos/1267329/pexels-photo-1267329.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260">                  </a>      </li>
               <li data-responsive="https://images.pexels.com/photos/2547972/pexels-photo-2547972.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260 375, https://images.pexels.com/photos/2547972/pexels-photo-2547972.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260 480, https://images.pexels.com/photos/2547972/pexels-photo-2547972.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260 800" data-src="https://images.pexels.com/photos/2547972/pexels-photo-2547972.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"      >        <a href="">          <img class="img-responsive" src="https://images.pexels.com/photos/2547972/pexels-photo-2547972.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260">                  </a>      </li>
               <li data-responsive="https://images.pexels.com/photos/163792/model-planes-airplanes-miniatur-wunderland-hamburg-163792.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260 375, https://images.pexels.com/photos/163792/model-planes-airplanes-miniatur-wunderland-hamburg-163792.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260 480, https://images.pexels.com/photos/163792/model-planes-airplanes-miniatur-wunderland-hamburg-163792.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260 800" data-src="https://images.pexels.com/photos/163792/model-planes-airplanes-miniatur-wunderland-hamburg-163792.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"      >        <a href="">          <img class="img-responsive" src="https://images.pexels.com/photos/163792/model-planes-airplanes-miniatur-wunderland-hamburg-163792.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260">                  </a>      </li>
               <li data-responsive="https://images.pexels.com/photos/163726/belgium-antwerp-shipping-container-163726.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260 375, https://images.pexels.com/photos/163726/belgium-antwerp-shipping-container-163726.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260 480, https://images.pexels.com/photos/163726/belgium-antwerp-shipping-container-163726.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260 800" 	  data-src="https://images.pexels.com/photos/163726/belgium-antwerp-shipping-container-163726.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260">        <a href="">          <img class="img-responsive" src="https://images.pexels.com/photos/163726/belgium-antwerp-shipping-container-163726.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260">                  </a>      </li>
               <li data-responsive="https://images.pexels.com/photos/1267338/pexels-photo-1267338.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260 375, https://images.pexels.com/photos/1267338/pexels-photo-1267338.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260 480, https://images.pexels.com/photos/1267338/pexels-photo-1267338.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260 800" data-src="https://images.pexels.com/photos/1267338/pexels-photo-1267338.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260">        <a href="">          <img class="img-responsive" src="https://images.pexels.com/photos/1267338/pexels-photo-1267338.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260">                  </a>      </li>
            </ul>
         </div>
      </div>
<!--
      <div class="pricing" id="s6">
	   <div class="container">
            <h2 class="section-title1	">Bronze   <span>Package</span></h2>
         </div>
	<div class="pricing-grid container">
		<div class="pricing-content">
			 
			<div class="pricing-list-content">
				
				<div class="point-offer">
					<h2>Features</h2>
					<ul>
						<li><i class="fas fa-check"></i>Sub Domain</li>
						<li><i class="fas fa-check"></i>Standard fix  Template</li>
						<li><i class="fas fa-check"></i>Up to 50 products</li>
						<li><i class="fas fa-check"></i>Video Bio Presentation</li>
						<li><i class="fas fa-check"></i>Published on MSMENEWS.NET</li>
						
					</ul>
					
				</div>
				
			</div>
		</div>
		<div class="pricing-content">
			 
			<div class="pricing-list-content">
				
				<div class="point-offer">
					<h2>Advantages</h2>
					<ul>
						<li><i class="fas fa-check"></i> Ectreme Score</li>
						<li><i class="fas fa-check"></i> Scores for mortgages, auto loan</li>
						<li><i class="fas fa-check"></i> Credit Reports</li>
						<li><i class="fas fa-check"></i> Score and credit reporting</li>
						<li><i class="fas fa-check"></i> $1 million identify theft insurance</li>
						<li><i class="fas fa-check"></i> 24/7 identify restoration</li>
					</ul>
					
				</div>
				
			</div>
		</div>
		<div class="pricing-content">
			 
			<div class="pricing-list-content">
				
				<div class="point-offer">
					<h2>Benefits</h2>
					<ul>
						<li><i class="fas fa-check"></i> Ectreme Score</li>
						<li><i class="fas fa-check"></i> Scores for mortgages, auto loan</li>
						<li><i class="fas fa-check"></i> Credit Reports</li>
						<li><i class="fas fa-check"></i> Score and credit reporting</li>
						<li><i class="fas fa-check"></i> $1 million identify theft insurance</li>
						<li><i class="fas fa-check"></i> 24/7 identify restoration</li>
					</ul>
					
				</div>
				
			</div>
		</div>
		
	</div>
</div>
-->
	  <div class="profile-right-fix">
	<a href=""><img src="assets/images/whatsapp.svg"></a>
    <a href=""><img src="assets/images/phone.svg"></a>
</div>
	  
	  
	  
	  
	  <script>$(document).ready(() => {  $("#lightgallery").lightGallery({    pager: true  });});</script><script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script><script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script><script src='https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js'></script><script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js'></script><script src='https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.14/js/lightgallery-all.min.js'></script><script  src="./script.js"></script> 
      <!-- start-smoth-scrolling -->
      <!-- js -->
      <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
      <!-- //js -->
      <script>
         $('div#groupinfo .myImg').click(function(){
           //  alert($("img", this).html());
         //	 alert($(this).find(".myImgx").attr("src"));
         //});
         
          /* $(".myImg").click(function() {
           var img = $(this).attr("src");*/
           var img = $(this).find(".myImgx").attr("src")
           var pname = $(this).find(".pnuname").text();
           	var pdes = $(this).find(".prodes").text();
         var modal = document.getElementById('mypopupx');
         var modalImg = document.getElementById("imgf");
             modal.style.display = "block";
             modalImg.src = img;
         	$("#namef").html(pname);
         	$("#pdes").html(pdes);
         
         });
           
         
           function closethis(){
              document.getElementById("mypopupx").style.display = "none";
              //$('.modal2').style.display="none";
           }
         
           
      </script>
     
       
        
       
       
        <section class="pt-50 pb-50  " style="background-color:#f1f3f6;">
               <div class="container">
                  <h2 class="section-title1	"><span>Our </span> Network</h2>
                  <div class="logo-container">
                     <img src="images/ibem.jpg">
                     <img src="images/msme.jpg">
                     <img src="images/wmit.jpg">
                     <img src="images/k12.jpg">
                     <img src="images/the-film-framer.jpg">
                    
                     <img src="images/wmtc.jpg">
                     <img src="images/wmstaffing.jpg"> 
					 <img src="images/gng.jpg">
                  </div>
               </div>
            </section>
       <div class="sidebar-widget  wow fadeInUp outer-top-vs testi pt-50">
            <div class="container ">
               <h2 class="section-title1	">What <span>Clients says </span> about <span> B2B Streets</span></h2>
               <div id="advertisement" class="advertisement">
                  <div id="myCarousel2" class="carousel slide" data-ride="carousel">
                     <div class="carousel-inner">
                         <div class="item active">
                              <img src="images/testi/5d9ac78288623.jpg" alt="..." style="width:100px;height:100px;margin: 10px auto;clear: both;display: block;">
                              <div class="carousel-caption" style="padding:10px;right: 0;left: 0;bottom: 30px;clear: both;margin-top: 50px;position: relative;text-align: center;">
                                <h3>Mr Prashant Kansal - Indus B2C Global Pvt.Ltd. (LeModish)</h3>
                                <p>On behalf of the entire family of Indus B2C Global Pvt.Ltd. (Brand:LeModish Hair Extensions), I would like to thank you for all your efforts towards the online marketing of the product line. Although there is still a lot of ground to be covered, but by assessing the outputs, no matter how little they are as of now, we are confident that we will reach there.</p><p><br />Keep up the good work! Looking forward to a long term engagement with you.</p>
                              </div>
                            </div> <div class="item">
                              <img src="images/testi/5d7758d5bd4dd.jpg" alt="..." style="width:100px;height:100px;margin: 10px auto;clear: both;display: block;">
                              <div class="carousel-caption" style="padding:10px;right: 0;left: 0;bottom: 30px;clear: both;margin-top: 50px;position: relative;text-align: center;">
                                <h3>Mr. Pankaj Malhotra  - UFC International (Leminex)</h3>
                                <p>Services of B2Bstreets gets 5 star rating&nbsp;when it comes to professionalism... Keep it up guys :-)</p>
                              </div>
                            </div> <div class="item">
                              <img src="images/testi/5d77553f1a428.jpg" alt="..." style="width:100px;height:100px;margin: 10px auto;clear: both;display: block;">
                              <div class="carousel-caption" style="padding:10px;right: 0;left: 0;bottom: 30px;clear: both;margin-top: 50px;position: relative;text-align: center;">
                                <h3>Mr Neeraj Kumar - Swasthi Earthing Solutions</h3>
                                <p>It&#39;s a plesure to get associate with this young &amp; creative organization. i am satisfied with their work &amp; commitment towards client&#39;s service. I also appreciate their Unique Visual Directory &amp; Viral Marketing concept.</p>
                              </div>
                            </div>         
                     </div>
                  </div>
                  <!-- /.owl-carousel --> 
               </div>
            </div>
         </div>
       
       
         
            <section class="section featured-product wow fadeInUp   pt-50 pb-50 mb-50" >
               <div class="container">
                  <h2 class="section-title1	"><span>Certification </span> </h2>
                  <div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
                       <div class="item item-carousel">
                         <div class="cretificate">
                            <img src="https://marketplace.canva.com/EAEfoC4df54/1/0/1600w/canva-gold-and-black-ribbon-achievement-certificate-zRVWwyljBI4.jpg">    
                           </div>
                        </div>
                      <div class="item item-carousel">
                         <div class="cretificate">
                            <img src="https://lh3.googleusercontent.com/proxy/PvaYDcidj3QEdIpZJE8wr9x0vkzlR24q05r2zN8hmKlX9cm2WPKpoQLoOV252WcINvzgNEZILRNtSw2F1tXg4YpCvSX5UG_XKnwYO-5OTWiMkRXe9dJY7nYlC1ZtaX0">    
                           </div>
                        </div>
                      <div class="item item-carousel">
                         <div class="cretificate">
                            <img src="https://www.a-law.eu/sites/default/files/styles/large/public/certificatedbpatricialeers-1.jpg?itok=fC9dV2-7">    
                           </div>
                        </div>
                      <div class="item item-carousel">
                         <div class="cretificate">
                            <img src="https://site-650878.mozfiles.com/files/650878/medium/Certificate.jpg?1592403661">    
                           </div>
                        </div>
                      <div class="item item-carousel">
                         <div class="cretificate">
                            <img src="https://2ed.in/wp-content/uploads/2020/04/2ed.in-corona-certificate-world-wide-1024x732.jpg">    
                           </div>
                        </div>
                      <div class="item item-carousel">
                         <div class="cretificate">
                            <img src="https://thumbs.dreamstime.com/b/certificate-appreciation-template-certificate-appreciation-template-geometrical-simple-shapes-134718760.jpg">    
                           </div>
                        </div>
                      <div class="item item-carousel">
                         <div class="cretificate">
                            <img src="https://i1.rgstatic.net/publication/344887187_Certificate_of_Appreciation_as_keynote_speakerEvolution_of_Research_Analysis/links/5f96ddfb458515b7cf9f09b1/largepreview.png">    
                           </div>
                        </div>
                      
                   </div>
                  <center> <a href="category.php" class="new-button">View All</a></center>
                  <!-- /.home-owl-carousel --> 
               </div>
            </section>
        
       <section class="bg-back" style="background: #f1f3f6;">
               <div class="container pt-50 pb-50">
                  <div class="col-xs-12 col-sm-12 col-md-12 homebanner-holder  ">
                     <section class="section featured-product wow fadeInUp">
                        <h2 class="section-title1	">Client  <span>Interview</span></h2>
                        <div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
                           
                              		  <div class="grid-owl">
                              		  <div class="item item-carousel">
                                            <div class="products">
                                              <div class="product">
                                                <div class="product-image"><video style="width:100%;height: 143px;" controls=""><source src="images/video/5d9ed5834e625.mp4#t=1.6" type="video/mp4">Your browser does
                              not support the video tag.</video> <div class="logo-part">
                              <img src="images/logo-par.png">
                              </div>   </div>
                                                <div class="product-info text-left">
                                                  <h3 class="name"><a href="swasthi-earthing-solutions">Swasthi Earthing Solutions</a></h3>
                              					<p>Swasthi Earthing Solutions established in 2007, Swasthi is a famous </p>
                              					<a href="" class="read-more">Read more</a>
                                                 </div>
                                             </div>
                                               </div>
                                            </div>
                                            </div>
                              		  <div class="grid-owl">
                              		  <div class="item item-carousel">
                                            <div class="products">
                                              <div class="product">
                                                <div class="product-image"><video style="width:100%;height: 143px;" controls=""><source src="images/video/5d6a622014b5a.mp4#t=1.6" type="video/mp4">Your browser does
                              not support the video tag.</video> <div class="logo-part">
                              <img src="images/logo-par.png">
                              </div>   </div>
                                                <div class="product-info text-left">
                                                  <h3 class="name"><a href="royal-equipments-co">Royal Equipments Co.</a></h3>
                              					<p>Swasthi Earthing Solutions established in 2007, Swasthi is a famous </p>
                              					<a href="" class="read-more">Read more</a>
                                                 </div>
                                             </div>
                                               </div>
                                            </div>
                                            </div>
                              		  <div class="grid-owl">
                              		  <div class="item item-carousel">
                                            <div class="products">
                                              <div class="product">
                                                <div class="product-image"><video style="width:100%;height: 143px;" controls=""><source src="images/video/5d6cb1f488762.mp4#t=1.6" type="video/mp4">Your browser does
                              not support the video tag.</video> <div class="logo-part">
                              <img src="images/logo-par.png">
                              </div>   </div>
                                                <div class="product-info text-left">
                                                  <h3 class="name"><a href="foodax-engineering-works">Foodax Engineering Works </a></h3>
                              					<p>Swasthi Earthing Solutions established in 2007, Swasthi is a famous </p>
                              					<a href="" class="read-more">Read more</a>
                                                 </div>
                                             </div>
                                               </div>
                                            </div>
                                            </div>
                              		  <div class="grid-owl">
                              		  <div class="item item-carousel">
                                            <div class="products">
                                              <div class="product">
                                                <div class="product-image"><video style="width:100%;height: 143px;" controls=""><source src="images/video/5d931a07cffe0.mp4#t=1.6" type="video/mp4">Your browser does
                              not support the video tag.</video> <div class="logo-part">
                              <img src="images/logo-par.png">
                              </div>   </div>
                                                <div class="product-info text-left">
                                                  <h3 class="name"><a href="ufc-international">UFC International</a></h3>
                              					<p>Swasthi Earthing Solutions established in 2007, Swasthi is a famous </p>
                              					<a href="" class="read-more">Read more</a>
                                                 </div>
                                             </div>
                                               </div>
                                            </div>
                                            </div>                        </div>
                        <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
                        <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
                        <!--------------------Row-2 done------------------>
                        <center> <a href="detail.php" class="new-button">View All</a></center>
                        <!-- /.home-owl-carousel --> 
                     </section>
                     <!-- /.scroll-tabs --> 
                  </div>
               </div>
            </section>
       
       
       <div class="container mtb-5 ">
            <div class="row">
                
                <div class="col-lg-12 text-center blog">
                      <h2 class="section-title1	">    <span>Blog</span></h2>
                    
                </div>
                <div class="col-lg-4">
                    <div class="blog-box">
                        <img src="https://brunn.qodeinteractive.com/wp-content/uploads/2018/09/blog-single-img-3.jpg">
                        <div class="blog-text">
                            <h4><span><i class="far fa-calendar-alt"></i> 01 Aug 2021</span>  <span><i class="fas fa-user"></i> By Admin</span></h4>
                            <h5>Heading</h5>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            <h6><a href="">Read More <i class="fas fa-long-arrow-alt-right"></i></a></h6>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="blog-box">
                        <img src="https://brunn.qodeinteractive.com/wp-content/uploads/2018/09/blog-single-img-2.jpg">
                        <div class="blog-text">
                            <h4><span><i class="far fa-calendar-alt"></i> 01 Aug 2021</span>  <span><i class="fas fa-user"></i> By Admin</span></h4>
                            <h5>Heading</h5>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            <h6><a href="">Read More <i class="fas fa-long-arrow-alt-right"></i></a></h6>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="blog-box">
                        <img src="https://brunn.qodeinteractive.com/wp-content/uploads/2018/09/blog-single-img-1.jpg">
                        <div class="blog-text">
                            <h4><span><i class="far fa-calendar-alt"></i> 01 Aug 2021</span> <span><i class="fas fa-user"></i> By Admin</span></h4>
                            <h5>Heading</h5>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            <h6><a href="">Read More <i class="fas fa-long-arrow-alt-right"></i></a></h6>
                        </div>
                    </div>
                </div>
           </div>
       </div>
       
       
       
       
       
       
      <div     style="    background: #f7eeeba3;" class="pt-50 pb-50 mb-50">
      <div class="container profile-form margin-top-0 margin-bottom-0" id="s5">
         <div id="popup" class="overlay">
            <h2 class="section-title1	">    <span>Enquiry</span> Now</h2>
            <div class="popup">
               <div class="dott-pattern  form-pat"></div>
               <div class="">
                  <form action="#" id="conform2">
                     <label class="info-title" for="exampleInputName">Your Name <span>*</span></label>
                     <input type="text" id="namen" name="namen" class="form-control unicase-form-control text-input cal" placeholder="">
                     <label class="info-title" for="exampleInputEmail1">Email Address <span>*</span></label>
                     <input class="form-control unicase-form-control text-input cal" type="email" id="emailxn" name="emailxn"  placeholder="">
                     <input class="form-control unicase-form-control text-input" type="hidden" id="emailexn" name="emailexn"  value="">
                     <label class="info-title" for="exampleInputEmail1">Contact <span>*</span></label>
                     <input class="form-control unicase-form-control text-input cal" type="number" id="phonen" name="phonen"  placeholder="">
                     <label class="info-title" for="exampleInputTitle">Location<span>*</span></label>
                     <input  type="text" id="contn" name="contn" class="form-control unicase-form-control text-input cal" placeholder="">
                     <label class="info-title" for="exampleInputComments">Your Requirements <span>*</span></label>
                     <textarea class="form-control unicase-form-control" name="msgn" id="msgn" ></textarea>
                     <input type="button" id="btnsn" name="btnsn" onclick="send_normal()" value="Submit" class="new-button">
                  </form>
                  <p id="trimbn" style="color:red;"></p>
               </div>
            </div>
         </div>
      </div>
      </div>
      <div class="foot-cate container">
         <div class="foot-cate-list">
            <h2>Categories:</h2>
            <ul>
               <li><a href="">Siren Manufacturer</a></li>
               <li><a href="">Hydraulic Press</a></li>
               <li><a href="">Dona Making Machine</a></li>
               <li><a href="">Stainless Steel Scrubber Making Machines </a></li>
               <li><a href="">Lubricant Oil</a></li>
               <li><a href="">Shaker Bottle and Flask</a></li>
               <li><a href="">Corporate Gifts</a></li>
               <li><a href="">Pet Blowing machine</a></li>
               <li><a href="">Paint Chemicals</a></li>
               <li><a href="">Footwear Manufacturer</a></li>
               <li><a href="">Elevator Manufacture</a></li>
               <li><a href="">Puff Panel</a></li>
               <li><a href="">XLPE Cables</a></li>
               <li><a href="">Bee Honey</a></li>
               <li><a href="">Organic Food</a></li>
               <li><a href="">Coding Machine</a></li>
               <li><a href="">Aluminum Foil Container</a></li>
               <li><a href="">PVC Sheet</a></li>
               <li><a href="">Industrial Belts</a></li>
               <li><a href="">Kitchen Appliances</a></li>
               <li><a href="">Toys Manufacturer </a></li>
               <li><a href="">Electrical Parts</a></li>
               <li><a href="">E Rickshaw</a></li>
               <li><a href="">E Vehicles</a></li>
               <li><a href="">Earthing Electrodes</a></li>
               <li><a href="">Food Processing Plants</a></li>
               <li><a href="">Inflatable and sky balloons</a></li>
               <li><a href="">PTFE Wires & Cables</a></li>
               <li><a href="">Ac & Dc Drive, Dhoop & Agarbatti</a></li>
               <li><a href="">Human Hairs Extensions</a></li>
               <li><a href="">Laser Cutting Service</a></li>
               <li><a href="">Bike Parts</a></li>
               <li><a href="">Laser Cutting Machine</a></li>
               <li><a href="">Packaging Tapes</a></li>
               <li><a href="">Air ventilation and Cooling system</a></li>
               <li><a href="">seasoning & spices</a></li>
               <li><a href="">Commercial Kitchen & Hotel Equipment</a></li>
               <li><a href="">Packaging machine</a></li>
               <li><a href="">Chemical Supplies</a></li>
            </ul>
         </div>
      </div>
     
      <script>
         function send_normal(){
         	
         var reg = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
         var reg2 =/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
         	
         	var name = $('#namen').val(); //alert(name);
         	var contact = $('#contn').val(); //alert(contact);
         	var phone = $('#phonen').val(); //alert(phone);
             var emailx = $('#emailxn').val(); //alert(email);
         	var msg = encodeURIComponent($('#msgn').val()); //alert(msg);
         	var emailex = $('#emailexn').val(); //alert(emailex);
         	var sub = "an Enquiry";
             if(name.trim() == '' ){
         		$('#trimbn').show();
         		$('#trimbn').html('Please enter Name.');
         		$('#trimbn').addClass('toolshow').removeClass('toolhide');
         		 $('#namen').focus();
         		return false;
         	}else if(emailx.trim() == '' ){
         		$('#trimbn').show();
         		$('#trimbn').html('Please enter email.');
         		$('#trimbn').addClass('toolshow').removeClass('toolhide');
         		 $('#emailxn').focus();
         		return false;
         	}else if(emailx.trim() != '' && !reg.test(emailx)){
         		$('#trimbn').show();
         		$('#trimbn').html('Please enter valid email.');
         		$('#trimbn').addClass('toolshow').removeClass('toolhide');
         		$('#emailxn').focus();
         		return false;
         }else if(phone.trim() == '' ){
         		$('#trimbn').show();
         		$('#trimbn').html('Please enter contact.');
         		$('#trimbn').addClass('toolshow').removeClass('toolhide');
         		 $('#phonen').focus();
         		return false;
           }else if(phone.trim() != '' && !reg2.test(phone)){
         	  $('#trimbn').show();
         	  $('#trimbn').html('Please enter valid contact.');
         		$('#trimbn').addClass('toolshow').removeClass('toolhide');
         		 $('#phonen').focus();
         		return false;
         	}else if(contact.trim() == '' ){
         		$('#trimbn').show();
         		$('#trimbn').html('Please enter Title.');
         		$('#trimbn').addClass('toolshow').removeClass('toolhide');
         		 $('#contn').focus();
         		return false;
          }else if(msg.trim() == '' ){
         	$('#trimbn').show();
         	$('#trimbn').html('Please enter message.');
         		$('#trimbn').addClass('toolshow').removeClass('toolhide');
         		 $('#msgn').focus();
         		return false;		
         	}
         	else{
                 $.ajax({
                     type:'POST',
                     url:'send_mail_normal.php',
         			 data:'sendFrmSubmit=1&name='+name+'&emailx='+emailx+'&contact='+contact+'&msg='+msg+'&sub='+sub+'&emailex='+emailex+'&vid='+vid+'&phone='+phone,
                     beforeSend: function () {
                           $('#btnsn').html("Sending Mail");
                         $('#btnsn').attr("disabled","disabled");
                         },
                     success:function(msg){
                         if(msg == 'ok'){
         					$('#trimbn').show();
                             $('#trimbn').html('<span style="color:green;">Thank you , We will contact you soon.</span>');
         					 $('#conform2 textarea').val('');
                        }else{
         				   $('#trimbn').show();
                             $('#trimbn').html('<span style="color:red;">Some problem occurred, please try again.</span>');
                         }
                           $('#btnsn').html("Submit");
                         $('#btnsn').removeAttr("disabled");
         				 $('#conform2 .cal').val('');
         			    }
                 });
             }
         }
      </script>	
      <script>
         $(function() {
           
           // contact form animations
           $('#contact').click(function() {
             $('#contactForm').fadeToggle();
           })
           $(document).mouseup(function (e) {
             var container = $("#contactForm");
         
             if (!container.is(e.target) // if the target of the click isn't the container...
                 && container.has(e.target).length === 0) // ... nor a descendant of the container
             {
                 container.fadeOut();
             }
           });
           
         });
      </script>
      <script>
         $(document).ready(function(){
         //MODALS
         	//OPEN
         		$('body').on('click','.modal_open',function(e) {
         			e.preventDefault();
         			$('.modal_overlay').addClass('active');
         			$('.modal').addClass('active');
         		});
         	//CLOSE
         		$('body').on('click','.modal_overlay',function(e) {
         			e.preventDefault();
         			$('.modal_overlay').removeClass('active');
         			$('.modal').removeClass('active');
         		});
         		$('body').on('click','.modal_close',function(e) {
         			e.preventDefault();
         			$('.modal_overlay').removeClass('active');
         			$('.modal').removeClass('active');
         		});
         	
         });
      </script>
      <script>
         // Get the modal
         var modal = document.getElementById("myModal");
         
         // Get the button that opens the modal
         var btn = document.getElementById("myBtn");
         
         // Get the <span> element that closes the modal
         var span = document.getElementsByClassName("close")[0];
         
         // When the user clicks the button, open the modal 
         btn.onclick = function() {
           modal.style.display = "block";
         }
         
         // When the user clicks on <span> (x), close the modal
         span.onclick = function() {
           modal.style.display = "none";
         }
         
         // When the user clicks anywhere outside of the modal, close it
         window.onclick = function(event) {
           if (event.target == modal) {
             modal.style.display = "none";
           }
         }
      </script>
      <!-- JavaScripts placed at the end of the document so the pages load faster -->
      <script src="assets/js/bootstrap.min.js"></script>
      <script src="assets/js/bootstrap-hover-dropdown.min.js"></script>
      <script src="assets/js/owl.carousel.min.js"></script>
      <script src="assets/js/echo.min.js"></script>
      <script src="assets/js/jquery.easing-1.3.min.js"></script>
      <script src="assets/js/bootstrap-slider.min.js"></script>
      <script src="assets/js/jquery.rateit.min.js"></script>
      <script type="text/javascript" src="assets/js/lightbox.min.js"></script>
      <script src="assets/js/bootstrap-select.min.js"></script>
      <script src="assets/js/wow.min.js"></script>
      <script src="assets/js/scripts.js"></script>
   </body>
</html>