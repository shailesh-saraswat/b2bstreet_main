
<div class="Pro-list-g ">
                                            <div class="my-Slide-Container">
                                                @if($product_page->product_images!=null)
                                                @foreach ($product_page->product_images as $image)
                                                <div class="mySlides" style="display: block;">
                                                    <!-- @if (isset($image['product_image'])) -->
                                                    <img src="{{ asset($image['product_image']) }}"
                                                        style="width:100%">
                                                        <!-- @endif -->
                                                </div>
                                                @endforeach
                                                @endif
                                            </div>

                                            <div class="product-list-grid-two">
                                                <h3></h3>
                                                <ul>
                                                
                                                    <li><i class="fas fa-angle-left"></i><span>Quality
                                                            Available:</span>{{ $product_page->quality }}</li>
                                                    <li><i class="fas fa-angle-left"></i><span>Packaging Type:</span>
                                                        {{ $product_page->packaging_type }}</li>
                                                    <li><i class="fas fa-angle-left"></i><span>Storage Tips:</span>
                                                        {{ $product_page->storage_tips }}</li>
                                                    <li><i class="fas fa-angle-left"></i><span>Packaging
                                                            Size:</span>{{ $product_page->packaging }}</li>
                                                           
                                                </ul>
                                            </div>


                                            <!-- <a class="prev" onclick="plusSlides(-1)">❮</a> -->
                                            <!-- <a class="next" onclick="plusSlides(1)">❯</a> -->
                                        </div>