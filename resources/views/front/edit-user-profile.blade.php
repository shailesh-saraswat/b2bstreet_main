@extends('layouts.front')
@section('content')
@include('front.seo-keywords')

      <div class="breadcrumb">
         <div class="container">
            <div class="breadcrumb-inner">
               <ul class="list-inline list-unstyled">
                  <li><a href="{{route('Front.Index')}}">Home</a></li>
                  <li class='active'>Update User Details</li>
               </ul>
            </div>
         </div>
      </div>
        
       <div class="container mtb-5">

          
                <div class="row">
                    <div class="col-lg-3 ">
                       <div class="tabs">
                        <ul>
                            <a href="{{route('Front.UserProfile')}}"><li class="active">User Details</li></a>   
                           <!--  <a href="{{route('Front.Notification')}}"><li>Nautification</li></a>  -->
                            <a href="{{url('/edit-user-profile/'.Auth::user()->id)}}"><li>Update Your Profile</li></a> 
                         <a href="{{route('SupAdmin.Logout')}}"><li>Logout</li></a>  
                        </ul> 
                       </div>
                   </div>

                    <div class="col-lg-9">                   
                    <div class="edit-details">
                    @include('admin.partials.messages')
                    <form method="post" action="{{route('Front.Edit.UserProfile',[Auth::user()->id])}}" enctype="multipart/form-data">
                          @csrf
                    <input type="hidden" name="personal_information" value="personal_information">

                    <div class="col-md-12">
                        <div class="heading">
                            <p>Personal Information</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="text-center form">
                            <div class="row">
                               <div class="small-12 medium-2 large-2 columns">
                                 <div class="circle">
                                  <!-- <img src="{{Auth::user()->user_image}}" alt="User"> -->
                                 </div>
                                 <div class="p-image">
                                   <i class="fa fa-camera upload-button"></i>
                                   @if($user->user_image != null)
                                    <input class="file-upload" type="file" name="user_image" id="user_image" accept="image/*"/>
                                    <img src="{{ asset($user['user_image']) }}" style="width:90px;" />
                                    @else
                                    <img src="{{asset('public/images/1.png')}}" style="width:90px;" />
                                     @endif
                                 </div>
                                   <!-- <p>Profile Picture</p> -->
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="form">
                             <input type="hidden" name="id" value="{{Auth::user()->id}}">
                            <div class="form-group">
                                <label>Name*</label>
                                <input type="text" name="name" id="name" value="{{Auth::user()->name}}">
                            </div>
                            <div class="form-group">
                                <label>Designation*</label>
                                <input type="text" name="designation" id="designation" value="{{ $user['designation'] }}">
                            </div>
                            <div class="form-group">
                                <label>Mobile Number*</label>
                                <input type="text" name="mobile" id="mobile" value="{{Auth::user()->mobile}}">
                            </div>
                            <div class="form-group">
                                <label>Email Id</label>
                                <input type="text" name="email" id="email" value="{{Auth::user()->email}}" >
                            </div>
                            <div class="form-group">
                                <label>Alternate Mobile Number</label>
                                <input type="text" name="alternate_number" id="alternate_number" value="{{ $user['alternate_number'] }}">
                            </div>
                            <div class="form-group">
                                <label>Alternate Email ID</label>
                                <input type="text" name="alternate_email" id="alternate_email" value="{{ $user['alternate_email'] }}">
                            </div>
                            <div class="form-group text-right">
                                <button class="save-btn" type="submit" name="submit" id="submit" value="submit">Save</button>
                            </div>
                        </div>

                    </div>
                </form>
                </div>
           
           
              <div class="edit-details">
                 <form method="post" action="{{route('Front.Edit.UserProfile',[Auth::user()->id])}}" enctype="multipart/form-data">
                          @csrf
                 <input type="hidden" name="address_information" value="address_information">
                    <div class="col-md-12">
                        <div class="heading">
                            <p>Address Information</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form">
                            <div class="form-group">
                                <label>PIN Code</label>
                                <input type="text" name="pin" id="pin">
                            </div>
                            <div class="form-group">
                                <label>State</label>
                                <input type="text" name="state" id="state">
                            </div>
                            <div class="form-group">
                                <label>House No./Block</label>
                                <input type="text" name="house_no" id="house_no">
                            </div>
                            <div class="form-group">
                                <label>Locality</label>
                                <input type="text" name="locality" id="locality">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form">
                            <div class="form-group">
                                <label>City*</label>
                                <input type="text" name="city" id="city">
                            </div>
                            <div class="form-group">
                                <label>Country*</label>
                                <input type="text" name="country" id="country">
                            </div>
                            <div class="form-group">
                                <label>Area/Street</label>
                                <input type="text" name="area" id="area">
                            </div>
                            <div class="form-group">
                                <label>LandMark</label>
                                <input type="text" name="landmark" id="landmark">
                            </div>
                            <div class="form-group text-right">
                                <button class="save-btn" type="submit" name="submit" id="submit" value="submit">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
                </div>


           <div class="edit-details">
             <form method="post" action="{{route('Front.Edit.UserProfile',[Auth::user()->id])}}" enctype="multipart/form-data">
            @csrf
          <input type="hidden" name="company_information" value="company_information">
                    <div class="col-md-12">
                        <div class="heading">
                            <p>Company Information</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form">
                            <div class="form-group">
                                <label>Company Name</label>
                                <input type="text" name="company_name" id="company_name">
                            </div>
                            <div class="form-group">
                                <label>Website</label>
                                <input type="text" name="website" id="website">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form">
                            <div class="form-group">
                                <label>GSTIN</label>
                                <input type="text" name="gstin" id="gstin">
                            </div>
                            <div class="form-group text-right">
                                <button class="save-btn" type="submit" name="submit" id="submit" value="submit">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
                </div>

       <div class="edit-details">
         <form method="post" action="{{route('Front.Edit.UserProfile',[Auth::user()->id])}}" enctype="multipart/form-data">
          @csrf
         <input type="hidden" name="bank_account_details_information" value="bank_account_details_information">
                    <div class="col-md-12">
                        <div class="heading">
                            <p>Bank Account Details</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form">
                            <div class="form-group">
                                <label>IFSC Code</label>
                                <input type="text" name="ifcs_code" id="ifcs_code">
                            </div>
                            <div class="form-group">
                                <label>Bank Name</label>
                                <input type="text" name="bank_name" id="bank_name">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 mb-5">
                        <div class="form">
                            <div class="form-group">
                                <label>Account Number</label>
                                <input type="text" name="account_no" id="account_no">
                            </div>
                            <div class="form-group">
                                <label>Account Type</label>
                                <input type="text" name="account_type" id="account_type">
                            </div>
                            <div class="form-group text-right">
                               <button class="save-btn" type="submit" name="submit" id="submit" value="submit">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
                </div>
            </div>
            </div>
        </div> 
 
   

   
      
     
       
       <script>
        $(document).ready(function() {

    
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.profile-pic').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    

    $(".file-upload").on('change', function(){
        readURL(this);
    });
    
    $(".upload-button").on('click', function() {
       $(".file-upload").click();
    });
});
       </script>
 
  @endsection 