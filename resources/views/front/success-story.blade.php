 @extends('layouts.front')
@section('content')
@include('front.seo-keywords')

 <div class="menu-zindex">
         <div class="breadcrumb">
            <div class="container">
               <div class="breadcrumb-inner">
                  <ul class="list-inline list-unstyled">
                     <li><a href="https://b2bstreets.com">Home</a></li>
                     <li class='active'><a href="{{route('Front.successStory')}}">Success Story</a></li>
                  </ul>
               </div>
               <!-- /.breadcrumb-inner -->
            </div>
            <!-- /.container -->
         </div>
         <!-- /.breadcrumb -->
    
          
<section id="success-banner" style="background-image: url({{ asset('public/images/blog/success-story.jpg') }});background-size: cover; position: relative;height: 400px;">
    <div class="container">
        <div class="row"></div>
    </div>
</section>          
          

<section id="success-story">
    <div class="container">
        @foreach($stories as $story)
        <div class="row align">
            <div class="col-md-8">
                <div class="success-content">
                    <h3>{{$story->title}}</h3>
                    <span><i class="fa fa-history"></i> Posted on {{$story->date}}</span>
                    <hr>
                    <p>{{$story->description}}</p>
                </div>
            </div>
            <div class="col-md-4">
                <img src="{{ asset($story['image']) }}">
            </div>
        </div> 
        @endforeach               
        <hr>        
<!--         <div class="row align">
            <div class="col-md-4">
                <img src="{{ asset('public/images/blog/success.jpg') }}">
            </div>
            <div class="col-md-8">
                <div class="success-content">
                    <h3>Category: B2B Streets Achievers</h3>
                    <span><i class="fa fa-history"></i> Posted on April 19, 2017 by Editor's Desk</span>
                    <hr>
                    <p>B2B Streets is the perfect platform to tap the new age consumers and acquire new customers! – Sorabh Agarwal, Executive Director – Action Construction Equipment Ltd. Introduction Action Construction Equipment Ltd. India’s Largest manufacturers of Mobile Cranes and Tower Cranes…. <br>B2B Streets is the perfect platform to tap the new age consumers and acquire new customers! – Sorabh Agarwal, Executive Director – Action Construction Equipment Ltd. Introduction Action Construction Equipment Ltd. India’s Largest manufacturers of Mobile Cranes and Tower Cranes….</p>
                </div>            
            </div>
            
        </div> -->
    

    </div>
</section>

</div>

@endsection