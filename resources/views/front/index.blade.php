@extends('layouts.front')
@section('content')
@include('front.modal')
@include('front.seo-keywords')
<style>
   a.homePost-request {
   border: none;
   padding: 14.5px 30px;
   border-radius: 5px;
   background: #fff;
   font-size: 15px;
   border: 1px solid #fff;
   transition: .5s all ease;
   display: -webkit-inline-box;
   color: #333;
   }
   .modal-open {
   overflow: unset;
   }
   .tt-dataset {
   height: 25px;
   }
   .tt-menu {
   position: absolute;
   top: 100%;
   left: 0px;
   z-index: 100;
   background-color: rgb(85, 100, 149);
   display: none;
   padding: 5px;
   line-height: 22px;
   }
   .log-s {
   display: flex;
   justify-content: center;
   gap: 10px;
   }
   .log-s a {
   background: #556495;
   padding: 10px 20px;
   color: #fff;
   border-radius: 5px;
   }
.owl-gg {
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    gap: 20px;
}
.owl-gg .products {
    margin-right: 0;
}
</style>
<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"> -->

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Great+Vibes&display=swap" rel="stylesheet">
<section class="banner-part" style="background-image: url({{$banner->banner_image}});">
   <div class="container">
      <div class="banner-content">
         <div class="">
            <div class="ban-inp">
               <form method="post" action="{{ url('searchCategory') }}">
                  @csrf
                  <div class="ban-d-fl">
                     <div class="text-inp" id="dd">
                        <input type="text" name="keyword" id="search" name="search" placeholder="Type here to search" required >
                        <!-- <img src="{{ 'assets/images/voice.svg' }}"> -->
                  <ul id="search-table" class="table table-hover ttt" style="margin-bottom: -1px;">
                  </ul>
               </div>

                    
                     <div class="loc-inp" id="loc">
                        <input type="text" id="loca" placeholder="Type Location">
                        <img src="{{ 'assets/images/pin.svg' }}">
                     </div>
                     <div class="btn-inp">
                        <center>
                           <button>Submit</button>
                        </center>
                        <!--  <input type="submit" name="submit" value="submit"> -->
                     </div>
                  </div>
               </form>
            </div>
            <h1>{{ $banner->title }}</h1>
            <p>{{ $banner->description }}</p>
            <div class="hashtag-banner">
               <h2>Trending Hashtag </h2>
               <ul>
                  <li>#Home Supplies</li>
                  <li>#Industrial Supplies</li>
                  <li>#Industrial Supplies</li>
                  <li>#Medical Equipment</li>
                  <li>#Furniture &amp; Supplies</li>
                  <li>#Medical Equipment</li>
                  <li>#Service Providers</li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</section>

<script type="text/javascript">

   function myFunction() {
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById("search");
    filter = input.value.toUpperCase();
    //alert(filter);
    ul = document.getElementById("search-table");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("a")[0];
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}

$('#search').on('keyup',function(){

   $value=$(this).val();
   $.ajax({
   type : 'get',
   url : '{{URL::to('search1')}}',
   data:{'search':$value},
   success:function(data){

    //  $('#search-table').css("display", "block");
   //$('tbody').html(data);
    $('#search-table').css("display", "block");
     $('#search-table').html(data);
     myFunction();
   }
   
   });
});
</script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script> -->
<script type="text/javascript">
$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>
<!-- ============================================== FEATURED PRODUCTS ============================================== -->
<!-- ============================================== CONTENT ============================================== -->
<section class="bg-back" style="background: #fff;">
   <div class="container pt-50 pb-50">
      <div class="col-xs-12 col-sm-12 col-md-12 homebanner-holder  ">
         <section class="section featured-product wow fadeInUp">
            <h2 class="section-title1">Trending <span>Video Bio</span></h2>
            <div class="owl-gg">
               @foreach ($video_trending as $video)
               <div class="grid-owl">
                  <div class="item item-carousel">
                     <div class="products">
                        <div class="product">
                           <div class="product-image">
                              <video style="width:100%;height: 143px;" controls="">
                                 <source src="{{ asset($video['video']) }}#t=15" type="video/mp4">
                              </video>
                              <div class="logo-part">
                                 <img src="{{ asset($video['image']) }}">
                              </div>
                           </div>
                           <div class="product-info text-left">
                             <h3 class="name"><a href="{{url('/'.$video->user['slug'])}}">{{$video->user['company_name'] }}</a></h3>
                             <!--  <p>{{ $video['description'] }}</p> -->
                              <!--   <a href="" class="read-more">Read more</a> -->
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               @endforeach
            </div>
           <center> <a href="{{route('Front.trendingVideo')}}" class="new-button">View All</a></center>
            <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
            <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
            <!--------------------Row-2 done------------------>
            <!-- /.home-owl-carousel -->
         </section>
         <!-- /.scroll-tabs -->
      </div>
   </div>
</section>


<div class="container">
   <div class="homeads">
      <a href="http://www.jawlatechnology.in/" target="_blank"><img src="{{ 'assets/images/ads3.jpg' }}"
         style="border: 1px solid #ccc;"></a>
   </div>
</div>


<section class="section featured-product wow fadeInUp   pt-50 pb-50" style="background: rgb(255 251 226 / 70%);">
<div class="container">
<h2 class="section-title1  "><span>Trending </span> Products</h2>
<div class="owl-gg">
   @if (count($products) > 0 && $products !== null)
         @foreach ($products as $prod)
      <div class="grid-owl">
      <div class="item item-carousel">
         <div class="products">
            <div class="product">
            @if ($prod->firstImage)
            <div class="product-image">
            <div class="image"> <img src="{{ asset($prod->firstImage->product_image) }}" alt="" style="height:200px;">
            </div>
            </div>
          @endif
            <div class="product-info text-left cate-headi">
            <h3 class="name"><a href="{{ URL::to('/product/' . $prod->slug) }}">{!! Str::limit(strip_tags($prod->title), 20) !!}</a>
            </h3>
            </div>
            </div>
         </div>
      </div>
   </div>
    @endforeach
    @else
   @endif
</div>

 <center> <a href="{{route('Front.TrendingProducts')}}" class="new-button">View All</a></center>
<!-- /.home-owl-carousel -->
</div>
</section>
<!-- /.section -->


<!-- ============================================== CONTENT ============================================== -->
<section class="bg-back" style="background: #f7eeeba3;">
   <div class="container pt-50 pb-50">
      <div class="col-xs-12 col-sm-12 col-md-12 homebanner-holder  ">
         <section class="section featured-product wow fadeInUp">
            <h2 class="section-title1  ">Recommended <span>Video Bio</span></h2>

            <div class="owl-gg">
              @foreach ($video_recommended as $video)
               <div class="grid-owl">
                  <div class="item item-carousel">
                     <div class="products">
                        <div class="product">
                           <div class="product-image">
                              <video style="width:100%;height: 143px;" controls="">
                                 <source src="{{ asset($video['video']) }}#t=15" type="video/mp4">
                              </video>
                              <div class="logo-part">
                                 <img src="{{ asset($video['image']) }}">
                              </div>
                           </div>
                           <div class="product-info text-left">
                             <h3 class="name"><a href="{{url('/'.$video->user['slug'])}}">{{$video->user['company_name'] }}</a></h3>
                             <!--  <p>{{ $video['description'] }}</p> -->
                              <!--   <a href="" class="read-more">Read more</a> -->
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               @endforeach
            </div>

         <center> <a href="{{route('Front.recommendedVideo')}}" class="new-button">View All</a></center>
               <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
               <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
               <!--------------------Row-2 done------------------>
               <!--  <center> <a href="#" class="new-button">View All</a></center> -->
               <!-- /.home-owl-carousel -->
         </section>
         <!-- /.scroll-tabs -->
         </div>
      </div>
</section>


<div class="home-form container pt-50 pb-50">
<div class="home-form-grid">
<div class="home-form-left">
<div class="home-form-top">
<h2>India's Largest Market Place</h2>
<!-- <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
industry's standard dummy text ever</p> -->
</div>
<div class="home-form-bottom">
<div class="owl-carousel form-car">
<div class="form-overlap-content">
<h2>Hydraulic Accumulator</h2>
<p>Buyer is looking for 'Hydraulic Accumulator'</p>
<div class="sta-flex">
<p><i class="fas fa-map-marker-alt"></i>India </p>
<p class="c-fl"> <img src="{{ 'assets/images/india.svg' }}"> 07 August, 2021
</p>
</div>
<a href="" data-toggle="modal" data-target="#exampleModal">Contact Buyer Now</a>
</div>
<div class="form-overlap-content">
<h2>Hydraulic Accumulator</h2>
<p>Buyer is looking for 'Hydraulic Accumulator'</p>
<div class="sta-flex">
<p><i class="fas fa-map-marker-alt"></i>India </p>
<p class="c-fl"> <img src="{{ 'assets/images/india.svg' }}"> 07 August, 2021
</p>
</div>
<a href="" data-toggle="modal" data-target="#exampleModal">Contact Buyer Now</a>
</div>
<div class="form-overlap-content">
<h2>Hydraulic Accumulator</h2>
<p>Buyer is looking for 'Hydraulic Accumulator'</p>
<div class="sta-flex">
<p><i class="fas fa-map-marker-alt"></i>India </p>
<p class="c-fl"> <img src="{{ 'assets/images/india.svg' }}"> 07 August, 2021
</p>
</div>
<a href="" data-toggle="modal" data-target="#exampleModal">Contact Buyer Now</a>
</div>
<div class="form-overlap-content">
<h2>Hydraulic Accumulator</h2>
<p>Buyer is looking for 'Hydraulic Accumulator'</p>
<div class="sta-flex">
<p><i class="fas fa-map-marker-alt"></i> India </p>
<p class="c-fl"> <img src="{{ 'assets/images/india.svg' }}"> 07 August, 2021
</p>
</div>
<a href="" data-toggle="modal" data-target="#exampleModal">Contact Buyer Now</a>
</div>
</div>
</div>
</div>
<script src="assets/css/owl.carousel.min.js"></script>
<script>
   $('.owl-carousel.form-car').owlCarousel({
       loop: true,
       margin: 10,
       responsiveClass: true,
       responsive: {
           0: {
               items: 1,
               nav: true
           },
           600: {
               items: 2,
               nav: false
           },
           1000: {
               items: 2,
               nav: true,
               loop: false
           }
       }
   })
</script>
   
<div class="home-form-right">
<h2>One Request, Multiple Quotes</h2>
<!-- <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
industry's standard dummy text ever</p> -->
<form>
<div class="form-group" id="the-basics">
<label>Products &amp; Services<label>
<input type="text" id="services" value="" class="form-control"
   placeholder="Please type what you are looking for...">
</label></label>
</div>
<div class="form-group">
<div class="home-inside-form-grid">
<div class="home-inside-form-grid-left">
<label>Quantity</label>
<div class="home-fl">
<select name="quantity" id="qty_unit">
<option value="Nos">Nos</option>
<option value="Kilogram">Kilogram</option>
<option value="Pieces">Pieces</option>
<option value="Tons">Tons</option>
</select>
<input type="number" id="qty">
</div>
</div>
<div class="home-inside-form-grid-left">
<label>Order Value</label>
<div class="home-fl">
<select id="order_data">
<option value="5000 to 10000">5000 to 10000</option>
<option value="10001 to 20000">10001 to 20000</option>
<option value="20001 to 50000">20001 to 50000</option>
<option value="Upto 1 Lakh">Upto 1 Lakh</option>
</select>
<!--  <input type="text" id="value"> -->
</div>
</div>
</div>
</div>
<a href="#" class="homePost-request rsM" data-toggle="modal" data-target="#myModal4">Post Request</a>
</form>
</div>
</div>
</div>
<!-- ============================================== FEATURED PRODUCTS ============================================== -->


<section class="section featured-product wow fadeInUp   pt-50 pb-50"
   style="    background: rgb(255 251 226 / 70%);">
<div class="container">
<h2 class="section-title1  "><span>Trending </span> Category</h2>
<div class="owl-gg">
   @php
   $category = \App\Models\Category::where('status', 1)
   ->where('name', '!=', 'null')
   ->limit(8)
   ->get();
   @endphp
   @foreach ($category as $cat)
      <div class="grid-owl">
      <div class="item item-carousel">
         <div class="products">
            <div class="product">
            <div class="product-image">
            <div class="image"> <img src="{{ asset($cat['image']) }}" alt=""> </div></div>
            <div class="product-info text-left cate-headi">
            <h3 class="name"><a href="{{ URL::to('/category/' . $cat->slug) }}">{{ $cat['name'] }} </a></h3>
            </div>
            </div>
         </div>
      </div>
   </div>
    @endforeach
</div>

 <center> <a href="{{route('Front.AllCategory')}}" class="new-button">View All</a></center>
<!-- /.home-owl-carousel -->
</div>
</section>
<!-- /.section -->
<!-- ============================================== FEATURED PRODUCTS ============================================== -->
<section class="section featured-product wow fadeInUp   pt-50 pb-50 hot-seller" style="">
<div class="container">
<h2 class="section-title1  "><span>Hot </span> Seller</h2>
<div class="products-gridss">
@foreach ($video_trending as $video)
<div class="products">
<div class="product">
<div class="product-image">
<div class="image"><a href="{{url('/'.$video->user['slug'])}}">
   <img src="{{ asset($video['image']) }}" alt="" style="height:200px; object-fit: contain;padding:25px;"></a> </div>
</div>
<div class="product-info text-left cate-headi">
<h3 class="name"><a
   href="{{url('/'.$video->user['slug'])}}">{{ $video->user['company_name'] }}</a>
</h3>
</div>
</div>
</div>
@endforeach
</div>
<!-- /.home-owl-carousel -->
<center> <a href="{{route('Front.hotSeller')}}" class="new-button">View All</a></center>
</div>
</section>


<!-- ============================================== FEATURED PRODUCTS ============================================== -->
<section class="section featured-product wow fadeInUp   pt-50 pb-50 hot-seller" style=" ">
<div class="container">
<h2 class="section-title1  "><span>Hot </span> Products</h2>
<div class="products-gridss">
@foreach ($products as $prod)
<div class="products">
<div class="product">
@if ($prod->firstImage)
<div class="product-image">
<div class="image"> <a
   href="#"><img
   src="{{ asset($prod->firstImage->product_image) }}" alt=""
   style="height: 250px!important;"></a> </div>
</div>
@endif
<div class="product-info text-left cate-headi">
<h3 class="name"><a
   href="{{ URL::to('/product/' . $prod->slug) }}">{!! Str::limit(strip_tags($prod->title), 20) !!}</a>

</h3>
</div>
</div>
</div>
@endforeach
</div>
<!-- /.home-owl-carousel -->
<center> <a href="{{route('Front.hotProduct')}}" class="new-button">View All</a></center>
</div>
</section>
<!-- /.section -->


<section id="company-week" style="background-image: url({{ asset('public/images/category-banner.jpg') }});">
    <div class="companiesoftheweek pt-30 pb-30">
        <div class="container">
            <div class="row align-items-center">
               @foreach ($companies as $company)
                <div class="main-box">
                     <div class="head">
                     <h2>Company Of The Week</h2>
                        <h3><a href="{{url('/'.$company->user['slug'])}}" style="color:black;">{{$company->user['company_name'] }}</a></h3>						      
                        <p>{!!$company->description!!}</p>                                  
                      </div>
                   <div class="video-bg" style="background-image: url({{ asset('public/images/1234.jpg') }});">
                      <div class="company-vedio">
                           <video width="100%" height="350" controls="">
                              <source src="{{ asset($company['video']) }}" type="video/mp4">
                              <source src="movie.ogg" type="video/ogg">
                            </video>
                        </div>
                    </div> 
                </div>
                @endforeach
            </div>
        </div>
    </div>    
</section>


<script>
    $(document).ready(function () {
        function playFile() {
            $(".player").not(this).each(function () {
                $(this).get(0).pause();
            });
            this[this.get(0).paused ? "play" : "pause"]();
        }

        $('.player').on("click play", function() {
            playFile.call(this);
        });
    })
</script>

<section class="ms ">
<div class="container two-slide pt-50 pb-50 msme-int">
<h2 class="section-title1  "><span>MSME </span> Interviews</h2>
<div class="two-grid">
<div class="products-grids">
@foreach ($interviews as $interview)
<div class="product">
<div class="product-image">
<video class="player" style="height:150px;width:100%;" controls="">
<source src="{{ asset($interview['video']) }}#t=15" type="video/mp4">
</video>
</div>
<div class="product-info text-left">
<h3 class="name">{{ $interview->title }}</h3>
</div>
</div>
@endforeach
</div>
<div class="col-lg-12 mt-4">
<center class="m-auto"> <a href="{{ route('Front.msme-interviews') }}"
   class="new-button">View All</a></center>
</div>
</div>
</div>
</section>
<div class="sidebar-widget  wow fadeInUp outer-top-vs testi pt-50"
   style="background-image: url({{ asset('public/images/testi.jpg') }});">
<div class="container ">
<h2 class="section-title1  ">What <span>Clients says </span> about <span> B2B Streets</span></h2>
<div id="advertisement" class="advertisement">
@foreach ($testimonials as $testimonial)
<div id="myCarousel2" class="carousel slide" data-ride="carousel">
<div class="carousel-inner">
<div class="tes-gr-50">
<div class="item active">
<div class="">
<img src="{{ asset($testimonial['image']) }}" alt="..."
   style="width:100px;height:100px;margin: 10px auto;clear: both;display: block;">
<div class="carousel-caption"
   style="padding:10px;right: 0;left: 0;bottom: 30px;clear: both;margin-top: 50px;position: relative;text-align: center;">
<h3>{{ $testimonial->title }}</h3>
<p>{{ $testimonial->description }}</p>
</div>
</div>
</div>
</div>
</div>
</div>
@endforeach
<!-- /.owl-carousel -->
</div>
</div>
</div>
<section class="pt-50 pb-50  ">
<div class="container">
<h2 class="section-title1  "><span>Our </span> Network</h2>
<div class="logo-container">
<a href="https://www.ibem.co.in/"><img src="{{ 'assets/images/ibem.jpg' }}"></a>
<a href="https://www.webmobril.services/dev/thefilmframers/"><img src="{{ 'assets/images/the-film-framer.jpg' }}"></a>
<a href="https://www.k12news.net/"><img src="{{ 'assets/images/k12.jpg' }}">
<a href="https://msmenews.net//"><img src="{{ 'assets/images/blog/msme.jpeg' }}" style="width: 80%;">
<a href="https://www.webmobril.com/"><img src="{{ 'assets/images/wmit.jpg' }}"></a>
<a href="https://www.wmtc.in/"><img src="{{ 'assets/images/wmtc.jpg' }}"></a>
<a href="https://www.wmstaffingsolutions.com/"><img src="{{ 'assets/images/wmstaffing.jpg' }}"></a>
<!-- <img src="{{ 'assets/images/gng.jpg' }}"> -->
</div>
</div>
</section>
<section class="  pt-50 pb-50">
<div class="container ">
<div class="brand-grid">
<img src="{{ 'assets/images/B2B-streets_Banner.jpg' }}">
<!-- <img src="{{ 'assets/images/gallery/5d6ce9a8a30f7.jpg' }}"> -->
<!-- <img src="{{ 'assets/images/gallery/5d6ce9b68a62a.jpg' }}"> -->
</div>
</div>
</section>
<div class="foot-cate container">
<h2 class="section-title1  "><span>Our </span> Categories</h2>
<div class="foot-cate-list">
<!-- <h2>Categories:</h2> -->
@php
   $category = \App\Models\Category::where('status', 1)
   ->where('name', '!=', 'null')
   ->limit(20)
   ->get();
   @endphp
   @foreach ($category as $cat)
<ul>
<li><a href="{{ URL::to('/category/' . $cat->slug) }}">{{ $cat['name'] }} </a></li>
</ul>
@endforeach
 
</div>
</div>
<section style="display:none;">
<div id="myCarousel" class="carousel slide" data-ride="carousel">
<!-- Indicators -->
<ol class="carousel-indicators">
<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
<li data-target="#myCarousel" data-slide-to="0"></li>
<li data-target="#myCarousel" data-slide-to="0"></li>
</ol>
<!-- Wrapper for slides -->
<div class="carousel-inner">
<div class="item active">
<img src="images/gallery/5d6ce9b68a62a.jpg" alt="..." style="width:100%;height:400px;">
</div>
<div class="item"> <img src="images/gallery/5d6ce9ae838aa.jpg" alt="..."
   style="width:100%;height:400px;"></div>
<div class="item"> <img src="images/gallery/5d6ce9a8a30f7.jpg" alt="..."
   style="width:100%;height:400px;"></div>
</div>
<!-- Left and right controls -->
<a class="left carousel-control" href="#myCarousel" data-slide="prev">
<span class="glyphicon glyphicon-chevron-left"></span>
<span class="sr-only">Previous</span>
</a>
<a class="right carousel-control" href="#myCarousel" data-slide="next">
<span class="glyphicon glyphicon-chevron-right"></span>
<span class="sr-only">Next</span>
</a>
</div>
</section>
<div id="brands-carousel" class="logo-slider wow fadeInUp" style="display:none;">
<div class="logo-slider-inner">
<div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
<div class="item m-t-15"> <a href="#" class="image"> <img
   data-echo="assets/images/brands/brand1.png" src="assets/images/blank.gif" alt=""> </a> </div>
<!--/.item-->
<div class="item m-t-10"> <a href="#" class="image"> <img
   data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif" alt=""> </a> </div>
<!--/.item-->
<div class="item"> <a href="#" class="image"> <img
   data-echo="assets/images/brands/brand3.png" src="assets/images/blank.gif" alt=""> </a> </div>
<!--/.item-->
<div class="item"> <a href="#" class="image"> <img
   data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif" alt=""> </a> </div>
<!--/.item-->
<div class="item"> <a href="#" class="image"> <img
   data-echo="assets/images/brands/brand5.png" src="assets/images/blank.gif" alt=""> </a> </div>
<!--/.item-->
<div class="item"> <a href="#" class="image"> <img
   data-echo="assets/images/brands/brand6.png" src="assets/images/blank.gif" alt=""> </a> </div>
<!--/.item-->
<div class="item"> <a href="#" class="image"> <img
   data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif" alt=""> </a> </div>
<!--/.item-->
<div class="item"> <a href="#" class="image"> <img
   data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif" alt=""> </a> </div>
<!--/.item-->
<div class="item"> <a href="#" class="image"> <img
   data-echo="assets/images/brands/brand1.png" src="assets/images/blank.gif" alt=""> </a> </div>
<!--/.item-->
<div class="item"> <a href="#" class="image"> <img
   data-echo="assets/images/brands/brand5.png" src="assets/images/blank.gif" alt=""> </a> </div>
<!--/.item-->
<div class="item m-t-10"> <a href="#" class="image"> <img
   data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif" alt=""> </a> </div>
</div>
</div>
</div>
</div>
</div>
<!-- /.container -->
</div>
<!-- /#top-banner-and-menu -->
 
<!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
<script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>
<link href="https://twitter.github.io/typeahead.js/css/examples.css"> -->
<!-- <script type="text/javascript">
   $('.advertisement').owlCarousel({
       loop: true,
       margin: 10,
       nav: true,
       autoplay: true,
       responsive: {
           0: {
               items: 1
           },
           600: {
               items: 1
           },
           1000: {
               items: 1
           }
       }
   })
</script> -->
<script>
   $(document).ready(function() {
       // Get value on button click and show alert
       $(".rsM").click(function() {
           var service = $("#services").val();
           var qty_unit = $("#qty_unit").val();
           var quantity = $("#qty").val();
           var order_data = $("#order_data").val();
           var value = $("#value").val();
   
           $("#product_name").val(service);
           $("#quantity_unit").val(qty_unit);
           $("#order").val(order_data);
           $("#quantity").val(quantity);
   
       });
   });
</script>
<script>
   var substringMatcher = function(strs) {
       return function findMatches(q, cb) {
           var matches, substrRegex;
   
           // an array that will be populated with substring matches
           matches = [];
   
           // regex used to determine if a string contains the substring `q`
           substrRegex = new RegExp(q, 'i');
   
           // iterate through the pool of strings and for any string that
           // contains the substring `q`, add it to the `matches` array
           $.each(strs, function(i, str) {
               if (substrRegex.test(str)) {
                   // the typeahead jQuery plugin expects suggestions to a
                   // JavaScript object, refer to typeahead docs for more info
                   matches.push({
                       value: str
                   });
               }
           });
   
           cb(matches);
       };
   };
   
   $('#the-basics #services').typeahead({
           hint: true,
           highlight: true,
           minLength: 1
       },
   
       {
           name: 'states',
           displayKey: 'value',
           source: substringMatcher(states)
       });
</script>
<script type="text/javascript">
   $(document).ready(function() {
   
       var substringMatcher = function(strs) {
           return function findMatches(q, cb) {
               var matches, substrRegex;
   
               // an array that will be populated with substring matches
               matches = [];
   
               // regex used to determine if a string contains the substring `q`
               substrRegex = new RegExp(q, 'i');
   
               // iterate through the pool of strings and for any string that
               // contains the substring `q`, add it to the `matches` array
               $.each(strs, function(i, str) {
                   if (substrRegex.test(str)) {
                       // the typeahead jQuery plugin expects suggestions to a
                       // JavaScript object, refer to typeahead docs for more info
                       matches.push({
                           value: str
                       });
                   }
               });
   
               cb(matches);
           };
       };
   
      
   
       $('#dd #bb').typeahead({
               hint: true,
               highlight: true,
               minLength: 1
           },
   
           {
               name: 'states',
               displayKey: 'value',
               source: substringMatcher(states)
           });
   
   });
</script>
<script type="text/javascript">
   $(document).ready(function() {
   
       var substringMatcher = function(strs) {
           return function findMatches(q, cb) {
               var matches, substrRegex;
   
               // an array that will be populated with substring matches
               matches = [];
   
               // regex used to determine if a string contains the substring `q`
               substrRegex = new RegExp(q, 'i');
   
               // iterate through the pool of strings and for any string that
               // contains the substring `q`, add it to the `matches` array
               $.each(strs, function(i, str) {
                   if (substrRegex.test(str)) {
                       // the typeahead jQuery plugin expects suggestions to a
                       // JavaScript object, refer to typeahead docs for more info
                       matches.push({
                           value: str
                       });
                   }
               });
   
               cb(matches);
           };
       };
   
       var states = [<?php
      $max = count($location);
      foreach ($location as $key => $value) {
          echo '"' . $value . '"';
          if ($max > $key + 1) {
              echo ',';
          }
      }
      ?>];
   
       $('#loc #loca').typeahead({
               hint: true,
               highlight: true,
               minLength: 1
           },
   
           {
               name: 'states',
               displayKey: 'value',
               source: substringMatcher(states)
           });
   
   });
</script>


<section id="pay-modal">
    <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
   
      <div class="modal-body">
          <div class="col-md-12 text text-center">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
               <h3>Want to see full details of buyer ?</h3>
            
             
              <h4 class="text-center">Hydraulic Accumulator</h4>
              <ul>
                  <li>Name : XXX</li>
                  <li>Phone No. : XXXX</li>
              </ul> 
                        <form class="form-horizontal">
                            <div class="form-group">
                            <label for="inputEmail3" class="col-md-offset-2 col-sm-2 control-label">Name:</label>
                            <div class="col-md-6">
                              <input type="text" class="form-control" id="inputEmail3" placeholder="ABC">
                            </div>
                          </div>
                            <div class="form-group">
                            <label for="inputEmail3" class="col-md-offset-2 col-sm-2 control-label">Phone No.:</label>
                            <div class="col-md-6">
                              <input type="text" class="form-control" id="inputEmail3" placeholder="12345679">
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="inputEmail3" class="col-md-offset-2 col-sm-2 control-label">Email:</label>
                            <div class="col-md-6">
                              <input type="email" class="form-control" id="inputEmail3" placeholder="abc@gmail.com">
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="inputPassword3" class="col-md-offset-2 col-sm-2 control-label">Location:</label>
                            <div class="col-md-6">
                              <input type="text" class="form-control" id="inputPassword3" placeholder="India">
                            </div>
                          </div>
                        </form>
                <button type="submit" class="button  btn_blue mt40 upper text-center">
                <i class="fa fa-paper-plane-o" aria-hidden="true"></i> Buy Now
                </button>
          </div>
      </div>
    </div>
  </div>
</div>
</section>


<style>
#pay-modal .modal-content {
    background-color: #fff;
    height: 500px;
}
    #pay-modal .modal-body {
    padding: 0px !important;
}
    #pay-modal .button.btn_blue {
    background: #556495;
    padding: 10px 25px;
    border-radius: 5px;
    color: #fff;
    border: 1px solid #556495;
    transition: .5s all ease;
}
    #pay-modal ul li{
        line-height: 2.5;
    }
    #pay-modal ul{
        margin-bottom: 10px;
    }
    #pay-modal .close {
        top: 0px;
    right: 0px;
    position: relative;
    transform: translate(54%, 0%);
    background: #556495;
    padding: 8px;
    opacity: inherit;
}
    #pay-modal .modal-content p{
  font-size: 14px;line-height: 25px;
}
    #pay-modal .modal-content .form-control{
      font-size: 12px;
    color: #767676ee;
    }
ul#search-table {
    background-color: #f5f5f5;
    overflow: scroll;
    height: 400px;
    position: absolute;
    top: 100%;
    padding: 15px;
    overflow-x: hidden;
}
ul#search-table li{
    border-bottom: 1px solid #aeaeae;
    line-height: 35px;
}
ul#search-table li:hover {
  color: #fff !important;
  background-color: #556495 !important;
}
ul#search-table li:hover a{
  color: #fff !important;
  text-decoration: none !important;
}
ul#search-table li a{
    background: none;
    padding: 0px;
    color: #333;
}
ul#search-table li a: hover{
   border: none !important;
   text-decoration: none;
}
 .body-content .my-wishlist-page .my-wishlist table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{
       padding: 15px !important;
    width: 450px;
    border-bottom: 1px solid #55649538;
 }
 .banner-content a:hover{
   border: none !important;
 }
 
</style>


<script>
$(document).ready(function(){  
   $('#search-table').css("display", "none");
});
</script>

@endsection