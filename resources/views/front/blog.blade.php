@extends('layouts.front')
@section('content')
@include('front.seo-keywords')

       
      
 


         <div class="breadcrumb">
            <div class="container">
               <div class="breadcrumb-inner">
                  <ul class="list-inline list-unstyled">
                     <li><a href="https://b2bstreets.com">Home</a></li>
                     <li class='active'>Blog</li>
                  </ul>
               </div>
               <!-- /.breadcrumb-inner -->
            </div>
            <!-- /.container -->
         </div>
         <!-- /.breadcrumb -->
      

<section id="blog">
    <div class="container">
        <div class="row">
            <h2 class="text-center">Leatest Blogs</h2>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="blog">
                    <img src="{{ asset('public/images/blog/blog-1.jpg') }}" style="width:100%">
                    <div class="blog-con">
                        <div class="row">
                        <div class="col-md-4">
                            <h5><i class="fa fa-user"></i> Admin</h5>
                        </div>
                        <div class="col-md-4">
                            <h5><i class="fa fa-tags"></i> Decorate</h5>
                        </div>
                    </div>
                    <h4>B2B Streets Achievers</h4>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                             <h5><i class="fa fa-calendar-alt"></i> April 19</h5>
                        </div>
                        <div class="col-md-6">
                             <h5><a href="{{route('Front.BlogInfo')}}">Read More</a></h5>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-4">
                <div class="blog">
                    <img src="{{ asset('public/images/blog/blog-1.jpg') }}" style="width:100%">
                    <div class="blog-con">
                        <div class="row">
                        <div class="col-md-4">
                            <h5><i class="fa fa-user"></i> Admin</h5>
                        </div>
                        <div class="col-md-4">
                            <h5><i class="fa fa-tags"></i> Decorate</h5>
                        </div>
                    </div>
                    <h4>B2B Streets Achievers</h4>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                             <h5><i class="fa fa-calendar-alt"></i> April 19</h5>
                        </div>
                        <div class="col-md-6">
                             <h5><a href="{{route('Front.BlogInfo')}}">Read More</a></h5>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-4">
                <div class="blog">
                    <img src="{{ asset('public/images/blog/blog-1.jpg') }}" style="width:100%">
                    <div class="blog-con">
                        <div class="row">
                        <div class="col-md-4">
                            <h5><i class="fa fa-user"></i> Admin</h5>
                        </div>
                        <div class="col-md-4">
                            <h5><i class="fa fa-tags"></i> Decorate</h5>
                        </div>
                    </div>
                    <h4>B2B Streets Achievers</h4>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                             <h5><i class="fa fa-calendar-alt"></i> April 19</h5>
                        </div>
                        <div class="col-md-6">
                             <h5><a href="{{route('Front.BlogInfo')}}">Read More</a></h5>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
          
          

@endsection