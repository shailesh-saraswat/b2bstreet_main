
 <!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Meta -->
      <title>B2B Streets | Indias First Visual B2B Search Engine </title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
      <meta name="description" content="Indias First Visual B2B Search Engine, Get Unlimited Business Deals with Indias Best Mart Business Directory, For More Details Hit The Call Button.">
      <meta name="author" content="">
      <meta name="keywords" content="B2B Streets, Indias First Visual B2B Search Engine">
      <meta name="robots" content="all">
      <!-- Bootstrap Core CSS -->
      <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
      <!-- Customizable CSS -->
      
      <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/blue.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/simple-line-icons.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/owl.transitions.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/animate.min.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/rateit.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/anuj.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-select.min.css') }}">
      <!-- Icons/Glyphs -->
      <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.css') }}">
      <!-- Fonts -->
      <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
      <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,600italic,700,700italic,800' rel='stylesheet' type='text/css'>
      <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,500i,600,700,800,900" rel="stylesheet">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

<style>
.video-pro-content video { 
    height: unset; 
}
.abt-comp.pt-50.pb-50 .container p {
    width: 100% !important;
    text-align: justify;
}



</style>


   <body class="cnt-home inside-header">
      @foreach($banners as $banner)
      
       <div class="cl-header-fixed">
       <div class="cl-header container">
         <div class="cl-grid">
            <div class="cl-logo">
               <img src="{{asset($banner['image'])}}">
            </div>
            <div class="cl-menu">
               <div class="nav">
                  <a href="#s1" class="">Intro</a>
                  <a href="#s2">About </a>
                  <a href="#s3" class="active">Products</a>
                  <a href="#s4" class="">Gallery</a>
                 <!--  <a href="#s6" class="">Packages</a> -->
                  <a href="#s5" class="">Contact Us</a>
               </div>
            </div>
         </div>
      </div>
      </div>
      @endforeach
      <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js'></script>
<style>
   .cl-logo img {
   width: 67px;
}
</style>
      <script>
function scrollNav() {
  $('.nav a').click(function(){
    $(".active").removeClass("active");     
    $(this).addClass("active");
    ff
    $('html, body').stop().animate({
      scrollTop: $($(this).attr('href')).offset().top - 160
    }, 300);
    return false;
  });
}
scrollNav();
      </script>Project : B2B Streets 
Task : Admin pannel and frontend
Status : Working On new module and bug fixing


      
      <!-- ============================================== HEADER : END ============================================== -->f

      @include('front.seo-keywords')
      
      <div class="profile-container" id="s1">
         <div class="breadcrumb bg-none">
            <div class="container">
               <div class="breadcrumb-inner">
                  <ul class="list-inline list-unstyled">
                     <li><a href="{{route('Front.Index')}}">Home</a></li>
                     <li class="active" style="color: white!important;">Video Bio</li>
                  </ul>
               </div>
               <!-- /.breadcrumb-inner -->   
            </div>
            <!-- /.container -->
         </div>
           <div class="container">
            <div style="float: right;color: white;"><i class="fa fa-eye"></i>{{$user->viewer}}</div>
            @foreach($banners as $banner)
            <div class="pro-grid">
               <div class="video-right">
                  <div class="video-frame">
                     <div class="video-pro-content">
                        <video controls="">
                           <source src="{{asset($banner['video'])}}#t=4">                                            
                        </video>
                     </div>

                  </div>f
               </div>
               <div class="profile-flex ">
                  <div class="profile-img">
                     <div class="profile-cont">
                        

                        <div class="profile-logo-div">
                           <img class="profile-logo" src="{{asset($banner['image'])}}">
                           <h2>{{$banner['title']}}</h2>
                        </div>
                        <div class="info-lable"><span>Name: </span>{{$banner['name']}}</div>
                        <div class="info-lable"><span>Address:</span>{{$banner['address']}}</div>
                        <!-- <div class="info-lable"><span>Contact Number:</span>{{$banner['mobile']}}</div> -->
                        <div class="info-lable"><span>Email Id:</span>{{$banner['email']}}</div>
                        <div class="info-lable"><span>Website:</span>{{$banner['website']}}</div>
                        <a href="#s5" class="new-button">Enquiry Now</a>                                              
                     </div>
                  </div>
               </div>fffff
            </div>
             @endforeach
         </div>


         <div class="dott-pattern"></div>
      </div>
      <div class="abt-comp pt-50 pb-50" id="s2">
         <div class="container">
             @foreach($abouts as $about)

            <h2>{{$about['title']}}</h2>
            <p>{!!$about['description']!!}</p>
     @endforeach
         </div>
      </div>

      <section id="s3" class="section featured-product wow fadeInUp   pt-50 pb-50 mb-50 hot-seller animated profile-product" style="background: rgb(244, 241, 249); visibility: visible; animation-name: fadeInUp;">
         <div class="container">
            <h2 class="section-title1  ">Our   <span>Products/Services</span></h2>
            <div class="products-gridss mBB">
               @foreach($products as $product)
               
            
               <div class="products">
                  <div class="product">
                     @foreach($product->product_images as $p_img)
            
                     <div class="product-image">
                        <div class="image"> <a href="{{ route('Front.productDetails',[$user->slug,$product->slug]) }}"><img src="{{asset($p_img['product_image'])}}" alt="No Data"  ></a> </div>
                     </div>
                      @endforeach
                     <div class="product-info text-left cate-headi">
                        <h3 class="name"><a href="{{ route('Front.productDetails',[$user->slug, $product->slug]) }}">{{$product['title']}}</a></h3>
                     </div>
                  </div>
               </div>
         
                @endforeach
            </div>
            <!-- /.home-owl-carousel -->       
         </div>
      </section>


      <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.14/css/lightgallery.css'>
      <div class="cont Gs" id="s4">
         <div class="container">
            <h2 class="section-title1  ">Our   <span>Gallery</span></h2>
         </div>
          <div class="container">
         <div class="demo-gallery">
            <ul id="lightgallery">
             @foreach($galleries as $gallery)
               <li data-responsive="{{ asset($gallery['image']) }}"data-src="{{ asset($gallery['image']) }}">        
               <a href=""><img class="img-responsive" src="{{ asset($gallery['image']) }}"></a> </li>
            @endforeach
            </ul>
         </div>
         </div>
      </div>
     <div class="profile-right-fix">
    <a href=""><img src="{{ asset('public/images/phone.svg') }}"></a>
</div>
     
     
     
     
     <script>$(document).ready(() => {  $("#lightgallery").lightGallery({    pager: true  });});</script><script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script><script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script><script src='https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js'></script><script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js'></script><script src='https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.14/js/lightgallery-all.min.js'></script><script  src="./script.js"></script> 
      <!-- start-smoth-scrolling -->
      <!-- js -->
      <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
      <!-- //js -->
      <script>
         $('div#groupinfo .myImg').click(function(){
           //  alert($("img", this).html());
         //  alert($(this).find(".myImgx").attr("src"));
         //});
         
          /* $(".myImg").click(function() {
           var img = $(this).attr("src");*/
           var img = $(this).find(".myImgx").attr("src")
           var pname = $(this).find(".pnuname").text();
            var pdes = $(this).find(".prodes").text();
         var modal = document.getElementById('mypopupx');
         var modalImg = document.getElementById("imgf");
             modal.style.display = "block";
             modalImg.src = img;
            $("#namef").html(pname);
            $("#pdes").html(pdes);
         
         });
           
         
           function closethis(){
              document.getElementById("mypopupx").style.display = "none";
              //$('.modal2').style.display="none";
           }
         
           
      </script>
      <script>
         function send(){
            var vid= ;
         var reg = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
         var reg2 =/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
            
            var name = $('#name').val(); //alert(name);
            var contact = $('#cont').val(); //alert(contact);
            var phone = $('#phone').val(); //alert(phone);
             var emailx = $('#emailx').val(); //alert(email);
            var msg = encodeURIComponent($('#msg').val()); //alert(msg);
            var proname = $('#namef').html(); 
            var emailex = $('#emailex').val(); //alert(emailex);
            var sub = "an Enquiry";
             if(name.trim() == '' ){
               $('#trimb2').show();
               $('#trimb2').html('Please enter Name.');
               $('#trimb2').addClass('toolshow').removeClass('toolhide');
                $('#name').focus();
               return false;
            }else if(emailx.trim() == '' ){
               $('#trimb2').show();
               $('#trimb2').html('Please enter email.');
               $('#trimb2').addClass('toolshow').removeClass('toolhide');
                $('#emailx').focus();
               return false;
            }else if(emailx.trim() != '' && !reg.test(emailx)){
               $('#trimb2').show();
               $('#trimb2').html('Please enter valid email.');
               $('#trimb2').addClass('toolshow').removeClass('toolhide');
               $('#emailx').focus();
               return false;
         }else if(phone.trim() == '' ){
               $('#trimb2').show();
               $('#trimb2').html('Please enter contact.');
               $('#trimb2').addClass('toolshow').removeClass('toolhide');
                $('#phone').focus();
               return false;
           }else if(phone.trim() != '' && !reg2.test(phone)){
              $('#trimb2').show();
              $('#trimb2').html('Please enter valid contact.');
               $('#trimb2').addClass('toolshow').removeClass('toolhide');
                $('#phone').focus();
               return false;
            }else if(contact.trim() == '' ){
               $('#trimb2').show();
               $('#trimb2').html('Please enter Title.');
               $('#trimb2').addClass('toolshow').removeClass('toolhide');
                $('#cont').focus();
               return false;
          }else if(msg.trim() == '' ){
            $('#trimb2').show();
            $('#trimb2').html('Please enter message.');
               $('#trimb2').addClass('toolshow').removeClass('toolhide');
                $('#msg').focus();
               return false;     
            }
            else{
                 $.ajax({
                     type:'POST',
                     url:'send_mail2.php',
                   data:'sendFrmSubmit=1&name='+name+'&emailx='+emailx+'&contact='+contact+'&msg='+msg+'&sub='+sub+'&emailex='+emailex+'&vid='+vid+'&phone='+phone+'&proname='+proname,
                     beforeSend: function () {
                           $('#btns').html("Sending Mail");
                         $('#btns').attr("disabled","disabled");
                         },
                     success:function(msg){
                         if(msg == 'ok'){
                        $('#trimb2').show();
                             $('#trimb2').html('<span style="color:green;">Thank you , We will contact you soon.</span>');
                         $('#conform textarea').val('');
                        }else{
                        $('#trimb2').show();
                             $('#trimb2').html('<span style="color:red;">Some problem occurred, please try again.</span>');
                         }
                           $('#btns').html("Submit");
                         $('#btns').removeAttr("disabled");
                      $('#conform .cal').val('');
                      }
                 });
             }
         }
      </script>   
       
       
        <section class="pt-50 pb-50  " style="background-color:#f1f3f6;">
               <div class="container">
                  <h2 class="section-title1  "><span>Our </span>Clients</h2>
                  <div class="logo-container">

                     @foreach($networks as $network)
                     <img src="{{ asset($network['image']) }}">
                   
                     @endforeach
                  </div>
               </div>
            </section>


       <div class="sidebar-widget  wow fadeInUp outer-top-vs testi pt-50">
            <div class="container ">
               <h2 class="section-title1  ">What <span>Clients says </span> about <span> B2B Streets</span></h2>
               <div id="advertisement" class="advertisement">

                   @foreach($testimonials as $testimonial)

                  <div id="myCarousel2" class="carousel slide" data-ride="carousel">
                     <div class="carousel-inner">
                         <div class="item active">
                              <img src="{{ asset($testimonial['image']) }}" alt="..." style="width:100px;height:100px;margin: 10px auto;clear: both;display: block;">
                              <div class="carousel-caption" style="padding:10px;right: 0;left: 0;bottom: 30px;clear: both;margin-top: 50px;position: relative;text-align: center;">
                                <h3>{{$testimonial->title}}</h3>
                                <p>{!! $testimonial->description !!}</p>
                              </div>
                            </div>                              
                     </div>
                  </div>
                  @endforeach
                  <!-- /.owl-carousel --> 
               </div>
            </div>
         </div>
       
       
         
            <section class="section featured-product wow fadeInUp   pt-50 pb-50 mb-50" >
               <div class="container">
                  <h2 class="section-title1  "><span>Certification </span> </h2>
                      @if(count($certificates)>0 && $certificates !== null)
                  <div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
                      @foreach($certificates as $certificate)
                       <div class="item item-carousel">
                         <div class="cretificate">
                            <img src="{{ asset($certificate['image']) }}">    
                           </div>
                        </div>
                      @endforeach

                   </div>
                    @else
                      <h3>No certificates Found</h3>
                      @endif
                    
                  <!-- <center> <a href="#" class="new-button">View All</a></center> -->
                  <!-- /.home-owl-carousel --> 
               </div>
            </section>
        
       
       
       <div class="container mtb-5 blogNw ">
         <div class="col-lg-12 text-center blog">
               <h2 class="section-title1 "><span>Blog</span></h2>                    
         </div>
            <div class="row">
                @foreach($blogs as $blog)
                <div class="col-lg-4">
                    <div class="blog-box">
                        <img src="{{ asset($blog['image']) }}">
                        <div class="blog-text">
                            <h4><span><i class="far fa-calendar-alt"></i>{{$blog->date}}</span>  <span><i class="fas fa-user"></i>{{$blog->user_name}}</span></h4>
                            <h5>{{$blog->title}}</h5>
                            <p>{{$blog->description}}.</p>
                            <h6><a href="">Read More <i class="fas fa-long-arrow-alt-right"></i></a></h6>
                        </div>
                    </div>
                </div>
                @endforeach
           </div>
       </div>
       
       
      <div style="background: #f7eeeba3;" class="pt-50 pb-50">
      <div class="container profile-form margin-top-0 margin-bottom-0" id="s5">
         <div id="popup" class="overlay">
            <h2 class="section-title1  "><span>Enquiry</span> Now</h2>
            <div class="popup">
               <div class="dott-pattern  form-pat"></div>
               <div class="">
                @if(Session::has('message'))
                <div class="alert alert-danger"  role="alert">{{Session::get('message')}}</div>
                @elseif(Session::has('success'))
                <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                @endif
                   <form id="conform2" method="post" action="{{route('Front.StoresuplierProfile')}}">
                      @csrf
                        
                     <input type="hidden" name="user_id"  value="{{$banner->user_id}}">
                     
                     <input type="hidden" name="supplier_email"  value="{{$banner['email']}}">
                      
                     <label class="info-title" for="exampleInputName">Your Name <span>*</span></label>
                     <input type="text" id="name" name="name" class="form-control unicase-form-control text-input cal" placeholder="" value="{{old('name')}}">
                     @if($errors->has('name'))
                     <div class="error">{{ $errors->first('name') }}</div>
                     @endif

                     <label class="info-title" for="exampleInputEmail1">Email Address <span>*</span></label>
                     <input class="form-control unicase-form-control text-input cal" type="email" id="email" name="email"  placeholder="" value="{{old('email')}}">
                     @if($errors->has('email'))
                     <div class="error">{{ $errors->first('email') }}</div>
                     @endif

                     <label class="info-title" for="exampleInputEmail1">Contact <span>*</span></label>
                     <input class="form-control unicase-form-control text-input" type="text" id="mobile" name="mobile"  value="" value="{{old('mobile')}}">
                     @if($errors->has('mobile'))
                     <div class="error">{{ $errors->first('mobile') }}</div>
                     @endif
                    
                     <label class="info-title" for="exampleInputTitle">Location<span>*</span></label>
                     <input type="text" class="form-control unicase-form-control text-input cal"  id="location" name="location"  placeholder="" value="{{old('location')}}">
                      @if($errors->has('location'))
                     <div class="error">{{ $errors->first('location') }}</div>
                     @endif
                     
                     <label class="info-title" for="exampleInputComments">Your Requirements <span>*</span></label>
                      <textarea type="text" class="form-control unicase-form-control" name="requirements" id="requirements">{{old('requirements')}}</textarea>
                       @if($errors->has('requirements'))
                        <div class="error">{{ $errors->first('requirements') }}</div>
                        @endif
 
                     <input type="submit" name="submit" id="submit" value="Submit" class="new-button">                  
                  </form>
                  <p id="trimbn" style="color:red;"></p>
               </div>
            </div>
         </div>
      </div>
      </div>


<div class="copyright-bar1">
               <div class="container">
                  <div class="row align-items-center">
                        <div class="col-md-6 text-left">
                            <p>All right reserved @IBEM  Solutions LLP.</p>
                        </div>
                      <div class="col-md-6 text-right">
                           
                           <a href="https://b2bstreets.com/"><img src="{{ asset('assets/images/footer-logo2.png') }}" style="width: 127px;"></a>
                           <a href="https://www.ibem.co.in/"><img src="{{asset ('assets/images/slider/footer-logo1.png') }}" width="10%"></a>
                        </div>
                   </div>
               </div>
            </div>



<style>
.copyright-bar1 p {
    margin-bottom: 0;
    text-align: left;
    padding-top: 15px;
    font-size: 14px;
    color: #000;
}    
    .copyright-bar1 {
    background: none;
    padding: 10px 0px;
    z-index: 99;
    position: relative;
    border-top: 1px solid #eeee;
    margin-top: -20px;
    background-color: #ededed;
}
.error{
   color: red;
}
 
</style>
      <!-- JavaScripts placed at the end of the document so the pages load faster --> 
      <script src="https://b2bstreets.com/assets/js/bootstrap-hover-dropdown.min.js"></script> 
      <script src="https://b2bstreets.com/assets/js/owl.carousel.min.js"></script> 
      <script src="https://b2bstreets.com/assets/js/echo.min.js"></script> 
      <script src="https://b2bstreets.com/assets/js/jquery.easing-1.3.min.js"></script> 
      <script src="https://b2bstreets.com/assets/js/bootstrap-slider.min.js"></script> 
      <script src="https://b2bstreets.com/assets/js/jquery.rateit.min.js"></script> 
      <script type="text/javascript" src="https://b2bstreets.com/assets/js/lightbox.min.js"></script> 
      <script src="https://b2bstreets.com/assets/js/bootstrap-select.min.js"></script> 
      <script src="https://b2bstreets.com/assets/js/wow.min.js"></script> 
      <script src="https://b2bstreets.com/assets/js/scripts.js"></script>
   </body>
</html>
