@extends('layouts.front')
@section('content')
@include('front.seo-keywords') 


 <div class="breadcrumb">
            <div class="container">
               <div class="breadcrumb-inner">
                  <ul class="list-inline list-unstyled">
                     <li><a href="{{route('Front.Index')}}">Home</a></li>
                     <li class='active'>Complaint</li>
                  </ul>
               </div>
               <!-- /.breadcrumb-inner -->
            </div>
            <!-- /.container -->
         </div>
         <!-- /.breadcrumb -->


<section id="compl" style="background-image: url({{ asset('public/images/blog/complaint-banner.jpg') }});background-size: cover; position: relative;height: 400px;">
    <div class="container">
        <div class="row">
            
        </div>
    </div>
</section>
          
 <style>
     .error{
    color: red;
}
 </style>         
<section id="complaint">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                <div class="row">
                    <div class="complaint-box">
                        <div class="complaint-heading">
                            <h2 class="text-center">Complaint Form</h2>
                       @if(Session::has('message'))
                <div class="alert alert-danger"  role="alert">{{Session::get('message')}}</div>
                @elseif(Session::has('success'))
                <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                @endif
                            <p class="text-center">Please use the form below to share your concern with us and we will come back to you with the right solution.</p>
                            <hr>
                            <h5>Please tell the type of complaint you want to report</h5>

                            <form method="post" action="{{route('Front.Add.Complaint')}}">
                                @csrf
                                <div class="form-group">
                                <label for="exampleInputtext">Company Name*</label>
                                <input type="text" class="form-control" id="company_name" name="company_name" placeholder="Provide your company name" value="{{old('company_name')}}">
                                @if($errors->has('company_name'))
                                <div class="error">{{ $errors->first('company_name') }}</div>
                                @endif
                               </div>
                                <div class="form-group">
                                <label for="exampleInputtext">Your Name*</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Provide your name" value="{{old('name')}}">
                                @if($errors->has('name'))
                                <div class="error">{{ $errors->first('name') }}</div>
                                @endif
                              </div>
                                <div class="form-group">
                                <label for="exampleInputtext">Complaint Title*</label>
                                <input type="text" class="form-control" id="complain_title" name="complain_title" placeholder="Provide Short Title To Your Complaint" value="{{old('complain_title')}}">
                                @if($errors->has('complain_title'))
                                <div class="error">{{ $errors->first('complain_title') }}</div>
                                @endif
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail1">Communication Email*</label>
                                <input type="text" class="form-control" id="email" name="email" placeholder="Provide email for communication" value="{{old('email')}}">
                                @if($errors->has('email'))
                                <div class="error">{{ $errors->first('email') }}</div>
                                @endif

                              </div>
                                <div class="form-group">
                                <label for="exampleInputtext">Your Contact Information*</label>
                                <input type="text" class="form-control" id="contact_no" name="contact_no" placeholder="Mobile / Cell Phone" value="{{old('contact_no')}}">
                                @if($errors->has('contact_no'))
                                <div class="error">{{ $errors->first('contact_no') }}</div>
                                @endif
                              </div>
                                
                             <div class="form-group">
                                <label for="exampleInputtext" >Complaint Description*</label>
                                 <textarea class="form-control" rows="5" id="description" name="description" value="{{old('description')}}"></textarea>
                                 @if($errors->has('description'))
                                <div class="error">{{ $errors->first('description') }}</div>
                                @endif
                              </div>
                               
                              <button type="submit" class="btn btn-default">Submit</button>
                            </form>
                        </div>
                    </div>
              </div>
            </div>
            
            
        </div>
        
        
    </div>
</section>


@endsection