@extends('layouts.front')
@section('content') 
@include('front.seo-keywords')

<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="{{route('Front.Index')}}">Home</a></li>
				
				<li class='active'>{{$page->title}}</li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->

<div class="body-content outer-top-xs">
	<div class="container">
		<div class="ad-grid">
    <div class="ad-grid-left">
      <h2>{{$page->name}}</h2>
   <ul>
	  {!!$page->content!!}
</ul>

@if($page->short_content !=null)
<div class="ad-high">  
 
    <p>{!!$page->short_content!!}</p>
</div>
@endif
</div>

@include('front.enquiry_form')

</div><!-- /.row -->
	</div><!-- /.container -->
</div>

@endsection 




