@extends('layouts.adminlay')
@section('content')
    <style>
        body {
            background-color: #f1f3f6;
        }

        .header .logo {
            width: 150px;
        }

        .header {
            background-color: #fff;
            padding: 10px 0px;
        }

        .form {
            margin: 60px 0px;
        }

        .submit {
            margin-right: 35px;
        }

        p strong {
            padding: 50px;
        }

        .para-1 {

            padding-bottom: 50px;
            padding-left: 50px;
            padding-right: 50px;
        }

        section {
            margin: 100px 0px;
        }

        .form .container {
            border-radius: 10px;
            box-shadow: 0px 5px 5px #ccc;
            width: 800px;
        }

        .heading h5 {
            margin: 20px 0px;
            font-size: 23px;
        }

        .heading h6 {
            text-align: center;
            font-size: 30px;
        }

        .submit button {
            padding: 7px 20px;
            background-color: #007bff;
            color: white;
            border-radius: 50px;
            border: 0px;
            font-size: 14px;
            box-shadow: 0px 2px 2px #ccc;
            transition: 200ms;
            outline: none;
        }

        .submit button:hover {
            box-shadow: 0px 5px 10px #ccc;
        }

        .form h6 {
            margin-bottom: 40px;
            font-size: 22px;
            font-weight: 600;
            padding-top: 20px;
        }

        .form h5 {
            font-size: 20px;
            padding: 0px 50px;
            color: purple;
            margin-top: 50px;
        }

        .form label {
            color: #222222;
            font-size: 14px;
            margin-bottom: 2px;
            width: 25%;
            font-weight: 600;
            text-align: right;
            padding-right: 50px;
        }

        .form input {
            width: 75%;
            border-radius: 2px;
            border: 1px solid #ccc;
            padding: 7px 10px;
            outline: none;
        }

        .footer {
            padding: 100px 0px;
            background-color: white;
            text-align: center;
        }

        .footer-logo {
            width: 200px;
            margin: auto;
        }

        .form-group {
            display: flex;
            align-items: center;
            margin-bottom: 30px;
        }

        .para {
            padding-bottom: 20px;
        }

        .para-1 {
            color: #8b8b8b;
        }

        .headerr {
            padding: 20px;
            background-color: #971d88;
            color: white;
        }

        form.form .row {
            padding: 0px 50px;
        }

        @media only screen and (max-width:991px) {
            .form-group {
                display: block;
            }
             section {
            margin: 00px 0px;
        }
            body > .header .navbar {
                margin-left: 00px;
            }

            .form-group label {
                width: 100%;
                text-align: left;
            }

            .form label {
                padding-right: 0px;
            }

            form.form .row {
                padding: 15px;
            }

            .form input {
                width: 100%;
            }

            .submit {
                margin-right: 0px;
            }

            .form h6 {
                margin-bottom: 20px;
                font-size: 22px;
                font-weight: 600;
                padding-top: 0px;
            }
            p strong{
                padding: 0px;
            }

            .para-1 {

                padding-bottom: 0px;
                padding-left: 0px;
                padding-right: 0px;
            }

            .col-lg-12.checkboxx input {
                width: auto;
                vertical-align: top;
            }
            .col-lg-12.checkboxx{
                padding-left: 0px !important;
            }
            .logo{
                display: none;
            }
            .text-right{
                text-align: left !important;
            }
            .submit {
                margin-right: 0px;
                margin-bottom: 30px;
            }
        }
        
        
        

        .col-lg-12.checkboxx input {
            width: auto;
            vertical-align: top;
        }

        .col-lg-12.checkboxx {
            padding-left: 275px;
            margin-bottom: 50px;
        }

        .col-lg-12.checkboxx label {
            width: 80%;
            text-align: left;
            margin-left: 5px;

        }

    </style>
    <section>


        <div class="container p-0 bg-white">

            <div class="headerr">
                <h4 class="text-center">Bank Details </h4>
            </div>
            {{-- @if (empty($bank)) --}}
            <form id="myform" action="{{ url('/addbank_data') }}" method="post" class="form">
                @csrf
                <!-- <form class="form"> -->
                {{-- @endif --}}
                <div class="row align-items-center">
                    <div class="col-lg-12 col-6">
                        <div class="heading">
                            <h6>Benificiary</h6>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">

                            <input type="hidden" name="id" value="{{ $bank['id'] }}">
                            <input type="hidden" name="user_id" value="{{ $seller['id'] }}">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Currency Type :</label>
                            <input type="text" name="currency_type" value="{{ $bank['currency_type'] }}">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Beneficiary (Legal name on the Bank account) :</label>
                            <input type="text" name="bank_account_benificiary"
                                value="{{ $bank['bank_account_benificiary'] }}">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Beneficiary Address :</label>
                            <input type="text" name="beneficiary_address" value="{{ $bank['beneficiary_address'] }}">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Beneficiary City, Country :</label>
                            <input type="text" name="beneficiary_city" value="{{ $bank['beneficiary_city'] }}">
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="heading">
                            <h6>Receiving Bank</h6>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Bank Swift code :</label>
                            <input type="text" name="swift_code_receiving" value="{{ $bank['swift_code_receiving'] }}">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Bank Name:</label>
                            <input type="text" name="bank_name_receiving" value="{{ $bank['bank_name_receiving'] }}">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Bank Account Number :</label>
                            <input type="text" name="bank_account_receiving"
                                value="{{ $bank['bank_account_receiving'] }}">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>IBAN :</label>
                            <input type="text" name="iban_receiving" value="{{ $bank['iban_receiving'] }}">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>City:</label>
                            <input type="text" name="city_receiving" value="{{ $bank['city_receiving'] }}">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Country :</label>
                            <input type="text" name="country_receiving" value="{{ $bank['country_receiving'] }}">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Sort code:</label>
                            <input type="text" name="sort_code_receivings" value="{{ $bank['sort_code_receivings'] }}">
                        </div>
                    </div>
                </div>
                <hr>
                <h5 class="text-center">Intermediary Bank or Correspondent Bank for Further Credit To (FFC) :</h5>
                <p class="text-center para">This section only applies when intermediary Banks are involved in the wire
                    transfer.
                </p>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="heading">
                            <h6>Domestic (US) Intermediary Bank</h6>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>ABA Routing OR Bank SWIFT code :</label>
                            <input type="text" name="aba_routing" value="{{ $bank['aba_routing'] }}">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Bank Account Number :</label>
                            <input type="text" name="bank_account_domestic" value="{{ $bank['bank_account_domestic'] }}">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Bank Name:</label>
                            <input type="text" name="bank_name_domestic" value="{{ $bank['bank_name_domestic'] }}">
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="heading">
                            <h6>International Intermediary Bank</h6>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Bank SWIFT Code :</label>
                            <input type="text" name="swift_code_international"
                                value="{{ $bank['swift_code_international'] }}">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>IBAN :</label>
                            <input type="text" name="iban_international" value="{{ $bank['iban_international'] }}">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Bank Name:</label>
                            <input type="text" name="bank_name_internatinal"
                                value="{{ $bank['bank_name_internatinal'] }}">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Bank Account Number :</label>
                            <input type="text" name="bank_account_international"
                                value="{{ $bank['bank_account_international'] }}">
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>City:</label>
                            <input type="text" name="city_international" value="{{ $bank['city_international'] }}">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Country :</label>
                            <input type="text" name="country_international" value="{{ $bank['country_international'] }}">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Sort code:</label>
                            <input type="text" name="sort_code_international"
                                value="{{ $bank['sort_code_international'] }}">
                        </div>
                    </div>

                    <div class="col-lg-12 checkboxx">
                        <input type="checkbox" name="agree" id="agree_checkbox" value="yes" />
                        <label for="agree_checkbox">I do here by confirm to share my bank details with X4US Teamwith any
                            oblications.The same details would be used by them for the payout.</label>


                        <div style="display:none; color:red" id="agree_chk_error">
                            Can't proceed as you didn't agree to the terms!
                        </div>

                    </div>
                    @if (empty($bank))
                       
                        <div class="col-lg-12 text-right mt-5">
                            <div class="submit">
                                <button type="submit">Add Bank</button>
                                {{-- <input type="submit" name="submit" value="Submit" /> --}}
                            </div>
                        </div>
                    @endif
            </form>
            <div class="col-lg-12 mt-5">
                <p><strong> Note: * Indicate required fields</strong></p>
                <p class="para-1">Note: Usually IU wires directly to the Beneficiary Bank - if this is the case, the
                    section above is not needed. If the vendor does not provide correspondent / Intermediary bank, assume IU
                    wiring directly to the receiving / beneficiary bank.</p>
            </div>
                <hr>
                <div class="col-lg-12 text-center">
                    
                     @endsection
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js">
        </script>
        <script type="text/javascript">
            $("#myform").on("submit", function(form) {
                if (!$("#agree_checkbox").prop("checked")) {
                    $("#agree_chk_error").show();
                } else {
                    $("#agree_chk_error").hide();
                }
                return false;
            })
        </script>
   

</section>
