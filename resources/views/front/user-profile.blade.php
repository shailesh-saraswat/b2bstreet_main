@extends('layouts.front')
@section('content')
@include('front.seo-keywords')

<style>
    .info{
        color: green;
    }
</style>
      <div class="breadcrumb">
         <div class="container">
            <div class="breadcrumb-inner">
               <ul class="list-inline list-unstyled">
                  <li><a href="{{route('Front.Index')}}">Home</a></li>
                  <li class='active'>User Profile Details</li>
               </ul>
            </div>
         </div>
      </div>
        
       
           <div class="container">
               <div class="profile">  
                   <div class="row ">
                    <div class="col-lg-3 ">
                       <div class="tabs">
                        <ul>
                            <a href="{{route('Front.UserProfile')}}"><li class="active">User Details</li></a>   
                            <!-- <a href="{{route('Front.Notification')}}"><li>Nautification</li></a>  -->
                            <a href="{{url('/edit-user-profile/'.Auth::user()->id)}}"><li>Update Your Profile</li></a> 
                            <a href="{{route('SupAdmin.Logout')}}"><li>Logout </li></a> 
                        </ul> 
                       </div>
                   </div>

                   <div class="col-lg-9">
                <div class="user-name">
                    <div class="col-lg-6">
                        <div class="dp">

                            <img src="{{asset('public/images/1.png')}}">
                        </div>
                        <div class="name">
                            @if(Auth::check())
                            <h5>{{Auth::user()->name}}</h5>
                            <p>{{Auth::user()->address}}</p>
                             @endif  
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="name">
                        <p>Product of Interest :</p>
                        <p>Member Since : <b></b></p>
                    </div>
                </div>
                </div> 
               
               
               

                <div class="user-name">
                    <div class="col-lg-12">
                    <p class="sub-heading">Contact Information</p>
                    </div>
                    <div class="col-lg-6">
                        <ul>
                            @if(Auth::check())
                           <li>Primary Mobile : <span>{{Auth::user()->mobile}} <!-- <b class="info">[verified]</b> --></span></li>
                            <li>Primary Email : <span>{{Auth::user()->email}} <!-- <b class="info">[verified]</b> --></span></li>             
                            <li>Address : <span>{{Auth::user()->address}}</span></li>       
                           @else
                            
                             @endif  
                        </ul>
                    </div>
                    <div class="col-lg-6">
                        <ul>
                            <li>Alternative Mobile : <span>{{Auth::user()->alternate_number}}</span></li>
                            <li>Alternative Email : <span>{{Auth::user()->alternate_email}}</span></li>
                        </ul>
                    </div>
                </div> 
      
               

                <div class="user-name">
                    <div class="col-lg-12">
                    <p class="sub-heading">Company Information</p>
                    </div>
                    <div class="col-lg-6">
                        <ul>
                            <li>Company Name : <span></span></li>
                            <li>Website : <span></span></li>
                        </ul>
                    </div>
                    <div class="col-lg-6">
                        <ul>
                            <li>GSTIN : <span></span></li>
                        </ul>
                    </div>
                </div> 
                <div class="user-name">
                    <div class="col-lg-12">
                    <p class="sub-heading">Bank Account Details</p>
                    </div>
                    <div class="col-lg-6">
                        <ul>
                            <li>IFSC Code : <span></span></li>
                            <li>Bank Name : <span></span></li>
                        </ul>
                    </div>
                    <div class="col-lg-6">
                        <ul>
                            <li>Account Number : <span></span></li>
                            <li>Account Type : <span></span></li>
                        </ul>
                    </div>
                </div> 
                   </div>
                   </div>
            </div>
           </div>
   
 @endsection 