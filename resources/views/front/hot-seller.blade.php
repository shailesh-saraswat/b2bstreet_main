@extends('layouts.front')
@section('content')
@include('front.seo-keywords')
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="{{route('Front.Index')}}">Home</a></li>
				
				<li class='active'><a href="{{route('Front.hotSeller')}}">Hot Seller</a></li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->



<div class="body-content outer-top-xs">
	<section class="section featured-product wow fadeInUp   pt-50 pb-50 mb-50 hot-seller" style=" ">
<div class="container">
<h2 class="section-title1  "><span>Hot </span> Seller</h2>
<div class="products-gridss">
@foreach ($hotseller as $video)
<div class="products">
<div class="product">
<div class="product-image">
<div class="image"><a href="{{ url('/suplier-profile/' . $video['user_id']) }}">
   <img src="{{ asset($video['image']) }}" alt="" style="height:200px; object-fit: contain;padding:25px;"></a> </div>
</div>
<div class="product-info text-left cate-headi">
<h3 class="name"><a
   href="{{url('/'.$video->user['slug'])}}">{{ $video->user['company_name'] }}</a>
</h3>
</div>
</div>
</div>
@endforeach
</div>
<!-- /.home-owl-carousel -->
</div>
</section>
<!-- /.section -->
</div>
@endsection



