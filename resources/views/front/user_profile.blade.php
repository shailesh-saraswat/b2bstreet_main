
<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Meta -->
      <meta charset="utf-8">
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="keywords" content="MediaCenter, Template, eCommerce">
      <meta name="robots" content="all">
      <title>B2B Streets</title>
      <!-- Bootstrap Core CSS -->
      <link rel="stylesheet" href="assets/css/bootstrap.min.css">
      <!-- Customizable CSS -->
      <link rel="stylesheet" href="assets/css/main.css">
      <link rel="stylesheet" href="assets/css/blue.css">
      <link rel="stylesheet" href="assets/css/simple-line-icons.css">
      <link rel="stylesheet" href="assets/css/owl.carousel.css">
      <link rel="stylesheet" href="assets/css/owl.transitions.css">
      <link rel="stylesheet" href="assets/css/animate.min.css">
      <link rel="stylesheet" href="assets/css/rateit.css">
      <link rel="stylesheet" href="assets/css/anuj.css">
      <link rel="stylesheet" href="assets/css/bootstrap-select.min.css">
      <!-- Icons/Glyphs -->
      <link rel="stylesheet" href="assets/css/font-awesome.css">
      <!-- Fonts --> 
      <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
      <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,600italic,700,700italic,800' rel='stylesheet' type='text/css'>
      <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,500i,600,700,800,900" rel="stylesheet">
   </head>
   <body class="cnt-home">
      <style>
.main-header.new-header {
    grid-template-columns: 100%;
}
</style>
  <link href="https://fonts.googleapis.com/css2?family=Public+Sans:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="width:70%;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h2>Enquiry now</h2>
        </div>
        <div class="modal-body">
         <div class="">
			<form action="#" id="conform2">
    <label class="info-title" for="exampleInputName">Your Name <span>*</span></label>
		    <input type="text" id="name2" name="name2" class="form-control unicase-form-control text-input cal" placeholder="">

     <label class="info-title" for="exampleInputEmail1">Email Address <span>*</span></label>
		    <input class="form-control unicase-form-control text-input cal" type="email" id="emailx2" name="emailx2"  placeholder="">

 <label class="info-title" for="exampleInputEmail1">Contact <span>*</span></label>
		    <input class="form-control unicase-form-control text-input cal" type="number" id="phone2" name="phone2"  placeholder="">

     <label class="info-title" for="exampleInputTitle">Location <span>*</span></label>
		    <input  type="text" id="cont2" name="cont2" class="form-control unicase-form-control text-input cal" placeholder="Location">
			
			 <label class="info-title" for="exampleInputComments">Your Requirements<span>*</span></label>
		    <textarea class="form-control unicase-form-control" name="msg2" id="msg2" ></textarea>
			
    <input type="button" id="btns2" name="btns2" onclick="send22()" value="Submit">
	  <p id="trimb3" style="color:red;"></p>
  </form>
		</div>
        </div>
       </div>
      
    </div>
  </div>
  
<header class="header-style-1"> 
  <div class="container">
	<div class="main-header new-header"> 
			<div class="logo-holder"> 
			<div class="logo"> 
					<a href="index-2.php"> <img src="assets/images/B2B-logo.svg" alt="logo"> </a>
				</div>

<div class="menu-essential">
    <ul>
        <li><a href=""><img src="assets/images/verified.svg"> Covid-19 Supplies</a></li>
        <li><a href=""><img src="assets/images/buyer-1.svg"> For Buyers</a></li>
        <li><a href=""><img src="assets/images/delivery-man.svg"> For Suppliers</a></li>
        <li><a href=""><img src="assets/images/information-1.svg"> Need Help</a></li> 
        <li class="mr-20"><a href=""><img src="assets/images/user-1.svg"> Login</a></li> 
        <li><a href=""><img src="assets/images/add-user.svg"> Register</a></li> 
        
    </ul> 
</div>
				 
 
			</div> 

   
  </div>
  </div>
  </header>
  <div class="header-style-1 new-cate"> 
  <div class="container">
	<div class=""> 
			<div class="logo-holder"> 
			
				 
 <div class="header-nav animate-dropdown red-menu">
  <div class=""> 
      <div class="yamm navbar navbar-default" role="navigation">
        <div class="navbar-header">
       <button data-target="#mc-horizontal-menu-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button"> 
       <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <div class="nav-bg-class container">
          <div class="navbar-collapse collapse" id="mc-horizontal-menu-collapse">
            <div class="nav-outer">
              <ul class="nav navbar-nav">
                <!-- <li class="active dropdown yamm-fw hv-item double-menu"> <a href="index.php">All Categories <i class="fas fa-chevron-down"></i> </a>  -->
				
				
				
				<!-- <ul class="dropdown-menu"> -->
				
				
<!-- <div class="mega-menu container"> -->
	<!-- <div class="mega-menu-grid"> -->
		<!-- <div class="mega-menu-content"> -->
			<!-- <h2>Industrial Plants &amp; Machines</h2> -->
				<!-- <ul class="links list-unstyled"> -->
					<!-- <li><a href="sub-category.php?url=food-processing-plants"><i class="fas fa-chevron-right"></i> Food Processing Plants</a></li> -->
					<!-- <li><a href="sub-category.php?url=pet-stretch-blowing-machine"><i class="fas fa-chevron-right"></i> Pet Stretch Blowing Machine</a></li> -->
					<!-- <li><a href="sub-category.php?url=industrial-machinery"><i class="fas fa-chevron-right"></i> Industrial Machinery</a></li> -->
					<!-- <li><a href="sub-category.php?url=conveying-system"><i class="fas fa-chevron-right"></i> Conveying System </a></li> -->
					<!-- <li><a href="sub-category.php?url=industrial-heating-ventilation-and-air-conditioning-equipment-and-components"><i class="fas fa-chevron-right"></i> Industrial Heating, Ventilation, and Air Conditioning Equipment and Components</a></li> -->
				<!-- </ul> -->

		<!-- </div> -->
		<!-- <div class="mega-menu-content"> -->
			<!-- <h2>Electronics &amp; Electricals</h2> -->
				<!-- <ul class="links list-unstyled">  <li><a href="sub-category.php?url=wires-and-cables"><i class="fas fa-chevron-right"></i> Wires and Cables </a></li> <li><a href="sub-category.php?url=earthing-equipments"><i class="fas fa-chevron-right"></i> Earthing Equipments</a></li> <li><a href="sub-category.php?url=industrial-siren-and-alarm-system"><i class="fas fa-chevron-right"></i> Industrial Siren and Alarm System</a></li> <li><a href="sub-category.php?url=electric-panel"><i class="fas fa-chevron-right"></i> Electric Panel</a></li> <li><a href="sub-category.php?url=lights"><i class="fas fa-chevron-right"></i> Lights </a></li> <li><a href="sub-category.php?url=sound-systems"><i class="fas fa-chevron-right"></i> Sound Systems</a></li></ul> -->

		<!-- </div><div class="mega-menu-content"> -->
			<!-- <h2>Handicrafts &amp; Decoratives</h2> -->
				<!-- <ul class="links list-unstyled"> -->
					<!-- <li><a href="sub-category.php?url=corporate-business-gifts"><i class="fas fa-chevron-right"></i> Corporate &amp; Business Gifts</a></li> -->
					<!-- <li><a href="sub-category.php?url=statues"><i class="fas fa-chevron-right"></i> Statues</a></li> -->
				<!-- </ul> -->
<!-- <div class="mega-menu-content bor-none"> -->
			<!-- <h2>Automobile Parts &amp; Spares</h2> -->
				<!-- <ul class="links list-unstyled"> -->
					<!-- <li><a href="sub-category.php?url=automobile-fittings-components"><i class="fas fa-chevron-right"></i> Automobile Fittings &amp; Components</a></li> -->
				<!-- </ul> -->
		<!-- </div><div class="mega-menu-content bor-none"> -->
			<!-- <h2>Service Providers </h2> -->
				<!-- <ul class="links list-unstyled"> -->
					<!-- <li><a href="sub-category.php?url=automobile-fittings-components"><i class="fas fa-chevron-right"></i>Consultancy Services </a></li> -->
				<!-- </ul> -->
		<!-- </div> -->

		<!-- </div><div class="mega-menu-content"> -->
			<!-- <h2>Industrial Supplies</h2> -->
				<!-- <ul class="links list-unstyled"> -->
					<!-- <li><a href="sub-category.php?url=bottles"><i class="fas fa-chevron-right"></i>Heating Equipment </a></li> -->
					<!-- <li><a href="sub-category.php?url=lunch-boxes"><i class="fas fa-chevron-right"></i>Rubber and Rubber Products</a></li> -->
					
				<!-- </ul> -->
<!-- <div class="mega-menu-content bor-none"> -->
			<!-- <h2>Instrumentation &amp; Control Equipment</h2> -->
				<!-- <ul class="links list-unstyled"> -->
					<!-- <li><a href="sub-category.php?url=variable-frequency-drive-vfd"><i class="fas fa-chevron-right"></i> Variable Frequency Drive (VFD)</a></li> -->
				<!-- </ul> -->
<!-- <div class="mega-menu-content bor-none"> -->
			<!-- <h2>Adhesive &amp; Pressure Sensitive Tapes</h2> -->
				<!-- <ul class="links list-unstyled"> -->
					<!-- <li><a href="sub-category.php?url=tapes"><i class="fas fa-chevron-right"></i> Tapes</a></li> -->
				<!-- </ul> -->
		<!-- </div> -->
		<!-- </div> -->

		<!-- </div><div class="mega-menu-content"> -->
			<!-- <h2>Home Supplies</h2> -->
				<!-- <ul class="links list-unstyled"> -->
					<!-- <li><a href="sub-category.php?url=bottles"><i class="fas fa-chevron-right"></i> Bottles</a></li> -->
					<!-- <li><a href="sub-category.php?url=lunch-boxes"><i class="fas fa-chevron-right"></i> Lunch Boxes </a></li> -->
					<!-- <li><a href="sub-category.php?url=pooja-items"><i class="fas fa-chevron-right"></i> Pooja Items </a></li> -->
				<!-- </ul> -->
<!-- <div class="mega-menu-content bor-none"> -->
			<!-- <h2>Automobile Interior, Exterior &amp; Accessories</h2> -->
				<!-- <ul class="links list-unstyled"> -->
					<!-- <li><a href="sub-category.php?url=automobile-interior-exterior-and-accessories"><i class="fas fa-chevron-right"></i> Automobile Interior, Exterior and Accessories</a></li> -->
				<!-- </ul> -->


		<!-- </div> -->

		<!-- </div> -->
		
		
		

		
		
		
	<!-- </div> -->
<!-- </div> -->
<!-- </ul> -->
				<!-- </li>  -->
				
				
				 
<nav>
  <ul class="ul-reset">
    <li><a href="#">Home</a></li>
    <li class="droppable">
      <a href="#">Category Two</a>
      <div class="mega-menu12">
       <div class="mega-menu container">
	<div class="mega-menu-grid">
		<div class="mega-menu-content">
			<h2>Industrial Plants &amp; Machines</h2>
				<ul class="links list-unstyled">
					<li><a href="sub-category.php?url=food-processing-plants"><i class="fas fa-chevron-right"></i> Food Processing Plants</a></li>
					<li><a href="sub-category.php?url=pet-stretch-blowing-machine"><i class="fas fa-chevron-right"></i> Pet Stretch Blowing Machine</a></li>
					<li><a href="sub-category.php?url=industrial-machinery"><i class="fas fa-chevron-right"></i> Industrial Machinery</a></li>
					<li><a href="sub-category.php?url=conveying-system"><i class="fas fa-chevron-right"></i> Conveying System </a></li>
					<li><a href="sub-category.php?url=industrial-heating-ventilation-and-air-conditioning-equipment-and-components"><i class="fas fa-chevron-right"></i> Industrial Heating, Ventilation, and Air Conditioning Equipment and Components</a></li>
				</ul>

		</div>
		<div class="mega-menu-content">
			<h2>Electronics &amp; Electricals</h2>
				<ul class="links list-unstyled">  <li><a href="sub-category.php?url=wires-and-cables"><i class="fas fa-chevron-right"></i> Wires and Cables </a></li> <li><a href="sub-category.php?url=earthing-equipments"><i class="fas fa-chevron-right"></i> Earthing Equipments</a></li> <li><a href="sub-category.php?url=industrial-siren-and-alarm-system"><i class="fas fa-chevron-right"></i> Industrial Siren and Alarm System</a></li> <li><a href="sub-category.php?url=electric-panel"><i class="fas fa-chevron-right"></i> Electric Panel</a></li> <li><a href="sub-category.php?url=lights"><i class="fas fa-chevron-right"></i> Lights </a></li> <li><a href="sub-category.php?url=sound-systems"><i class="fas fa-chevron-right"></i> Sound Systems</a></li></ul>

		</div><div class="mega-menu-content">
			<h2>Handicrafts &amp; Decoratives</h2>
				<ul class="links list-unstyled">
					<li><a href="sub-category.php?url=corporate-business-gifts"><i class="fas fa-chevron-right"></i> Corporate &amp; Business Gifts</a></li>
					<li><a href="sub-category.php?url=statues"><i class="fas fa-chevron-right"></i> Statues</a></li>
				</ul>
<div class="mega-menu-content bor-none">
			<h2>Automobile Parts &amp; Spares</h2>
				<ul class="links list-unstyled">
					<li><a href="sub-category.php?url=automobile-fittings-components"><i class="fas fa-chevron-right"></i> Automobile Fittings &amp; Components</a></li>
				</ul>
		</div><div class="mega-menu-content bor-none">
			<h2>Service Providers </h2>
				<ul class="links list-unstyled">
					<li><a href="sub-category.php?url=automobile-fittings-components"><i class="fas fa-chevron-right"></i>Consultancy Services </a></li>
				</ul>
		</div>

		</div>
		<div class="mega-menu-content">
			<h2>Industrial Supplies</h2>
				<ul class="links list-unstyled">
					<li><a href="sub-category.php?url=bottles"><i class="fas fa-chevron-right"></i>Heating Equipment </a></li>
					<li><a href="sub-category.php?url=lunch-boxes"><i class="fas fa-chevron-right"></i>Rubber and Rubber Products</a></li>
					
				</ul>
		<div class="mega-menu-content bor-none">
			<h2>Instrumentation &amp; Control Equipment</h2>
				<ul class="links list-unstyled">
					<li><a href="sub-category.php?url=variable-frequency-drive-vfd"><i class="fas fa-chevron-right"></i> Variable Frequency Drive (VFD)</a></li>
				</ul>

		</div>

		</div>
		<div class="mega-menu-content">
			<h2>Home Supplies</h2>
				<ul class="links list-unstyled">
					<li><a href="sub-category.php?url=bottles"><i class="fas fa-chevron-right"></i> Bottles</a></li>
					<li><a href="sub-category.php?url=lunch-boxes"><i class="fas fa-chevron-right"></i> Lunch Boxes </a></li>
					<li><a href="sub-category.php?url=pooja-items"><i class="fas fa-chevron-right"></i> Pooja Items </a></li>
				</ul>
 <div class="mega-menu-content bor-none">
			<h2>Adhesive &amp; Pressure Sensitive Tapes</h2>
				<ul class="links list-unstyled">
					<li><a href="sub-category.php?url=tapes"><i class="fas fa-chevron-right"></i> Tapes</a></li>
				</ul>
		</div>

		</div>
		
		
		

		
		
		
	</div>
</div>
      </div><!-- .mega-menu-->
    </li><!-- .droppable -->
    <li class="droppable">
      <a href="#">Category Two</a>
      <div class="mega-menu12">
       <div class="mega-menu container">
	<div class="mega-menu-grid">
		<div class="mega-menu-content">
			<h2>Industrial Plants &amp; Machines</h2>
				<ul class="links list-unstyled">
					<li><a href="sub-category.php?url=food-processing-plants"><i class="fas fa-chevron-right"></i> Food Processing Plants</a></li>
					<li><a href="sub-category.php?url=pet-stretch-blowing-machine"><i class="fas fa-chevron-right"></i> Pet Stretch Blowing Machine</a></li>
					<li><a href="sub-category.php?url=industrial-machinery"><i class="fas fa-chevron-right"></i> Industrial Machinery</a></li>
					<li><a href="sub-category.php?url=conveying-system"><i class="fas fa-chevron-right"></i> Conveying System </a></li>
					<li><a href="sub-category.php?url=industrial-heating-ventilation-and-air-conditioning-equipment-and-components"><i class="fas fa-chevron-right"></i> Industrial Heating, Ventilation, and Air Conditioning Equipment and Components</a></li>
				</ul>

		</div>
		<div class="mega-menu-content">
			<h2>Electronics &amp; Electricals</h2>
				<ul class="links list-unstyled">  <li><a href="sub-category.php?url=wires-and-cables"><i class="fas fa-chevron-right"></i> Wires and Cables </a></li> <li><a href="sub-category.php?url=earthing-equipments"><i class="fas fa-chevron-right"></i> Earthing Equipments</a></li> <li><a href="sub-category.php?url=industrial-siren-and-alarm-system"><i class="fas fa-chevron-right"></i> Industrial Siren and Alarm System</a></li> <li><a href="sub-category.php?url=electric-panel"><i class="fas fa-chevron-right"></i> Electric Panel</a></li> <li><a href="sub-category.php?url=lights"><i class="fas fa-chevron-right"></i> Lights </a></li> <li><a href="sub-category.php?url=sound-systems"><i class="fas fa-chevron-right"></i> Sound Systems</a></li></ul>

		</div><div class="mega-menu-content">
			<h2>Handicrafts &amp; Decoratives</h2>
				<ul class="links list-unstyled">
					<li><a href="sub-category.php?url=corporate-business-gifts"><i class="fas fa-chevron-right"></i> Corporate &amp; Business Gifts</a></li>
					<li><a href="sub-category.php?url=statues"><i class="fas fa-chevron-right"></i> Statues</a></li>
				</ul>
<div class="mega-menu-content bor-none">
			<h2>Automobile Parts &amp; Spares</h2>
				<ul class="links list-unstyled">
					<li><a href="sub-category.php?url=automobile-fittings-components"><i class="fas fa-chevron-right"></i> Automobile Fittings &amp; Components</a></li>
				</ul>
		</div><div class="mega-menu-content bor-none">
			<h2>Service Providers </h2>
				<ul class="links list-unstyled">
					<li><a href="sub-category.php?url=automobile-fittings-components"><i class="fas fa-chevron-right"></i>Consultancy Services </a></li>
				</ul>
		</div>

		</div>
		<div class="mega-menu-content">
			<h2>Industrial Supplies</h2>
				<ul class="links list-unstyled">
					<li><a href="sub-category.php?url=bottles"><i class="fas fa-chevron-right"></i>Heating Equipment </a></li>
					<li><a href="sub-category.php?url=lunch-boxes"><i class="fas fa-chevron-right"></i>Rubber and Rubber Products</a></li>
					
				</ul>
		<div class="mega-menu-content bor-none">
			<h2>Instrumentation &amp; Control Equipment</h2>
				<ul class="links list-unstyled">
					<li><a href="sub-category.php?url=variable-frequency-drive-vfd"><i class="fas fa-chevron-right"></i> Variable Frequency Drive (VFD)</a></li>
				</ul>

		</div>

		</div>
		<div class="mega-menu-content">
			<h2>Home Supplies</h2>
				<ul class="links list-unstyled">
					<li><a href="sub-category.php?url=bottles"><i class="fas fa-chevron-right"></i> Bottles</a></li>
					<li><a href="sub-category.php?url=lunch-boxes"><i class="fas fa-chevron-right"></i> Lunch Boxes </a></li>
					<li><a href="sub-category.php?url=pooja-items"><i class="fas fa-chevron-right"></i> Pooja Items </a></li>
				</ul>
 <div class="mega-menu-content bor-none">
			<h2>Adhesive &amp; Pressure Sensitive Tapes</h2>
				<ul class="links list-unstyled">
					<li><a href="sub-category.php?url=tapes"><i class="fas fa-chevron-right"></i> Tapes</a></li>
				</ul>
		</div>

		</div>
		
		
		

		
		
		
	</div>
</div>
      </div><!-- .mega-menu-->
    </li><!-- .droppable -->
    <li><a href="#">Category Three</a></li>
    <li><a href="#">Category Four</a></li>
    <li><a href="#">Category Five</a></li>
    <li><a href="#">All Category</a></li>
  </ul><!-- .container .ul-reset -->
</nav>
                 
              </ul>
			  
              <!-- /.navbar-nav -->
           
            </div>
            <!-- /.nav-outer --> 
          </div>
          <!-- /.navbar-collapse --> 
          
        </div>
        <!-- /.nav-bg-class --> 
      </div>
      <!-- /.navbar-default --> 
    </div>
    <!-- /.container-class --> 
     
  </div>
			</div> 

   
  </div>
  </div>
  </div>
  <div class="menu-zindex">
<script>
(function () {
    var oldVal2;
    var checkLength = function (val2) {
        $('#nearx2').html(val2);
    }
    $('#nearu2').bind('DOMAttrModified textInput input change keypress paste', function () {
        var val2 = this.value;
        if (val2 !== oldVal2) {
            oldVal2 = val2;
            checkLength(val2);
        }
		 $.ajax({
            type:'POST',
            url:'nearyou2.php',
			 data:'nearFrmSubmit=1&oldVal2='+oldVal2,
            beforeSend: function () {
              },
            success:function(msg){
				$('#near2').html(msg);
			    }
		});
    });
}());
 </script> 		        
       
   
 
 



<style> 









</style>
 


      <!-- ============================================== HEADER : END ============================================== -->
      <div class="breadcrumb">
         <div class="container">
            <div class="breadcrumb-inner">
               <ul class="list-inline list-unstyled">
                  <li><a href="index.php">Home</a></li>
                  <li class='active'>Account</li>
               </ul>
            </div>
         </div>
      </div>
        
       
           <div class="container">
               <div class="profile">  
                   <div class="row ">
                    <div class="col-lg-3 ">
                       <div class="tabs">
                        <ul>
                            <a href="user_profile.php"><li class="active">User Details</li></a>   
                            <a href="nautification.php"><li>Nautification</li></a> 
                            <a href="edit_user_profile.php"><li>Update Your Profile</li></a> 
                        </ul> 
                       </div>
                   </div>

                   <div class="col-lg-9">
                <div class="user-name">
                    <div class="col-lg-6">
                        <div class="dp">
                            <img src="images/proimg.png">
                        </div>
                        <div class="name">
                            <h5>Anuj Kumar</h5>
                            <p>E-177/5 Sec-91 Faridabad Haryana - 121003</p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="name">
                        <p>Product of Interest :</p>
                        <p>Member Since : <b>1 Year</b></p>
                    </div>
                </div>
                </div> 
                <div class="user-name">
                    <div class="col-lg-12">
                    <p class="sub-heading">Contact Information</p>
                    </div>
                    <div class="col-lg-6">
                        <ul>
                            <li>Primary Mobile : <span>91 - 8826883562 <b>[verified]</b></span></li>
                            <li>Primary Email : <span>anuj63672@gmail.com</span></li>
                            <li>Address : <span>E-177/5 Sec-91 Faridabad Haryana - 121003</span></li>
                        </ul>
                    </div>
                    <div class="col-lg-6">
                        <ul>
                            <li>Alternative Mobile : <span></span></li>
                            <li>Alternative Email : <span></span></li>
                        </ul>
                    </div>
                </div> 
                <div class="user-name">
                    <div class="col-lg-12">
                    <p class="sub-heading">Company Information</p>
                    </div>
                    <div class="col-lg-6">
                        <ul>
                            <li>Company Name : <span></span></li>
                            <li>Website : <span></span></li>
                        </ul>
                    </div>
                    <div class="col-lg-6">
                        <ul>
                            <li>GSTIN : <span></span></li>
                        </ul>
                    </div>
                </div> 
                <div class="user-name">
                    <div class="col-lg-12">
                    <p class="sub-heading">Bank Account Details</p>
                    </div>
                    <div class="col-lg-6">
                        <ul>
                            <li>IFSC Code : <span></span></li>
                            <li>Bank Name : <span></span></li>
                        </ul>
                    </div>
                    <div class="col-lg-6">
                        <ul>
                            <li>Account Number : <span></span></li>
                            <li>Account Type : <span></span></li>
                        </ul>
                    </div>
                </div> 
                   </div>
                   </div>
            </div>
           </div>
   
      
      
<!-- ============================================================= FOOTER ============================================================= -->
<footer id="footer" class="footer color-bg">
  <div class="footer-bottom">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-3">
          <div class="module-heading">
            <h4 class="module-title">About Us</h4>
          </div>
          <!-- /.module-heading -->
          <div style=" color:#666;"><p>B2bstreets is one of the india&rsquo;s leading online B2B marketplace, making online media presence of any Business to showcase it worlwide, connecting buyers with suppliers, Importers with Exporters. B2bstreets.com is totally focused on making worldwide presence of manufactur </div>
          
          <!-- /.module-body --> 
        </div>
        <!-- /.col -->
        
        <div class="col-xs-12 col-sm-6 col-md-2">
          <div class="module-heading">
            <h4 class="module-title">Important Links</h4>
          </div>
          <!-- /.module-heading -->
          
          <div class="module-body">
            <ul class='list-unstyled'>
              <li class="first"><a href="index.php" title="Home page">Home</a></li>
              <li><a href="about.php" title="About us">About</a></li>
              <li><a href="gallery.php" title="faq">Gallery</a></li>
              <li><a href="advertisement.php" title="Popular Searches">Advertisements</a></li>
			   <li class="last"><a href="contact.php" title="Where is my order?">Contact</a></li>
            </ul>
          </div>
          <!-- /.module-body --> 
        </div>
        <!-- /.col -->
        
        <div class="col-xs-12 col-sm-6 col-md-3">
          <div class="module-heading">
            <h4 class="module-title">Corporation</h4>
          </div>
          <!-- /.module-heading -->
          
          <div class="module-body">
            <ul class='list-unstyled'>
              <li class="first"><a title="Your Account" href="business-news.php">Business News</a></li>
              <li><a title="Information" href="registration.php">Govt. Registration/Certification</a></li>
              <li><a title="Addresses" href="business-loan.php">Business Loan</a></li>
              <li><a title="Addresses" href="man-power.php">Find Quality Man Power</a></li>
              
            </ul>
          </div>
          <!-- /.module-body --> 
        </div>
        <!-- /.col -->
        
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="module-heading">
            <h4 class="module-title">Contact Us</h4>
          </div>
          <!-- /.module-heading -->
          
         <div class="module-body">
            <ul class="toggle-footer" style="">
              
              
                    <li class="media">
                <div class="pull-left"> <span class="icon fa-stack fa-lg"> <i class="fa fa-map-marker fa-stack-1x fa-inverse"></i> </span> </div>
                <div class="media-body"> 
				<span><a href="#"><b>Delhi: </b>No.- 287, GF, Anarkali Complex, Near Jhandewalan Metro Station, Jhandewalan Extension, New Delhi 110055</a></span>
				<span><a href="#"><b>Noida: </b>G-18, Sector-63, Noida (Delhi-NCR), PIN: 201301, India</a></span>
				<span><a href="#"><b>Bengaluru: </b>#46, 7th B main road Jayanagar, 4th Block, Bangalore 560011</a></span>
				
				</div>
              </li>
              <li class="media">
                <div class="pull-left"> <span class="icon fa-stack fa-lg"> <i class="fa fa-mobile fa-stack-1x fa-inverse"></i> </span> </div>
                <div class="media-body">
                 <p> +91-11-45538594<br> </p>
                </div>
</li>
    <li class="media">
              <div class="pull-left"> <span class="icon fa-stack fa-lg"> <i class="fa fa-envelope fa-stack-1x fa-inverse"></i> </span> </div>
                <div class="media-body">
                 <p><a href="#">info@b2bstreets.com</a> </p>
                </div>
    </li>
 
            </ul>
			<div class=" no-padding social">
        <ul class="link">
          <li class="fb pull-left"><a target="_blank" rel="nofollow" href="https://www.facebook.com/B2Bstreets/?ref=br_rs" title="Facebook"></a></li>
          <li class="tw pull-left"><a target="_blank" rel="nofollow" href="https://twitter.com/B2Bstreets" title="Twitter"></a></li>
          <li class="googleplus pull-left"><a target="_blank" rel="nofollow" href="google.com" title="GooglePlus"></a></li>
          <li class="linkedin pull-left"><a target="_blank" rel="nofollow" href="https://www.linkedin.com/company/b2bstreets/" title="Linkedin"></a></li>
          <li class="youtube pull-left"><a target="_blank" rel="nofollow" href="https://www.youtube.com/channel/UCa0M9ZiCkz4eHZUKdubL2bg" title="Youtube"></a></li>
        </ul>
      </div>
          </div>
          <!-- /.module-body --> 
        </div>
      </div>
    </div>
  </div>
	 
  <div class="copyright-bar">
    <div class="container">
      
      <div class="col-xs-12  no-padding">
        <p style="color:#fff;">@ 2019 Copyright By Indian Business Education Media </p>
      </div>
    </div>
  </div>
</footer>

</div>

<script>
function send22(){
var reg = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
var reg2 =/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
	
	var name = $('#name2').val();
	var contact = $('#cont2').val();	
	var phone2 = $('#phone2').val();
    var email = $('#emailx2').val();
	var msg = $('#msg2').val();
	var sub = "a Requirement";
    if(name.trim() == '' ){
		$('#trimb3').show();
		$('#trimb3').html('Please enter Name.');
		$('#trimb3').addClass('toolshow').removeClass('toolhide');
		 $('#name2').focus();
		return false;
	}else if(email.trim() == '' ){
		$('#trimb3').show();
		$('#trimb3').html('Please enter email.');
		$('#trimb3').addClass('toolshow').removeClass('toolhide');
		 $('#emailx2').focus();
		return false;
	}else if(email.trim() != '' && !reg.test(email)){
		$('#trimb3').show();
		$('#trimb3').html('Please enter valid email.');
		$('#trimb3').addClass('toolshow').removeClass('toolhide');
		$('#emailx2').focus();
		return false;
	 }else if(phone2.trim() == '' ){
		$('#trimb3').show();
		$('#trimb3').html('Please enter contact.');
		$('#trimb3').addClass('toolshow').removeClass('toolhide');
		 $('#phone2').focus();
		return false;
  }else if(phone2.trim() != '' && !reg2.test(phone2)){
	  $('#trimb3').show();
	  $('#trimb3').html('Please enter valid contact.');
		$('#trimb3').addClass('toolshow').removeClass('toolhide');
		 $('#phone2').focus();
		return false;	
	}else if(contact.trim() == '' ){
		$('#trimb3').show();
		$('#trimb3').html('Please enter Title.');
		$('#trimb3').addClass('toolshow').removeClass('toolhide');
		 $('#cont2').focus();
		return false;
 }else if(msg.trim() == '' ){
	$('#trimb3').show();
	$('#trimb3').html('Please enter message.');
		$('#trimb3').addClass('toolshow').removeClass('toolhide');
		 $('#msg2').focus();
		return false;		
	}
	else{
        $.ajax({
            type:'POST',
            url:'send_mail3.php',
			 data:'sendFrmSubmit=1&name='+name+'&email='+email+'&contact='+contact+'&msg='+msg+'&sub='+sub+'&phone2='+phone2,
            beforeSend: function () {
                $('#btns2').attr("disabled","disabled");
                },
            success:function(msg){
                if(msg == 'ok'){
					$('#trimb3').show();
                    $('#trimb3').html('<span style="color:green;">Thank you , We will contact you soon.</span>');
					 $('#conform2 .cal').val('');
					 $('#conform2 textarea').val('');
               }else{
				   $('#trimb3').show();
                    $('#trimb3').html('<span style="color:red;">Some problem occurred, please try again.</span>');
                }
                $('#btns2').removeAttr("disabled");
				 $('#conform2 .cal').val('');
			    }
        });
    }
}
</script>	
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5d60f214eb1a6b0be609222b/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
      <!-- JavaScripts placed at the end of the document so the pages load faster -->
      <script src="assets/js/jquery-3.2.1.min.js"></script>
      <script src="assets/js/bootstrap.min.js"></script>
      <script src="assets/js/bootstrap-hover-dropdown.min.js"></script>
      <script src="assets/js/owl.carousel.min.js"></script>
      <script src="assets/js/echo.min.js"></script>
      <script src="assets/js/jquery.easing-1.3.min.js"></script>
      <script src="assets/js/bootstrap-slider.min.js"></script>
      <script src="assets/js/jquery.rateit.min.js"></script>
      <script type="text/javascript" src="assets/js/lightbox.min.js"></script>
      <script src="assets/js/bootstrap-select.min.js"></script>
      <script src="assets/js/wow.min.js"></script>
      <script src="assets/js/scripts.js"></script>
   </body>
</html>