<footer id="footer" class="footer color-bg" style="
">
  <div class="footer-bottom" style="
">
    <div class="container">
      <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 all-add">
          <div class="module-heading">
            <h2 class="section-title1  "><span>Contact</span> Us</h2>
          </div>
          <!-- /.module-heading -->
          
         <div class="module-body">
            <ul class="toggle-footer" style="">
              
              
                    <li class="media">
                
                <div class="media-body"> 
                <span><a href="#"><b>New Delhi </b>No.- 287, GF, Anarkali Complex, Near Jhandewalan Metro Station, Jhandewalan Extension, New Delhi 110055.</a></span>
                <span><a href="#"><b>Noida </b>G-18, Sector-63, Noida (Delhi-NCR), PIN: 201301, India</a></span>
                <span><a href="#"><b>Bengaluru</b>#46, 7th B main road Jayanagar, 4th Block, Bengaluru 560011</a></span>
                
                </div>
              </li>
              
    
 
            </ul>
            
          </div>
          <!-- /.module-body --> 
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 new-f">
          <div class="module-heading foot-he">
            <h4 class="module-title">About Us</h4>
          </div>
          <!-- /.module-heading -->
          <div style=" color:#666;"><p>B2BStreets.com is one of India’s largest online B2B marketplace and the World's 1st,Business Video Directory, providing a unique platform to promote products and services in, Different categories. We help businesses to make their worldwide presence via stellar video bios and connect buyers with suppliers, importers with exporters and traders with manufacturers. </p></div>
          
          <!-- /.module-body --> 
        </div>
        <!-- /.col -->
        
        <div class="col-xs-12 col-sm-6 col-md-2 new-f">
          <div class="module-heading foot-he">
            <h4 class="module-title">Quick Links</h4>
          </div>
          <!-- /.module-heading -->
          
          <div class="module-body">
            <ul class="list-unstyled">
                <li class="first"><a href="https://b2bstreets.com" title="Home page"><i class="fas fa-angle-right"></i> Home</a></li>
                <li><a href="https://b2bstreets.com/pages/about-us" title="About Us "><i class="fas fa-angle-right"></i> About Us</a></li>
                <li><a href="https://b2bstreets.com/gallery" title="Gallery"><i class="fas fa-angle-right"></i> Gallery</a></li>
                <li class="last"><a href="" title="Success story"><i class="fas fa-angle-right"></i> Success story</a></li>
                <li class="last"><a href="" title="Complaint"><i class="fas fa-angle-right"></i> Complaint </a></li>
                <li class="last"><a href="" title="Career"><i class="fas fa-angle-right"></i> Career</a></li>
                <li class="last"><a href="" title="Package"><i class="fas fa-angle-right"></i> Package</a></li>
                <li class="last"><a href="" title="Press Release"><i class="fas fa-angle-right"></i> Press Release</a></li>
                <li class="last"><a href="" title="Blog"><i class="fas fa-angle-right"></i> Blog </a></li>
                <li class="last"><a href="https://b2bstreets.com/contact" title="Contact"><i class="fas fa-angle-right"></i> Contact</a></li>
                
            </ul>
          </div>
        </div>
        <!-- /.col -->
        
        <div class="col-xs-12 col-sm-6 col-md-3 new-f">
          <div class="module-heading foot-he">
            <h4 class="module-title">Corporation</h4>
          </div>
          <!-- /.module-heading -->
          
          <div class="module-body">
           
                        <ul class="list-unstyled">
                            <li><a href="https://b2bstreets.com/pages/government-registration-certification" title="Government Registration/ Certification "><i class="fas fa-angle-right"></i> Government Registration/ Certification</a></li>
                            <li><a href="https://b2bstreets.com/pages/business-loan" title="Business Loan "><i class="fas fa-angle-right"></i> Business Loan</a></li>
                            <li><a href="https://b2bstreets.com/pages/find-quality-manpower" title="Find Quality Manpower "><i class="fas fa-angle-right"></i> Find Quality Manpower</a></li>
                            <li><a href="https://b2bstreets.com/pages/advertisements" title="Advertisements "><i class="fas fa-angle-right"></i> Advertisements</a></li>
                           
                           
              
            </ul>
                      </div>

          <!-- /.module-body --> 
        </div>
        <!-- /.col -->
        
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="module-heading foot-he">
            <h4 class="module-title">Contact Us</h4>
          </div>
          <!-- /.module-heading -->
          
         <div class="module-body cnt-f">
            <ul class="toggle-footer" style="">
            <li class="media"></li>
            <li class="media">
            <div class="pull-left"> <span class="icon fa-stack fa-lg"> <i class="fa fa-mobile fa-stack-1x fa-inverse"></i> </span> </div>
            <div class="media-body">
            <p> +91-11-45538594<br></p>
            </div>
         </li>
    <li class="media">
            <div class="pull-left"> <span class="icon fa-stack fa-lg"> <i class="fa fa-envelope fa-stack-1x fa-inverse"></i> </span> </div>
            <div class="media-body">
            <p>info@b2bstreets.com </p>
            </div>
    </li>
 </ul>
        <div class=" no-padding social">
        <ul class="link">
          <li class="fb pull-left"><a target="_blank" rel="nofollow" href="https://www.facebook.com/B2Bstreets/?ref=br_rs" title="Facebook" disabled=""></a></li>
          <li class="tw pull-left"><a target="_blank" rel="nofollow" href="https://twitter.com/B2Bstreets" title="Twitter" disabled=""></a></li>
         <!--  <li class="googleplus pull-left"><a target="_blank" rel="nofollow" href="#" title="GooglePlus" disabled></a></li> -->
          <li class="linkedin pull-left"><a target="_blank" rel="nofollow" href="https://www.linkedin.com/company/b2bstreets/" title="Linkedin" disabled=""></a></li>
          <li class="youtube pull-left"><a target="_blank" rel="nofollow" href="https://www.youtube.com/channel/UCa0M9ZiCkz4eHZUKdubL2bg" title="Youtube" disabled=""></a></li>
        </ul>
      </div>
          </div>
          <!-- /.module-body --> 
        </div>
      </div>
    </div>
  </div>
     
  <div class="copyright-bar">
    <div class="container">
      
      <div class="col-xs-12  no-padding">
        <p style="color:#fff;">All right reserved @IBEM Solutions LLP.</p>
      </div>
    </div>
  </div>
</footer>

</div>

<script>
function send22(){
var reg = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
var reg2 =/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
    
    var name = $('#name2').val();
    var contact = $('#cont2').val();    
    var phone2 = $('#phone2').val();
    var email = $('#emailx2').val();
    var msg = $('#msg2').val();
    var sub = "a Requirement";
    if(name.trim() == '' ){
        $('#trimb3').show();
        $('#trimb3').html('Please enter Name.');
        $('#trimb3').addClass('toolshow').removeClass('toolhide');
         $('#name2').focus();
        return false;
    }else if(email.trim() == '' ){
        $('#trimb3').show();
        $('#trimb3').html('Please enter email.');
        $('#trimb3').addClass('toolshow').removeClass('toolhide');
         $('#emailx2').focus();
        return false;
    }else if(email.trim() != '' && !reg.test(email)){
        $('#trimb3').show();
        $('#trimb3').html('Please enter valid email.');
        $('#trimb3').addClass('toolshow').removeClass('toolhide');
        $('#emailx2').focus();
        return false;
     }else if(phone2.trim() == '' ){
        $('#trimb3').show();
        $('#trimb3').html('Please enter contact.');
        $('#trimb3').addClass('toolshow').removeClass('toolhide');
         $('#phone2').focus();
        return false;
  }else if(phone2.trim() != '' && !reg2.test(phone2)){
      $('#trimb3').show();
      $('#trimb3').html('Please enter valid contact.');
        $('#trimb3').addClass('toolshow').removeClass('toolhide');
         $('#phone2').focus();
        return false;   
    }else if(contact.trim() == '' ){
        $('#trimb3').show();
        $('#trimb3').html('Please enter Title.');
        $('#trimb3').addClass('toolshow').removeClass('toolhide');
         $('#cont2').focus();
        return false;
 }else if(msg.trim() == '' ){
    $('#trimb3').show();
    $('#trimb3').html('Please enter message.');
        $('#trimb3').addClass('toolshow').removeClass('toolhide');
         $('#msg2').focus();
        return false;       
    }
    else{
        $.ajax({
            type:'POST',
            url:'send_mail3.php',
             data:'sendFrmSubmit=1&name='+name+'&email='+email+'&contact='+contact+'&msg='+msg+'&sub='+sub+'&phone2='+phone2,
            beforeSend: function () {
                $('#btns2').attr("disabled","disabled");
                },
            success:function(msg){
                if(msg == 'ok'){
                    $('#trimb3').show();
                    $('#trimb3').html('<span style="color:green;">Thank you , We will contact you soon.</span>');
                     $('#conform2 .cal').val('');
                     $('#conform2 textarea').val('');
               }else{
                   $('#trimb3').show();
                    $('#trimb3').html('<span style="color:red;">Some problem occurred, please try again.</span>');
                }
                $('#btns2').removeAttr("disabled");
                 $('#conform2 .cal').val('');
                }
        });
    }
}
</script>   
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5d60f214eb1a6b0be609222b/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
       <!-- Subscribe Modal -->
  
      <!-- JavaScripts placed at the end of the document so the pages load faster --> 
      <script src="{{ asset('assets/js/bootstrap-hover-dropdown.min.js') }}"></script> 
      <script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script> 
      <script src="{{ asset('assets/js/echo.min.js') }}"></script> 
      <script src="{{ asset('assets/js/jquery.easing-1.3.min.js') }}"></script> 
      <script src="{{ asset('assets/js/bootstrap-slider.min.js') }}"></script> 
      <script src="{{ asset('assets/js/jquery.rateit.min.js') }}"></script> 
      <script type="text/javascript" src="{{ asset('assets/js/lightbox.min.js') }}"></script> 
      <script src="{{ asset('assets/js/bootstrap-select.min.js') }}"></script> 
      <script src="{{ asset('assets/js/wow.min.js') }}"></script> 
      <script src="{{ asset('assets/js/scripts.js') }}"></script>
   </body>
</html>


