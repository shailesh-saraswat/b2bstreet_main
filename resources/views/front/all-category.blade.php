@extends('layouts.front')
@section('content')
@include('front.seo-keywords')
<div class="breadcrumb">
   <div class="container">
      <div class="breadcrumb-inner">
         <ul class="list-inline list-unstyled">
            <li><a href="{{route('Front.Index')}}">Home</a></li>
            
            <li class='active'><a href="{{route('Front.AllCategory')}}">All Categories</a></li>
         </ul>
      </div><!-- /.breadcrumb-inner -->
   </div><!-- /.container -->
</div><!-- /.breadcrumb -->


<section class="categories-list mb-50">
   <div class="container">
   @php
   $maincategories = \App\Models\Maincategory::where('status', 1) 
   ->get();
   @endphp
   @foreach ($maincategories as $menu)
   <h2 class="section-title1  ">{{ $menu->name }}</h2>      
   <div class=" more-cate-grid">
      @php
      $category = \App\Models\Category::where('maincategory_id', $menu->id)
      ->where('status', 1)
      ->where('name', '!=', 'null')
      ->get();
      @endphp
            @foreach ($category as $cat)          
            <div class="widget blue-widget">               
               <div class="widget-header">
               <h1><figure style=" background: #0487DD none repeat scroll 0 0;">
                  <img src="{{($cat->image)}}"></figure><a href="{{ URL::to('/category/' . $cat->slug) }}">{{ $cat->name }}</a></h1>
               </div>
               @php
            //dd($cat->id);
            $subcategory = \App\Models\SubCategory::where('categories_id', $cat->id)
            ->get();
            @endphp
               @foreach ($subcategory as $sub)
               <div class="widget-body">
                  <ul class="trends">
                     <li><a href="{{URL::to('/products/'.$sub->slug)}}">{{ $sub->name }} 
                        <span class="item-numbers"></span></a></li>                    
                  </ul>
               </div>

               @endforeach
            </div>
            @endforeach
      </div>
      @endforeach
   </div>
</section>

@endsection



