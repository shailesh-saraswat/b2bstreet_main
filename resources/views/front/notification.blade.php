@extends('layouts.front')
@section('content')
      <div class="breadcrumb">
         <div class="container">
            <div class="breadcrumb-inner">
               <ul class="list-inline list-unstyled">
                  <li><a href="{{route('Front.Index')}}">Home</a></li>
                  <li class='active'>Notification</li>
               </ul>
            </div>
         </div>
      </div>
        
       
           <div class="container">
               <div class="profile">  
                   <div class="row ">
                    <div class="col-lg-3 ">
                       <div class="tabs">
                        <ul>
                           <a href="{{route('Front.UserProfile')}}"><li class="active">User Details</li></a>   
                            <a href="{{route('Front.Notification')}}"><li>Nautification</li></a> 
                            <a href="{{route('Front.Edit.UserProfile')}}"><li>Update Your Profile</li></a> 
                             <a href="{{route('SupAdmin.Logout')}}"><li>Logout</li></a> 
                        </ul> 
                       </div>
                   </div>

                   <div class="col-lg-9">
                       <div class="nautification">
                        <table>
                          <tr>
                            <th colspan="8"><label><input type="checkbox" id="select-all" />All</label></th>
                          </tr>
                          <tr>
                            <td><label><input type="checkbox"/></label> Alfreds Futterkiste</td>
                            <td>B2bstreets is one of the india’s leading online B2B marketplace</td>
                            <td>1:00 PM
                              <div class="delete-box">
                                <ul>
                                    <li><a href="" data-toggle="modal" data-target="#reply-forward" title="Reply"><i class="fas fa-reply"></i></a></li>  
                                    <li><a href="" data-toggle="modal" data-target="#reply-forward" title="Forward"><i class="fas fa-share"></i></a></li>  
                                    <li><a href="" title="Delete"><i class="fas fa-trash"></i></a></li>  
                                </ul>
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td><label><input type="checkbox"/></label> Alfreds Futterkiste</td>
                            <td>B2bstreets is one of the india’s leading online B2B marketplace B2B marketplace</td>
                            <td>11:00 AM
                                <div class="delete-box">
                                <ul>
                                    <li><a href="" data-toggle="modal" data-target="#reply-forward" title="Reply"><i class="fas fa-reply"></i></a></li>  
                                    <li><a href="" data-toggle="modal" data-target="#reply-forward" title="Forward"><i class="fas fa-share"></i></a></li>  
                                    <li><a href="" title="Delete"><i class="fas fa-trash"></i></a></li>  
                                </ul>
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td><label><input type="checkbox"/></label> Alfreds Futterkiste</td>
                            <td>B2bstreets is one of the india’s leading online B2B marketplace</td>
                            <td>10:00 AM
                                <div class="delete-box">
                                <ul>
                                    <li><a href="" data-toggle="modal" data-target="#reply-forward" title="Reply"><i class="fas fa-reply"></i></a></li>  
                                    <li><a href="" data-toggle="modal" data-target="#reply-forward" title="Forward"><i class="fas fa-share"></i></a></li>  
                                    <li><a href="" title="Delete"><i class="fas fa-trash"></i></a></li>  
                                </ul>
                              </div>  
                            </td>
                          </tr>
                          <tr>
                            <td><label><input type="checkbox"/></label> Alfreds Futterkiste</td>
                            <td>B2bstreets is one of the india’s leading online B2B marketplace</td>
                            <td>10:00 AM
                                <div class="delete-box">
                                <ul>
                                    <li><a href=""  data-toggle="modal" data-target="#reply-forward" title="Reply"><i class="fas fa-reply"></i></a></li>  
                                    <li><a href=""  data-toggle="modal" data-target="#reply-forward" title="Forward"><i class="fas fa-share"></i></a></li>  
                                    <li><a href="" title="Delete"><i class="fas fa-trash"></i></a></li>  
                                </ul>
                              </div>  
                            </td>
                          </tr>
                          <tr>
                            <td><label><input type="checkbox"/></label> Alfreds Futterkiste</td>
                            <td>B2bstreets is one of the india’s leading online B2B marketplace</td>
                            <td>10:00 AM
                                <div class="delete-box">
                                <ul>
                                    <li><a href=""  data-toggle="modal" data-target="#reply-forward" title="Reply"><i class="fas fa-reply"></i></a></li>  
                                    <li><a href=""  data-toggle="modal" data-target="#reply-forward" title="Forward"><i class="fas fa-share"></i></a></li>  
                                    <li><a href="" title="Delete"><i class="fas fa-trash"></i></a></li>  
                                </ul>
                              </div>  
                            </td>
                          </tr>
                          <tr>
                            <td><label><input type="checkbox"/></label> Alfreds Futterkiste</td>
                            <td>B2bstreets is one of the india’s leading online B2B marketplace</td>
                            <td>10:00 AM
                                <div class="delete-box">
                                <ul>
                                    <li><a href=""  data-toggle="modal" data-target="#reply-forward" title="Reply"><i class="fas fa-reply"></i></a></li>  
                                    <li><a href=""  data-toggle="modal" data-target="#reply-forward" title="Forward"><i class="fas fa-share"></i></a></li>  
                                    <li><a href="" title="Delete"><i class="fas fa-trash"></i></a></li>  
                                </ul>
                              </div>  
                            </td>
                          </tr>
                        </table>
                       </div>
                   </div>
                   </div>
            </div>
           </div>
       
       
       
       
       
       
       
       
       
       
       
       
<div class="container">

  <!-- The Modal -->
  <div class="modal" id="reply-forward">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <form class="reply">
              <label>Write Your Message</label>
                <textarea></textarea>
            </form>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-primary">Send</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>
</div>

   
      
     @endsection 