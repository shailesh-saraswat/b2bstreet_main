@extends('layouts.front')
@section('content')
@include('front.seo-keywords')
<link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/owl.theme.default.min.css') }}">
<style>
   .mySlides img {
   height: 205px;
   }
   .product-list-grid-two {
   padding-left: 10px;
   }
   a.data {
   all: unset;
   }
   footer#footer {
   margin-top: 50px;
   }
   .mySlides {
    display: none !important;
}
.platimumContainer .my-Slide-Container img { 
    object-fit: contain; 
}

.details-top-grid-left .mySlides img {
    width: 70% !important;
    object-fit: cover;
    margin-bottom: 4px;
    border-radius: 10px;
    display: flex;
    align-items: center!important;
    margin: 0 auto;
}

</style>


 


<div class="breadcrumb darker-bread">
   <div class="container">
      <div class="breadcrumb-inner">
         <ul class="list-inline list-unstyled">
            <li><a href="{{ route('Front.Index') }}"> Home</a></li>
            
            <li>
               @if($products)
               <a href="#">{{$products->main_category['name']}}</a><i class="fa fa-chevron-right"></i>
               @endif
            </li>
            
            <li>
                @if($products)<a href="#">{{$products->category['name']}}</a><i class="fa fa-chevron-right"></i>@endif

            </li>

            <li>   @if($products)<a href="#">{{$products->subcategory['name']}}</a>@endif</li>
            <li><a href="#">{{ $products['name'] }}</a></li>           
         </ul>
      </div>
   </div>
</div>
<div class="container-fluid">
   <h2 class="inside-main-heading"> {{ $products['name'] }} <span></span></h2>
   <div class="pl-list-page">
      <div class="pl-left">
         <div class="pl-left-cate">
            <h3>Related Category</h3>
            <ul>
               @foreach ($data as $value)
               <li><a href="{{ URL::to('/products/' . $value->slug) }}">{{ $value->name }}</a></li>
               @endforeach
            </ul>
         </div>
         <!--  <div class="pl-left-cate">
            <h3>Related Brands</h3>
            <ul>
               <li><a href="">Milton Pet Bottles</a></li>
            </ul>
            </div> -->
         <div class="pl-left-cate">
            <h3>Location</h3>
            <input type="text">
            <ul>
               <li><a href="">Delhi</a></li>
               <li><a href="">Noida</a></li>
               <li><a href="">Gurgaon</a></li>
               <li><a href="">Ghaziabad</a></li>
               <li><a href="">East Delhi</a></li>
               <li><a href="">West Delhi</a></li>
            </ul>
         </div>
         <!--  <div class="pl-left-cate">
            <h3>Featured Categories</h3>
            <ul>
                <li>
                    <a href="">
                        <img src="{{ asset('public/images/product/1.jpg') }}">
                        <h4>Pet Jars</h4>
                    </a>
                </li>
                <li>
                    <a href="">
                        <img src="{{ asset('public/images/product/2.jpg') }}">
                        <h4>Medical Equipment</h4>
                    </a>
                </li>
                <li>
                    <a href="">
                        <img src="{{ asset('public/images/product/3.jpg') }}">
                        <h4>Pet Jars</h4>
                    </a>
                </li>
                <li>
                    <a href="">
                        <img src="{{ asset('public/images/product/a.jpg') }}">
                        <h4>Ventilator Circuits</h4>
                    </a>
                </li>
            </ul>
            </div> -->
      </div>
      <div class="pl-right">       
         <div class="pr-list-second ad-container">
            <div>
               @if (count($product_page) > 0 && $product_page !== null)
               <div class="platimumContainer customm">
                  @foreach ($product_page as $data)
                  <div class="platinum-section">
                     <div class="product-list-grid">
                        <div class="product-list-grid-one">
                           <div class="Pro-list-g ">
                              <div class="my-Slide-Container">
                                 <?php 
                                    $pro = $data->id;
                                    $yash= DB::table('product_images')->select('product_image')->where('products_id',$pro)->limit(1)->get();
                                    $result = (array) json_decode($yash);
                                    
                                    foreach($result as $v){?>
                                 <img id="imgchng" src="<?php echo asset($v->product_image); ?>">
                                 <div class="mySlides" style="display:none;">
                                    <img src="<?php echo asset($v->product_image); ?>" style="width:100%">
                                 </div>
                                 <?php }?>
                              </div>
                              <!-- <a class="prev" onclick="plusSlides(-1)">❮</a> -->
                              <!-- <a class="next" onclick="plusSlides(1)">❯</a> -->
                              <div class="caption-container">
                                 <p id="caption">Mountains and fjords</p>
                              </div>
                              <div class="sd ">
                                 <?php 
                                    $pro = $data->id;                                    
                                    $push= DB::table('product_images')->select('id','product_image')->where('products_id',$pro)->get();
                                    $result = (array) json_decode($push);
                                    
                                    foreach($result as $v){
                                    //print_r($v->product_image); ?>
                                 <div class="column">
                                    <img id="imgsrc" class="demo cursor"
                                       src="<?php echo asset($v->product_image); ?>" style="width:100%"
                                       onclick="chang(this.src)" alt="">
                                    <!-- currentSlide(<?php echo $v->id; ?>) -->
                                 </div>
                                 <?php }
                                    //dd($yash);
                                    //print_r($yash);
                                    
                                         ?>
                              </div>
                           </div>
                         
                        </div>
                        <div class="product-list-grid-two">
                          @if($data->user) 
                          <a class="data"
                              href="{{ route('Front.productDetails',[$data->user->slug,$data->slug]) }}">
                              <h3>{!! Str::limit(strip_tags($data->title), 20) !!}</h3>
                           </a>
                           @endif
                           <p class="part-price fa fa-inr"><i
                              aria-hidden="true"></i>{{ $data->price }} </p>
                           <ul>
                              <li><i class="fas fa-angle-left"></i><span>Quality
                                 Available:</span>{{ $data->quality }}
                              </li>
                              <li><i class="fas fa-angle-left"></i><span>Packaging Type:</span>
                                 {{ $data->packaging_type }}
                              </li>
                              <li><i class="fas fa-angle-left"></i><span>Storage Tips:</span>
                                 {{ $data->storage_tips }}
                              </li>
                              <li><i class="fas fa-angle-left"></i><span>Packaging
                                 Size:</span>{{ $data->packaging }}
                              </li>
                              
                           </ul>
                          
                        </div>
                        <div class="product-list-grid-three last-three-div"
                           style="background-color: #e4f2ee94;">
                           <div>
                              <div>
                                 <h2><a href="{{url('/'.$data->user['slug'])}}">{{ $data->user['company_name'] }}</a></h2>
                                 <p>{!! $data->user['address'] !!}</p>
                              </div>
                              <div class="veri">
                                 <div>
                                    <img src="{{ asset('public/images/product/prize.svg') }}">
                                    <h5>Verified</h5>
                                 </div>
                                 <?php
                                    $user_id=$data->user['id'];
                                    $video = DB::table('suplier_banners')->where('user_id',$user_id);
                                  if($video->count()>0) { 
                                    $video=$video->first();?>
                                    <div class="myBtn2" onclick="myvideoFunction('{{$video->video}}')" >
                                    <img src="{{ asset('public/images/product/youtube.svg') }}">
                                    <h5>Company Video</h5>
                                 </div>
                                 <?php }else{ ?>
                                     <div id="myBtn2">
                                    <p>No Video Found</p>
                                    <img src="{{ asset('public/images/product/youtube.svg') }}">
                                    <h5>Company Video</h5>
                                 </div>
                                 <?php }    
                                 ?>

                                   <?php
                                    $user_id=$data->user['id'];
                                    $video = DB::table('interviews')->where('user_id',$user_id);
                                  if($video->count()>0) { 
                                    $video=$video->first();?>
                                    <div class="myBtn3" onclick="myvideoFunction('{{$video->video}}')" >
                                    <img src="{{ asset('public/images/product/interview.svg') }}">
                                    <h5>Owner Interview</h5>
                                 </div>
                                 <?php }else{ ?>
                                     <div id="myBtn3">
                                    <p>No Video Found</p>
                                    <img src="{{ asset('public/images/product/interview.svg') }}">
                                    <h5>Owner Intervie</h5>
                                 </div>
                                 <?php }    
                                 ?>
                              
                              </div>
                           </div>
                           <div class="list-cont-no">
                              <div class="mob-no">
                                 <i class="fas fa-mobile-alt"></i> {{ $data->user['mobile'] }}
                              </div>
                              <div id="myBtn3" data-id="{{ $data['slug'] }}"
                                 class="cont-no-button myBtn3">
                                 <a href="#"> Contact Supplier</a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  @endforeach
               </div>
               @else
               <h1>No Products Found!!!</h1>
               @endif
               <script>
                  var slideIndex = 1;
                  showSlides(slideIndex);
                  
                  function plusSlides(n) {
                      showSlides(slideIndex += n);
                  }
                  
                  function currentSlide(n) {
                      showSlides(slideIndex = n);
                  }
                  
                  function showSlides(n) {
                      var i;
                      var slides = document.getElementsByClassName("mySlides");
                      var dots = document.getElementsByClassName("demo");
                  
                  
                  
                      var captionText = document.getElementById("caption");
                      if (n > slides.length) {
                          slideIndex = 1
                      }
                      if (n < 1) {
                          slideIndex = slides.length
                      }
                      for (i = 0; i < slides.length; i++) {
                          slides[i].style.display = "none";
                      }
                      for (i = 0; i < dots.length; i++) {
                          dots[i].className = dots[i].className.replace("active", "");
                      }
                      slides[slideIndex - 1].style.display = "block";
                      dots[slideIndex - 1].className += " active";
                      captionText.innerHTML = dots[slideIndex - 1].alt;
                  }
               </script>
               <script type="text/javascript">
                  const chang = src => {
                      document.getElementById('imgchng').src = src
                  }
               </script>
               <div class="product-list-ads">
                  <a href="https://www.sunliteorganiconline.com/" target="_blank"><img src="{{ asset('public/images/product/ads4.jpg')}}"></a>
               </div>
               <div class="product-list-ads">
                  <a href="https://chaahathomesinfratech.com/" target="_blank"><img src="{{asset('public/images/product/ads5.jpg')}}"></a>
               </div>
            </div>
            <div class="ad-square-section">
               <div class="prod-right-square-img">
                  <a href="https://www.srmachinery.in/" target="_blank"><img src="{{asset('public/images/product/ads2.jpg')}}"></a>
               </div>
               <div class="needhelp">
                  <div>
                     <div>
                        <h2>What service do you need?
                           <span>B2B Streets will help you</span>
                        </h2>
                        @if(Session::has('message'))
                <div class="alert alert-danger"  role="alert">{{Session::get('message')}}</div>
                @elseif(Session::has('success'))
                <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                @endif
                        <form method="post" action="{{url('products/{slug}')}}">
                           @csrf
                           <div class="form-lable-container">
                              <label>Name</label>
                              <input type="text" name="service_name" placeholder="Full Name" value="{{old('service_name')}}">
                              @if($errors->has('service_name'))
                                <div class="error">{{ $errors->first('service_name') }}</div>
                                @endif
                           </div>
                           <div class="form-lable-container">
                              <label>Company Name</label>
                              <input type="text" name="company_name" id="company_name" placeholder="Xyz Pvt. Ltd " value="{{old('company_name')}}">
                              @if($errors->has('company_name'))
                                <div class="error">{{ $errors->first('company_name') }}</div>
                                @endif
                           </div>
                           <div class="form-lable-container">
                              <label>Phone Number</label>
                              <input type="text" name="phone" id="phone" placeholder="1234567890" value="{{old('phone')}}">
                              @if($errors->has('phone'))
                                <div class="error">{{ $errors->first('phone') }}</div>
                                @endif
                           </div>
                           <div class="form-lable-container">
                              <label>Email ID</label>
                              <input type="text" placeholder="abc@gmail.com" name="service_email"  value="{{old('service_email')}}">
                              @if($errors->has('service_email'))
                                <div class="error">{{ $errors->first('service_email') }}</div>
                                @endif
                           </div>
                           <div class="form-lable-container">
                              <label>Query</label>
                              <textarea name="massage" id="massage" placeholder="Query">{{old('massage')}}</textarea>
                              @if($errors->has('massage'))
                                <div class="error">{{ $errors->first('massage') }}</div>
                                @endif
                           </div>
                           <button type="submit" value="submit">Submit</button>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<style>
     .error{
    color: red;
}
 </style>  
<!-- The Modal -->
<div class="company-video-play" id="custModal1">
   <div id="myModal23" class="modal">
      <div class="modal-content">
         <div class="modal-header" id="clos1">
            <span class="close12">&times;</span>
         </div>
         <div class="modal-body">
         <div class="company-vedio" >
           
         <video width="100%" height="400" controls>
         <source src="" type="video/mp4">
         </video>                  
            </div>
         </div>
        
      </div>
   </div>
</div>
<script>
   // Get the modal
   var modal = document.getElementById("myModal23");
   var span = document.getElementsByClassName("close12")[0];

   span.onclick = function() {
       modal.style.display = "none";
   }
   window.onclick = function(event) {
       if (event.target == modal) {
           modal.style.display = "none";
       }
   }
</script>

<script type="text/javascript">
    // $(document).ready(function () {
 function myvideoFunction(videoid) {
  var modal = document.getElementById("myModal23");
 var videourls = '{{ URL::asset('') }}';
 var response = '<div class="company-vedio" ><video width="100%" height="400" controls><source src="'+videourls+'/'+videoid+'" type="video/mp4"></video></div>';
  modal.style.display = "block";
            $('.modal-body').html(response);
            $('#custModal1').modal('show');

}
</script>



<!-- The Modal -->
<div class="company-video-play" id="custModal">
   <div id="myModal2" class="modal">
      <div class="modal-content">
         <div class="modal-header" id="clos">
            <span class="close1">&times;</span>
         </div>
         <div class="modal-body">
         <div class="company-vedio" >
           
         <video width="100%" height="400" controls>
         <source src="" type="video/mp4">
         </video>                  
            </div>
         </div>
        
      </div>
   </div>
</div>
<script>
   // Get the modal
   var modal = document.getElementById("myModal2");
   
   // Get the button that opens the modal
   // var btn = getElementsByClassName("myBtn2"); //document.getElementById("myBtn2");
   
   // Get the <span> element that closes the modal
   var span = document.getElementsByClassName("close1")[0];
   
   // When the user clicks the button, open the modal 
  //  function myFunction(var videoid) {
  // // document.getElementById("demo").innerHTML = "Hello World";
  //         modal.style.display = "block";
  //  }
   // btn.onclick = function() {
   //     modal.style.display = "block";
   // }
   
   // When the user clicks on <span> (x), close the modal
   span.onclick = function() {
       modal.style.display = "none";
   }
   
   // When the user clicks anywhere outside of the modal, close it
   window.onclick = function(event) {
       if (event.target == modal) {
           modal.style.display = "none";
       }
   }
</script>

<script type="text/javascript">
    // $(document).ready(function () {
 function myvideoFunction(videoid) {
  var modal = document.getElementById("myModal2");
 var videourl = '{{ URL::asset('') }}';
 var response = '<div class="company-vedio" ><video width="100%" height="400" controls><source src="'+videourl+'/'+videoid+'" type="video/mp4"></video></div>';
  modal.style.display = "block";
            $('.modal-body').html(response);
            $('#custModal').modal('show');

}
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src="{{ asset('assets/css/owl.carousel.min.js') }}"></script>
<script>
   $('.owl-carousel.cate-right').owlCarousel({
       loop: true,
       margin: 10,
       responsiveClass: true,
       responsive: {
           0: {
               items: 1,
               nav: true
           },
           600: {
               items: 6,
               nav: false
           },
           1000: {
               items: 6,
               nav: true,
               loop: false
           }
       }
   })
</script>
<div class="company-video-play lookBuy">
   <div id="myModal3" class="modal">
      <div class="modal-content prodModal3">
         <div class="modal-header">
            <span class="close3">&times;</span>
         </div>
         <div class="modal-body">
            <div class="popupform new-f">
               <div class="popup-content ">
                  <div class="platinum-section">
                     <div class="product-list-grid">
                        <div class="product-list-grid-one" id="productinfomodal">
                           <div class="Pro-list-g ">
                              <div class="my-Slide-Container">
                                 <div class="mySlides" style="display: none;">
                                    <img src="{{ asset('public/images/product/5.jpg') }}"
                                       style="width:100%">
                                 </div>
                              </div>
                              <div class="product-list-grid-two">
                                 <h3></h3>
                                 <ul>
                                    <li><i class="fas fa-angle-left"></i><span>Quality Available:</span> A
                                       Grade
                                    </li>
                                    <li><i class="fas fa-angle-left"></i><span>Packaging Type:</span> HDPE
                                       &amp; Gunny Bag
                                    </li>
                                    <li><i class="fas fa-angle-left"></i><span>Storage Tips:</span> Fresh
                                    </li>
                                    <li><i class="fas fa-angle-left"></i><span>Packaging Size:</span> 20 kg
                                    </li>
                                 </ul>
                              </div>
                              <!-- <a class="prev" onclick="plusSlides(-1)">❮</a> -->
                              <!-- <a class="next" onclick="plusSlides(1)">❯</a> -->
                           </div>
                        </div>
                        <div class="cont-sup-fo">
                           <h2>Looking to buy something?</h2>
                           <p>Please fill the form and supplier will contact you soon.</p>
                           <form action="{{url('/add-supplier-contact')}}"  id="contactForm" method="post" name="contactForm">
                              @csrf
                       @if(Session::has('message'))
                       <div class="alert alert-danger"  role="alert">{{Session::get('message')}}</div>
                       @elseif(Session::has('success'))
                       <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                       @endif
                              <div class="alert alert-danger alert-dismissible custom_alert" style="display: none;">
                                 <span class="alert_message"></span>
                              </div>
                              <input type="hidden" id="user_id" name="user_id" >
                              <div class="form-group">
                                 <label>Name</label>
                                 <input type="text" id="name" name="name" value="{{old('name')}}" >
                                  @if($errors->has('name'))
                                      <div class="error">{{ $errors->first('name') }}</div>
                                     @endif
                              </div>
                              <div class="form-group">
                                 <label>Phone Number *</label>
                                 <input type="text" id="mobile" name="mobile" value="{{old('mobile')}}">
                                  @if($errors->has('mobile'))
                                      <div class="error">{{ $errors->first('mobile') }}</div>
                                     @endif
                              </div>
                              <div class="form-group">
                                 <label>Email Address</label>
                                 <input type="text" type="email" id="email_data" name="email" value="{{old('email')}}">
                                  @if($errors->has('email'))
                                      <div class="error">{{ $errors->first('email') }}</div>
                                     @endif
                              </div>
                              <div class="form-group">
                                 <label>Location</label>
                                 <input type="text" id="location" name="location" value="{{old('location')}}">
                                  @if($errors->has('location'))
                                      <div class="error">{{ $errors->first('location') }}</div>
                                     @endif
                              </div>
                              <div class="form-group">
                                 <label class="info-title" for="exampleInputComments">Your Requirements <span>*</span></label>  <textarea class="form-control unicase-form-control" name="requirements" id="requirements" value="{{old('requirements')}}"></textarea>
                                  @if($errors->has('requirements'))
                                      <div class="error">{{ $errors->first('requirements') }}</div>
                                     @endif
                              </div>
                              <button type="submit" id="submit">Submit</button>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
   // Get the modal
   var modal3 = document.getElementById("myModal3");
   
   // Get the button that opens the modal
   var btn3 = document.getElementById("myBtn3");
   
   // Get the <span> element that closes the modal
   var span3 = document.getElementsByClassName("close3")[0];
   
   // When the user clicks the button, open the modal 
   // btn3.onclick = function() {
   //   modal3.style.display = "block";
   // }
   
   // // When the user clicks on <span> (x), close the modal
   span3.onclick = function() {
       modal3.style.display = "none";
   }
   // document.ready function
   //   $(function() {
   // selector has to be . for a class name and # for an ID
   //   $('#myBtn3').click(function(e) {
   //       e.preventDefault(); // prevent form from reloading page
   //       alert("hiii");
   //       productid = $(this).data("id");
   //       $.ajax({
   //           'url': '/productmodal',
   //           'type': 'POST',
   //           data: {
   //               'productid': productid
   //           }
   //           success: function(data) {
   //               alert('request sent!');
   //           }
   
   //       });
   //   });
   //   });
   $('.myBtn3').click(function(e) {
       productid = $(this).data("id");
       var APP_URL = {!! json_encode(url('/')) !!}
       var saveData = $.ajax({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           },
           type: 'POST',
           url: APP_URL + "/productmodal",
           data: {
               'slug': productid
           },
   
           success: function(resultData) {
              // alert(resultData)
               modal3.style.display = "block";
               $('#productinfomodal').html(resultData);
   
           }
       });
       saveData.error(function() {
           alert("Something went wrong");
       });
   });
   
   // When the user clicks anywhere outside of the modal, close it
   window.onclick = function(event) {
       if (event.target == modal) {
           modal.style.display = "none";
       }
   }
   
   // When the user clicks anywhere outside of the modal, close it
   window.onclick = function(event) {
       if (event.target == modal) {
           modal3.style.display = "none";
       }
   }
</script>
<script>
   function logintabClick(evt, loginTabName) 
   {
     var i, logintabcontent, logintablinks;
     logintabcontent = document.getElementsByClassName("logintabcontent");
     for (i = 0; i < logintabcontent.length; i++) {
      logintabcontent[i].style.display = "none";
     }
     logintablinks = document.getElementsByClassName("logintablinks");
     for (i = 0; i < logintablinks.length; i++) {
      logintablinks[i].className = logintablinks[i].className.replace(" active", "");
     }
     document.getElementById(loginTabName).style.display = "block";
     evt.currentTarget.className += " active";
   }
   
   document.getElementById("defaultOpen").click();
</script> 

@endsection