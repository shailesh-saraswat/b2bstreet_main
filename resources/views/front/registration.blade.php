@extends('layouts.front')
@section('content') 
@include('front.seo-keywords')


<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="{{route('Front.Index')}}">Home</a></li>
				
				<li class='active'>Government Registration/ Certification</li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb --><div class="body-content outer-top-xs">	<div class="container">		<div class="ad-grid">    <div class="ad-grid-left">    <h2>Government Registration/ Certification</h2><p>We help MSME to get the Registrations and Certifications services from Various Government and Non-Government Organizations, please drop us your queries if you required anything from the below list:-</p><ul>	<li> <i class="fas fa-angle-left"></i> MSME Registration (Udyog Aadhar)</li>	<li> <i class="fas fa-angle-left"></i> NSIC Registration (GP)</li>	<li> <i class="fas fa-angle-left"></i> ISO Certification</li>	<li> <i class="fas fa-angle-left"></i> ISI License</li>	<li> <i class="fas fa-angle-left"></i> FSSAI License</li>	<li> <i class="fas fa-angle-left"></i> Import and Export Code</li>	<li> <i class="fas fa-angle-left"></i> DUNS Number</li>	<li> <i class="fas fa-angle-left"></i> Others..</li></ul>    </div>    <div class="ad-grid-right">        <div class="popupform new-f">                     <div class="popup-content">                                                <div class="popup-grid-container">    <h2>Enquiry Form</h2><form>    <div class="form-p-grid">                           <div class="form-lable-container">                              <label>Name</label>                              <input type="text" placeholder="Full Name">                           </div>                           <div class="form-lable-container">                              <label>Company Name</label>                              <input type="text" placeholder="Xyz Pvt. Ltd ">                           </div>                           <div class="form-lable-container">                              <label>Phone Number</label>                              <input type="text" placeholder="1234567890">                           </div>                           <div class="form-lable-container">                              <label>Email ID</label>                              <input type="text" placeholder="abc@gmail.com">                           </div>                                                      <div class="form-lable-container">                              <label>Product Name</label>                              <input type="text" placeholder="Product you want to sell">                           </div><div class="form-lable-container">                              <label>Qty</label>                              <input type="text" placeholder="Enter Qty.">                           </div></div>						   <div class="form-lable-container">                              <label>Add Address</label>                              <textarea>Add Address</textarea>                           </div><button type="submit">Submit</button>                        </form></div>                     </div>                  </div>    </div></div><!-- /.row -->		</div><!-- /.container --></div>


 @endsection 