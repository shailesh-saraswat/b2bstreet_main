@extends('layouts.front')
@section('content')
@include('front.seo-keywords')
 
<style>
	
*{
	margin:0px;
	padding:0px;
	box-sizing:border-box;
	
}


body{
	background-color:#f1f3f6;
}

.main-box{
	width:860px;
	margin:auto;
	display:flex;
	font-family:Montserrat;	
	height:80vh;
	align-items:center;
	
}

.small-box{
	display:flex;
	height:550px;
	overflow:hidden;
	border-radius:5px;
	box-shadow:0px 0px 5px #ccc;
	background-color:white;
	align-items:center;

}
.form-box{
	width:50%;
	padding:50px;
}

.form-box form .form-group input{
	width:100%;
	padding:10px;
	border:1px solid #ccc;
	border-radius:5px;
	outline:none;
}

.form-box form .form-group label{
	font-family:sans-serif;
	font-size:12px;
	color:grey;
	
}

.form-img{
	width:50%;
	
}

.form-img img{
	width:100%;
	max-width:100%;
	height:100%;
	vertical-align:middle;
}
.form-group button{
	padding:10px 30px;
	border-radius:5px;
	border:0px;
	margin-top:20px;
	width:100%;
	background-color: lightskyblue;
}

.form-group textarea{
    width:100%;
    height:120px;
    border-radius:5px;
    outline:none;
    padding:10px;
    border:1px solid #ccc;
}
.submi_button button{
	width:100%;
	padding:10px;
	background-color:#11afa7;
	color:#fff;
	border-radius:5px;
	outline:none;
	border:0px;
}
.form-box h2{
	font-weight:400;
}
.info-2{
	width:50%;
	padding:50px;

}

.info-2 h2{
	font-weight:400;
}

.info-2 h4{
	margin-bottom:20px;
	font-weight:400;
	font-size:12px;
	color:grey;
	line-height:20px;
	
}
.location_icon {
    margin-bottom: 20px;
    margin-right: 15px;
}

.phone_icon{
	margin-bottom:20px;
	
}

.mail_icon{
	margin-bottom:20px;
}

.map iframe{
	width:100%;
	height:400px;
	border:0px;
}
.info-2 i{
	background-color: #11afa7;
    height: 40px;
    width: 40px;
    border-radius: 50%;
    color: #fff;
    padding: 13px;
}
.Information_text {
    display: flex;
	align-items:center;
}
.phone_icon{
	margin-right:10px;
}
.mail_icon{
	margin-right:10px;
}
.main-box {
    height: 100%;
    align-items: center;
    margin-bottom: 20px;
}
.small-box {
    height: unset;
}
@media only screen and (max-width: 991px) and (min-width: 320px)
{
.main-box {
    width: 100%;
    display: unset;
    height: unset;
}
.small-box {
    display: unset;
    height: unset; 
}	
.form-box {
    width: 100%;
    padding: 15px;
}	
.info-2 {
    width: 100%;
    padding: 15px;
}

}

.dropdown-menu > .dropdown {
  position: relative;
}

.dropdown-menu > .dropdown a::after {
  transform: rotate(-90deg);
  position: absolute;
  right: .9rem;
  top: .9rem;
}

.dropdown-menu > .dropdown .dropdown-menu {
  top: -.7rem;
  left: 100%;
  border-radius: 0 .25rem .25rem .25rem;
}
.new-navig {
   position: fixed;
    top: 12px;
    right: 10px;
}

.mobile-menu-show.new-navig button {
    background: none;
    border: none;
    position: absolute;
    right: 13px;
    top: -33px;
}

.mobile-menu-show.new-navig .navbar-collapse.collapse.in {
    width: 100%;
    margin-top: 28px;
    padding: 0;
}

.mobile-menu-show.new-navig .navbar-collapse.collapse.in ul {
    width: 100%;
}

.mobile-menu-show.new-navig .navbar-collapse.collapse.in ul li {
    width: 100%;
    border-bottom: 1px solid #ccc;
    /* padding: 8px; */
    /* padding-left: 15px; */
}

.mobile-menu-show.new-navig nav {
    width: 100%;
    position: relative;
    top: 11px;
    z-index: 999999;
    background: #efefef;
    margin-top: 36px;
}

.logo {
    width: 100%;
    justify-content: space-between;
}

.mobile-menu-show.new-navig {
    position: absolute;
    right: 9px;
    width: 100%;
    left: 0;
}

div#navbarNavDropdown {
    margin-top: 0;
    padding: 0;
}

.navbar-nav {
    margin: 0;
}

.navbar-nav .open a {
    background: none !important;
    text-decoration: none;
}

.navbar-nav .open .dropdown-menu>li>a, .navbar-nav .open .dropdown-menu .dropdown-header {
    padding: 0;
}

.mobile-menu-show.new-navig .navbar-nav li:first-child {
    background: none;
}

.mobile-menu-show.new-navig li.nav-item {
    text-align: left;
    padding: 8px;
    padding-left: 15px;
}

.mobile-menu-show.new-navig a.dropdown-item.dropdown-toggle {
    padding: 8px;
    padding-left: 0px;
    border-bottom: 1px solid #acacac;
}

li.dropdown {}

.mobile-menu-show.new-navig li.dropdown {
    border: none !important;
}

ul.dropdown-menu {
    background: #b0adad;
}

li.nav-item.dropdown.open {
    background: #e7e7e7;
}

.mobile-menu-show.new-navig a {
    font-size: 14px;
}

.mobile-menu-show.new-navig ul.dropdown-menu.show {padding: 8px;padding-left: 25px;}

.mobile-menu-show.new-navig ul.dropdown-menu.show li {
    padding: 8px;
    padding-left: 25px;
    border-bottom: 1px solid #ccc;
}

.mobile-menu-show.new-navig ul.dropdown-menu.show li a {
    /* border-bottom: 1px
 solid #ccc; */
}
ul.dropdown-menu {
    padding-left: 15px;
}

.mobile-menu-show.new-navig .dropdown-menu>li>a {
    font-weight: 400;
    color: #333 !important;
}
a#navbarDropdownMenuLink {
    color: #333 !important;
    font-weight: 600;
}
ul.dropdown-menu.show {
    padding: 0 !important;
    padding-left: 15px !important;
}

ul.dropdown-menu.show li {
    padding: 0 !important;
    border-bottom: 1px solid #b9b9b9 !important;
}

ul.dropdown-menu.show li a {
    padding: 8px !important;
    padding-left: 0px !important;
}
ul.dropdown-menu {
    /* background: white !important; */
}

ul.dropdown-menu.show {
    
    padding-bottom: 12px !important;
}
.error{
    color: red;
}

.alert p {
    opacity: 0;
    margin-top: 25px;
    font-size: 21px;
    text-align: center;
    -webkit-transition: opacity 2s ease-in;
    -moz-transition: opacity 2s ease-in;
    -o-transition: opacity 2s ease-in;
    -ms-transition: opacity 2s ease-in;
    transition: opacity 2s ease-in;
}
</style>


<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="{{route('Front.Index')}}">Home</a></li>
				
				<li class='active'>Contact Us</li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->
	<div class="main-box">
		<div class="small-box">
			<div class="form-box">
                @if(Session::has('message'))
                <div class="alert alert-danger"  role="alert">{{Session::get('message')}}</div>
                @elseif(Session::has('success'))
                <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                @endif

					<h2 >Contact Us</h2>
					<form method="post" action="{{route('Front.Add.form')}}">
                    @csrf
					<div class="form-group">   
                        <label>Name</label>
                        <input type="text" placeholder="Full Name" name="name" id="name" >
                        @if($errors->has('name'))
                        <div class="error">{{ $errors->first('name') }}</div>
                        @endif
                    </div>

                    <div class="form-group">   
                        <label>Email</label>
                        <input type="text" placeholder="Email" name="email" id="email" value="{{old('email')}}">
                        @if($errors->has('email'))
                        <div class="error">{{ $errors->first('email') }}</div>
                        @endif
                    </div>

                    <div class="form-group">   
                        <label>Phone</label>
                        <input type="text" placeholder="Phone" name="phone" id="phone" maxlength="10" value="{{old('phone')}}" value="{{old('phone')}}">
                        @if($errors->has('phone'))
                        <div class="error">{{ $errors->first('phone') }}</div>
                        @endif
                    </div>              
                    <div class="form-group">   
                        <label>Message</label>
                        <textarea id="massage" name="massage" placeholder="Message" value="{{old('massage')}}" value="{{old('massage')}}"></textarea>
                        @if($errors->has('massage'))
                        <div class="error">{{ $errors->first('massage') }}</div>
                       @endif
                    </div>

					<div class="submi_button">
						<button type="submit" value="submit">Submit</button>
					</div>
				</form>
			</div>
			<div class="info-2">
			<h2>Information</h2><br>
			<div class="Information_text">
				<div class="location_icon">
					<i class="fas fa-map-marker-alt"></i>
				</div>
				<div class="info_location">
					<h4>No.- 153, LGF, Anarkali Complex, Near Jhandewalan Metro Station, Jhandewalan Extension, New Delhi 110055</h4>
				
				</div>
				
			</div>
			<div class="Information_text">
				<div class="phone_icon">
					<i class="fas fa-phone"></i>
				</div>
				<div class="info_location">
					<h4>+91-11-45538594</h4>
				
				</div>
				
			</div>
			
			<div class="Information_text">
				<div class="mail_icon">
					<i class="fas fa-envelope"></i>
				</div>
				<div class="info_location">
					<h4>Contact@gmail.com</h4>
				
				</div>
				
			</div>
		</div>
	</div>
	</div>
	<div class="map" style="margin-bottom: 60px;">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3501.51919685757!2d77.19870106549544!3d28.644168890304098!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390cfd5d928e88d9%3A0xdbae9ccb98624723!2sAnarkali%20Complex%20148%20M.C.KASHYAP%20%26ASSOCIATE!5e0!3m2!1sen!2sin!4v1632987959771!5m2!1sen!2sin"></iframe>
	</div>

<script>
 // dropdown-toggle class not added for submenus by current WP Bootstrap Navwalker as of November 15, 2017.
$('.dropdown-menu > .dropdown > a').addClass('dropdown-toggle');

$('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
  if (!$(this).next().hasClass('show')) {
    $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
  }
  var $subMenu = $(this).next(".dropdown-menu");
  $subMenu.toggleClass('show');
  $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
    $('.dropdown-menu > .dropdown .show').removeClass("show");
  });
  return false;
});
</script>
<script>
      function logintabClick(evt, loginTabName) 
      {
        var i, logintabcontent, logintablinks;
        logintabcontent = document.getElementsByClassName("logintabcontent");
        for (i = 0; i < logintabcontent.length; i++) {
         logintabcontent[i].style.display = "none";
        }
        logintablinks = document.getElementsByClassName("logintablinks");
        for (i = 0; i < logintablinks.length; i++) {
         logintablinks[i].className = logintablinks[i].className.replace(" active", "");
        }
        document.getElementById(loginTabName).style.display = "block";
        evt.currentTarget.className += " active";
      }

      document.getElementById("defaultOpen").click();
   </script> 

@endsection
