   <style>
     .error{
    color: red;
}
 </style>  
    <div class="ad-grid-right">
        <div class="popupform new-f">
                     <div class="popup-content">                        
                     <div class="popup-grid-container">
                     @if(Session::has('message'))
                     <div class="alert alert-danger"  role="alert">{{Session::get('message')}}</div>
                     @elseif(Session::has('success'))
                     <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                @endif 
                       <h2>Enquiry Form</h2>
                         
                       <form method="post" id="contactForm" class="contactForm" action="{{url('add_enquiry_form')}}">
                        @csrf
                       <div class="form-p-grid">                                                      
                      <input type="hidden" name="page_id" id="page_id" value="{{$page->id}}">

                           <div class="form-lable-container">   
                              <label>Name</label>
                              <input type="text" placeholder="Full Name" name="name" id="name">
                              @if($errors->has('name'))
                                <div class="error">{{ $errors->first('name') }}</div>
                                @endif
                           </div>

                           <div class="form-lable-container">
                              <label>Company Name</label>
                              <input type="text" placeholder="Company Name" name="company_name" id="company_name">
                              @if($errors->has('company_name'))
                                <div class="error">{{ $errors->first('company_name') }}</div>
                                @endif
                           </div>
                           <div class="form-lable-container">
                              <label>Phone Number</label>
                              <input type="text" placeholder="Phone" name="phone" id="phone" >
                              @if($errors->has('phone'))
                                <div class="error">{{ $errors->first('phone') }}</div>
                                @endif
                           </div>
                           <div class="form-lable-container">
                              <label>Email ID</label>
                              <input type="text" placeholder="Email ID" name="email" id="email">
                              @if($errors->has('email'))
                                <div class="error">{{ $errors->first('email') }}</div>
                                @endif
                           </div>
                          </div>
                        <div class="form-lable-container">
                           <label>Requirement</label>
                           <textarea id="massage" name="massage" placeholder="Requirement"></textarea>
                           @if($errors->has('massage'))
                                <div class="error">{{ $errors->first('massage') }}</div>
                                @endif
                           </div>
                           <button type="submit" value="submit">Submit</button>                  
                         </form>
                      </div>
                     </div>
                  </div>
    </div>

    <script>
      function logintabClick(evt, loginTabName) 
      {
        var i, logintabcontent, logintablinks;
        logintabcontent = document.getElementsByClassName("logintabcontent");
        for (i = 0; i < logintabcontent.length; i++) {
         logintabcontent[i].style.display = "none";
        }
        logintablinks = document.getElementsByClassName("logintablinks");
        for (i = 0; i < logintablinks.length; i++) {
         logintablinks[i].className = logintablinks[i].className.replace(" active", "");
        }
        document.getElementById(loginTabName).style.display = "block";
        evt.currentTarget.className += " active";
      }

      document.getElementById("defaultOpen").click();
   </script> 

