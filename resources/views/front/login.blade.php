@extends('layouts.front')
@section('content')  


<style>
    profile-form{
        background-color: bisque;
    }

    .contact-form.profile-form {
    border-radius: 10px !important;
    box-shadow: 5px 15px 15px #0000000f;
        background-color: #f1f3f6;
        padding: 50px;
}
    
    
    .row.login form{
        background-color: transparent;
        box-shadow: none !important;
    }
    
    .breadcrumb{
        margin-bottom: 0px;
    }
    
    
    .custom-form1 {
        background-image: linear-gradient(45deg, #ffffff59, #ffffff00);
    }
    .custom-form1 {
        background-image: linear-gradient(45deg, #ffffff59, #ffffff00);
    }
    
    
    
    
    .body-content {
    background-color: #aecfae;
    background-position: bottom;
    background-size: cover;
    background-image: url(https://images.unsplash.com/photo-1578916171728-46686eac8d58?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1074&q=80);
    position: relative;
        height: 80vh;
        display: flex;
        align-items: center;
}
    .body-content::before {
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0,0,0,.7);
    content: "";
}
    
    .form-heading h1 {
    font-size: 40px;
        font-weight: bold;
    color: white;
} 
    .form-heading p {
    font-size: 16px;
    color: white;
}
    .linkss .text-right{
        color: white;
    }
    .cont-sec-cont span.facebook a i{
        background-color: #6c85da !important;
    }
    .cont-sec-cont p{
        color: white;
    }
    
    .profile-form label{
        color: white;
    }
    .or-log::before{
        background-color: white;
    }
    .or-log p {
        top: 25px;
        color: #556495;
        background-color: transparent;
    }
    .row.login button.logintablinks.active{
        background-color: #fff;
    }
    .row.login button{
        border: 1px solid #ccc;
        border-radius: 50px;
        margin-top: 30px;
    }
    .or-log::before{
        display: none;
    }
    
    .loginTab button {
    width: 50%;
}
    .loginTab{
        margin-bottom: 0px;
    }
    .body-content1 {
    background-color: #aecfae;
}
  
    .body-content2 {
    background-color: #eb92936e;
}
    
    .linkss .text-right a{
        color: yellow !important;
    }
    
    .left-section h2 {
    color: #556495;
    font-size: 40px;
    font-weight: 600;
}
    .left-section p {
    color: #556495;
}
    .left-section p a {
    color: red !important;
}
    
.left-section {
    text-align: center;
    width: 50%;
    justify-content: center;
    display: flex;
    flex-direction: column;
    margin: auto;
}
    
    .cont-sec-cont ul{
        width: 100% !important;
    } 
    .cont-sec-cont ul button{
        padding: 15px;
    }
    
    .form-group{
        position: relative;
    }
   
    .form-group p {
    position: absolute;
    top: 50%;
    right: 10px;
    color: white;
    transform: translate(0, -50%);
}  

    .loginTab button {
    width: 48%;
    margin: 0px 20px;
}
    
    
    .loginTab{
        justify-content: unset !important;
    }

    
    .row.login form{
        padding: 50px 0px !important;
    }
    .or-log p{
        width: 150px !important;
    }
    
    .row.login button.logintablinks.active {
    background-color: #556495;
    color: #fff !important;
}
    
    .body-content {
    background-color: #aecfae;
    background-position: bottom;
    background-size: cover;
    background-image: url(https://images.unsplash.com/photo-1578916171728-46686eac8d58?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1074&q=80);
    position: relative;
    height: auto;
    display: flex;
    align-items: center;
}
    .body-content1 {
    background-image: url(https://www.rolandberger.com/img/Tiles/rb_dig_21_001_03_art_351_resilience_in_logistics_supply_chains_st.jpg);
}
</style>

 <div class="breadcrumb">
         <div class="container">
            <div class="breadcrumb-inner">
               <ul class="list-inline list-unstyled">
                  <li><a href="{{route('Front.Index')}}">Home</a></li>
                  
               </ul>
            </div>
            <!-- /.breadcrumb-inner -->
         </div>
         <!-- /.container -->
      </div>
      <!-- /.breadcrumb -->
      <div class="body-content">
         <div class="container">
            @include('admin.partials.messages')
            <div class="row login">
               <div class="col-lg-6 offset-lg-3">
                     <div class="contact-form profile-form">
                        <div class="cont-f-grid">
                     <div class="loginTab">
                        <button class="logintablinks buyer-btn" onclick="logintabClick(event, 'buyer')" id="defaultOpen">Buyer</button>
                        <button class="logintablinks supplier-btn" onclick="logintabClick(event, 'supplier')">Supplier</button>
                     </div>   
                            <div class="left-section">
                        <h2>Login</h2>
                        <p>Don't have an account ? <a href="{{url('/sign_up?section=0')}}">Sign Up</a></p>
                    </div>
                           <form id="buyer"  action="{{route('buyer_login')}}"  class="logintabcontent" method="post"  enctype="multipart/form-data">

                               @csrf
                               
                               <input type="hidden" id="name" name="type" value="3" class="form-control unicase-form-control text-input" placeholder="">
                              <div class="cont-sec-cont">
                                 <div class="form-group">
                                   
                                    <input type="text" id="email" name="email" class="form-control unicase-form-control text-input" placeholder="Email Address" required="">
                                 </div>
                              </div>
                              <div class="cont-sec-cont">
                                 <div class="form-group">
                                    
                                    <input class="form-control unicase-form-control text-input" type="password" id="password" name="password"  placeholder="Password" required="">
                                     <p><a href="{{route('Front.forgot_password')}}">Forgot Password</a></p>
                                 </div>
                              </div>
                              <div class="cont-sec-cont linkss">
                                  <ul>
                                    <button type="submit" id="submit" name="submit" value="submit">Log Buyer</button>
                                  </ul>
                              </div>
                               <div class="or-log"><p>OR signup with</p></div>
                            <div class="cont-sec-cont text-center">
                               
                                <span class="facebook"><a href="{{url('/redirect')}}"><i class="fab fa-facebook-f"></i></a></span>
                               <!--  <span class="google"><a href=""><i class="fab fa-google"></i></a></span> -->
                              </div>
                           </form>
                     <form id="supplier" class="logintabcontent" method="post" action="{{route('suplier_login')}}"   enctype="multipart/form-data">
                     @csrf
                        
                        <div class="cont-sec-cont">
                           <div class="form-group">
                             
                              <input type="text" id="email" name="email" class="form-control unicase-form-control text-input" placeholder="Email Address" required="">
                           </div>
                        </div>
                        <div class="cont-sec-cont">
                           <div class="form-group">
                              
                              <input class="form-control unicase-form-control text-input" type="password" id="password" name="password"  placeholder="Password" required="">
                               <p><a href="{{route('Front.forgot_password')}}">Forgot Password</a></p>
                           </div>
                        </div>

                        <div class="cont-sec-cont linkss">
                           <ul>
                              <button type="submit" id="submit" name="submit" value="submit">Log Supplier</button>
                           </ul>
                        </div>
                        <div class="or-log"><p>OR signup with</p></div>
                        <div class="cont-sec-cont text-center">
                          
                           <span class="facebook"><a href="{{url('/redirect')}}"><i class="fab fa-facebook-f"></i></a></span>
                           <!-- <span class="google"><a href=""><i class="fab fa-google"></i></a></span> -->
                        </div>
                     </form>                    
                        </div>
                        <p id="trimb" style="clear:both;color:red;"></p>
                     </div>
               </div>
            </div>
            <!-- /.row -->
            <!-- ============================================== BRANDS CAROUSEL ============================================== -->
         </div>
      </div>
   <script>
      function logintabClick(evt, loginTabName) 
      {
        var i, logintabcontent, logintablinks;
        logintabcontent = document.getElementsByClassName("logintabcontent");
        for (i = 0; i < logintabcontent.length; i++) {
         logintabcontent[i].style.display = "none";
        }
        logintablinks = document.getElementsByClassName("logintablinks");
        for (i = 0; i < logintablinks.length; i++) {
         logintablinks[i].className = logintablinks[i].className.replace(" active", "");
        }
        document.getElementById(loginTabName).style.display = "block";
        evt.currentTarget.className += " active";
      }

      document.getElementById("defaultOpen").click();
   </script>  
     

@endsection 
@section('js')
<script type="text/javascript">
   $('.login_btn').on('click',function(){
    var email         = $('#email').val();
    var password      = $('#password').val();
    var error_message = "";
    if(email=="")
    {
      error_message = "Please Enter Email.";
    }
    else if(password=="")
    {
      error_message = "Please Enter Password.";
    }
    //alert(error_message);
    if(error_message!='')
    {
      $('.custom_alert').css('display','block');
      $('.alert_message').text(error_message);
      close_alert();
      return false;
    }
    else
    {
      $('#loginForm').submit();
    }
  });
</script>

<script>
    $(document).ready(function(){
        $(".supplier-btn").click(function(){
            $(".body-content").addClass("body-content1")
            $(".profile-form").css({"background-color":"rgb(223 230 255)"})
        })
        $(".buyer-btn").click(function(){
            $(".body-content").removeClass("body-content1")
            $(".profile-form").css({"background-color":"rgb(255 222 213)"})
        })
    });
</script>

@endsection