@extends('layouts.front')
@section('content')
@include('front.seo-keywords')
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="{{route('Front.Index')}}">Home</a></li>
				
				<li class='active'>{{$page->title}}</li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->
<div class="body-content outer-top-xs">
	<div class='rel'>
	<div class="dott-pattern  form-pat abt-top-pat  "></div>
	<div class='container'>
		<div class='  single-product mb-50'>
			 
            <div class="detail-block">
				<div class=" wow fadeInUp">
                
					         			
					<div class=' product-info-block abt-inside-container'>
						<div class="product-info">
							@foreach($abouts as $about)
								<div class="two-grid">
								
									<div class="abt-inside-left">
									<h1 class="name">{{$about->title}}</h1>

									  <p>{!! $about->content !!}</p>
										
									</div>
									<div class="abt-inside-right">
									
										<img src="{{ asset($about['image']) }}">
										
									</div>
									
								</div>
								@endforeach
						</div><!-- /.product-info -->
					</div><!-- /.col-sm-7 -->
				</div><!-- /.row -->
            </div>
				 
			<div class="clearfix"></div>
		</div><!-- /.row -->
		</div><!-- /.container -->
		</div><!-- /.container -->

		
		
		
		
		<section class="section featured-product wow fadeInUp   pt-50 pb-50 hot-seller animated abt-team" style="background: rgb(244, 241, 249); ">
		
		<div class="container">
            <div class="row">
                <h2 class="section-title1"> Our <span>Team </span></h2> 
               
                   <div class="team-g">
                   	 @foreach($ourteams as $ourteam)                    
                   	 <div class="">
                     <div class="team-sec">
                            <img src="{{ asset($ourteam['image']) }}">
                            <h3>{!! $ourteam->designation !!}</h3>
                            <h5>{{$ourteam->name}}</h5>
                            <p>{!! $ourteam->description !!}</p>
                        </div>
                        
                    </div>
                     @endforeach               
                      </div>
               
            </div>
            </div>	
        </section>
		
		
	<div class="abouticon pt-50 pb-50">
	<div class="container">  
	<div class="counter-grid">  
				<div class="box">
		
					<img src="{{ asset('assets/images/icon1.png') }}" class="img-fluid" alt="icon" title="icon">
					<h4>7064</h4>
					<p>Cup of Coffee</p>
				</div>  
				<div class="box">
					<img src="{{ asset('assets/images/icon2.png') }}" class="img-fluid" alt="icon" title="icon">
					<h4>60,544</h4>
					<p>Happy Users</p>
				</div>  
				<div class="box">
					<img src="{{ asset('assets/images/icon3.png') }}" class="img-fluid" alt="icon" title="icon">
					<h4>32,786</h4>
					<p>Ads Posts</p>
				</div>  
				<div class="box">
					<img src="{{ asset('assets/images/icon4.png') }}" class="img-fluid" alt="icon" title="icon">
					<h4>3,20,185</h4>
					<p>Total Users</p>
				</div> 
	</div>
	</div>
</div>	
	<div class="why pt-50 pb-50" style="background-color: #e4f2ee94;">
	<div class="dott-pattern  form-pat abt-top-pat mis"></div>
	<div class="container">
	<div class="why-grid-container">
		<div class="why-grid-left">
			<img src="{{ asset('assets/images/why.jpg') }}">
		</div>
		<div class="why-grid-right">
				@foreach($smarts as $smart)
			<div class="why-right-section">
				<h2>{{$smart->title}}</h2>
				<p>{!! $smart->content !!}</p>
			</div>

			@endforeach
			
		</div>
		
	</div>
</div>	
</div>	
		
		<div class="clients-sec pt-50 pb-50">
		<div class="container">
		 <h2 class="section-title1	"> Our <span>Client's </span></h2> 
			<div class="client-grid">
				@foreach($clients as $client)
				<figure><img src="{{ asset($client['image']) }}"> </figure>

				@endforeach
				<!-- <figure><img src="{{'assets/images/c.jpg'}}"> </figure>
				<figure><img src="{{'assets/images/d.jpg'}}"> </figure>
				<figure><img src="{{'assets/images/a.jpg'}}"> </figure>
				<figure><img src="{{'assets/images/f.jpg'}}"> </figure>
				<figure><img src="{{'assets/images/g.jpg'}}"> </figure>
				<figure><img src="{{'assets/images/c.jpg'}}"> </figure>
				<figure><img src="{{'assets/images/h.jpg'}}"> </figure>
				<figure><img src="{{'assets/images/i.jpg'}}"> </figure>
				<figure><img src="{{'assets/images/a.jpg'}}"> </figure>
				<figure><img src="{{'assets/images/c.jpg'}}"> </figure>
				<figure><img src="{{'assets/images/d.jpg'}}"> </figure>
				<figure><img src="{{'assets/images/a.jpg'}}"> </figure>
				<figure><img src="{{'assets/images/f.jpg'}}"> </figure> -->
				<figure><h3>1585+</h3></figure>
			</div>
		</div>
		</div>
	
		
</div><!-- /.body-content -->
@endsection



