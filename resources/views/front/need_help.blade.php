@extends('layouts.front')
@section('content') 

<style>
   .error{
    color: red;
}
</style>
      <div class="breadcrumb">
         <div class="container">
            <div class="breadcrumb-inner">
               <ul class="list-inline list-unstyled">
                  <li><a href="{{route('Front.Index')}}">Home</a></li>
                  <li class='active'>Help</li>
               </ul>
            </div>
            <!-- /.breadcrumb-inner -->
         </div>
         <!-- /.container -->
      </div>
      <!-- /.breadcrumb -->
      <div class="body-content">
         <div class="container">
            <div class="row login ">
               <div class="col-lg-6 offset-lg-3">
                     <div class="contact-form profile-form">
                        <div class="cont-f-grid">
                           <form method="post" action="{{route('Front.store.need_help')}}" class="bg-warmm">
                           @if(Session::has('message'))
                           <div class="alert alert-danger"  role="alert">{{Session::get('message')}}</div>
                           @elseif(Session::has('success'))
                           <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                           @endif
                           @csrf
                               <div class="form-heading">
                                <h1>Do you have any Query</h1>
                                   <p>Feel free to contact us</p>
                               </div>
                              <div class="cont-sec-cont">
                                 <div class="form-group">
                                    <label class="info-title" for="exampleInputName">Name <span>*</span></label>
                                    <input type="text" id="name" name="name" class="form-control unicase-form-control text-input" placeholder="" value="{{old('name')}}">
                                    @if($errors->has('name'))
                                    <div class="error">{{ $errors->first('name') }}</div>
                                    @endif
                                 </div>
                              </div>
                            <div class="cont-sec-cont">
                                 <div class="form-group">
                                    <label class="info-title" for="exampleInputName">Email Address <span>*</span></label>
                                    <input type="email" id="email" name="email" class="form-control unicase-form-control text-input" placeholder="" value="{{old('email')}}">
                                     @if($errors->has('email'))
                                    <div class="error">{{ $errors->first('email') }}</div>
                                    @endif
                                 </div>
                              </div>
                               <div class="cont-sec-cont">
                                 <div class="form-group">
                                    <label class="info-title" for="exampleInputName">Phone No. <span>*</span></label>
                                    <input type="text" id="name" name="phone" class="form-control unicase-form-control text-input" placeholder="" maxlength="10" value="{{old('phone')}}">
                                     @if($errors->has('phone'))
                                    <div class="error">{{ $errors->first('phone') }}</div>
                                    @endif
                                 </div>
                              </div>
                              <div class="cont-sec-cont">
                                 <div class="form-group">
                                    <label class="info-title" for="exampleInputName">Message. <span>*</span></label>
                                     <textarea class="form-control unicase-form-control text-input" name="massage" placeholder="" value="{{old('massage')}}"></textarea>
                                      @if($errors->has('massage'))
                                    <div class="error">{{ $errors->first('massage') }}</div>
                                    @endif
                                 </div>
                              </div>
                              
                              <div class="cont-sec-cont linkss">
                                  <ul>
                                    <button  type="submit" value="submit">Send</button>
                                  </ul>
                                  
                              </div>
                           </form>
                        </div>
                        <p id="trimb" style="clear:both;color:red;"></p>
                     </div>
               </div>
            </div>
            <!-- /.row -->
            <!-- ============================================== BRANDS CAROUSEL ============================================== -->
         </div>
      </div>
      
      
@endsection 