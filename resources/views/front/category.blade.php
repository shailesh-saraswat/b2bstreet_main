@extends('layouts.front')
@section('content')
@include('front.seo-keywords')
<div class="breadcrumb">
   <div class="container">
      <div class="breadcrumb-inner">
         <ul class="list-inline list-unstyled">
            <li><a href="{{route('Front.Index')}}">Home</a></li>
            <li class='active'>All Category</li>
         </ul>
      </div>
      <!-- /.breadcrumb-inner -->
   </div>
   <!-- /.container -->
</div>
<!-- /.breadcrumb -->
<section id="tablets">
   <div class="container">
      <div class="row">
         <div class="col-md-12 headd">
            <h3>{{$category->name}}</h3>
         </div>
      </div>
      @foreach ($subcategory as $sub) 
      <div class="drug-box">
         <div class="row text-center">
            <div class="tablet-content">
               <div class="left-side">
                  <div class="tablet-box">
                     <img src="{{($sub->image)}}">
                     <h4><a href="{{ URL::to('/products/' . $sub->slug) }}">{{ $sub->name }}</a></h4>
                  </div>
               </div>
               <div class="right-side">
                  <?php
                     if(!empty($sub->products)){
                      foreach($sub->products as $p){ 
                      ?>
                  <div class="tablet-box">
                     <img src="<?php echo asset($p->product_img); ?>" style="height: 150px; width: 180px;">
                     <h4><a href="{{ URL::to('/product/' . $p->slug) }}"><?php echo $p->title; ?></a></h4>
                  </div>
                  <?php }} ?>  
               </div>
            </div>
            <div class="view-more{{ $sub->id }}">
               <div class="row">
                  <div class="tablet-content1">
                     <div class="tablet-box"></div>
                     <?php
                        if(!empty($sub->products1)){
                         foreach($sub->products1 as $p1){ 
                         ?>
                     <div class="tablet-box">
                        <img src="<?php echo asset($p1->product_img1); ?>" style="height: 150px; width: 200px;">
                        <h4><a href="{{ URL::to('/product/' . $p->slug) }}"><?php echo $p1->title; ?></a></h4>
                        <!--  <p>(123)</p> -->
                     </div>
                     <?php }} ?>  
                  </div>
               </div>
            </div>
            <button class="btn btn-deafult{{ $sub->id }}">View More <i class="fa fa-caret-down"></i></button>
         </div>
      </div>

<style type="text/css">.view-more{{ $sub->id }}{
   display: none;
   }</style>
      <script>       
   $('.btn-deafult{{ $sub->id }}').click(function(){
      var $this = $(this);
      $this.toggleClass('SeeMore2');
      if($this.hasClass('SeeMore2')){
          $this.text('View Less');         
      } else {
          $this.text('View More');
      }
   });
         
</script> 
 <script>
   $(document).ready(function(){
     $(".btn-deafult{{ $sub->id }}").click(function(){
       $(".view-more{{ $sub->id }}").slideToggle();
     });
   });
</script> 
      @endforeach
   </div>
</section>
<!-- 
<script>
   $('.btn-deafult').click(function(){
   var $this = $(this);
   $this.toggleClass('SeeMore2');
   if($this.hasClass('SeeMore2')){
   $this.text('View Less');         
   } else {
   $this.text('View More');
   }
   });
</script>
<script>       
   $('.btn-deafult1').click(function(){
      var $this = $(this);
      $this.toggleClass('SeeMore2');
      if($this.hasClass('SeeMore2')){
          $this.text('View Less');         
      } else {
          $this.text('View More');
      }
   });
   
           
      
</script>   
<script>
   $(document).ready(function(){
     $(".btn-deafult").click(function(){
       $(".view-more").slideToggle();
     });
   });
</script>
<script>
   $(document).ready(function(){
     $(".btn-deafult1").click(function(){
       $(".view-more1").slideToggle();
     });
   });
</script>
  -->
<style>
   #tablets{
   margin-bottom: 30px;
   }
   .headd h3{
   text-align: center;
   margin-bottom: 40px;
   }
   .drug-box{
   border: 1px solid #000;
   border-radius: 5px;
   padding: 20px;
   }  
   .tablet{
   display: grid;
   grid-template-columns: 15%;
   }
   .left-side {
   padding: 5px;
   width: 12.5%;
   border-right: 1px solid #000;
   }
   .left-side img{
   width: 90%;
   }
   .right-side {
   display: grid;
   grid-template-columns: repeat(6, 1fr);
   }
   .tablet-box h4{
   font-size: 14px !important;
   font-weight: normal;
   }
   .tablet-content{
   display: flex;
   text-align: center;
   }
   .tablet-content1 :nth-child(1) {
   box-shadow: none !important;
   }
   .tablet-box:hover {
   box-shadow:0 1px 5px 1px #bdbbbb;
   border-radius: 5px;
   }
   .tablet-content h3 {
   font-size: 15px;
   font-weight: 600;
   }
   .drug-box .btn-deafult{
   text-align: center;
   border: 1px solid #556495;
   background: #556495;
   color: #fff;
   padding: 4px 12px;
   font-size: 12px;
   width: 100px;
   margin-top: 20px;
   margin-bottom: 25px;
   }
   .drug-box .btn-deafult1{
   text-align: center;
   border: 1px solid #556495;
   background: #556495;
   color: #fff;
   padding: 4px 12px;
   font-size: 12px;
   width: 100px;
   margin-top: 20px;
   margin-bottom: 25px;
   }
   .tablet-content1{
   display: flex;
   text-align: center;
   padding: 14px;
   background-color: #f5f5f56e;
   margin-left: 136px;
   }
   .tablet-content1 h3 {
   font-size: 15px;
   font-weight: 600;
   }
   
   .view-more1{
   display: none;
   }
   @media only screen and (max-width: 600px) {
   .tablet-box {
   padding: 5px;
   width: 50%;
   float: left;
   border: none !important;
   }
   .tablet-box h4 {
   font-size: 10px !important;
   font-weight: normal;
   }
   .tablet-box p{
   font-size: 10px;
   }
   .tablet-content h3 {
   font-size: 8px;
   font-weight: 600;
   }
   .tablet-content{
   display: block;
   }
   .headd h3 {
   text-align: center;
   margin-bottom: 40px;
   font-size: 18px;
   }
   }
</style>

@endsection