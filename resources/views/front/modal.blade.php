<style>
     .error{
    color: red;
}
 </style> 

<div class="modal fade popupfullcontainer" id="modal-subscribe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
         <div class="modal-dialog" role="document">
           <div class="modal-content">
               <div class="modal-header border-0">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                  <h4 class="modal-title" id="myModalLabel"></h4>
               </div>
               <div class="modal-body">
                  <div class="popupform new-f">
                     <div class="popup-content">                       
                     <div class="popup-grid-container">

                     <div class="popup-left-grid">
                       <img src="{{ asset('assets/images/gallery/Quick-enquiry.png') }}">

                        <div class="pop-image-cont">
                           <h2>Quick Enquiry</h2>
                            <p>Enter the Information to get update</p>
                        </div>
    
                     </div>
                      

                <form method="post" action="{{route('Front.Quick.form')}}">
                   @if(Session::has('massage'))
                <div class="alert alert-danger"  role="alert">{{Session::get('massage')}}</div>
                @elseif(Session::has('success'))
                <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                @endif  
                        <div class="form-p-grid">
                          
                            @csrf

                           <div class="form-lable-container">
                              <label>Name</label>
                              <input type="text" name="name" id="name" placeholder="Full Name" value="{{old('name')}}">
                              @if($errors->has('name'))
                                <div class="error">{{ $errors->first('name') }}</div>
                                @endif
                           </div>
                           <div class="form-lable-container">
                              <label>Company Name</label>
                              <input type="text" name="company_name" id="company_name" placeholder="Company Name" value="{{old('company_name')}}">
                              @if($errors->has('company_name'))
                                <div class="error">{{ $errors->first('company_name') }}</div>
                                @endif
                           </div>
                           <div class="form-lable-container">
                              <label>Phone Number</label>
                              <input type="text" name="phone" id="phone" placeholder="Phone" value="{{old('phone')}}">
                              @if($errors->has('phone'))
                                <div class="error">{{ $errors->first('phone') }}</div>
                                @endif
                           </div>
                           <div class="form-lable-container">
                              <label>Email ID</label>
                              <input type="text" name="email" id="email" placeholder="Email Id" value="{{old('email')}}">
                              @if($errors->has('email'))
                                <div class="error">{{ $errors->first('email') }}</div>
                                @endif
                           </div>
                           
                           <div class="form-lable-container">
                              <label>Product Name</label>
                              <input type="text" name="product_name" id="product_name" placeholder="Product Name" value="{{old('product_name')}}">
                              @if($errors->has('product_name'))
                                <div class="error">{{ $errors->first('product_name') }}</div>
                                @endif

                           </div><div class="form-lable-container">
                              <label>Qty</label>
                              <input type="text" name="quality" id="quality" placeholder="Enter Qty." value="{{old('quality')}}">
                              @if($errors->has('quality'))
                                <div class="error">{{ $errors->first('quality') }}</div>
                                @endif
                           </div>
                         </div>
                           <div class="form-lable-container">
                              <label>Add Address</label>
                              <textarea name="address" id="address" placeholder="Add Address" placeholder="Address" >{{old('quality')}}</textarea>
                               @if($errors->has('address'))
                                <div class="error">{{ $errors->first('address') }}</div>
                                @endif  
                           </div>                          
                           <button type="submit"  value="submit">Submit</button>
                        </form>
                     </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>






      <script>
         $ (window).ready (function () {
            setTimeout (function () {
                $ ('#modal-subscribe').modal ("show")
            }, 3000)
         })
      </script>
<!-- End modal -->

