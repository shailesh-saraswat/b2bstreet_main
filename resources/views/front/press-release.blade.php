@extends('layouts.front')
@section('content')
@include('front.seo-keywords')


<div class="breadcrumb">
            <div class="container">
               <div class="breadcrumb-inner">
                  <ul class="list-inline list-unstyled">
                     <li><a href="{{route('Front.Index')}}">Home</a></li>
                     <li class='active'><a href="{{route('Front.pressRelease')}}">Press Release</a></li>
                  </ul>
               </div>
               <!-- /.breadcrumb-inner -->
            </div>
            <!-- /.container -->
         </div>
         <!-- /.breadcrumb -->
      

<section id="media-banner" style="background-image: url({{ asset('public/images/blog/b2b-press.png') }});background-size: cover; position: relative;height: 400px;">
    <div class="container">
        <div class="row">
             <div class="headings">
                <h2>B2B Streets Press Release</h2>
                <h4>Interested in joining </h4>
            </div>
        </div>
    </div>
</section>
         
          
<section id="media">
    <div class="container">
        
        <div class="media-box">
            @foreach($press as $pres)
            <div class="row align">
                <div class="col-md-2">
                    <div class="media-icon">
                        <img src="{{asset($pres['image'])}}" class="img-fluid">
                    </div>
                </div>
                <div class="col-md-10">
                    <div class="media-content">
                        <h3>{{$pres->title}}</h3>
                    <span><i class="fa fa-history"></i> Posted on {{$pres->date}}</span>
                        <p>{!!$pres->content!!}</p>
                        <blockquote>
                            <p>{!!$pres->short_content!!}</p>
                        </blockquote>
                        
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        
       <div class="media-box">
        @foreach($pressrelease as $release)
            <div class="row align">
                <div class="col-md-10">
                    <div class="media-content">
                        <h3>{{$release->title}}</h3>
                    <span><i class="fa fa-history"></i> Posted on {{$release->date}}</span>
                        <p>{!!$release->content!!}</p>
                        <blockquote>
                            <p>{!!$release->short_content!!}</p>
                        </blockquote>
                        
                    </div>
                </div>
                 <div class="col-md-2">
                    <div class="media-icon">
                        <img src="{{asset($release['image'])}}" class="img-fluid">
                    </div>
                </div>
            </div>
            @endforeach
        </div>

    </div>
</section>
@endsection