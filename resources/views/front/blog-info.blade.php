@extends('layouts.front')
@section('content')
@include('front.seo-keywords')
       
      <div class="menu-zindex">
         <div class="breadcrumb">
            <div class="container">
               <div class="breadcrumb-inner">
                  <ul class="list-inline list-unstyled">
                     <li><a href="https://b2bstreets.com">Home</a></li>
                     <li class='active'>Blog Details</li>
                  </ul>
               </div>
               <!-- /.breadcrumb-inner -->
            </div>
            <!-- /.container -->
         </div>
         <!-- /.breadcrumb -->
      

<section id="blog-details">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="blog-details">
                    
                    <div class="blog-slider">
                       <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                              </ol>
                              <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                  <img src="{{ asset('public/images/blog/b1.jpg') }}" alt="..." class="img-fluid">
                                </div>
                                <div class="item">
                                  <img src="{{ asset('public/images/blog/b1.jpg') }}" alt="..." class="img-fluid">
                                </div>
                                  <div class="item">
                                  <img src="{{ asset('public/images/blog/b1.jpg') }}" alt="..." class="img-fluid">
                                </div>
                              </div>

                              <!-- Controls -->
                              <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                              </a>
                              <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                              </a>
                            </div> 
                    </div>
                    
                    <div class="blog-title">
                        <h3>Action Construction Equipment Ltd. – Acing New Heights</h3>
                        <span>25/03/2019 | <a href="#">Corano</a></span>
                    </div>
                    
                    <div class="blog-content">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate perferendis consequuntur illo aliquid nihil ad neque, debitis praesentium libero ullam asperiores exercitationem deserunt inventore facilis, officiis,
                        </p>
                        <blockquote>
                            <p>Quisque semper nunc vitae erat pellentesque, ac placerat arcu consectetur.
                                venenatis elit ac ultrices convallis. Duis est nisi, tincidunt ac urna sed,
                                cursus blandit lectus. In ullamcorper sit amet ligula ut eleifend. Proin
                                dictum tempor ligula, ac feugiat metus. Sed finibus tortor eu scelerisque
                                scelerisque.
                            </p>
                        </blockquote>
                        <p>aliquam maiores asperiores recusandae commodi blanditiis ipsum tempora culpa possimus assumenda ab quidem a voluptatum quia natus? Ex neque, saepe reiciendis quasi velit perspiciatis error vel quas quibusdam autem nesciunt voluptas odit quis dignissimos eos aspernatur voluptatum est repellat. Pariatur praesentium, corrupti deserunt ducimus quo doloremque nostrum aspernatur saepe cupiditate sit autem exercitationem debitis, maiores vitae perferendis nemo? Voluptas illo, animi temporibus quod earum molestias eaque, iure rem amet autem dignissimos officia dolores numquam distinctio esse voluptates optio pariatur aspernatur omnis? Ipsam qui commodi velit natus reiciendis quod quibusdam nemo eveniet similique animi!</p>
                    </div>
                    
                    <div class="success-bottom">
                        <div class="row">
                            <div class="col-md-4">
                                <p><i class="fa fa-bars"></i> Category: B2B Streets Achievers</p>
                            </div>
                            <div class="col-md-8">
                                <p><i class="fa fa-tags"></i> Tags: ace ltd, action construction equipment ltd.</p>
                            </div>
                        </div>
                    </div>
                    
                    <div class="social-icon">
                        <h5>Share: </h5>
                        <img src="{{ asset('public/images/blog/social.png') }}" class="img-fluid" width="25%">
                    </div>
                    
                    <!--  
                        <div class="blog-social-icon">
                        <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a>
                        <a href="#" class="google"><i class="fa fa-google-plus"></i></a>
                        </div> 
                    !-->
                    
                    <div class="comment-section">
                        <h5>03 Comment</h5>
                                <ul>
                                    <li>
                                        <div class="author-avatar">
                                            <img src="{{ asset('public/images/blog/1.jpg') }}" alt="">
                                        </div>
                                        <div class="comment-body">
                                            <h5 class="comment-author">Admin</h5>
                                            <div class="comment-post-date">
                                                15 Dec, 2019 at 9:30pm
                                            </div>
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim maiores
                                                adipisci optio ex, laboriosam facilis non pariatur itaque illo sunt?</p>
                                        </div>
                                    </li>
                                    <li class="comment-children">
                                        <div class="author-avatar">
                                           <img src="{{ asset('public/images/blog/1.jpg') }}" alt="">
                                        </div>
                                        <div class="comment-body">
                                            <h5 class="comment-author">Admin</h5>
                                            <div class="comment-post-date">
                                                20 Nov, 2019 at 9:30pm
                                            </div>
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim maiores
                                                adipisci optio ex, laboriosam facilis non pariatur itaque illo sunt?</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="author-avatar">
                                            <img src="{{ asset('public/images/blog/1.jpg') }}" alt="">
                                        </div>
                                        <div class="comment-body">
                                            <h5 class="comment-author">Admin</h5>
                                            <div class="comment-post-date">
                                                25 Jan, 2019 at 9:30pm
                                            </div>
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim maiores
                                                adipisci optio ex, laboriosam facilis non pariatur itaque illo sunt?</p>
                                        </div>
                                    </li>
                                </ul>
                    </div>
                    
                    <div class="blog-comment-wrapper">
                                <h5>Leave a reply</h5>
                                <p>Your email address will not be published. Required fields are marked *</p>
                                <form action="#">
                                    <div class="comment-post-box">
                                        <div class="row">
                                            
                                            <div class="col-lg-4 col-md-4 p-0">
                                                <label>Name</label>
                                                <input type="text" class="coment-field" placeholder="Name">
                                            </div>
                                            <div class="col-lg-4 col-md-4 p-0">
                                                <label>Email</label>
                                                <input type="text" class="coment-field" placeholder="Email">
                                            </div>
                                            <div class="col-lg-4 col-md-4 p-0">
                                                <label>Website</label>
                                                <input type="text" class="coment-field" placeholder="Website">
                                            </div>
                                            <div class="col-12">
                                                <label>Comment</label>
                                                <textarea name="commnet" placeholder="Write a comment"></textarea>
                                            </div>
                                            <div class="col-12">
                                                <div class="coment-btn">
                                                    <input class="btn btn-sqr" type="submit" name="submit" value="Post Comment">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                </div>
            </div>
            
            <div class="col-md-3">
                <div class="blog-sidebar">
                    
                    <div class="title">
                        <h5>Search</h5>
                        <hr>
                    </div>
                    
                    <div class="sidebar-serch-form">
                        <form action="#">
                             <input type="text" class="search-field" placeholder="search here">
                                    <button type="submit" class="search-btn"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                    
                     <div class="heading">
                        <h3>Recent Post</h3>
                    </div>
                    <div class="right-side">
                        
                         <div class="recent-post">
                                    <div class="recent-post-item">
                                        <figure class="product-thumb">
                                            <a href="#">
                                                <img src="{{ asset('public/images/blog/post.jpg') }}" alt="blog image">
                                            </a>
                                        </figure>
                                        <div class="recent-post-description">
                                            <div class="product-name">
                                                <h6><a href="#">Auctor gravida enim</a></h6>
                                                <p>march 10 2019</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="recent-post-item">
                                        <figure class="product-thumb">
                                            <a href="blog-details.html">
                                              <img src="{{ asset('public/images/blog/post.jpg') }}" alt="blog image">
                                            </a>
                                        </figure>
                                        <div class="recent-post-description">
                                            <div class="product-name">
                                                <h6><a href="#">gravida auctor dnim</a></h6>
                                                <p>march 18 2019</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="recent-post-item">
                                        <figure class="product-thumb">
                                            <a href="blog-details.html">
                                              <img src="{{ asset('public/images/blog/post.jpg') }}" alt="blog image">
                                            </a>
                                        </figure>
                                        <div class="recent-post-description">
                                            <div class="product-name">
                                                <h6><a href="#">enim auctor gravida</a></h6>
                                                <p>march 14 2019</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                        
                        <div class="heading">
                            <h3>Top Posts & Pages</h3>
                        </div>
                        
                        <div class="top-section1">
                            <div class="list">
                                 <ul>
                                <li><a href="{{route('Front.Index')}}">Home</a></li>
                                <li><a href="{{url('/pages/{slug}')}}">About Us</a></li>
                                <li><a href="{{route('Front.Gallery')}}">Gallery</a></li>
                                <li><a href="{{route('Front.Contact')}}">Contact Us</a></li>
                                <li><a href="{{route('Front.successStory')}}">Success Story</a></li>
                                <li><a href="{{route('Front.Blog')}}">Blog</a></li>
                                <li><a href="{{route('Front.Package')}}">Package</a></li>
                                <li><a href="{{route('Front.Complaint')}}">Complaint</a></li>
                                <li><a href="{{route('Front.Career')}}">Careers @ B2B Streets</a></li>
                            </ul>
                            </div> 
                         </div>
                        
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
      
          

      
</div>
      
          
@endsection