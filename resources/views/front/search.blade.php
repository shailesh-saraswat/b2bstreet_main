@extends('layouts.front')
@section('content')
@include('front.seo-keywords')

<style type="text/css">
   a.data {
    all: unset;
}
</style>
 <div class="breadcrumb darker-bread">
         <div class="container">
            <div class="breadcrumb-inner">
               <ul class="list-inline list-unstyled">
                  <li><a href="index.php">Home</a></li>
                  <li class='active'>Home Supplies</li>
               </ul>
            </div>
         </div>
      </div>
      <div class="container-fluid">
         <h2 class="inside-main-heading">Home Supplies </h2>
         <div class="pl-list-page">
            <div class="pl-left">
               <div class="pl-left-cate">
                  <h3>Related Category</h3>
                  <ul>
                     <li><a href="">PET Jars</a></li>
                     <li><a href="">Water Bottle</a></li>
                     <li><a href="">Plastic Bottles</a></li>
                     <li><a href="">Phenyl Bottles</a></li>
                     <li><a href="">Hand Wash Bottle</a></li>
                     <li><a href="">PET Water Bottle</a></li>
                  </ul>
               </div>
               <!-- <div class="pl-left-cate">
                  <h3>Related Brands</h3>
                  <ul>
                     <li><a href="">Milton Pet Bottles</a></li>
                  </ul>
               </div> -->
               <div class="pl-left-cate">
                  <h3>Location</h3>
                  <input type="text">
                  <ul>
                     <li><a href="">Delhi</a></li>
                     <li><a href="">Noida</a></li>
                     <li><a href="">Gurgaon</a></li>
                     <li><a href="">Ghaziabad</a></li>
                     <li><a href="">East Delhi</a></li>
                     <li><a href="">West Delhi</a></li>
                  </ul>
               </div>
                <div class="pl-left-cate">
            <h3>Featured Categories</h3>
            <ul>
               <li>
                  <a href="">
                     <img src="{{ asset('public/images/product/1.jpg') }}">
                     <h4>Pet Jars</h4>
                  </a>
               </li>
               <li>
                  <a href="">
                     <img src="{{ asset('public/images/product/2.jpg') }}">
                     <h4>Medical Equipment</h4>
                  </a>
               </li>
               <li>
                  <a href="">
                     <img src="{{ asset('public/images/product/3.jpg') }}">
                     <h4>Pet Jars</h4>
                  </a>
               </li>
               <li>
                  <a href="">
                     <img src="{{ asset('public/images/product/a.jpg') }}">
                     <h4>Ventilator Circuits</h4>
                  </a>
               </li>
            </ul>
         </div>
            </div>
            <div class="pl-right">
               
               <div class="pr-list-second ad-container">
                  <div>
                  <div class="platimumContainer">
                     <div class="platinum-section">

                         @foreach($pro_data as $value)

                       <div class="product-list-grid">


                           <div class="product-list-grid-one">
                           <div class="Pro-list-g ">
    <div class="my-Slide-Container">

 @if($value['product_images'])
                     @foreach($value['product_images'] as $image)
                  <div class="mySlides" style="display: none;">
                    
                     <img src="{{ asset($image['product_image']) }}" style="width:100%">
                  </div>
                  @endforeach
                  @endif

  

 

    
  

 
     
    </div>
    
  <!-- <a class="prev" onclick="plusSlides(-1)">❮</a> -->
  <!-- <a class="next" onclick="plusSlides(1)">❯</a> -->

  <div class="caption-container">
    <p id="caption">Mountains and fjords</p>
  </div>

  <div class="sd ">

 @if($value['product_images'])
                     @foreach($value['product_images'] as $image)
    <div class="column">
      <img class="demo cursor" src="{{ asset($image['product_image']) }}" style="width:100%" onclick="currentSlide(1)" alt="">
    </div>
 @endforeach
                  @endif

   
    
  </div>
</div>

                              
                              <div class="shape"></div>
                              <div class="platimum" style="
                                 background-color: #2dbe6c;
                                 ">Platinum</div>
                           </div>

                           
                          
                         @if(!empty($value['title']))
                           <div class="product-list-grid-two">


                                <a class="data" href="{{url('/products/product-details',$value['slug'])}}"><h3>{{$value['title']}}</h3></a>


                              <p class="part-price fa fa-inr"><i aria-hidden="true"></i>{{$value['price']}}</p>
                              <ul>
                                 <li><i class="fas fa-angle-left"></i><span>Quality Available:</span>{{$value['quality']}}</li>
                                 <li><i class="fas fa-angle-left"></i><span>Packaging Type:</span> {{$value['packaging_type']}}</li>
                                 <li><i class="fas fa-angle-left"></i><span>Storage Tips:</span> {{$value['storage_tips']}}</li>
                                 <li><i class="fas fa-angle-left"></i><span>Packaging Size:</span> {{$value['packaging']}}</li>
                              </ul>
                              <!-- <a href="" class="rm">Read More</a> -->
                           </div>
                        @else
                        <h1>No Data Found</h1>
                        @endif
                           <div class="product-list-grid-three last-three-div" style="background-color: #e4f2ee94;">
                              <div>
                                 <div>

                                     <h2>{!! $value['company_name'] !!}</h2>

                                    <p>{!! $value['address'] !!}</p>
                                 </div>
                                <div class="veri">
                                <!--  <div>
                                    <img src="{{ asset('public/images/product/trust.svg') }}">
                                    <h5>Trustseal Verified</h5>
                                 </div> -->
                                 <div>
                                    <img src="{{ asset('public/images/product/prize.svg') }}">
                                    <h5>Verified</h5>
                                 </div>
                                 <div id="myBtn2">
                                    <img src="{{ asset('public/images/product/youtube.svg') }}">
                                    <h5>Company Video</h5>
                                 </div>
                                 <div>
                                    <img src="{{ asset('public/images/product/interview.svg') }}">
                                    <h5>Owner Interview</h5>
                                 </div>
                              </div>
                              </div>
                              <div class="list-cont-no">
                                 <div class="mob-no">
                                    <i class="fas fa-mobile-alt"></i> {{$value['mobile']}}
                                 </div>
                                 <div id="myBtn3" class="cont-no-button">
                                   <a href="#"> Contact Supplier</a>
                                 </div>
                              </div>
                           </div>
                        </div>


                          @endforeach
                     </div>
                     
                        
                        
                     </div>
                     
                     
<script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>

                     
                     
                     
                     
                     <div class="product-list-ads">
                      <a href="https://www.sunliteorganiconline.com/" target="_blank">  <img src="{{ asset('public/images/product/ads4.jpg') }}"></a>
                     </div>

                     <div class="product-list-ads">
                       <a href="https://chaahathomesinfratech.com/" target="_blank"> <img src="{{ asset('public/images/product/ads5.jpg') }}"></a>
                     </div>

                  </div>
                  <div class="ad-square-section">
                     <div class="prod-right-square-img">
                      <a href="https://www.srmachinery.in/" target="_blank">  <img src="{{ asset('public/images/product/ads2.jpg') }}"></a>
                     </div>
                     <div class="needhelp">
                        <div class="popupform">
                           <div class="popup-content">
                              <h2>What service do you need?
                                 <span>B2B Streets will help you</span>
                              </h2>
                              <form>
                                 <div class="form-lable-container">
                                    <label>Name</label>
                                    <input type="text" placeholder="Full Name">
                                 </div>
                                 <div class="form-lable-container">
                                    <label>Company Name</label>
                                    <input type="text" placeholder="Xyz Pvt. Ltd ">
                                 </div>
                                 <div class="form-lable-container">
                                    <label>Phone Number</label>
                                    <input type="text" placeholder="1234567890">
                                 </div>
                                 <div class="form-lable-container">
                                    <label>Email ID</label>
                                    <input type="text" placeholder="abc@gmail.com">
                                 </div>
                                 <div class="form-lable-container">
                                    <label>Query</label>
                                    <textarea>Enter your message</textarea>
                                 </div>
                                 <button>Submit</button>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
       
      <!-- The Modal -->
      <div class="company-video-play">
        <div id="myModal2" class="modal">
            <div class="modal-content">
                <div class="modal-header">
                <span class="close1">&times;</span> 
                </div>
                <div class="modal-body">
                <div class="company-vedio">
                        <iframe width="100%" height="400" src="https://www.youtube.com/embed/wbnaHgSttVo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                    </div>
                </div> 
            </div>
        </div>
        </div>
      

      <div class="company-video-play lookBuy">
        <div id="myModal3" class="modal">
            <div class="modal-content prodModal3">
                <div class="modal-header">
                <span class="close3">&times;</span> 
                </div>
                <div class="modal-body">
                                  <div class="popupform new-f">
                                     <div class="popup-content ">
                                        
                                        <div class="platinum-section">
                                       <div class="product-list-grid">
                                           <div class="product-list-grid-one">
                                           <div class="Pro-list-g ">
                    <div class="my-Slide-Container">
                  <div class="mySlides" style="display: block;">
                     
                    <img src="images/1.jpg" style="width:100%">
                  </div>

                  

                  
                    
                  

                  
                     
                    </div>
                    
                  <div class="product-list-grid-two">
                                              <h3>A Grade Fresh Potato, HDPE &amp; Gunny Bag, Packaging Size: 20 kg</h3>
                                              
                                              <ul>
                                                 <li><i class="fas fa-angle-left"></i><span>Quality Available:</span> A Grade</li>
                                                 <li><i class="fas fa-angle-left"></i><span>Packaging Type:</span> HDPE &amp; Gunny Bag</li>
                                                 <li><i class="fas fa-angle-left"></i><span>Storage Tips:</span> Fresh</li>
                                                 <li><i class="fas fa-angle-left"></i><span>Packaging Size:</span> 20 kg</li>
                                              </ul>
                                              
                                           </div><!-- <a class="prev" onclick="plusSlides(-1)">❮</a> -->
                  <!-- <a class="next" onclick="plusSlides(1)">❯</a> -->

                  

                  
                </div>

                                              
                                              
                                              
                                           </div>
                <div class="cont-sup-fo"><h2>Looking to buy something?</h2>
                    <p>Please fill the form and supplier will contact you soon</p>
                    
                    <form>
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text">
                        </div>
                         <div class="form-group">
                            <label>Phone Number *</label>
                            <input type="text">
                        </div>
                         <div class="form-group">
                            <label>Location</label>
                            <input type="text">
                        </div>
                        <button type="submit">Submit</button>
                    </form>
                </div>
                                           
                                           
                                        </div>


                                     </div>
                                     </div>
                                  </div>
                               </div>
            </div>
        </div>
        </div>


<script>
// Get the modal
var modal = document.getElementById("myModal2");

// Get the button that opens the modal
var btn = document.getElementById("myBtn2");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close1")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// Get the modal
var modal3 = document.getElementById("myModal3");

// Get the button that opens the modal
var btn3 = document.getElementById("myBtn3");

// Get the <span> element that closes the modal
var span3 = document.getElementsByClassName("close3")[0];

// When the user clicks the button, open the modal 
btn3.onclick = function() {
  modal3.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span3.onclick = function() {
  modal3.style.display = "none";
}


// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal3.style.display = "none";
  }
}
</script>
      
      
      
      <script src="assets/css/owl.carousel.min.js"></script>
      <script>
         $('.owl-carousel.cate-right').owlCarousel({
           loop:true,
           margin:10,
           responsiveClass:true,
           responsive:{
               0:{
                   items:1,
                   nav:true
               },
               600:{
                   items:6,
                   nav:false
               },
               1000:{
                   items:6,
                   nav:true,
                   loop:false
               }
           }
         })
      </script>
      


@endsection 