<style>
    profile-form{
        background-color: bisque;
    }
    

    .loginTab{
        margin-bottom: 0px;
    }

    .profile-form {
    border-radius: 10px !important;
    box-shadow: 5px 15px 15px #0000000f;
        background-color: #f1f3f6;
        padding: 50px;
}
    
    
    .row.login form{
        background-color: transparent;
        box-shadow: none !important;
    }
    
    .breadcrumb{
        margin-bottom: 0px;
    }
    
    
    .custom-form1 {
        background-image: linear-gradient(45deg, #ffffff59, #ffffff00);
    }
    .custom-form1 {
        background-image: linear-gradient(45deg, #ffffff59, #ffffff00);
    }
    
    .profile-form label {
    font-size: 14px;
    font-weight: 400;
    display: none;
}
    
    
    .body-content {
    background-color: #aecfae;
    background-position: bottom;
    background-size: cover;
    background-image: url(https://images.unsplash.com/photo-1578916171728-46686eac8d58?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1074&q=80);
    position: relative;
        height: auto;
        display: flex;
        align-items: center;
}
    .body-content::before {
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0,0,0,.7);
    content: "";
}
    
    .form-heading h1 {
    font-size: 40px;
        font-weight: bold;
   
} 
    .form-heading p {
    font-size: 16px;
    
}
    .linkss .text-right{
       
    }
    .cont-sec-cont span.facebook a i{
        background-color: #6c85da !important;
    }
    .cont-sec-cont p{
      
    }
    
    .profile-form label{
       
    }
    .or-log::before{
        background-color: white;
    }
    .or-log p {
        top: 25px;
        color: #556495;
        background-color: transparent;
    }
    .row.login button.logintablinks.active{
        background-color: #fff;
    }
    .row.login button{
        border: 1px solid #ccc;
        border-radius: 50px;
        margin-top: 30px;
    }
    .or-log::before{
        display: none;
    }
    
    .loginTab button {
    width: 50%;
}
    .loginTab{
        margin-bottom: 0px;
    }
    .body-content1 {
    background-color: #aecfae;
}
  
    .body-content2 {
    background-color: #eb92936e;
}
    
    .linkss .text-right a{
        color: yellow !important;
    }
    
    .left-section h2 {
    color: #556495;
    font-size: 40px;
    font-weight: 600;
}
    .left-section p {
    color: #556495;
}
    .left-section p a {
    color: red !important;
}
    
.left-section {
    text-align: center;
    width: 50%;
    justify-content: center;
    display: flex;
    flex-direction: column;
    margin: auto;
}
    
    .cont-sec-cont ul{
        width: 100% !important;
    } 
    .cont-sec-cont ul button{
        padding: 15px;
    }
    
    .form-group{
        position: relative;
    }
   
    .form-group p {
    position: absolute;
    top: 50%;
    right: 10px;
    color: white;
    transform: translate(0, -50%);
}  
.loginTab button {
    width: 48%;
    margin: 0px 20px;
}
    .breadcrumb{
        margin-bottom: 0px !important;
    }
    
    .form-heading h1 {
    color: #556495;
    font-size: 40px !important;
    font-weight: 600 !important;
}  
    .form-heading p {
    color: #556495 !important;
        font-size: 14px !important;
}
    .loginTab{
        justify-content: unset !important;
    }

    
    .row.login form{
        padding: 0px 0px !important;
    }
    .or-log p{
        width: 150px !important;
    }
    
    .row.login button.logintablinks.active {
    background-color: #556495;
    color: #fff !important;
}
    
    .body-content {
    background-color: #aecfae;
    background-position: bottom;
    background-size: cover;
    background-image: url(https://images.unsplash.com/photo-1578916171728-46686eac8d58?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1074&q=80);
    position: relative;
    height: auto;
    display: flex;
    align-items: center;
}
    .body-content1 {
    background-image: url(https://www.rolandberger.com/img/Tiles/rb_dig_21_001_03_art_351_resilience_in_logistics_supply_chains_st.jpg);
}
    
    .bg-form{
        background-color: #8ab9bd;
    }
    .error{
        color: red;
    }
    
</style>
@extends('layouts.front')
@section('content') 

      <!-- ============================================== HEADER : END ============================================== -->
      <div class="breadcrumb">
         <div class="container">
            <div class="breadcrumb-inner">
               <ul class="list-inline list-unstyled">
                  <li><a href="{{route('Front.Index')}}">Home</a></li>
                  <li class='active'>Sign Up</li>
               </ul>
            </div>
            <!-- /.breadcrumb-inner -->
         </div>
         <!-- /.container -->
      </div>
      <!-- /.breadcrumb -->
      <div class="body-content">
           
         <div class="container">
            <div class="row login">
               <div class="col-lg-6 offset-lg-3">
                     <div class="contact-form profile-form">
                        <div class="cont-f-grid">
                     <div class="loginTab">
                        <button class="logintablinks buyer-btn <?php if($section==0){echo 'active';} ?>" onclick="logintabClick(event, 'buyer')" id="defaultOpen">Buyer</button>
                        <button class="logintablinks supplier-btn <?php if($section==1){echo 'active';} ?>" onclick="logintabClick(event, 'supplier')">Supplier</button>
                     </div> 
                            @if(Session::has('message'))
                <div class="alert alert-danger"  role="alert">{{Session::get('message')}}</div>
                @elseif(Session::has('success'))
                <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                @endif 
                           <form id="buyer" class="logintabcontent" method="post"   enctype="multipart/form-data" action="{{url('/sign_up/{section}')}}" <?php if($section==0){echo "style='display: block'";} ?> >
                               @csrf
                               <div class="form-heading">
                                <h1>Sign Up</h1>
                                   <p>Create a New Account</p>
                               </div>
                                <input type="hidden" id="name" name="type" value="3" class="form-control unicase-form-control text-input" placeholder="">
                              <div class="cont-sec-cont">
                                 <div class="form-group">
                                    <label class="info-title" for="exampleInputName">Name <span>*</span></label>
                                    <input type="text" id="buyer_name" name="name" class="form-control unicase-form-control text-input" placeholder="Name" value="{{old('name')}}">
                                    @if($errors->has('name'))
                                 <div class="error">{{ $errors->first('name') }}</div>
                                 @endif
                                 </div>
                              </div>
                            <div class="cont-sec-cont">
                                 <div class="form-group">
                                    <label class="info-title" for="exampleInputName">Email Address <span>*</span></label>
                                    <input type="email" id="buyer_email" name="email" class="form-control unicase-form-control text-input" placeholder="Email Address" value="{{old('email')}}">
                                    @if($errors->has('email'))
                                <div class="error">{{ $errors->first('email') }}</div>
                                @endif
                                 </div>
                              </div>
                               <div class="cont-sec-cont">
                                 <div class="form-group">
                                    <label class="info-title" for="exampleInputName">Phone No. <span>*</span></label>
                                    <input type="text"  id="buyer_mobile" name="mobile" class="form-control unicase-form-control text-input" placeholder="Phone No." value="{{old('mobile')}}">
                                    @if($errors->has('mobile'))
                                <div class="error">{{ $errors->first('mobile') }}</div>
                                @endif
                                 </div>
                              </div>

                        <div class="cont-sec-cont">
                           <div class="form-group">
                              <label class="info-title" for="exampleInputEmail1">Company Name (Not Mandatory) </label>
                              <input class="form-control unicase-form-control text-input" type="text" id="company_name" name="company_name"  placeholder="Company Name(Not Mandatory)" value="{{old('company_name')}}">
                              @if($errors->has('company_name'))
                                <div class="error">{{ $errors->first('company_name') }}</div>
                                @endif
                           </div>
                        </div>
                              <div class="cont-sec-cont">
                                 <div class="form-group">
                                    <label class="info-title" for="exampleInputEmail1">Password <span>*</span></label>
                                    <input class="form-control unicase-form-control text-input" type="password" id="buyer_password" name="password"  placeholder="Password" value="{{old('password')}}">
                                    @if($errors->has('password'))
                                <div class="error">{{ $errors->first('password') }}</div>
                                @endif
                                 </div>
                              </div>
                            <div class="cont-sec-cont">
                                 <div class="form-group">
                                    <label class="info-title" for="exampleInputEmail1">Confirm Password <span>*</span></label>
                                    <input class="form-control unicase-form-control text-input" type="password" id="buyer_confirm_password" name="confirm_password"  placeholder="Confirm Password" value="{{old('confirm_password')}}">
                                    @if($errors->has('confirm_password'))
                                <div class="error">{{ $errors->first('confirm_password') }}</div>
                                @endif
                                 </div>
                              </div>
                              <div class="cont-sec-cont linkss">
                                  <ul>
                                    <button type="submit" class="save_btn"  value="submit" id="submit" name="submit">Sign Up</button>
                                  </ul>
                                  <ul class="text-right">
                                    <li>Already have an account ? <a href="{{route('Front.login')}}">Sign In</a></li>
                                  </ul>
                              </div>
                               <div class="or-log"><p>OR</p></div>
                            <div class="cont-sec-cont text-center">
                                <p>Signup with</p>
                                <span class="facebook"><a href="{{url('/redirect')}}"><i class="fab fa-facebook-f"></i></a></span>
                               <!--  <span class="google"><a href=""><i class="fab fa-google"></i></a></span> -->
                              </div>
                           </form>
                      
                           <form id="supplier" class="logintabcontent" method="post" action="{{url('/sign_up_sup')}}" id="contactForm"  enctype="multipart/form-data" <?php if($section==1){echo "style='display: block'";} ?>>
                              @csrf
                               <div class="form-heading">
                                <h1>Sign Up</h1>
                                   <p>Create a New Account</p>
                               </div>
                              <div class="cont-sec-cont">
                                  <input type="hidden" id="name" name="type" value="2" class="form-control unicase-form-control text-input" placeholder="">
                                 <div class="form-group">
                                    <label class="info-title" for="exampleInputName">Name <span>*</span></label>
                                    <input type="text" id="name" name="name" class="form-control unicase-form-control text-input" placeholder="Name" value="{{old('name')}}">
                                    @if($errors->has('name'))
                                <div class="error">{{ $errors->first('name') }}</div>
                                @endif
                                 </div>
                              </div>
                            <div class="cont-sec-cont">
                                 <div class="form-group">
                                    <label class="info-title" for="exampleInputName">Email Address <span>*</span></label>
                                    <input type="email" id="email" name="email" class="form-control unicase-form-control text-input" placeholder="Email Address" value="{{old('email')}}">
                                    @if($errors->has('email'))
                                <div class="error">{{ $errors->first('email') }}</div>
                                @endif
                                 </div>
                              </div>
                               <div class="cont-sec-cont">
                                 <div class="form-group">
                                    <label class="info-title" for="exampleInputName">Phone No. <span>*</span></label>
                                    <input type="text" id="mobile" maxlength="12"  name="mobile" class="form-control unicase-form-control text-input" placeholder="Phone No." value="{{old('mobile')}}">
                                    @if($errors->has('mobile'))
                                <div class="error">{{ $errors->first('mobile') }}</div>
                                @endif
                                 </div>
                              </div>
                              <div class="cont-sec-cont">
                                 <div class="form-group">
                                    <label class="info-title" for="exampleInputEmail1">Password <span>*</span></label>
                                    <input class="form-control unicase-form-control text-input" type="password" id="password" name="password"  placeholder="Password" value="{{old('password')}}">
                                    @if($errors->has('password'))
                                <div class="error">{{ $errors->first('password') }}</div>
                                @endif
                                 </div>
                              </div>
                            <div class="cont-sec-cont">
                                 <div class="form-group">
                                    <label class="info-title" for="exampleInputEmail1">Confirm Password <span>*</span></label>
                                    <input class="form-control unicase-form-control text-input" type="password" id="confirm_password" name="confirm_password"  placeholder="Confirm Password" value="{{old('confirm_password')}}">
                                    @if($errors->has('confirm_password'))
                                <div class="error">{{ $errors->first('confirm_password') }}</div>
                                @endif
                                 </div>
                              </div>
                        <div class="cont-sec-cont">
                           <div class="form-group">
                              <label class="info-title" for="exampleInputEmail1">GST </label>
                              <input class="form-control unicase-form-control text-input" type="text" id="gst" name="gst" maxlength="20" minlength="15"  placeholder="GST" value="{{old('gst')}}">
                              @if($errors->has('gst'))
                                <div class="error">{{ $errors->first('gst') }}</div>
                                @endif
                           </div>
                        </div>
                        <div class="cont-sec-cont">
                           <div class="form-group">
                              <label class="info-title" for="exampleInputEmail1">Company Name </label>
                              <input class="form-control unicase-form-control text-input" type="text" id="company_name" name="company_name"  placeholder="Company Name (Not Mandatory)" value="{{old('company_name')}}">
                              @if($errors->has('company_name'))
                                <div class="error">{{ $errors->first('company_name') }}</div>
                                @endif
                           </div>
                        </div>
                        <div class="cont-sec-cont">
                           <div class="form-group">
                              <label class="info-title" for="exampleInputEmail1">Address <span>*</span></label>
                              <input class="form-control unicase-form-control text-input" type="text" id="address" name="address"  placeholder="Address" value="{{old('address')}}">
                              @if($errors->has('address'))
                                <div class="error">{{ $errors->first('address') }}</div>
                                @endif
                           </div>
                        </div>

                               <div class="cont-sec-cont">
                                 <div class="form-group">
                                    <label class="info-title" for="exampleInputName">State<span>*</span></label>
                                   <select class="form-control" name="state" id="state">
                           <option value="{{old('state')}}">Select State</option>

                           @foreach($state as $state_data)

                             <option value="{{ $state_data->id }}">{{ $state_data->name }}</option>
                             @endforeach
                     </select>
                                 </div>
                              </div>
                                <div class="cont-sec-cont">
                                 <div class="form-group">
                                    <label class="info-title" for="exampleInputName">City<span>*</span></label>
                                   <select class="form-control" name="city" id="city">
                             <option value="">Select City First</option>
                            
                            
                     </select>
                                 </div>
                              </div> 
                        <div class="cont-sec-cont">
                           <div class="form-group">
                              <label class="info-title" for="exampleInputEmail1">Upload Image </label>
                              <input class="form-control unicase-form-control text-input uplBuyer" type="file" accept="image/x-png,image/gif,image/jpeg"  id="doc_file" name="doc_file"  placeholder="">
                              @if($errors->has('doc_file'))
                                <div class="error">{{ $errors->first('doc_file') }}</div>
                                @endif
                           </div>
                        </div>                       
                              <div class="cont-sec-cont linkss">
                                  <ul>
                                    <button type="submit" class="submit" value="submit">Sign Up</button>
                                  </ul>
                                  <ul class="text-right">
                                    <li>Already have an account ? <a href="{{route('Front.login')}}">Sign In</a></li>
                                  </ul>
                              </div>
                               <div class="or-log"><p>OR</p></div>
                            <div class="cont-sec-cont text-center">
                                <p>signup with</p>
                                <span class="facebook"><a href="{{url('/redirect')}}"><i class="fab fa-facebook-f"></i></a></span>
                                <!-- <span class="google"><a href=""><i class="fab fa-google"></i></a></span> -->
                              </div>
                           </form>
                                          
                  </div>
                        <p id="trimb" style="clear:both;color:red;"></p>
                     </div>
               </div>
            </div>
            <!-- /.row -->
            <!-- ============================================== BRANDS CAROUSEL ============================================== -->
         </div>
      </div>
      
      <script>
      function logintabClick(evt, loginTabName) 
      {
        var i, logintabcontent, logintablinks;
        logintabcontent = document.getElementsByClassName("logintabcontent");
        for (i = 0; i < logintabcontent.length; i++) {
         logintabcontent[i].style.display = "none";
        }
        logintablinks = document.getElementsByClassName("logintablinks");
        for (i = 0; i < logintablinks.length; i++) {
         logintablinks[i].className = logintablinks[i].className.replace(" active", "");
        }
        document.getElementById(loginTabName).style.display = "block";
        evt.currentTarget.className += " active";
      }

      //document.getElementById("defaultOpen").click();
   </script>     
@endsection 
@section('js')

<script type="text/javascript">
    $("#state").on('change', function (e) {   
    var state_id= this.value;  

        var url ={!! json_encode(url('get_state_by_id')) !!}  ;        
          $.ajax({
             url:url,        
              data: {              
              "state_id": state_id,              
              },
        type:"GET",       
        success:function(data){
          $('#city').html(data.html);
        },
        error:function (){}
        });   
    });
</script>


<script>
    $(document).ready(function(){
        $(".supplier-btn").click(function(){
            $(".body-content").addClass("body-content1")
            $(".profile-form").css({"background-color":"rgb(223 230 255)"})
        })
        $(".buyer-btn").click(function(){
            $(".body-content").removeClass("body-content1")
            $(".profile-form").css({"background-color":"rgb(255 222 213)"})
        })
    });
</script>
@endsection