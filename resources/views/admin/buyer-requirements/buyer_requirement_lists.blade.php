@extends('admin.index')

@section('after-style')

<link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endsection

@section('content')

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('admin/admin_dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Manage Buyer Requirement</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Buyer Requirement List</h3>
                        </div>

                        
                        @if(session('message'))
                            <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
                        @endif
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="buyer_requirement_lists" class="table table-bordered table-striped " data-order='[[ 0, "asc" ]]'>


                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Product Name</th>
                                        <th>Description</th>
                                        <th>Quantity</th>
                                        <th>Approximate order value</th>
                                        <th>Email ID</th>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>Phone Number </th>
                                        <th>Terms and Conditions</th>
                                        <th>Whatsapp</th>
                                    <th data-orderable="false">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                       


                        @if(isset($buyers) && $buyers != null)
                        @foreach($buyers as $buyer)
                                            
                        <tr>
                            <td>{{$buyer->id}}</td>
                            <td>{{$buyer->product_name}}</td>                    
                            <td>{{$buyer->description}}</td>
                            <td>{{$buyer->quantity}}</td>
                            <td>{{$buyer->order}}</td>                    
                            <td>{{$buyer->email}}</td>
                            <td>{{$buyer->name}}</td>
                            <td>{{$buyer->address}}</td>   
                            <td>{{$buyer->phone}}</td> 
                            <td>{{$buyer->terms_conditions}}</td>  
                            <td>{{$buyer->whatsapp}}</td>                 
    
             <td class="w--100" style="text-align: center;">
                <a href="{{route('Admin.Delete.BuyerRequirement',[$buyer->id])}}" onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></a>
            </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->



@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<script>
    $(function() {
        $("#buyer_requirement_lists").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });
</script>

<script type="text/javascript">
    $(".pasination ul li").click(function() {
    if ($(".pasination ul li").removeClass("active")) {
        $(this).removeClass("active");
    }
    $(this).addClass("active");
});
</script>

@endsection