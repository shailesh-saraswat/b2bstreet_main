@extends('admin.index')

@section('content')
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>




<!-- <div class="wrapper"> -->

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('admin/admin_dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Add Products</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-1"></div>
                <div class="col-10">

                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Add Products</strong></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        @include('admin.partials.messages')
                        <form class="form-horizontal" method="post" action="{{route('Admin.Add.Product')}}" enctype="multipart/form-data">
                            @csrf
                        <div class="card-body">
                                     
                <div class="row form-group">
                <div class="col-lg-2"><label>Menu Category</label></div>
                <div class="col-lg-10">
                <select class="form-control select2" name="maincategory_id" id="menuCategory">
                 <option>Select Menu Category</option> 
                <option value="">Select Menu category</option>
                             @foreach($maincategories as $maincategory)
                             <option value="{{ $maincategory->id }}">{{ $maincategory->name }}</option>
                             @endforeach
                     </select>
                     </div>
                 </div>

                <div class="row form-group">
                <div class="col-lg-2"><label>Category</label></div>
                <div class="col-lg-10">
                <select class="form-control select2" name="categories_id" id="categoryId">
                <option value="">Select  Category</option>
                    @foreach($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach 
                    </select>
                    </div>
                 </div>

                 <div class="row form-group">
                <div class="col-lg-2"><label>Sub Category</label></div>
                <div class="col-lg-10">
                <select class="form-control select2" name="subcategories_id" id="subcategories_id">
                <option value="">Select Sub Category</option>
                    @foreach($subcategories as $subcategory)
                    <option value="{{ $subcategory->id }}">{{ $subcategory->name }}</option>
                    @endforeach 
                    </select>
                    </div>
                 </div>

                <!-- <div class="form-group row">
                    <label for="Menu Name" class="col-sm-2 col-form-label">Menu Category</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="maincategory_id" id="menuCategory">
                           <option value="">Select Menu category</option>
                           @foreach($maincategories as $maincategory)
                             <option value="{{ $maincategory->id }}">{{ $maincategory->name }}</option>
                             @endforeach
                     </select>
                   </div>
                </div> -->
                     
               <!--  <div class="form-group row">
                <label for="Menu Name" class="col-sm-2 col-form-label">Category </label>
                    <div class="col-sm-10">
                    <select class="form-control select3" name="categories_id" id="categoryId">
                    <option value="">Select Menu Category First</option> 
                     @foreach($categories as $category)
                             <option value="{{ $category->id }}">{{ $category->name }}</option>
                             @endforeach        
                     </select>
                   </div>
                 </div> -->

               <!--  <div class="form-group row">
                <label for="Menu Name" class="col-sm-2 col-form-label">Sub Category </label>
                    <div class="col-sm-10">
                    <select class="form-control" name="subcategories_id" id="subcategoryId">
                    <option value="">Select Category First</option>         
                     </select>
                   </div>
                 </div> -->

                 <div class="form-group row">
                        <label for="Product Name" class="col-sm-2 col-form-label">Product Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="title" name="title" value="{{old('title')}}" placeholder="Product Name" />
                        </div>
                     </div>

                    <div class="form-group row">
                        <label for="Price" class="col-sm-2 col-form-label">Price</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="price" name="price" value="{{old('price')}}" maxlength="10" placeholder="Price" />
                                    </div>
                            </div>           
                        <div class="form-group row">
                        <label for="Price" class="col-sm-2 col-form-label">Quantity Available</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="price" name="quality" value="{{old('price')}}" placeholder="Quantity Available" />
                                    </div>
                            </div>  
                             <div class="form-group row">
                        <label for="Price" class="col-sm-2 col-form-label">Packaging Type</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="price" name="packaging_type" value="{{old('price')}}" placeholder="Packaging Type" />
                                    </div>
                            </div>  
                             <div class="form-group row">
                        <label for="Price" class="col-sm-2 col-form-label">Storage Tips </label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="price" name="storage_tips" value="{{old('price')}}" placeholder="Storage Tips " />
                                    </div>
                            </div> 

                             <div class="form-group row">
                        <label for="Price" class="col-sm-2 col-form-label">Packaging Size </label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="price" name="packaging" value="{{old('price')}}" placeholder="Packaging" />
                                    </div>
                            </div>  
                            <div class="form-group row">
                               <label for="Description" class="col-sm-2 col-form-label">Description</label>
                               <div class="col-sm-10">
                               <textarea type="text" class="form-control textarea" id="description" name="description" value="{{old('description')}}" placeholder="Description"  rows="8"></textarea>
                            </div>
                           </div>       

                <div class="form-group row">
                    <label for="Supplier" class="col-sm-2 col-form-label">Supplier Name</label>
                        <div class="col-sm-10">
                        <select class="form-control" name="user_id">
                           <option value="">Select Supplier</option>
                           @foreach($users as $user)
                             <option value="{{$user->id}}">{{$user->name}}</option>
                             @endforeach
                     </select>
                   </div>
                </div>
             <div class="form-group row">
                <label for="Image" class="col-sm-2 col-form-label">Images</label>
                    <div class="col-sm-10">
                    <input type="file" class="" id="image" name="product_image[]" multiple="multiple" >
                    <img src="" style="width:150px;" />
                </div>
            </div>
            <div class="form-group row">
                                    <label for="Status" class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10">
                                        <select class="form-control select2" style="width: 100%;" name="status" required>
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                           
                                        </select>
                                    </div>
                                </div>
        
        </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-info">Add Products</button>
                                <a href="{{route('Admin.Products.Lists')}}" class="btn btn-default custom-btn">Cancel</a>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>

                </div>
                <div class="col-1"></div>
            </div>
        </div>
    </section>

</div>

<script>
    $('.select2').select2();
</script>

<script>
    $('.select3').select3();
</script>

<style>
.select2-container .select2-selection--single{
    height:38px !important;
}
.select2-container--default .select2-selection--single{
         border: 1px solid #ccc !important; 
     border-radius: 4px !important; 
}
.select2-container .select2-selection--single .select2-selection__rendered{
    padding-left: 0px !important;
}

.select2-results__option:hover {
    background-color: #fd7e14 !important;
}

.select2-container--default .select2-results__option {
    padding: 0px 12px !important;
    font-size: 16px !important;
}
.select2-container--default .select2-results__option--highlighted[aria-selected] {
    background-color: #fd7e14 !important;
    color: white;
}

.select2-container--default .select2-results>.select2-results__options {
    max-height: 200px;
    overflow-y: auto;
    box-shadow: 0px 5px 10px #ccc;
}

.select2-container {
    width: 100% !important;
}

.select2-container--default .select2-selection--single .select2-selection__rendered{
    line-height: 25px !important;
}
</style>

<!-- </div> -->

@endsection
@section('after-scripts')
<script type="text/javascript">
    $("#menuCategory").on('change', function (e) {   
    var maincategory_id= this.value;  

        var url ={!! json_encode(url('admin/get_category_by_menu_id')) !!}  ;        
          $.ajax({
             url:url,        
              data: {              
              "maincategory_id": maincategory_id,              
              },
        type:"GET",       
        success:function(data){
          $('#categoryId').html(data.html);
        },
        error:function (){}
        });   
    });
</script>
<script type="text/javascript">
    $("#categoryId").on('change', function (e) {   
    var categories_id= this.value;  

        var url ={!! json_encode(url('admin/get_subcategory_by_id')) !!}  ;        
          $.ajax({
             url:url,        
              data: {              
              "categories_id": categories_id,              
              },
        type:"GET",       
        success:function(data){
          $('#subcategoryId').html(data.html);
        },
        error:function (){}
        });   
    });
</script>

<script src="{{asset('public/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script>
@endsection

@section('after-style')
<link rel="stylesheet" href="{{asset('public/plugins/summernote/summernote-bs4.css')}}">
@endsection 


