@extends('admin.index')

@section('content')


<!-- <div class="wrapper"> -->

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('admin/admin_dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Update Products</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-1"></div>
                <div class="col-10">

                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Update Products</strong></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        @include('admin.partials.messages')
                        <form class="form-horizontal" method="post" action="{{url('admin/update_products')}}" enctype="multipart/form-data">
                            @csrf
               <input type="hidden" name="id" value="{{$product->id}}" />
                      
                <div class="card-body">

                <div class="form-group row">
                    <label for="Menu Name" class="col-sm-2 col-form-label">Menu Category</label>
                            <div class="col-sm-10">
                            <select class="form-control" name="maincategory_id" id="menuCategory">
                           <option value="">Select Menu category</option>
                           @foreach($maincategories as $maincategory)
                             <option value="{{ $maincategory->id }}" @if( $maincategory->id==$product->maincategory_id) {{'selected'}} @endif>{{ $maincategory->name }}</option>
                             @endforeach
                     </select>
                   </div>
                 </div>
                     
                <div class="form-group row">
                <label for="Menu Name" class="col-sm-2 col-form-label">Category </label>
                    <div class="col-sm-10">
                    <select class="form-control" name="categories_id" id="categoryId">
                    @foreach($categories as $cat)
                             <option value="{{ $cat->id }}" @if( $cat->id==$product->categories_id) {{'selected'}} @endif>{{ $cat->name }}</option>
                             @endforeach        
                     </select>
                   </div>
                 </div>


                <div class="form-group row">
                <label for="Menu Name" class="col-sm-2 col-form-label">Sub Category </label>
                    <div class="col-sm-10">
                    <select class="form-control" name="subcategories_id" id="subcategoryId">
                   @foreach($subcategories as $scat)
                             <option value="{{ $scat->id }}" @if( $scat->id==$product->subcategories_id) {{'selected'}} @endif>{{ $scat->name }}</option>
                             @endforeach              
                     </select>
                   </div>
                 </div>


            <div class="form-group row">
                <label for="Product Name" class="col-sm-2 col-form-label">Product Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="title" name="title" value="{{$product->title}}" placeholder="Product Name"/>
                    </div>
                </div>  

             <div class="form-group row">
                        <label for="Price" class="col-sm-2 col-form-label">Price</label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" id="price" name="price" value="{{$product->price}}" maxlength="10" placeholder="Price" />
                                    </div>
                            </div>           
                 <div class="form-group row">
                        <label for="Price" class="col-sm-2 col-form-label">Quantity Available</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="quality" name="quality" value="{{$product->quality}}" placeholder="Quantity Available" />
                                    </div>
                            </div>  
                             <div class="form-group row">
                        <label for="Price" class="col-sm-2 col-form-label">Packaging Type</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="packaging_type" name="packaging_type" value="{{$product->packaging_type}}" placeholder="Packaging Type" />
                                    </div>
                            </div>  
                             <div class="form-group row">
                        <label for="Price" class="col-sm-2 col-form-label">Storage Tips </label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="storage_tips" name="storage_tips" value="{{$product->storage_tips}}" placeholder="Storage Tips " />
                                    </div>
                            </div> 

                             <div class="form-group row">
                        <label for="Price" class="col-sm-2 col-form-label">Packaging Size </label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="packaging" name="packaging" value="{{$product->packaging}}" placeholder="Packaging" />
                                    </div>
                            </div>  
                            <div class="form-group row">
                               <label for="Description" class="col-sm-2 col-form-label">Description</label>
                               <div class="col-sm-10">
                               <textarea type="text" class="form-control textarea" id="description" name="description" value="" placeholder="Description"  rows="8">{{$product->description}}</textarea>
                            </div>
                           </div> 


                <div class="form-group row">
                <label for="Menu Name" class="col-sm-2 col-form-label">Supplier</label>
                    <div class="col-sm-10">
                    <select class="form-control" name="user_id">
                   @foreach($users as $user)
                             <option value="{{ $user->id }}" @if( $user->id==$product->user_id) {{'selected'}} @endif>{{ $user->name }}</option>
                             @endforeach              
                     </select>
                   </div>
                 </div>      


            

            <div class="form-group row">
                <label for="Image" class="col-sm-2 col-form-label">Images</label>
                    <div class="col-sm-10">
                    <input type="file" class="" id="image" name="product_image[]" multiple="multiple">
                   @foreach($product->product_images as $prod)                          
                        <td><img src="{{ asset($prod['product_image']) }}" width="60px">
                            <a href="{{route('Admin.Delete.ProductImages',[$prod->id])}}" onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></a>
                        </td>
                    @endforeach 
                </div>
            </div>

                            
            <div class="form-group row">
                    <label for="Status" class="col-sm-2 col-form-label">Status</label>
                        <div class="col-sm-10">
                            <select class="form-control select2" style="width: 100%;" name="status" id="status" required>
                                <option value="1" {{$product->status == 1  ? 'selected' : ''}}>Active</option>
                                <option value="0" {{$product->status == 0  ? 'selected' : ''}}>Inactive</option>
                                            
                            </select>
                        </div>
                    </div>    
                        
             
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-info">Update Products</button>
                                <a href="{{url('admin/products_lists')}}" class="btn btn-default custom-btn">Cancel</a>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>

                </div>
                <div class="col-1"></div>
            </div>
        </div>
    </section>

</div>

<!-- </div> -->

@endsection
@section('after-scripts')
<script type="text/javascript">
    $("#menuCategory").on('change', function (e) {   
    var maincategory_id= this.value;  

        var url ={!! json_encode(url('admin/get_category_by_menu_id')) !!}  ;        
          $.ajax({
             url:url,        
              data: {              
              "maincategory_id": maincategory_id,              
              },
        type:"GET",       
        success:function(data){
          $('#categoryId').html(data.html);
        },
        error:function (){}
        });   
    });
</script>
<script type="text/javascript">
    $("#categoryId").on('change', function (e) {   
    var categories_id= this.value;  

        var url ={!! json_encode(url('admin/get_subcategory_by_id')) !!}  ;        
          $.ajax({
             url:url,        
              data: {              
              "categories_id": categories_id,              
              },
        type:"GET",       
        success:function(data){
          $('#subcategoryId').html(data.html);
        },
        error:function (){}
        });   
    });
</script>


<script src="{{asset('public/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script>
@endsection

@section('after-style')
<link rel="stylesheet" href="{{asset('public/plugins/summernote/summernote-bs4.css')}}">
@endsection 


