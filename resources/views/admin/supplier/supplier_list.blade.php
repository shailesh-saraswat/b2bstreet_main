@extends('admin.index')

@section('after-style')

<link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<link rel='stylesheet' href='https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css'>
<link rel='stylesheet' href='https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css'>

@endsection

@section('content')

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('admin/admin_dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Manage Supplier</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Supplier List</h3>

                        <a href="{{route('Admin.Add.Supplier')}}" style="float: right;"><i class="fas fa-plus-square" style="font-size:25px;"></i></a>
                </div>

  @if(session('message'))
   <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
    @endif
<!-- /.card-header -->
<div class="card-body">
<table id="supplier_list" class="table table-bordered table-striped " data-order='[[ 0, "asc" ]]'>


    <thead>
        <tr>
            <th>Id</th>          
            <th>Company Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Enquiry Recieved</th> 
            <th>View Panel</th>
            <th>Subscription</th> 
            <th>Package Name</th> 
            <th>Viewer</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @if(isset($suppliers) && $suppliers != null)
    @foreach($suppliers as $supplier)

        <tr>
            <td>{{$supplier->id}}</td>
            <td>{{$supplier->company_name}}</td>
            <td>{{$supplier->email}}</td>
            <td>{{$supplier->mobile}}</td>
            <td><a href="#" style=" color: red;">
           <!--  <span class="info-box-text">Total Buyer</span> -->
            <span class="info-box-number">{{$supplier->user_id_count ?? 0}}</span>
           </a>
            </td>
           <td><a href="{{route('Admin.supplierPannel',[$supplier->id])}}"><i class="fas fa-eye"></i>&nbsp;&nbsp;</a></td>
           <td style="text-align:center">
            @if($supplier->subscription == 1)
            <p style="color:green">Yes</p>  
            @elseif($supplier->subscription == 0)
            <p style="color:red">No</p>  
            @endif
            </td>
            <td>{{$supplier->package_name}}</td>
            <td>{{$supplier->viewer}}</td>
            

            <td style="text-align: center;">
              

               <a href="{{route('Admin.Edit.Supplier',[$supplier->id])}}"><i class="fas fa-edit"></i>&nbsp;&nbsp;</a>

               <a href="{{route('Admin.Delete.Supplier',[$supplier->id])}}" onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></a>
             
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
   
</div>

@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<script src='https://code.jquery.com/jquery-1.12.3.js'></script>
<script src='https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js'></script>
<script src='https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js'></script>
<script src='https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js'></script>
<script src='https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js'></script>
<script src='https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js'></script>
<script src='https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js'></script>
<script src='https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js'></script>
<script>
    

$(document).ready(function() {
    $('#supplier_list').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'excelFlash', 'excel', 'pdf', 'print',{
            text: 'Reload',
            action: function ( e, dt, node, config ) {
                dt.ajax.reload();
            }
        }
        ]
    } );
} );

</script>
<style type="">
    .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
    background: #007bff !important;
    color: white !important;
}

.dataTables_wrapper .dataTables_paginate .paginate_button:hover{
    background: #007bff !important;
}
div.dataTables_wrapper div.dataTables_filter input {
    margin-left: 0.5em;
    display: inline-block;
    width: auto;
    outline: none;
    border: 1px solid #ccc;
}

</style>
@endsection