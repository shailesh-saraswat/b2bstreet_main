@extends('admin.index')

@section('content')

<!-- <div class="wrapper"> -->

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <!--  -->
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('Admin.Dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Update Supplier</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-1"></div>
                <div class="col-10">

                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Update Supplier <strong></strong></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        @include('admin.partials.messages')
                        <form class="form-horizontal" method="post" action="{{route('Admin.Update.Supplier')}} " enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id" value="{{$supplier->id}}" />
                            <div class="card-body">
                          

                                <div class="form-group row">
                                    <label for="Name" class="col-sm-2 col-form-label">Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="name" name="name" value="{{$supplier->name}}" placeholder="Name" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Company Name" class="col-sm-2 col-form-label">Company Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="company_name" name="company_name" value="{{$supplier->company_name}}" placeholder="Company Name">
                                    </div>
                                </div>

                                    <div class="form-group row">
                                    <label for="Email" class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="email" name="email" value="{{$supplier->email}}" placeholder="Email">
                                    </div>
                                </div>

                                <!--     <div class="form-group row">
                                    <label for="Password" class="col-sm-2 col-form-label">Password</label>
                                    <div class="col-sm-10">
                                        <input type="password" class="form-control" id="password" name="password" value="{{$supplier->password}}" placeholder="Password">
                                    </div>
                                </div> -->

                                <div class="form-group row">
                                    <label for="Phone" class="col-sm-2 col-form-label">Phone</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="mobile" name="mobile" value="{{$supplier->mobile}}" placeholder="Phone">
                                    </div>
                                </div>

                        <div class="form-group row">
                            <label for="Address" class="col-sm-2 col-form-label">Address</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" id="address" name="address" value="{{$supplier->address}}" placeholder="Address" />
                          </div>
                        </div> 


                        <div class="form-group row">
                                    <label for="Subscription" class="col-sm-2 col-form-label">Subscription</label>
                                    <div class="col-sm-10">
                                    <select class="form-control select2" style="width: 100%;" name="subscription" id="subscription" required>
                                        <option value="1" {{$supplier->subscription == 1  ? 'selected' : ''}}>Yes</option>
                                        <option value="0" {{$supplier->subscription == 0  ? 'selected' : ''}}>No</option>
                                           
                                </select>
                            </div>
                        </div>




                 <div class="form-group row">
                        <label for="Package Name" class="col-sm-2 col-form-label">Package Name</label>
                        <div class="col-sm-10">
                        <select class="form-control select2" style="width: 100%;" name="package_name" id="package_name" required>
                        <option value="Visual Initiator" {{$supplier->package_name == 1  ? 'selected' : ''}}>Visual Initiator</option>
                        <option value="Visual Certified" {{$supplier->package_name == 2  ? 'selected' : ''}}>Visual Certified</option>
                        <option value="Visual Sliver" {{$supplier->package_name == 3  ? 'selected' : ''}}>Visual Sliver </option>
                        <option value="Visual Gold" {{$supplier->package_name == 4  ? 'selected' : ''}}>Visual Gold </option>
                        <option value="Visual Platinum" {{$supplier->package_name == 5  ? 'selected' : ''}}>Visual Platinum </option>
                        <option value="Visual Customized" {{$supplier->package_name == 6  ? 'selected' : ''}}>Visual Customized</option>
                        </select>
                            </div>
                        </div>             

               <!--  <div class="form-group row">
                    <label for="Package Name" class="col-sm-2 col-form-label">Package Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="package_name" name="package_name" value="{{$supplier->package_name}}" placeholder="Package Name">
                     </div>
                </div>  -->

                <div class="form-group row">
                    <label for="Date" class="col-sm-2 col-form-label">Date</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control" id="date" name="date" value="{{$supplier->date}}" placeholder="Date">
                     </div>
                </div> 

                <div class="form-group row">
                    <label for="Amount" class="col-sm-2 col-form-label">Amount</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="amount" name="amount" value="{{$supplier->amount}}" placeholder="Amount">
                     </div>
                </div> 

                <div class="form-group row">
                    <label for="Viewer" class="col-sm-2 col-form-label">Viewer</label>
                        <div class="col-sm-10">
                        <input type="text" class="form-control" id="viewer" name="viewer" value="{{$supplier->viewer}}" placeholder="Viewer">
                        </div>
                    </div>
                                
                                <div class="form-group row">
                                <label for="Image" class="col-sm-2 col-form-label">Image</label>
                                <div class="col-sm-10">
                                    <input type="file" class="" id="image" name="image" value="">
                                    <img src="{{ asset($supplier['image']) }}" style="width:150px;" />
                                </div>
                            </div>
                                
                              
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-info">Update Supplier</button>
                                <a href="{{route('Admin.Suppliers.List')}}" class="btn btn-default float-right">Cancel</a>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>

                </div>
                <div class="col-1"></div>
            </div>
        </div>
    </section>

</div>

<!-- </div> -->
@endsection