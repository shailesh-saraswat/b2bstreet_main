@extends('admin.index')

@section('after-style')

<link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endsection

@section('content')

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('admin/admin_dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Manage Banner</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Banner List</h3>

                        <a href="{{route('Admin.Add.Banner')}}" style="float: right;"><i class="fas fa-plus-square" style="font-size:25px;"></i></a>
                </div>

  @if(session('message'))
   <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
    @endif
<!-- /.card-header -->
<div class="card-body">
<table id="category-list" class="table table-bordered table-striped " data-order='[[ 0, "asc" ]]'>


    <thead>
        <tr>
            <th>Id</th>          
            <th>Banner</th>
            <th>Description</th>
            <th>Image</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @if(isset($banners) && $banners != null)
    @foreach($banners as $banner)

        <tr>
            <td>{{$banner->id}}</td>
            <td>{{$banner->title}}</td>
            <td>{{$banner->description}}</td>
            
            <td><img src="{{ asset($banner['banner_image']) }}" width="60px"></td>
           

            <td style="text-align:center">
            @if($banner->status == 1)
            <p style="color:green">Active</p>  
            @elseif($banner->status == 0)
            <p style="color:red">In-active</p>  
            @endif
            </td>

            <td style="text-align: center;">
               @if($banner->is_admin != 1)

               <a href="{{route('Admin.Edit.Banner',[$banner->id])}}"><i class="fas fa-edit"></i>&nbsp;&nbsp;</a>

               <a href="{{route('Admin.Delete.Banner',[$banner->id])}}" onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></a>
                @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
   
</div>

@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<script>
    $(function() {
        $("#category-list").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });
</script>
@endsection