@extends('admin.index')

@section('content')

<!-- <div class="wrapper"> -->

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <!--  -->
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('Admin.Dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Quick Enquiry Form</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-1"></div>
                <div class="col-10">

                    <div class="card card-info">
                        <!-- <div class="card-header">
                            <h3 class="card-title">Update Quick Enquiry Form <strong></strong></h3>
                        </div> -->
                        <!-- /.card-header -->
                        <!-- form start -->
                        @include('admin.partials.messages')
                        <form class="form-horizontal" method="post" action="#" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id" value="{{$notify->id}}" />
                            <div class="card-body" style="padding-top: 50px;">
                          

                                <div class="form-group row">
                                    <label for="Name" class="col-sm-2 col-form-label">Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="name" name="name" value="{{$notify->name}}" placeholder="Name" required />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Company Name" class="col-sm-2 col-form-label">Company Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="company_name" name="company_name" value="{{$notify->company_name}}" placeholder="Company Name" required />
                                    </div>
                                </div>

                                    <div class="form-group row">
                                    <label for="Email" class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="email" name="email" value="{{$notify->email}}" placeholder="Email" required />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Phone" class="col-sm-2 col-form-label">Phone</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="phone" name="phone" value="{{$notify->phone}}" placeholder="Phone" required />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Product Name" class="col-sm-2 col-form-label">Product Name</label>
                                    <div class="col-sm-10">
                                        <input type="test" class="form-control" id="product_name" name="product_name" value="{{$notify->product_name}}" placeholder="Product Name" required />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Quality" class="col-sm-2 col-form-label">Quality</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="quality" name="quality" value="{{$notify->quality}}" placeholder="Quality" required />
                                    </div>
                                </div>
                            <div class="form-group row">
                                    <label for="Address" class="col-sm-2 col-form-label">Address</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="address" name="address" value="{{$notify->address}}" placeholder="Address" required />
                                    </div>
                                </div>

                                
                              
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <a href="{{route('Admin.Notify.Lists')}}" class="btn btn-default float-right">Cancel</a>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>

                </div>
                <div class="col-1"></div>
            </div>
        </div>
    </section>

</div>

<!-- </div> -->
@endsection