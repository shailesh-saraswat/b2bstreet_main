@extends('admin.index')

@section('after-style')

<link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endsection

@section('content')

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('admin/admin_dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Manage Quick Enquiry Form</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <!-- <div class="card-header">
                        <h3 class="card-title">Enquiry Form List</h3>

                        <a href="" style="float: right;"><i class="fas fa-plus-square" style="font-size:25px;"></i></a>
                </div> -->

  @if(session('message'))
   <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
    @endif
<!-- /.card-header -->
<div class="card-body">
<table id="enquiry-form-list" class="table table-bordered table-striped " data-order='[[ 0, "asc" ]]'>


    <thead>
        <tr>
            <th>Id</th>         
            <th>Name</th>
            <th>Company Name</th>
            <th>Phone Number</th>
            <th>Email ID</th>
            <th>Product Name</th>
            <th>Qty</th>
            <th>Add Address</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @if(isset($quickforms) && $quickforms != null)
    @foreach($quickforms as $quickform)

        <tr>
            <td>{{$quickform->id}}</td>
            <td>{{$quickform->name}}</td>
            <td>{{$quickform->company_name}}</td>
            <td>{{$quickform->phone}}</td>
            <td>{{$quickform->email}}</td>
            <td>{{$quickform->product_name}}</td>
            <td>{{$quickform->qty}}</td>
            <td>{{$quickform->address}}</td>
            
       
            <td style="text-align: center;">
               @if($quickform->is_admin != 1)

               <a href="{{route('Admin.Delete.QuickForm',[$quickform->id])}}" onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></a>
                @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
   
</div>

@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<script>
    $(function() {
        $("#enquiry-form-list").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });
</script>
@endsection