@extends('admin.index')

@section('after-style')

<link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endsection

@section('content')

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('admin/admin_dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Manage Supplier Enquiry Form</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <!-- <div class="card-header">
                        <h3 class="card-title">Enquiry Form List</h3>

                        <a href="" style="float: right;"><i class="fas fa-plus-square" style="font-size:25px;"></i></a>
                </div> -->

  @if(session('message'))
   <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
    @endif
<!-- /.card-header -->
<div class="card-body">
<table id="carrer_form_list" class="table table-bordered table-striped " data-order='[[ 0, "asc" ]]'>


    <thead>
        <tr>
            <th>Id</th>   
            <th>Name</th>
            <th>Email ID</th>
            <th>Phone Number</th>
            <th>Location</th>
            <th>Message</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @if(isset($career) && $career != null)
    @foreach($career as $caree)

        <tr>
            <td>{{$caree->id}}</td>
            <td>{{$caree->name}}</td>
            <td>{{$caree->email}}</td>
            <td>{{$caree->phone}}</td>
            <td>{{$caree->location}}</td>
            <td>{{$caree->message}}</td>
         
            <td style="text-align: center;">
               @if($caree->is_admin != 1)

               <a href="{{Route('Admin.Delete.Carrer',[$caree->id])}}" onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></a>
                @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
   
</div>

@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<script>
    $(function() {
        $("#carrer_form_list").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });
</script>
@endsection