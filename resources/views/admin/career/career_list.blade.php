@extends('admin.index')

@section('after-style')

<link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endsection

@section('content')

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('admin/admin_dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Manage  Career</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Career List</h3>

                            <a href="{{route('Admin.Add.Career.Form')}}" style="float: right;"><i class="fas fa-plus-square" style="font-size:25px;"></i></a>
                        </div>

                        
                        @if(session('message'))
                            <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
                        @endif
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="career_list" class="table table-bordered table-striped " data-order='[[ 0, "asc" ]]'>


                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Content</th>
                                        <th>Short Content</th>
                                        <th>Status</th>
                                    <th data-orderable="false">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                       


                        @if(isset($careers) && $careers != null)
                        @foreach($careers as $career)
                                            
                        <tr>
                            <td>{{$career->id}}</td>
                            <td>{{$career->title}}</td>                          
                            <td>{!!$career->description!!}</td>                          
                            <td>{!!$career->content!!}</td>
                            <td>{!!$career->short_content!!}</td>
           
       <td style="text-align:center">
        @if($career->status == 1)
        <p style="color:green">Active</p>  
        @elseif($career->status == 0)
        <p style="color:red">Incative</p>  
            @endif
        </td>

             <td class="w--100" style="text-align: center;">
             
                <a href="{{Route('Admin.Edit.Career',[$career->id])}}"><i class="fas fa-edit"></i>&nbsp;&nbsp;</a>
                
                <a href="{{Route('Admin.Delete.Career',[$career->id])}}" onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></a>
            </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->



@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<script>
    $(function() {
        $("#career_list").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });
</script>

<script type="text/javascript">
    $(".pasination ul li").click(function() {
    if ($(".pasination ul li").removeClass("active")) {
        $(this).removeClass("active");
    }
    $(this).addClass("active");
});
</script>

@endsection