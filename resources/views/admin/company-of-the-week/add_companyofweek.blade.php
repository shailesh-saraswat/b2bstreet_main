@extends('admin.index')

@section('content')

<!-- <div class="wrapper"> -->

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Add Company Of The Weeks</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
         <div class="col-1"></div>
         <div class="col-10">
         <div class="card card-info">
            <div class="card-header">
        <h3 class="card-title">Add Company Of The Weeks</strong></h3>
                </div>
                        <!-- /.card-header -->
                        <!-- form start -->
         @include('admin.partials.messages')
        <form class="form-horizontal" method="post" action="{{route('Admin.Add.CompanyOfWeek')}}" enctype="multipart/form-data">
            @csrf
            
            <div class="card-body">                 
                <div class="card-body">
                <div class="form-group row">
                <label for="Company Name" class="col-sm-2 col-form-label">Company Name</label>
                            <div class="col-sm-10">
                            <select class="form-control" name="user_id" id="menuCategory">
                           <option value="">Select Company Name</option>
                           @foreach($user as $users)
                           @if(!empty($users->company_name))
                             <option value="{{ $users->id }}">{{ $users->company_name }}</option>
                             @endif
                             @endforeach
                     </select>
                   </div>
                </div>                 

                <div class="form-group row">
                    <label for="Description" class="col-sm-2 col-form-label">Description</label>
                    <div class="col-sm-10">
                    <textarea type="text" class="form-control textarea" id="description" name="description" value="" placeholder="Description" value="{{old('company_name')}}" rows="10"></textarea>
                </div>
               </div>
            <div class="form-group row">
                <label for="Video" class="col-sm-2 col-form-label">Video</label>
                    <div class="col-sm-10">
                    <input type="file" class="" id="video" name="video">
                    <img src="" style="width:150px;" />
                </div>
            </div>

                               
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-info">Add Company Of The Weeks</button>
                <a href="{{route('Admin.CompanyOfWeek.Lists')}}" class="btn btn-default custom-btn">Cancel</a>
            </div>
             <!-- /.card-footer -->
        </form>
    </div>
  </div>
<div class="col-1"></div>
</div>
</div>
</section>

</div>

<!-- </div> -->

@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script>
@endsection
@section('after-style')
<link rel="stylesheet" href="{{asset('public/plugins/summernote/summernote-bs4.css')}}">
@endsection 

