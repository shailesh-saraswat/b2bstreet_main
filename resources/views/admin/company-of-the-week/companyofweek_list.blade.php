@extends('admin.index')

@section('after-style')

<link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endsection

@section('content')

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('admin/admin_dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Manage Company Of The Weeks</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Company Of The Week List</h3>

                        <a href="{{Route('Admin.Add.CompanyOfWeek.Form')}}" style="float: right;"><i class="fas fa-plus-square" style="font-size:25px;"></i></a>
                </div>

  @if(session('message'))
   <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
    @endif
<!-- /.card-header -->
<div class="card-body">
<table id="companyofweek_list" class="table table-bordered table-striped " data-order='[[ 0, "asc" ]]'>


    <thead>
        <tr>
            <th>Id</th>          
            <th>Company Name</th>  
            <th>Description</th>             
            <th>Video</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @if(isset($companies) && $companies != null)
    @foreach($companies as $company)

        <tr>
            <td>{{$company->id}}</td>
            <td>@if($company->user) {{$company->user->company_name}} @endif</td>
            
            <td>{{$company->description}}</td>
            <td><video width="100" height="100" controls>
            <source src="{{ asset($company['video']) }}" type="video/mp4">
            </video></td>

            <td style="text-align: center;">
               @if($company->is_admin != 1)

               <a href="{{Route('Admin.Edit.CompanyOfWeek',[$company->id])}}"><i class="fas fa-edit"></i>&nbsp;&nbsp;</a>

               <a href="{{Route('Admin.Delete.CompanyOfWeek',[$company->id])}}" onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></a>
                @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
   
</div>

@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<script>
    $(function() {
        $("#companyofweek_list").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });
</script>
@endsection