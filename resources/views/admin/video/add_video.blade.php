@extends('admin.index')

@section('content')




<!-- <div class="wrapper"> -->

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Add Video</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
         <div class="col-1"></div>
         <div class="col-10">
         <div class="card card-info">
            <div class="card-header">
        <h3 class="card-title">Add Video</strong></h3>
                </div>
                        <!-- /.card-header -->
                        <!-- form start -->
         @include('admin.partials.messages')
        <form class="form-horizontal" method="post" action="{{route('Admin.Add.Video.Form')}}" enctype="multipart/form-data">
            @csrf
            
            <div class="card-body">

                  <div class="form-group row">
                    <label for="Menu Name" class="col-sm-2 col-form-label">Company Name</label>
                            <div class="col-sm-10">
                            <select class="form-control" name="user_id" id="menuCategory">
                           <option value="">Select Company Name</option>
                           @foreach($user as $users)
                           @if(!empty($users->company_name))
                             <option value="{{ $users->id }}">{{ $users->company_name }}</option>
                             @endif
                             @endforeach
                     </select>
                   </div>
                 </div>

               <!--  <div class="form-group row">
                    <label for="Title" class="col-sm-2 col-form-label">Title</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="title" name="title" value="{{old('title')}}" placeholder="Title" required />
                     </div>
                    </div> -->

                <div class="form-group row">
                    <label for="Description" class="col-sm-2 col-form-label">Description</label>
                    <div class="col-sm-10">
                    <textarea type="text" class="form-control" id="description" name="description" value="{{old('description')}}" placeholder="Description"  rows="8"></textarea>
                </div>
            </div>
                          

            <div class="form-group row">
                <label for="Image" class="col-sm-2 col-form-label">Image</label>
                    <div class="col-sm-10">
                    <input type="file" class="" id="image" name="image">
                    <img src="" style="width:150px;" />
                </div>
            </div>

            <div class="form-group row">
                <label for="Video" class="col-sm-2 col-form-label">Video</label>
                    <div class="col-sm-10">
                    <input type="file" class="" id="video" name="video">
                    <img src="" style="width:150px;" />
                </div>
            </div>

            <div class="form-group row">
                <label for="Status" class="col-sm-2 col-form-label">Trending</label>
                    <div class="col-sm-10">
                        <select class="form-control select2" style="width: 100%;" name="is_trending" required>
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                        </select>
                    </div>
            </div>

             <div class="form-group row">
                <label for="Status" class="col-sm-2 col-form-label">Recommended</label>
                    <div class="col-sm-10">
                        <select class="form-control select2" style="width: 100%;" name="is_recommended" required>
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                        </select>
                    </div>
            </div>

                             
            <div class="form-group row">
                <label for="Status" class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-10">
                        <select class="form-control select2" style="width: 100%;" name="status" required>
                            <option value="0">Inactive</option>
                            <option value="1">Active</option>
                        </select>
                    </div>
            </div>

                               
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-info">Add Video</button>
                <a href="{{route('Admin.Video.Lists')}}" class="btn btn-default custom-btn">Cancel</a>
            </div>
             <!-- /.card-footer -->
        </form>
    </div>
  </div>
<div class="col-1"></div>
</div>
</div>
</section>

</div>

<!-- </div> -->
<script type="text/javascript">
    $("#menuCategory").on('change', function (e) {   
    var maincategory_id= this.value;  

        var url ={!! json_encode(url('admin/get_category_by_menu_id')) !!}  ;        
          $.ajax({
             url:url,        
              data: {              
              "maincategory_id": maincategory_id,              
              },
        type:"GET",       
        success:function(data){
          $('#categoryId').html(data.html);
        },
        error:function (){}
        });   
    });
</script>
@endsection
