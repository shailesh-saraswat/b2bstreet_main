@extends('admin.index')

@section('content')

<!-- <div class="wrapper"> -->

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <!--  -->
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('Admin.Dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Update Video</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-1"></div>
                <div class="col-10">

                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Update Video <strong></strong></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        @include('admin.partials.messages')
                        <form class="form-horizontal" method="post" action="{{route('Admin.Update.Video')}} " enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id" value="{{$video->id}}" />
                            <div class="card-body">
                      




                <div class="form-group row">
                    <label for="Menu Name" class="col-sm-2 col-form-label">Company Name</label>
                            <div class="col-sm-10">
                            <select class="form-control" name="user_id">
                           <option value="">Select Company Name</option>
                           @foreach($user as $users)
                           
                           @if(!empty($users->company_name))
           <option value="{{ $users->id }}" @if($video->user_id==$users->id){{'selected'}} @endif>{{ $users->company_name }}</option>
                             @endif
                             @endforeach
                     </select>
                   </div>
                 </div>


               <!--  <div class="form-group row">
                    <label for="Title" class="col-sm-2 col-form-label">Title</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="title" name="title" value="{{$video->title}}" placeholder="Title" required />
                     </div>
                </div> -->

                <div class="form-group row">
                    <label for="Description" class="col-sm-2 col-form-label">Description</label>
                    <div class="col-sm-10">
                    <textarea type="text" class="form-control" id="description" name="description" value="" placeholder="Description"  rows="8">{{$video->description}}</textarea>
                </div>
            </div>

                                

                            <div class="form-group row">
                                <label for="Image" class="col-sm-2 col-form-label">Image</label>
                                <div class="col-sm-10">
                                    <input type="file" class="" id="image" name="image" value="">
                                    <img src="{{ asset($video['image']) }}" style="width:150px;" />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="Video" class="col-sm-2 col-form-label">Video</label>
                                <div class="col-sm-10">
                                    <input type="file" class="" id="video" name="video" value="">
                                    <img src="{{ asset($video['video']) }}" style="width:150px;" />
                                </div>
                            </div>
                                
                                <div class="form-group row">
                                    <label for="Tranding" class="col-sm-2 col-form-label">Tranding</label>
                                    <div class="col-sm-10">
                                        <select class="form-control select2" style="width: 100%;" name="is_trending" id="is_trending" required>
                                            <option value="0" {{$video->is_trending == 0  ? 'selected' : ''}}>No</option>
                                            <option value="1" {{$video->is_trending == 1  ? 'selected' : ''}}>Yes</option>
                                        </select>
                                    </div>
                                </div>  

                                <div class="form-group row">
                                    <label for="Recommended" class="col-sm-2 col-form-label">Recommended</label>
                                    <div class="col-sm-10">
                                        <select class="form-control select2" style="width: 100%;" name="is_recommended" id="is_recommended" required>
                                            <option value="0" {{$video->is_recommended == 0  ? 'selected' : ''}}>No</option>
                                            <option value="1" {{$video->is_recommended == 1  ? 'selected' : ''}}>Yes</option>
                                        </select>
                                    </div>
                                </div>  
                                <div class="form-group row">
                                    <label for="Status" class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10">
                                        <select class="form-control select2" style="width: 100%;" name="status" id="status" required>
                                            <option value="0" {{$video->status == 0  ? 'selected' : ''}}>Inactive</option>
                                            <option value="1" {{$video->status == 1  ? 'selected' : ''}}>Active</option>
                                        </select>
                                    </div>
                                </div>                                

                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-info">Update Video</button>
                                <a href="{{route('Admin.Video.Lists')}}" class="btn btn-default float-right">Cancel</a>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>

                </div>
                <div class="col-1"></div>
            </div>
        </div>
    </section>

</div>

<!-- </div> -->
@endsection