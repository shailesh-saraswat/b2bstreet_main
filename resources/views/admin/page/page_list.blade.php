@extends('admin.index')

@section('after-style')

<link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endsection

@section('content')

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('admin/admin_dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Manage Pages</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Pages List</h3>

                        <a href="{{route('Admin.Add.Page.Form')}}" style="float: right;"><i class="fas fa-plus-square" style="font-size:25px;"></i></a>
                </div>

  @if(session('message'))
   <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
    @endif
<!-- /.card-header -->
<div class="card-body">
<table id="page_list" class="table table-bordered table-striped " data-order='[[ 0, "asc" ]]'>



    <thead>
        <tr>
            <th>Id</th>          
            <th>Title</th>
            <th>Name</th>
            <th>Content</th>
            <th>Short Content</th>
            <th>Image</th>
            <th>Status</th>
            <th>Type</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @if(isset($pages) && $pages != null)
    @foreach($pages as $page)
        <tr>
            <td>{{$page->id}}</td>
            <td>{{$page->title}}</td>
            <td>{{$page->name}}</td>
            <td class="w-200">{!! $page->content !!}</td>
            <td class="w-200">{!! $page->short_content !!}</td>
            <td><img src="{{ asset($page['image']) }}" width="60px"></td>
           
       <td style="text-align:center">@if($page->status == 1)<p style="color:green">Active</p>  
            @elseif($page->status == 0)
            <p style="color:red">In-active</p>@endif
            </td>

            <td style="text-align:center">@if($page->type == 1)<p>Normal</p>
            @elseif($page->type == 2)<p>Pages</p>
            @endif
            </td>
          

            <td style="text-align: center;">
               @if($page->is_admin != 1)

               <a href="{{route('Admin.Edit.Page',[$page->id])}}"><i class="fas fa-edit"></i>&nbsp;&nbsp;</a>

               <a href="{{route('Admin.Delete.Page]',[$page->id])}}" onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></a>
                @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
   
</div>

@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<script>
    $(function() {
        $("#page_list").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });
</script>
@endsection