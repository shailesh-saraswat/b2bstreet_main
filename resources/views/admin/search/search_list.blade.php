@extends('admin.index')

@section('after-style')

<link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

@endsection

@section('content')

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('admin/admin_dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Manage Interview</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Interview List</h3>

                        <a href="{{route('Admin.Add.Interview')}}" style="float: right;"><i class="fas fa-plus-square" style="font-size:25px;"></i></a>
                </div>

  @if(session('message'))
   <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
    @endif
<!-- /.card-header -->
<div class="card-body">
    <div class="form-group">
<input type="text" class="form-controller" id="search" name="search"></input>
</div>

<table id="search_list" class="table table-bordered table-striped " data-order='[[ 0, "asc" ]]'>


    <thead>
        <tr>
            <th>Id</th>          
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Company Name</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @if(isset($search) && $search != null)
    @foreach($search as $searchs)

        <tr>
            <td>{{$searchs->id}}</td>
            <td>{{$searchs->name}}</td>
            <td>{{$searchs->description}}</td>
            <td>{{$searchs->price}}</td>
            <td>{{$searchs->company_name}}</td>
    
            
            <td style="text-align: center;">
               @if($searchs->is_admin != 1)

               <a href="#"><i class="fas fa-edit"></i>&nbsp;&nbsp;</a>

               <a href="#" onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></a>
                @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
   
</div>

@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<script>
    $(function() {
        $("#search_list").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });
</script>

<script type="text/javascript">
$('#search').on('keyup',function(){
$value=$(this).val();
$.ajax({
type : 'get',
url : '{{URL::to('search')}}',
data:{'search':$value},
success:function(data){
$('tbody').html(data);
}
});
})
</script>
<script type="text/javascript">
$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>

@endsection