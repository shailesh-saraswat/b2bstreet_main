@extends('admin.index')

@section('content')

<!-- <div class="wrapper"> -->

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Update Main Categories</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-1"></div>
                <div class="col-10">

                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Update Main Category</strong></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        @include('admin.partials.messages')


                <form class="form-horizontal" method="post" action="       {{route('Admin.Update.Maincategory')}}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id" value="{{$maincategory->id}}" />
                            <div class="card-body">
                                
                                <div class="form-group row">
                                    <label for="Name" class="col-sm-2 col-form-label">Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="name" name="name" value="{{$maincategory->name}}" placeholder="Name" maxlength="
                                        40" required />
                                    </div>
                                </div>                        
                         <div class="form-group row">
                            <label for="Image" class="col-sm-2 col-form-label">Image</label>
                            <div class="col-sm-10">
                                <input type="file" class="" id="image" name="image" value="">
                                <img src="{{ asset($maincategory['image']) }}" style="width:150px;" />
                            </div>
                        </div>   
                             
                                <div class="form-group row">
                                    <label for="Status" class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10">
                                        <select class="form-control select2" style="width: 100%;" name="status" id="status" required>
                                             <option value="1" {{$maincategory->status == 1  ? 'selected' : ''}}>Active</option>
                                            <option value="0" {{$maincategory->status == 0  ? 'selected' : ''}}>Inactive</option>
                                           
                                        </select>
                                    </div>
                                </div>

                               
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-info">Add Main Category</button>
                                <a href="{{url('admin/maincategory_list')}}" class="btn btn-default custom-btn">Cancel</a>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>

                </div>
                <div class="col-1"></div>
            </div>
        </div>
    </section>

</div>

<!-- </div> -->

@endsection