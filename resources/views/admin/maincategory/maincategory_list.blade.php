@extends('admin.index')

@section('after-style')

<link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endsection

@section('content')

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('admin/admin_dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Manage Main Categories</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Main Categories List</h3>

                            <a href="{{url('admin/add_maincategory')}}" style="float: right;"><i class="fas fa-plus-square" style="font-size:25px;"></i></a>
                        </div>

                        
                        @if(session('message'))
                            <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
                        @endif
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="maincategory_list" class="table table-bordered table-striped " data-order='[[ 0, "asc" ]]'>


                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Image</th>
                                        <th>Status</th>
                                        <th data-orderable="false">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                        @if(isset($maincategories) && $maincategories != null)
                        @foreach($maincategories as $maincategory)
                                            
                        <tr>
                            <td>{{$maincategory->id}}</td>
                            <td>{{$maincategory->name}}</td>
                            <td><img src="{{ asset($maincategory['image']) }}" width="60px"></td>
                            
                            
                            
           
       <td style="text-align:center">
        @if($maincategory->status == 1)
        <p style="color:green">Active</p>  
        @elseif($maincategory->status == 0)
        <p style="color:red">In-active</p>  
            @endif
        </td>


             <td style="text-align: center;">
        <a href="javascript:void();" data-toggle="modal" data-target="#maincategory-info{{$maincategory->id}}"><i class="fas fa-eye"></i>&nbsp;&nbsp;</a>
            
        <a href="{{route('Admin.Store.Maincategory',[$maincategory->id])}}"><i class="fas fa-edit"></i>&nbsp;&nbsp;</a>


        
        <a href="{{url('admin/delete_maincategory/'.$maincategory->id)}}" onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></a>
       
                                                </td>

       
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
     @include('admin.maincategory.view_maincategory') 
</div>

@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<script>
    $(function() {
        $("#maincategory_list").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });
</script>
@endsection