@extends('admin.index')

@section('after-style')
<style>

    a{

        color:inherit;
    }
</style>

@endsection

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Admin Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Admin Dashboard</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Info boxes -->
            <div class="row">
                <div class="col-12 col-sm-6 col-md-3">
                    <a href="{{url('admin/buyer-list')}}">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Total Buyers</span>
                                <span class="info-box-number">{{$countBuyers ?? 0}}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                    </a>
                    <!-- /.info-box -->
                </div>

                <div class="col-12 col-sm-6 col-md-3">
                    <a href="{{url('admin/supplier_list')}}">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1" style="background-color: #d81b60!important;"><i class="fas fa-user"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Total Supplier</span>
                                <span class="info-box-number">{{$countSuppliers ?? 0}}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                    </a>
                    <!-- /.info-box -->
                </div>
                

                <!-- fix for small devices only -->
                <div class="clearfix hidden-md-up"></div>

            </div>
            <!-- /.row -->
        </div><!--/. container-fluid -->
    </section>

    <!-- /.content -->


     <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Enquiry Form</h1>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Info boxes -->
            <div class="row">
                <div class="col-12 col-sm-6 col-md-3">
                    <a href="{{url('admin/buyer_requirement_lists')}}">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1" style="background-color: #6f42c1!important;"><i class="fas fa-user"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Buy Leads</span>
                                <span class="info-box-number">{{$countLeads ?? 0}}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                    </a>
                    <!-- /.info-box -->
                </div>

                <div class="col-12 col-sm-6 col-md-3">
                    <a href="{{url('admin/enquiry_form_list')}}">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1" style="background-color: #fd7e14!important;"><i class="fas fa-user"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">B2B Enquiry</span>
                                <span class="info-box-number">{{$countEnquiryForm ?? 0}}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                    </a>
                    <!-- /.info-box -->
                </div>

                <div class="col-12 col-sm-6 col-md-3">
                    <a href="{{url('admin/supplier_contact_lists')}}">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1" style="background-color: #ad1075!important;"><i class="fas fa-user"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Seller Enquiry</span>
                                <span class="info-box-number">{{$countSellerform ?? 0}}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                    </a>
                    <!-- /.info-box -->
                </div>

                <div class="col-12 col-sm-6 col-md-3">
                   
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1" style="background-color: #0d9a71!important;"><i class="fas fa-user"></i></span>
                             
                            <a href="{{url('admin/Quick_form_list')}}">
                            <div class="info-box-content">
                                <span class="info-box-text">Quick Enquiry Form</span>
                                <span class="info-box-number">{{$countQuickforms ?? 0}}</span>
                            </div></a>
                            
                            <a href="{{url('admin/notify')}}">
                            <div class="info-box-content" style="color:red;">
                                <span class="info-box-text">Unread</span>
                                <span class="info-box-number">{{$notifyforms}}</span>
                            </div>
                        </a>
                            <!-- /.info-box-content -->
                        </div>
                  
                    <!-- /.info-box -->
                </div>
                

                <!-- fix for small devices only -->
                <div class="clearfix hidden-md-up"></div>

            </div>
            <!-- /.row -->
        </div><!--/. container-fluid -->
    </section>

    <!-- /.content -->
</div>
@endsection
