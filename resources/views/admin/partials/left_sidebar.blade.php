<style>
.side-active{
    background:#9e3058;
}
</style>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('admin/admin_dashboard')}}" class="brand-link">
        <img src="{{asset('public/images/About-img.png')}}" alt="Sanitization" class="brand-image img-circle elevation-3"
             style="opacity: .8;">
        <span class="brand-text font-weight-light">{{env('APP_NAME')}}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{auth()->user()->profile_image ?? asset('public/images/user-logo.png')}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="{{url('admin/admin_dashboard')}}" class="d-block">{{auth()->user()->name ?? ""}}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item {{ (request()->is('admin/profile')) || (request()->is('admin/change-password')) || (request()->is('admin/get-profile')) ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-info"></i>
                        <p>Manage Profile
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                     <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('Admin.Profile')}}" class="nav-link {{ (request()->is('admin/profile')) ? 'active-child' : '' }}">
                                <i class="fas fa-address-card nav-icon"></i>
                                <p>Profile</p>
                            </a>
                        </li>
                       <li class="nav-item">
                            <a href="{{route('Admin.Change.Password.Form')}}" class="nav-link {{ (request()->is('admin/change-password')) ? 'active-child' : '' }}">
                                <i class="fas fa-key nav-icon"></i>
                                <p>Change Password</p>
                            </a>
                        </li>
                        
                    </ul>
                </li>

             <li class="nav-item">
                    <a href="{{route('Admin.Buyers.List')}}" class="nav-link {{ (request()->is('admin/buyer-list')) || (request()->is('admin/add-buyer')) || (request()->is('admin/edit-buyer/*'))? 'active' : '' }}">
                        <i class="nav-icon fas fa-users-cog"></i>
                        <p>
                            Manage Buyers
                        </p>
                    </a>
                </li> 

                 <li class="nav-item">
                    <a href="{{route('Admin.Suppliers.List')}}" class="nav-link {{ (request()->is('admin/supplier_list')) || (request()->is('admin/add_supplier')) || (request()->is('admin/edit_supplier/*'))? 'active' : '' }}">
                        <i class="nav-icon fas fa-users-cog"></i>
                        <p>
                            Manage Supplier
                        </p>
                    </a>
                </li>  


           <li class="nav-item {{ (request()->is('admin/maincategory_list')) || (request()->is('admin/category_list')) || (request()->is('admin/subcategory_list')) ? 'menu-open' : '' }}">          

                    <a href="#" class="nav-link ">
                        <i class="nav-icon fas fa-list-alt"></i>
                        <p>Manage Category
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{route('Admin.maincategory_list')}}" class="nav-link {{ (request()->is('admin/maincategory_list')) || (request()->is('admin/add_maincategory')) || (request()->is('admin/update/*'))? 'active-child' : '' }}">
                     <i class="nav-icon fa fa-circle-o"></i>
                        <p>Main Category</p>
                    </a>
                </li> 
                 
                 <li class="nav-item">
                    <a href="{{route('Admin.category_list')}}" class="nav-link {{ (request()->is('admin/category_list')) || (request()->is('admin/add_category')) || (request()->is('admin/edit_category/*'))? 'active-child' : '' }}">
                        <i class="nav-icon fa fa-circle-o"></i>
                        <p>Category</p>
                    </a>
                </li>   
                    
                <li class="nav-item">
                    <a href="{{route('Admin.subcategory_list')}}" class="nav-link {{ (request()->is('admin/subcategory_list')) || (request()->is('admin/add_subcategory')) || (request()->is('admin/edit_subcategory/*'))? 'active-child' : '' }}">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p>Sub Category</p>
                    </a>
                </li>          
                </ul>
            </li>

            <li class="nav-item">
                    <a href="{{route('Admin.Products.Lists')}}" class="nav-link {{ (request()->is('admin/products_lists')) || (request()->is('admin/add_products')) || (request()->is('admin/edit_products/*'))? 'active' : '' }}">
                        <i class="nav-icon fas fa-users-cog"></i>
                        <p>
                            Manage Products
                        </p>
                    </a>
                </li>  

             <li class="nav-item {{ (request()->is('admin/ourteam_list')) || (request()->is('admin/our_client_list')) || (request()->is('admin/smartblock_list')) ? 'menu-open' : '' }}">  
                    <a href="#" class="nav-link ">
                        <i class="nav-icon fas fa-list-alt"></i>
                        <p>Manage About Us
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                   <a href="{{route('Admin.Smartblock.Lists')}}" class="nav-link {{ (request()->is('admin/smartblock_list')) || (request()->is('admin/add_smartblock')) || (request()->is('admin/edit_smartblock/*'))? 'active-child' : '' }}">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p>About Us</p>
                    </a>
                </li>    
             <li class="nav-item">
                   <a href="{{route('Admin.Ourteam.Lists')}}" class="nav-link {{ (request()->is('admin/ourteam_list')) || (request()->is('admin/add_ourteam')) || (request()->is('admin/edit_ourteam/*'))? 'active-child' : '' }}">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p>Our Team</p>
                    </a>
                </li>             
                <li class="nav-item">
                   <a href="{{route('Admin.OurClient.Lists')}}" class="nav-link {{ (request()->is('admin/our_client_list')) || (request()->is('admin/add_our_client')) || (request()->is('admin/edit_our_client/*'))? 'active-child' : '' }}">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p>Our Clients</p>
                    </a>                
                </li>                     
                     
                </ul>
            </li>

            <li class="nav-item {{ (request()->is('admin/video_list')) || (request()->is('admin/banner_list')) || (request()->is('admin/interview_list')) || (request()->is('admin/testimonial_list'))  || (request()->is('admin/page_list'))  || (request()->is('admin/gallery_list')) || (request()->is('admin/companyofweek_list')) ? 'menu-open' : '' }}">          

                    <a href="#" class="nav-link ">
                        <i class="nav-icon nav-icon fa fa-credit-card"></i>
                        <p>Manage CMS
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                   <a href="{{route('Admin.Banner.Lists')}}" class="nav-link {{ (request()->is('admin/banner_list')) || (request()->is('admin/add_banner')) || (request()->is('admin/edit_banner/*'))? 'active-child' : '' }}">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p>Banner</p>
                    </a>
                </li> 

                <li class="nav-item">
                   <a href="{{route('Admin.Video.Lists')}}" class="nav-link {{ (request()->is('admin/video_list')) || (request()->is('admin/add_video')) || (request()->is('admin/edit_video/*'))? 'active-child' : '' }}">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p>Video</p>
                    </a>
                </li> 

                <li class="nav-item">
                   <a href="{{route('Admin.Interview.Lists')}}" class="nav-link {{ (request()->is('admin/interview_list')) || (request()->is('admin/add_interview')) || (request()->is('admin/edit_interview/*'))? 'active-child' : '' }}">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p>Interview</p>
                    </a>
                </li> 

                <li class="nav-item">
                   <a href="{{route('Admin.Testimonial.Lists')}}" class="nav-link {{ (request()->is('admin/testimonial_list')) || (request()->is('admin/add_testimonial')) || (request()->is('admin/edit_testimonial/*'))? 'active-child' : '' }}">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p>Testimonial</p>
                    </a>
                </li> 

                 <li class="nav-item">
                   <a href="{{route('Admin.CompanyOfWeek.Lists')}}" class="nav-link {{ (request()->is('admin/companyofweek_list')) || (request()->is('admin/add_companyofweek')) || (request()->is('admin/edit_companyofweek/*'))? 'active-child' : '' }}">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p>Company Of The Weeks</p>
                    </a>
                </li> 
                <li class="nav-item">
                   <a href="{{route('Admin.Page.Lists')}}" class="nav-link {{ (request()->is('admin/page_list')) || (request()->is('admin/add_page')) || (request()->is('admin/edit_page/*'))? 'active-child' : '' }}">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p>Corporation Pages</p>
                    </a>
                </li> 

                
                <li class="nav-item">
                   <a href="{{route('Admin.Gallery.Lists')}}" class="nav-link {{ (request()->is('admin/gallery_list')) || (request()->is('admin/add_gallery')) || (request()->is('admin/edit_gallery/*'))? 'active-child' : '' }}">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p>Gallery</p>
                    </a>
                </li> 

                <li class="nav-item">
                   <a href="{{route('Admin.SuccessStory.Lists')}}" class="nav-link {{ (request()->is('admin/success_story_list')) || (request()->is('admin/add_success_story')) || (request()->is('admin/edit_success_story/*'))? 'active-child' : '' }}">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p>Success Story</p>
                    </a>
                </li> 

                <li class="nav-item">
                   <a href="{{route('Admin.PressRelease.Lists')}}" class="nav-link {{ (request()->is('admin/press_release_list')) || (request()->is('admin/add_press_release')) || (request()->is('admin/edit_press_release/*'))? 'active-child' : '' }}">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p>Press Release</p>
                    </a>
                </li> 

                <li class="nav-item">
                   <a href="{{route('Admin.Career.Lists')}}" class="nav-link {{ (request()->is('admin/career_list')) || (request()->is('admin/add_career')) || (request()->is('admin/edit_career/*'))? 'active-child' : '' }}">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p>Career</p>
                    </a>
                </li> 

                </ul>
            </li>



            <li class="nav-item {{ (request()->is('admin/contact_form_list')) || (request()->is('admin/Quick_form_list')) || (request()->is('admin/buyer_requirement_lists')) || (request()->is('admin/supplier_contact_lists')) || (request()->is('admin/enquiry_form_list')) ? 'menu-open' : '' }}">          

                    <a href="#" class="nav-link ">
                        <i class="nav-icon fas fa-book"></i>
                        <p>Manage Forms
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
               <ul class="nav nav-treeview">

                <li class="nav-item">
                   <a href="{{route('Admin.ContactForm.Lists')}}" class="nav-link {{ (request()->is('admin/contact_form_list'))  ? 'active-child' : '' }}">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p>Contact Us</p>
                    </a>
                </li> 
                <li class="nav-item">
                    <a href="{{route('Admin.Complaint.Lists')}}" class="nav-link {{ (request()->is('admin/complaint_list')) ? 'active-child' : '' }}">
                     <i class="nav-icon fa fa-circle-o"></i>
                        <p>Complanit Form</p>
                    </a>
                </li> 
                <li class="nav-item">
                    <a href="{{route('Admin.QuicktForm.Lists')}}" class="nav-link {{ (request()->is('admin/Quick_form_list')) ? 'active-child' : '' }}">
                     <i class="nav-icon fa fa-circle-o"></i>
                        <p>Quick Enquiry Form</p>
                    </a>
                </li> 

                <li class="nav-item">
                    <a href="{{route('Admin.BuyerRequirement.Lists')}}" class="nav-link {{ (request()->is('admin/buyer_requirement_lists')) ? 'active-child' : '' }}">
                     <i class="nav-icon fa fa-circle-o"></i>
                        <p>Buy Leads Form</p>
                    </a>
                </li> 

                <li class="nav-item">
                    <a href="{{route('Admin.SupplierContactForm.Lists')}}" class="nav-link {{ (request()->is('admin/supplier_contact_lists')) ? 'active-child' : '' }}">
                     <i class="nav-icon fa fa-circle-o"></i>
                        <p>Seller Enquiry Form</p>
                    </a>
                </li> 

                <li class="nav-item">
                    <a href="{{route('Admin.EnquiryForm.Lists')}}" class="nav-link {{ (request()->is('admin/enquiry_form_list')) ? 'active-child' : '' }}">
                     <i class="nav-icon fa fa-circle-o"></i>
                        <p>B2B Enquiry Form</p>
                    </a>
                </li> 

                <li class="nav-item">
                    <a href="{{route('Admin.Carrer.Lists')}}" class="nav-link {{ (request()->is('admin/carrer_form_list')) ? 'active-child' : '' }}">
                     <i class="nav-icon fa fa-circle-o"></i>
                        <p>Carrer Form</p>
                    </a>
                </li> 

                </ul>
            </li>

               <!--      <li class="nav-item">
                    <a href="{{route('Admin.BuyerRequirement.Lists')}}" class="nav-link {{ (request()->is('admin/buyer_requirement_lists')) || (request()->is('admin/add_buyer_requirement')) || (request()->is('admin/edit_buyer_requirement/*'))? 'active' : '' }}">
                        <i class="nav-icon fas fa-shopping-cart"></i>
                        <p>
                           Buyer Requirements
                        </p>
                    </a>
                </li>  -->

                <li class="nav-item">
                    <a href="{{route('Admin.Logout')}}" class="nav-link">
                        <i class="fas fa-sign-out-alt nav-icon"></i>
                        <p>Logout</p>
                    </a>
                </li>

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>