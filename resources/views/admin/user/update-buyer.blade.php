@extends('admin.index')

@section('content')

<!-- <div class="wrapper"> -->

@php

$user = $user ? $user : "";
$name = $user->name;

@endphp

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <!-- Edit {{$name}} -->
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Update User</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-1"></div>
                <div class="col-10">

                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Update <strong>{{$name}}</strong></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        @include('admin.partials.messages')
                        <form class="form-horizontal" method="post" action="{{url('admin/update-user')}}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id" value="{{$user->id}}" />
                            <div class="card-body">

                                <div class="form-group row">
                                    <label for="Name" class="col-sm-2 col-form-label">Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="name" name="name" value="{{$user->name}}" placeholder="Name" required />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="Email" class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" id="email" name="email" value="{{$user->email}}" placeholder="Email" required />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="Mobile" class="col-sm-2 col-form-label">Mobile</label>
                                    <div class="col-sm-10">
                                        <input type="phone" class="form-control" id="mobile" name="mobile" value="{{$user->mobile}}" placeholder="Mobile" required />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="Company Name" class="col-sm-2 col-form-label">Company Name</label>
                                    <div class="col-sm-10">
                                        <input type="phone" class="form-control" id="company_name" name="company_name" value="{{$user->company_name}}" placeholder="Company Name" required />
                                    </div>
                                </div>

                               
                                @if($user->type == 3)
                                <div class="form-group row" id="subscription">
                                    <label for="Subscribed" class="col-sm-2 col-form-label">Subscription Status</label>
                                    <div class="col-sm-10">
                                        <select class="form-control select2" style="width: 100%;" name="is_subscribed" id="is_subscribed" required>
                                            <!-- <option value="0" {{$user->is_subscribed == 0  ? 'selected' : ''}}>Not-subscribed</option> -->
                                            <option value="1" {{$user->is_subscribed == 1  ? 'selected' : ''}}>Subscribed</option>
                                        </select>
                                    </div>
                                </div>
                                @endif
                                
                                <div class="form-group row">
                                    <label for="Status" class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10">
                                        <select class="form-control select2" style="width: 100%;" name="status" id="status" required>
                                            <option value="0" {{$user->status == 0  ? 'selected' : ''}}>Inactive</option>
                                            <option value="1" {{$user->status == 1  ? 'selected' : ''}}>Active</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-info">Update User</button>
                                <a href="{{route('Admin.Users.List')}}" class="btn btn-default custom-btn">Cancel</a>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>

                </div>
                <div class="col-1"></div>
            </div>
        </div>
    </section>

</div>

<!-- </div> -->
@endsection
@section('after-scripts')

<script>  
    $(document).ready(function(){
        $('#type').on('change', function() {
        if (this.value == '3')
        {
            $("#subscription").show();
        }
        else
        {
            $("#subscription").hide();
        }
        });
    });
</script>

@endsection