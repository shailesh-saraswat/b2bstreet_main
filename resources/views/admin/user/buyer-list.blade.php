@extends('admin.index')

@section('after-style')

<link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<link rel='stylesheet' href='https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css'>
<link rel='stylesheet' href='https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css'>

@endsection

@section('content')
<style type="text/css">
    table.dataTable thead .sorting{
        background-image: url(../images/sort.png);
    }
    table.dataTable thead .sorting_desc
    {
      background-image: url(../images/)!important;  
    }
</style>
<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('admin/admin_dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Manage Buyers</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Buyers List</h3>

                            <!-- <a href="{{url('admin/add-buyer')}}" style="float: right;"><i class="fas fa-plus-square" style="font-size:25px;"></i></a> -->
                        </div>

                        
                        @if(session('message'))
                            <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
                        @endif
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="user-list" class="table table-bordered table-striped " data-order='[[ 0, "asc" ]]'>


                                <thead>
                                    <tr>
                                       
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Company Name</th>
                                        <th>Mobile</th>
                                        <th>Designation</th>
                                        <th>GSTIN</th>
                                        <th>Website</th>
                                        
                                        <th data-orderable="false">Status</th>
                                        <th data-orderable="false">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($users) && $users != null)
                                        @foreach($users as $user)
                                            
                                            <tr>
                                               
                                                <td>{{$user->name}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>{{$user->company_name}}</td>
                                                <td>{{$user->mobile}}</td>
                                                <td>{{$user->designation}}</td>
                                                <td>{{$user->gst}}</td>
                                                <td>{{$user->website}}</td>
                                               
                                                <!-- <td>@if($user->type == 2)
                                                        User
                                                    @elseif($user->type == 3)
                                                        Store Customer
                                                    @elseif($user->type == 1)
                                                        Admin
                                                    @endif            
                                                </td> -->
                                                
                                                
    <td style="text-align:center">
        @if($user->status == 1)
        <p style="color:green">Active</p>  
        @elseif($user->status == 0)
        <p style="color:red">In-active</p>  
            @endif
        </td>

    <td style="text-align: center;">
        @if($user->is_admin != 1)
        <a href="{{url('admin/delete-buyer/'.$user->id)}}" onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></a>
        @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
   
</div>

@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src='https://code.jquery.com/jquery-1.12.3.js'></script>
<script src='https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js'></script>
<script src='https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js'></script>
<script src='https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js'></script>
<script src='https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js'></script>
<script src='https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js'></script>
<script src='https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js'></script>
<script src='https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js'></script>
<script>
    

$(document).ready(function() {
    $('#user-list').DataTable( {

        dom: 'Bfrtip',
        buttons: [
            'copy', 'excelFlash', 'excel', 'pdf', 'print',{
            text: 'Reload',
            action: function ( e, dt, node, config ) {
                dt.ajax.reload();
            }
        }
        ]
    } );
} );

</script>

<style type="">
    .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
    background: #007bff !important;
    color: white !important;
}

.dataTables_wrapper .dataTables_paginate .paginate_button:hover{
    background: #007bff !important;
}
div.dataTables_wrapper div.dataTables_filter input {
    margin-left: 0.5em;
    display: inline-block;
    width: auto;
    outline: none;
    border: 1px solid #ccc;
}
</style>

@endsection