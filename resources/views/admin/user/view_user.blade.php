@foreach($users as $user)
<div class="modal fade" id="user-info{{$user->id}}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{$user->name}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Name" class="col-form-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="name" name="name" value="{{$user->name}}" />
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Email" class="col-form-label">Email</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="email" name="email" value="{{$user->email}}" />
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Mobile" class="col-form-label">Mobile</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="mobile" name="mobile" value="{{$user->mobile}}" />
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                     <div class="col-4">
                        <div class="form-group">
                            <label for="Last Name" class="col-form-label">Type</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="type" name="type" value="{{$user->type == 3 ? 'Store Customer' : 'User'}}" />
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Status" class="col-form-label">Status</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="status" name="status" value="{{$user->status == 1 ? 'Active' : 'In-active'}}" />
                            </div>
                        </div>
                    </div>
                </div>

                
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endforeach