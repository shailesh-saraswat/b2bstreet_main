@extends('admin.index')

@section('content')

<!-- <div class="wrapper"> -->

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <!--  -->
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('Admin.Dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Success Story</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-1"></div>
                <div class="col-10">

                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Success Story<strong></strong></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        @include('admin.partials.messages')
                        <form class="form-horizontal" method="post" action="{{route('Admin.Update.SuccessStory')}} " enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id" value="{{$stories->id}}" />
                            <div class="card-body">
                          

                                <div class="form-group row">
                                    <label for="Title" class="col-sm-2 col-form-label">Title</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="title" name="title" value="{{$stories->title}}" placeholder="Title" />
                                    </div>
                                </div>

                             <div class="form-group row">
                                    <label for="Date" class="col-sm-2 col-form-label">Date</label>
                                    <div class="col-sm-10">
                                        <input type="date" class="form-control" id="date" name="date" value="{{$stories->date}}" placeholder="Date" />
                                    </div>
                                </div>


                        <div class="form-group row">
                            <label for="Description" class="col-sm-2 col-form-label">Description</label>
                             <div class="col-sm-10">
                             <textarea type="text" class="form-control textarea" id="description" name="description" value="" placeholder="Description"  rows="10">{{$stories->description}}</textarea>
                            </div>
                        </div>

                            <div class="form-group row">
                                <label for="Image" class="col-sm-2 col-form-label">Image</label>
                                <div class="col-sm-10">
                                    <input type="file" class="" id="image" name="image" value="">
                                    <img src="{{ asset($stories['image']) }}" style="width:150px;" />
                                </div>
                            </div>

                    <div class="form-group row">
                    <label for="Status" class="col-sm-2 col-form-label">Status</label>
                        <div class="col-sm-10">
                            <select class="form-control select2" style="width: 100%;" name="status" id="status" required>
                                <option value="1" {{$stories->status == 1  ? 'selected' : ''}}>Left Content</option>
                                <option value="0" {{$stories->status == 0  ? 'selected' : ''}}>Right Content</option>
                                            
                            </select>
                        </div>
                    </div> 
                                
                              
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-info">Update Success Story</button>
                                <a href="{{route('Admin.SuccessStory.Lists')}}" class="btn btn-default float-right">Cancel</a>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>

                </div>
                <div class="col-1"></div>
            </div>
        </div>
    </section>

</div>

<!-- </div> -->
@endsection


@section('after-scripts')
<script src="{{asset('public/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script>
@endsection
@section('after-style')
<link rel="stylesheet" href="{{asset('public/plugins/summernote/summernote-bs4.css')}}">
@endsection 