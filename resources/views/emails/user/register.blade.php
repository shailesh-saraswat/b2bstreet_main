
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
	<!--[if gte mso 9]>
	<xml>
		<o:OfficeDocumentSettings>
		<o:AllowPNG/>
		<o:PixelsPerInch>96</o:PixelsPerInch>
		</o:OfficeDocumentSettings>
	</xml>
	<![endif]-->
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="format-detection" content="date=no" />
	<meta name="format-detection" content="address=no" />
	<meta name="format-detection" content="telephone=no" />
	<meta name="x-apple-disable-message-reformatting" />
    <!--[if !mso]><!-->
	<link href="https://fonts.googleapis.com/css?family=Alegreya+SC:400,400i,700,700i|Alegreya:400,400i,700,700i|Stardos+Stencil:400,700" rel="stylesheet" />
    <!--<![endif]-->
	<title>Welcome to Shortday Sinc</title>
	<!--[if gte mso 9]>
	<style type="text/css" media="all">
		sup { font-size: 100% !important; }
	</style>
	<![endif]-->
	

	<style type="text/css" media="screen">
		/* Linked Styles */
		body { padding:50px!important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important;  -webkit-text-size-adjust:none;
            background: #333!important;
        }
		a { color:cyan; text-decoration:none }
		p { padding:0 !important; margin:0 !important } 
		img { -ms-interpolation-mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */ }
		.mcnPreviewText { display: none !important; }

				
		/* Mobile styles */
		@media only screen and (max-device-width: 480px), only screen and (max-width: 480px) {
			.mobile-shell { width: 100% !important; min-width: 100% !important; }

			.text-header,
			.m-center { text-align: center !important; }
			.holder { padding: 0 10px !important; }

			.text-nav { font-size: 10px !important; }

			.center { margin: 0 auto !important; }

			.td { width: 100% !important; min-width: 100% !important; }
				
			.text-header .link-white { text-shadow: 0 3px 4px rgba(0,0,0,09) !important; }

			.m-br-15 { height: 15px !important; }
			.bg { height: auto !important; } 

			.h0 { height: 0px !important; }

			.m-td,
			.m-hide { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }

			.m-block { display: block !important; }

			.p30-15 { padding: 30px 15px !important; }
			.p15-15 { padding: 15px 15px !important; }
			.p30-0 { padding: 30px 0px !important; }
			.p0-0-30 { padding: 0px 0px 30px 0px !important; }
			.p0-15-30 { padding: 0px 15px 30px 15px !important; }
			.p0-15 { padding: 0px 15px 0px 15px !important; }
			.mp0 { padding: 0px !important; }
			.mp20-0-0 { padding: 20px 0px 0px 0px !important }
			.mp30 { padding-bottom: 30px !important; }
			.container { padding: 20px 0px !important; }
			.outer { padding: 0px !important }

			.fluid-img img { width: 100% !important; max-width: 100% !important; height: auto !important; }

			.column,
			.column-top,
			.column-dir,
			.column-empty,
			.column-empty2,
			.column-empty3,
			.column-bottom,
			.column-dir-top,
			.column-dir-bottom { float: left !important; width: 100% !important; display: block !important; }

			.column-empty { padding-bottom: 10px !important; }
			.column-empty2 { padding-bottom: 25px !important; }
			.column-empty3 { padding-bottom: 45px !important; }

			.content-spacing { width: 15px !important; }
			.content-spacing2 { width: 25px !important; }
		}
        
        
        .banner{
            position: relative;
        }
        
        .banner-text{
            position: absolute;
            top:50%;
            left: 50%;
            transform: translate(-50%,-50%);
        }
        
        .background-color{
            background-image: linear-gradient(to bottom right, #3957b0, #1e67ac);
            padding: 50px;
            box-shadow: 0px 0px 50px #000;
        }
        
	</style>
</head>
<body class="body" style="padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important;  -webkit-text-size-adjust:none;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" >
		<tr>
			<td align="center" valign="top">
				<table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
					<tr>
						<td class="td background-color" bgcolor="" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>
				

										<!-- Article / Title + Copy + Button -->
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="p0-15-30" style="padding: 20px">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class="h2 center pb20" style="color:#fff; font-family: sans-serif; font-size:30px; line-height:48px; text-align:center;"><multiline>Welcome to Shortday Sinc</multiline></td>
														</tr>
														
														
													</table>
												</td>
											</tr>
										</table>
										<!-- END Article / Title + Copy + Button -->

										<!-- Article / Full Width Image + Title + Copy + Button -->
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr class="banner" style="position: relative;">
												<td class="fluid-img pb40" style="font-size:0pt; line-height:0pt; text-align:left; padding-bottom:10px;"><img src="{{url('assets/images/bg2.jpg')}}" width="650" height="366" border="0" alt="" /></td>
                                                
											</tr>
											<tr>
												<td class="p0-15-30" >
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class="h3" style="color:#fff; font-family:sans-serif; font-size:30px; line-height:60px; text-align:center;"><multiline>Thanks for Registering at Shortday Sinc.</multiline></td>
														</tr>
														<!-- Button -->
														
														<!-- END Button -->
													</table>
												</td>
											</tr>
										</table>
	
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="p30-15" style="padding:20px">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<th class="column-top " width="100%" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top; text-align: center;">
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td class="h2 m-center pb25" style="color:#f1f1f1; font-family:Arial, sans-serif; font-size:20px; line-height:48px; text-align:center; "><multiline>Hi {{$user_data->name}}</multiline></td>
																	</tr>
																	<tr>
																		<td class="text2 m-center pb25" style="color:#f5f5f5; font-family:Arial, sans-serif; font-size:14px; line-height:28px; text-align:center; padding-bottom:px;">
																			<multiline>Thank you creating your account at Shortday Sync. </multiline>
																		</td>
																	</tr>
                                                                    <tr>
																		<td class="text2 m-center pb25" style="color:#f2f2f2; font-family:Arial, sans-serif; font-size:14px; line-height:28px; text-align:center; padding-bottom:px;">
																			<multiline>Your Account details are as below: -</multiline>
																		</td>
																	</tr>
                                                                    <tr>
																		<td class="text2 m-center pb25" style="color:#fff; font-family:Arial, sans-serif; font-size:14px; line-height:28px; text-align:center; padding-bottom:px;">
                                                                            <multiline>Login Name: <a>{{$user_data->email}}</a></multiline>
																		</td>
																	</tr>
																</table>
															</th>
															<th class="column-empty3" width="80" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;"></th>
															
														</tr>
													</table>
												</td>
											</tr>
										</table>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="text2 m-center pb25" style="color:#fff; font-family:Arial, sans-serif; font-size:14px; line-height:28px; text-align:center; padding:0px 50px;">
												    <multiline>If you have any queries, kindly send email to anup.kumar@webmobril.com or reach us on +918960850272 and we’ll happy to help you.</multiline>
												</td>
											</tr>
										</table>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="text2 m-center pb25" style="color:#fff; font-family:Arial, sans-serif; font-size:14px; line-height:28px; text-align:center; padding-bottom:50px;">
												    <multiline>Shortday Sync Team</multiline>
												</td>
											</tr>
										</table>
										<!-- END Contant Us -->
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>
