<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogDetails extends Model
{
    protected $fillable=[

    'title',
    'date',
    'short_content',
    'content',
    'image',
    ];
}
