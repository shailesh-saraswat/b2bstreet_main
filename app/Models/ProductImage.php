<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $fillable = [

    'products_id',
    'product_image',
    'status',
    ];

     public function product()
    {
        return $this->belongsTo('App\Models\Product','products_id','id');
    }
    
}
