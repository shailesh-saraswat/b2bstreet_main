<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EnquiryForm extends Model
{
    protected $fillable =[
   
    'page_id',
    'name',
    'company_name',
    'phone',
    'email',
    'product_name',
    'qty',
    'address',
    'status',

    ];

      public function page()
    {
        return $this->belongsTo('App\Models\Page','page_id','id');
    }
}

