<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SuplierGallery extends Model
{
    protected $fillable = [

    'title',
    'description',
    'image',


    ];

    public function user(){

        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
