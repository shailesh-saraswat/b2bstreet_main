<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
     protected $fillable = [
        
        'maincategory_id',
        'categories_id',
        'subcategories_id',
        'title',
        'price',
        'description',   
        'status',
        
       
    ];


   public function main_category()
    {
        return $this->belongsTo('App\Models\Maincategory','maincategory_id','id');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category','categories_id','id');
    }

    public function subcategory()
    {
        return $this->belongsTo('App\Models\SubCategory','subcategories_id','id');
    }  

   
     public function product_images(){

        return $this->hasMany('App\Models\ProductImage', 'products_id', 'id');
    }

 public function firstImage()
    {
        return $this->hasOne('App\Models\ProductImage', 'products_id', 'id');
    }

     public function user(){

        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }


    public function interview(){

        return $this->belongsTo('App\Models\Interview', 'interview_id', 'id');
    }

}
