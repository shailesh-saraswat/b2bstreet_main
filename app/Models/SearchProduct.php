<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SearchProduct extends Model
{
    protected $fillable = [

   'name',
   'description',
   'price',
   'company_name',
    ];
}
