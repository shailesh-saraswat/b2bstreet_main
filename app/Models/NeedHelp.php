<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NeedHelp extends Model
{
    protected $table = "need_help";
}
