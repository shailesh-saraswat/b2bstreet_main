<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BuyerRequirement extends Model
{
   protected $fillable =[

   'product_name',
   'description',
   'quantity',
   'order',
   'email',
   'phone',
   'terms_conditions',
   'whatsapp'

   ];
}
 