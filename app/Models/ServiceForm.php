<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceForm extends Model
{
    protected $fillable=[

     'name',
     'company_name',
     'phone',
     'email',
     'massage',
    ];
}
