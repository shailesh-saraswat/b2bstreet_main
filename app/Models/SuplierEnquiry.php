<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SuplierEnquiry extends Model
{
    protected $fillable = [

     'name',
     'email',
     'mobile',
     'location',
     'requirements',
    ];
}
