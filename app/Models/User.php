<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
//use Laravel\Passport\HasApiTokens;
// use Laravel\Cashier\Billable;

class User extends Authenticatable
{
 //   use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var arrayaddress
     */
     protected $fillable = [
        'name', 
        'email', 
        'mobile',
        'password',
        'type',
        'status',
        'pin',
        'is_vendor_approved',
        'is_profile_completed',
        'is_admin',
        'device_type',
        'device_token',

        'video_url',

        'gst',
        'address',
        'company_name',
        'doc_file',
        'designation',
        'alternate_number',
        'alternate_email',
        'pin',
        'state',
        'house_no',
        'locality',
        'city',
        'country',
        'area',
        'landmark',
        'website',
        'gstin',
        'ifcs_code',
        'bank_name',
        'account_no',
        'account_type',

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function tokens(){

        return $this->hasMany('App\Models\PassportToken');
    }

    public function subscriptions(){

        return $this->hasMany('App\Models\Subscription', 'user_id', 'id');
    }

    public function type() {
        
        return $this->belongsTo('App\Models\User_Type', 'type', 'id');
    }

    public function notifications(){

        return $this->hasMany('App\Models\Notification', 'user_id', 'id');
    }

    public function plan(){

        return $this->belongsTo('App\Models\Plan', 'subscribed_plan', 'id');
    }
}
