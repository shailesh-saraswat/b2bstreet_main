<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends model
{
    protected $fillable = [
        'id',
        'maincategory_id',
        'name',
        'description',
        'image',  
        'slug',
        'status',
        
       
    ];

  
    public function main_category()
    {
        return $this->belongsTo('App\Models\Maincategory','maincategory_id','id');
    }
public function subcategory()
    {
        return $this->belongsTo('App\Models\SubCategory','subcategories_id','id');
    }
    
    public function category()
    {
        return $this->belongsTo('App\Models\Category','categories_id','id');
    }

   /* public function parent_category()
    {
        return $this->belongsTo('App\Models\Category','parent_id','id');
    }*/
}



