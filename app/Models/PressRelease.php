<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PressRelease extends Model
{
    protected $fillable =[
        'title',
        'date',
        'short_content',
        'content',
        'image',
        'status',

    ];
}
