<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = [

        'id',
        'title',
        'description',
        'image',
        'video',
        'is_trending',
        'is_recommended',
        'file_name',
        'file_path',
        'thumbnail_name',
        'thumbnail_path',
        'status',


    ];
    public function user(){

        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
