<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $table = 'subcategories';

    protected $fillable = [
        'id',
        'maincategory_id',
        'categories_id',
        'name',
        'description',
        'image',  
        'slug',
        'status',
        
       
    ];

     public function main_category()
    {
        return $this->belongsTo('App\Models\Maincategory','maincategory_id','id');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category','categories_id','id');
    }

}
