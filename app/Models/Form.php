<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
   protected $fillable = [

    'form_name',
    'name', 
    'company_name',
    'phone', 
    'email', 
    'product_name',
    'quality',
    'address',
    'query',
    'type',
    'status',




    ];
}
