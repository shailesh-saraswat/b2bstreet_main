<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CareerForm extends Model
{
    protected $fillable =[
    'name',
    'email',
    'phone',
    'location',
    'message',
    ];
}
