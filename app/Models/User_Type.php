<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User_Type extends Model
{
    protected $table = "users_type";

    protected $fillable = ['name'];

    public function user(){

        return $this->hasOne('App\Models\User', 'type', 'id');
    }
}
