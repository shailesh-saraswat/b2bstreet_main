<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SuplierTestimonial extends Model
{
   protected $fillable = [
    
    'id',
    'title',
    'description',
    'image',
    'status',
   ];

   public function user(){

        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
