<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SuplierBanner extends Model
{
   protected $fillable = [
    
    'id',
    'user_id',
    'name',
    'address',
    'mobile',
    'email',
    'website',
    'image',
    'video',
    'status',

    ];

    public function user(){

        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
