<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    protected $fillable = [
    'company_name',
    'name',
    'complain_title',
    'email',
    'contact_no',
    'description',
    ];
}
