<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = [

    'title',
    'name',
    'content',
    'short_content',
    'image',
    'slug',
    'status',
    'type',
    ];

    protected static $pageType=array(
        1=>"Normal Pages",
        2=>"Corporate Pages"
    );
    public static function getPageType(){
        return self::$pageType;
    }
}

