<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductDetailContact extends Model
{
    protected $table = "product_details_contact";


    public function user(){

        return $this->belongsTo('App\Models\User', 'user_id', 'id')->where('type',2);
    }
}
