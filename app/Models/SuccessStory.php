<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SuccessStory extends Model
{
    protected $fillable =[

   'title',
   'date',
   'description',
   'image',
   'status',

   ];
}
