<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ourteam extends Model
{
   protected $fillable = [

    'name',
    'designation',
    'description',
    'image',
    'status',
    ];
}
