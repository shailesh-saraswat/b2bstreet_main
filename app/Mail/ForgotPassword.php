<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForgotPassword extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $link;
    public $user_name;
    public function __construct($user_name,$link)
    {
        $this->user_name = $user_name;
        $this->link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email = $this->view('emails.forgot.password')
            ->subject('B2B Streets Reset Password')
            ->with([
            'link' => $this->link,
            'username' => $this->user_name,
        ]);
        return $email;
        //return $this->markdown('emails.forgot.password');
    }
}
