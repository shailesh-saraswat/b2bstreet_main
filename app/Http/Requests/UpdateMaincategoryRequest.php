<?php 

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMaincategoryRequest extends FormRequest
{
    
    
    public function rules()
    {
        return [
            
            'name'                 => 'required',
            'image'                 => '',
            'status'               => 'required',
          
        ];
    }
}