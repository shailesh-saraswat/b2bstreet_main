<?php 

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->type == 1;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
 /*   public function rules()
    {
        return [
            
            'name'          => 'required',
            'email'         => 'required|email|unique:users',
            'mobile'        => 'required|min:6|max:15|unique:users',
            'password'      => 'required|min:6',
            'status'        => 'required',
            ''
        ];
    }*/
}