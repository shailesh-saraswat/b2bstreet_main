<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSubcategoryRequest extends FormRequest
{
    
    public function rules()
    {
        
        return [
            
           'name'                 => 'required',
           'description'          => '',
           'image'                => '',
           'status'               => 'required',
        ];
    }
}
