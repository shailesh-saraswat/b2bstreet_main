<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            return redirect(RouteServiceProvider::HOME);
        }
        if (Auth::guard($guard)->check() && auth()->user()->type == 1){
            return redirect('/admin/dashboard');
        }

        if (Auth::guard($guard)->check() && auth()->user()->type == 2){
            return redirect('sup_admin/suplier_dashboard');
        }
        return $next($request);
    }
}
