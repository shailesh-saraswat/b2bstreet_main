<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Maincategory;
use App\Models\SubCategory;
use App\Models\ProductDetailContact;
use App\Models\Cities;
use App\Models\State;
use App\Models\Category;
use App\Models\Banner;
use App\Models\Video;
use App\Models\Testimonial;
use App\Models\Interview;
use App\Models\Ourteam;
use App\Models\Page;
use App\Models\Smartblock;
use App\Models\EnquiryForm;
use App\Models\OurClient;
use\App\Models\Gallery;
use App\Models\ContactForm;
use App\Models\Form;
use App\Models\ServiceForm;
use App\Models\User;
use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use DB;
use Illuminate\Support\Facades\Hash;
use Session;
use App\Models\BuyerRequirement;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use App\Mail\ForgotPassword;
use Carbon\Carbon;
use Response;
use App\Models\SuplierBanner;
use App\Models\SuplierAbout;
use App\Models\NeedHelp;
use App\Models\SuplierGallery;
use App\Models\Network;
use App\Models\SuplierTestimonial;
use App\Models\Certificate;
use App\Models\Blog;
use App\Models\SuplierEnquiry;
use App\Models\CompanyOfWeek;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Mail\EmailVerification;
use App\Mail\UserVerification;
use App\Models\SuccessStory;
use App\Models\PressRelease;
use App\Models\Complaint;
use App\Models\CareerForm;
use App\Models\Career;
use App\Models\PackageForm;
use URL;

class CommonController extends Controller
{

   public function index(Request $request)
    {
      $maincategories = Maincategory::all();
      $categories = Category::query()->where('status',1)->limit(16)->get()->toArray();
      $banner=Banner::query()->where('status',1)->first();
      $video_trending = Video::query()->with('user')->where('status',1)->limit(8)->where('is_trending',1)->get();
      $video_recommended = Video::query()->with('user')->where('status',1)->limit(8)->where('is_recommended',1)->get();
      $testimonials = Testimonial::all();
      $interviews = Interview::query()->limit(8)->get();
      $smartblocks=Smartblock::query()->where('status',0)->get();
      $products=Product::query()->with('firstImage')->orderBy('id','desc')->limit('8')->get();
      $user = User::all();
      $companies = CompanyOfWeek::all();
      $cat=Category::query()->where('status',1)->limit(10)->get()->toArray();
      $medic = SubCategory::groupBy('categories_id')->where('maincategory_id',2)->get();
      $products_data = Product::select('title')->get();
      $location = Cities::select('name')->get();

        foreach($location as $key=>$location_list)
       {
        $lData[] = $location_list->name;
       }
       foreach($products_data as $key=>$list)
       {
      // print_r($products_data);
        
       }
      return view('front.index')->with('categories',$categories)->with('banner',$banner)->with('video_trending', $video_trending)->with('video_recommended', $video_recommended)->with('testimonials',$testimonials)->with('interviews',$interviews)->with('smartblocks',$smartblocks)->with('products',$products)->with('user',$user)->with('cat',$cat)->with('medic',$medic)->with('location',$lData)->with('companies',$companies)->withMaincategories($maincategories);    
      }

 
    public function suplierProfileData(Request $request,$slug)
    {       
    $user=User::where('slug',$slug)->first(); 
    $categories = Category::all();
    $banners = SuplierBanner::where('user_id',$user['id'])->get();
    $abouts = SuplierAbout::where('user_id',$user['id'])->get();
    $galleries = SuplierGallery::where('user_id',$user['id'])->get();
    $networks = Network::where('user_id',$user['id'])->get();
    $testimonials = SuplierTestimonial::where('user_id',$user['id'])->get();
    $certificates = Certificate::where('user_id',$user['id'])->get();
    $blogs = Blog::where('user_id',$user['id'])->get();
    $products = Product::where('user_id',$user['id'])->with('product_images')->get();
    //$products = Product::where('user_id',$id)->with('product_images')->get();
 // dd($products);
    $product_img = ProductImage::where('products_id',$user['id'])->get();

    return view('front.suplier-profile')->withBanners($banners)->withAbouts($abouts)->withGalleries($galleries)->withNetworks($networks)->withTestimonials($testimonials)->withCertificates($certificates)->withBlogs($blogs)->withProducts($products)->withProductImages($product_img)->withCategories($categories)->with('user', $user);
    }
      public function getPage($slug)
      {
      
      $page=Page::where('slug',$slug)->first();

      if(!$page){
        return redirect()->back();
      }
      return view('front.page',compact('page'));
      }



    public function productDetails(Request $request,$id, $slug)
      {
       // dd(5646);
         try{

            $user=User::where('slug',$id)->first();
             if(!$user){
                return redirect()->back();
            }

            $banners = SuplierBanner::where('user_id',$user->id)->get();

            $products=Product::where('slug',$slug)->where('user_id',$user->id)->with('product_images')->first();

           
      if(!$products){
        return redirect()->back();
      }
           return view('front.product-details',compact('products','banners'));
            
          }catch(Exception $e){

            return redirect()->back()
            ->withErrors($e->getMessage());
          }
      
      }

   public function getSubCategory($slug)
      {
      $user=User::where('slug',$slug)->first();     
      $products=SubCategory::where('slug',$slug)->first();
      $product_page = Product::with('main_category')->with('category')->with('subcategory')->where('subcategories_id',$products['id'])->groupBy('user_id')->where('status','1')->get(); 
       $data = SubCategory::where('name','!=',$slug)->where('categories_id',$products['categories_id'])->get();
      return view('front.products',compact('products','product_page','data','user'));
      }


    public function productList($slug)
    {
      $user=User::where('slug',$slug)->first(); 

      // $products=SubCategory::where('slug',$slug)->first();
      $product_page = Product::with('main_category')->with('category')->with('subcategory')->where('slug',$slug)->groupBy('user_id')->where('status','1')->get(); 
      // dd($product_page[0]->subcategories_id);
     $products=SubCategory::where('id',$product_page[0]->subcategories_id)->first();
// dd($products);
       $data = SubCategory::where('name','!=',$slug)->where('categories_id',$products->categories_id)->get();
       // dd($data);
      return view('front.products',compact('products','product_page','data','banners','user'));
    }



    public function categoryProduct(Request $request,$slug)
    {    
    $category=Category::where('slug',$slug)->first();
    $subcategory = SubCategory::where('categories_id',$category->id)->get();

        foreach($subcategory as $sub => $val)
        {
            $productpage = Product::where('subcategories_id', $val->id)->where('status','1')->orderBy('id', 'DESC')->take(6)->get();
            foreach($productpage as $key => $pro){

                $product_img = ProductImage::where('products_id',$pro->id)->first();
               // dd($product_img->product_image);
                if(!empty($product_img->product_image)){
                    $productpage[$key]->product_img = $product_img->product_image;    
                } else {
                    $productpage[$key]->product_img = null;
                }                 
            }


            $productpage1 = Product::where('subcategories_id', $val->id)->where('status','1')->orderBy('id', 'DESC')->skip(6)->take(12)->get();
            foreach($productpage1 as $key => $pro1){
                $product_img1 = ProductImage::where('products_id',$pro1->id)->first();
              
                if(!empty($product_img1->product_image)){
                    $productpage1[$key]->product_img1 = $product_img1->product_image;    
                } else {
                    $productpage1[$key]->product_img1 = null;
                }
                 
            }
            $subcategory[$sub]->products = $productpage;

            $subcategory[$sub]->products1 = $productpage1;
        }
    // dd($subcategory);
  
    return view('front.category')->withSubcategory($subcategory)->withCategory($category)->withProductpage($productpage);
    }


    public function about($slug)
     {
      $page = Page::where('slug',$slug)->first(); 
      $view="front.page";
      $clients = OurClient::all();      
      $ourteams = OurTeam::all();
      $abouts=Smartblock::query()->where('status',1)->get();
      $smarts=Smartblock::query()->where('status',2)->get();
          
      if($slug=='about-us')
      {
         $view="front.about";
      }
         
      return view($view,compact('page','ourteams'))->withClients($clients)->with('smarts',$smarts)->with('abouts',$abouts);
      }
 
    

    public function addEnquiryForm(Request $request)
      {
        try{
              $validator = Validator::make($request->all(), [
                'name' => 'required|min:3|max:16',
                'company_name' => 'required|min:3|max:16',
                'email'=>'required|email|unique:users,email',
                'phone' =>'required|numeric|unique:users,mobile|regex:/[0-9]{10}/',
                'massage' =>'required',   
                
            ]);
   
            if($validator->fails())
            {
            return redirect()->back()->withErrors($validator)->withInput();
            }
          
        $enquiry = new EnquiryForm();
        $enquiry->page_id = $request->input('page_id');
        $enquiry->name = $request->name;
        $enquiry->company_name = $request->company_name;
        $enquiry->phone = $request->phone;
        $enquiry->email = $request->email;
        $enquiry->massage = $request->massage;  
        $enquiry->save();

            Session::flash('message',"Your Enquiry Form has been Submitted Successfully.");
            return redirect()->back();

        }catch(\Exception $e){
            Session::flash('message',$e->getMessage());
            return redirect()->back();
    }
    }


    public function Contact()
    {       
    return view('front.contact');
    }
      
    public function addContactForm(Request $request)
      {
        try{
              $validator = Validator::make($request->all(), [
                'name' => 'required|min:3|max:16',
                'email'=>'required|email|unique:users,email',
                'phone' =>'required|numeric|unique:users,mobile|regex:/[0-9]{10}/',
                'massage' =>'required',   
                
            ]);
   
            if($validator->fails())
            {
            return redirect()->back()->withErrors($validator)->withInput();
            }
          
         $contact = new ContactForm();
         $contact->name = $request->name;
         $contact->email =$request->email;
         $contact->phone =$request->phone;
         $contact->massage =$request->massage;
         $contact->save();

        $to_name = $request->name;
        $email = $request->email;
        $phone = $request->phone;
        $massage = $request->massage;
        $to_email = 'anu.kumari@webmobriltechnologies.com';
        $body = 'B2b : | Contact Us Page Message.';
        $data = array('name' => $to_name,'email' => $email ,'phone' => $phone, 'massage' => $massage, 'body' => $body);
          \Mail::send('emails.thanks', $data, function($message) use ($to_name, $to_email) {
              $message->to($to_email, $to_name)
                      ->subject(' Contact Us Page Message.');
        });
            Session::flash('message',"Contact Form Submitted Successfully");
            return redirect()->back();

        }catch(\Exception $e){
            Session::flash('message',$e->getMessage());
            return redirect()->back();
    }
    }



   
   public function addServiceForm(Request $request)
      {
        try{
              $validator = Validator::make($request->all(), [
                'service_name' => 'required|min:3|max:16',
                'company_name' => 'required|min:3|max:16',
                'service_email'=>'required|email|unique:users,email',
                'phone' =>'required|numeric|unique:users,mobile|regex:/[0-9]{10}/',
                'massage' =>'required',   
                
            ]);
   
            if($validator->fails())
            {
            return redirect()->back()->withErrors($validator)->withInput();
            }
          
        $serviceform = new EnquiryForm();
        $serviceform->service_name=$request->service_name;
        $serviceform->company_name=$request->company_name;
        $serviceform->phone=$request->phone;
        $serviceform->service_email=$request->service_email;           
        $serviceform->massage=$request->massage;
        $serviceform->save();
        
            Session::flash('message',"Your Service Form has been Submmitted Successfully.");
            return redirect()->back();

        }catch(\Exception $e){
            Session::flash('message',$e->getMessage());
            return redirect()->back();
    }
    }


      public function gallery()
      {

      $galleries = Gallery::all();

      return view('front.gallery')->withGalleries($galleries);
      }
    
      public function msmeInterviews()
      {
        $interviews = Interview::all();
      return view('front.msme-interviews',compact('interviews'));
      } 

    public function sign_up(Request $request)
    { 
        $section  = $request->section;
        // echo $lastName; die;
       $city = Cities::all();
       $state = State::all();
       return view('front.sign_up',compact('city','state','section'));
    }





    // for supplier registration

     public function suplierRegister(Request $request)
      {
        try{
              $validator = Validator::make($request->all(), [
                'name' => 'required| min:3|max:16',
                'email' => 'required|email|unique:users,email,',
                'password' => 'required | min:8|max:16',
                'confirm_password' => 'required|same:password',
                'mobile' =>  'required|numeric|unique:users,mobile|regex:/[0-9]{10}/',
                'gst' =>'required',
                'state'=>'required',
                'city'=>'required',
                'address'=>'required',
                'company_name'=>'',
                'doc_file' =>'',
                
            ]);
   
            if($validator->fails())
            {
            return redirect()->back()->withErrors($validator)->withInput();
            }
          
        $user = new User();
    $user->name = $request->name;
    $user->type = $request->type;
    $user->email = $request->email;
    $token = Str::random(60);
     $link = url('/') . '/password/' . $token . '?email=' . ($user->email);
                    \DB::table('password_resets')->insert([
                        'email' => $request->email,
                        'token' => $token,
                        'created_at' => Carbon::now()
                    ]);
            \Mail::to($user->email)->send(new UserVerification($user->name, $link));
    $user->mobile = $request->mobile;
    $user->password = bcrypt($request->password);
    $user->supplierpassword = $request->password;
    $user->gst = $request->gst;
    $user->state = $request->state;
    $user->city = $request->city;
    $user->address = $request->address;
    $user->company_name = $request->company_name;
    $user->slug = Str::slug($user->company_name, '-');
    if ($request->hasFile('doc_file')) 
    {

    $image = $request->file('doc_file');

    $name = $image->getClientOriginalName();
    $destinationFolder = "public/images/doc_file/";
    if(file_exists($destinationFolder.$name))
    {
    unlink($destinationFolder.$name);
    }
    $image->move($destinationFolder,$name);  
    $image ="public/images/doc_file/".$name;
    $user->doc_file=$image;

    } 
     $user->save();

    $to_name = $request->name;
    $email = $request->email;
    $mobile = $request->mobile;
    $password = $request->password;
    $gst = $request->gst;
    $state = $request->state;
    $city = $request->city;
    $address = $request->address;
    $company_name = $request->company_name;
    $to_email = 'anu.kumari@webmobriltechnologies.com';
    $body = 'B2b : | Sign Up Supplier Page Message.';
    $data = array('name' => $to_name,'email' => $email ,'mobile' => $mobile, 'password' => $password,'gst' => $gst ,'state' => $state, 'city' => $city,'address' => $address, 'company_name' => $company_name, 'body' => $body);
          \Mail::send('emails.thanksmail.suplier_thanks', $data, function($message) use ($to_name, $to_email) {
              $message->to($to_email, $to_name)
                      ->subject(' Supplier Registered Successfully Mail Message.');
          });

            Session::flash('message',"Suplier User Registered Successfully.");
            return redirect()->back();

        }catch(\Exception $e){
            Session::flash('message',$e->getMessage());
            return redirect()->back();
    }
    }



        /*Registration Buyer*/

    public function storebuyer(Request $request)
      {
        try{
              $validator = Validator::make($request->all(), [
                'name' => 'required| min:3|max:16',
                'email' => 'required|email|unique:users,email,',
                'company_name' => '',
                'password' => 'required | min:8|max:16',
                'confirm_password' => 'required|same:password',
                'mobile' =>  'required|numeric|unique:users,mobile|regex:/[0-9]{10}/', 
                
            ]);
   
            if($validator->fails())
            {
            return redirect()->back()->withErrors($validator)->withInput();
            }
          
    $user = new User();
    $user->name = $request->name;
    $user->type = $request->type;
    $user->email = $request->email;
    $token = Str::random(60);
    $link = url('/') . '/password/' . $token . '?email=' . ($user->email);
                    \DB::table('password_resets')->insert([
                        'email' => $request->email,
                        'token' => $token,
                        'created_at' => Carbon::now()
                    ]);
            \Mail::to($user->email)->send(new UserVerification($user->name, $link));
    
    $user->mobile = $request->mobile;
    $user->company_name = $request->company_name;
    $user->password = bcrypt($request->password);        
    $user->save();

    $to_name = $request->name;
    $email = $request->email;
    $mobile = $request->mobile;
    $password = $request->password;
    $company_name = $request->company_name;
    $to_email = 'anu.kumari@webmobriltechnologies.com';
    $body = 'B2b : | Sign Up Buyer Page Message.';
    $data = array('name' => $to_name,'email' => $email ,'mobile' => $mobile, 'password' => $password,'company_name' => $company_name, 'body' => $body);
          \Mail::send('emails.thanksmail.buyer_thanks', $data, function($message) use ($to_name, $to_email) {
              $message->to($to_email, $to_name)
                      ->subject(' Buyer Registered Successfully Mail Message.');
          });

            Session::flash('message',"Buyer User Registered Successfully.");
            return redirect()->back();

        }catch(\Exception $e){
            Session::flash('message',$e->getMessage());
            return redirect()->back();
    }
    }




    public function getStateByName()
    {
        $state_id=$_GET['state_id'];
        
        $html="<option value='' >Select City</option>";
        if($state_id !=""){   

        $sessions=Cities::where('state_id',$state_id)->get();
        if(count($sessions) > 0){
                foreach ($sessions as  $value) {
                   $html.="<option value='".$value->id."'>".$value->name."</option>";
                }
            }else{
                $html.="<option value='' disabled='disabled' >Menu has no Cities</option>";
            }

        }else{
           $html.="<option value='' disabled='disabled' >Menu has no Cities</option>"; 
        }
        return response()->json(array('html'=>$html));
 
       }

    public function loginBuyer(Request $request){
         try{
        $rules = array(
           'email' => 'required|email',
           'password' => 'required|'
        );

        /*return $rules; */
        $remember_me = $request->has('remember') ? true : false;

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('/login')
                ->withErrors("Email and Password is required to login.")
                ;

        } else {

            $userdata = array(
                'email' => $request->email,
                'password' => $request->password,
            );
         
            if (Auth::attempt($userdata, $remember_me)) {
                
                if(auth()->user()->type!=3 ){

                    Auth::logout();
                    return Redirect::to('/login')
                        ->withErrors("Forbidden! You do not have permission to access this route.");
                }
                
                    return Redirect::to('/user-profile');
              
            } else {

                return Redirect::to('/login')
                    ->withErrors("The credentials do not match with our records.");

            }
        
         }
     }catch(Exception $e){

            return redirect()->back()
                ->withErrors($e->getMessage());
        }
    }






    public function loginFront()
    {
 
       return view('front.login');
    }




    public function loginPage()
    {

     if(Auth::user()){
          return Redirect::to('sup_admin/suplier_dashboard');  
    }

       return view('front.login');
       
    }

     public function storeLogin(Request $request){
        try{

        $rules = array(
           'email' => 'required|email',
           'password' => 'required|'
        );

        $remember_me = $request->has('remember') ? true : false;

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('/login')
                ->withErrors("Email and Password is required to login.")
                ->withInput();

        } else {

            $userdata = array(
                'email' => $request->email,
                'password' => $request->password,
            );
         
            if (Auth::attempt($userdata, $remember_me)) {
                
                if(auth()->user()->type!=2){

                    Auth::logout();
                    return Redirect::to('/login')
                        ->withErrors("Forbidden! You do not have permission to access this route.");
                }
                
                    return Redirect::to('sup_admin/suplier_dashboard');
              
            } else {

                return Redirect::to('/login')
                    ->withErrors("The credentials do not match with our records.");

            }
        
         }
     }catch(Exception $e){

            return redirect()->back()
                ->withErrors($e->getMessage());
        }
    }



      /*  public function suplierDashboard(){

         try{
         
        $users = User::all()->whereNOtIn('type',['1','2'] );
        $countUsers = isset($users) ? count($users) : 0;
        $countEnquiryForm = isset($enquiryforms) ? count($enquiryforms) : 0;
       
        return view('sup_admin.suplier_dashboard')
                ->withCountUsers($countUsers)
                ->withCountEnquiryForm($countEnquiryForm);

        }catch(Exception $e){

            return redirect()->back()
                ->withErrors($e->getMessage());
        }
      }*/

      public function logout(Request $request) {

        Auth::logout();

        return Redirect::to('/login')->withMessage('Logged out successfully.');
      }
   
     


   
/*   Forgot password page.*/

      public function forgotPassword()
      {

       return view('front.forgot_password');
      
      }

    public function changePassword()
    {  
    return view('front.change_password');    
    } 
      


     public function reset_password_page($token, Request $request)
     {
        
 //dd($request);
        $tokenData = \DB::table('password_resets')
            ->where('token', $token)->first();

        if ($tokenData) {
            $email = $request->email;
           
            return view('front.change_password', compact('token', 'email'));
        } else {
            Session::flash('message', "Token not valid or may be expired please try again.");
            return redirect('/login');
        }
    }
  

    public function resetPassword(Request $request)
    {
        //Validate input
        $validator = Validator::make($request->all(), [
            
            'password' => 'required',
        ]);

        //check if payload is valid before moving on
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $password = $request->password;
        
        $email = $request->email;

        $user = User::where('email',$email)->first();
     
        if ($user) {
            $user->password = bcrypt($password);
            $user->update(); //or $user->save();
            \DB::table('password_resets')->where('email', $user->email)
                ->delete();
            Session::flash('message', 'Password reset successfully');
            return redirect('/login');
        } else {
            Session::flash('message', 'Network error please try again.');
            return redirect('/login');
        }
    }


    public function storePassword(Request $request)
    {
       
        try {
            $rules = array(
                'email' => 'required|email',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            } else {
                $user = User::where('email', $request->email)->first();
        
                if ($user) {
                    $token = Str::random(60);
                   
                    $link = url('/') . '/password/' . $token . '?email=' . base64_encode($user->email);
                    \DB::table('password_resets')->insert([
                        'email' => $request->email,
                        'token' => $token,
                        'created_at' => Carbon::now()
                    ]);
                    Mail::to($user->email)->send(new ForgotPassword($user->name, $link));
                    Session::flash('log_message', "Password reset link sent to your email id successfully.");
                return redirect()->back();
                } else {
                   
                    Session::flash('message', "Email id not exist please try with registered mail id.");
                    return redirect('/forgot_password');
                }
            }
        } catch (QueryException $ex) {
            Session::flash('message', $ex->getMessage());
            return redirect('/forgot_password');
        }
    }

      public function needHelp()
      {
       return view('front.need_help');
      
      }  
  

      public function covid()
      {
        $category=Category::query()->where('status',1)->limit(4)->get();
      return view('front.covid-19')->withCategory($category);
      }

    
    public function addBuyForm(Request $request)
      {
        try{
              $validator = Validator::make($request->all(), [
                'name' => 'required|min:3|max:16',
                'email'=>'required|email|unique:users,email',
                'mobile' =>'required|numeric|unique:users,mobile|regex:/[0-9]{10}/',
                'location' =>'required', 
                'requirements'=>'required',  
                
            ]);
   
            if($validator->fails())
            {
            return redirect()->back()->withErrors($validator)->withInput();
            }
          
            $suplier_buy = new SuplierEnquiry();
            $suplier_buy->user_id = $request->user_id;
            $suplier_buy->name=$request->name;
            $suplier_buy->email=$request->email;
            $suplier_buy->mobile=$request->mobile;
            $suplier_buy->location=$request->location;
            $suplier_buy->requirements=$request->requirements;              
            $suplier_buy->save();

            $to_name = $request->name;
            $email = $request->email;
            $mobile = $request->mobile;
            $location = $request->location;
            $requirements = $request->requirements;
            $to_email = 'anu.kumari@webmobriltechnologies.com';
            $sup_email = $request->supplier_email;

           
            $body = 'B2b : | Sign Up Supplier Page Message.';
            $data = array('name' => $to_name,'email' => $email ,'mobile' => $mobile, 'location' => $location, 'requirements' => $requirements, 'body' => $body);
            \Mail::send('emails.thanksmail.suplier_product_thanks', $data, function($message) use ($to_name, $to_email,$sup_email) {
              $message->to($to_email, $to_name)
                       ->cc([$sup_email])
                      ->subject(' Product Enquiry Mail Message.');
            });


            Session::flash('message',"Your Enquiry Has Been Submitted Successfully.");
            return redirect()->back();

        }catch(\Exception $e){
            Session::flash('message',$e->getMessage());
            return redirect()->back();
    }
    }



    public function addSuplierRequirment(Request $request)
      {
        try{
              $validator = Validator::make($request->all(), [
                'name' => 'required|min:3|max:16',
                'email'=>'required|email|unique:users,email',
                'mobile' =>'required|numeric|unique:users,mobile|regex:/[0-9]{10}/',
                'location' =>'required', 
                'requirements'=>'required',  
                
            ]);
   
            if($validator->fails())
            {
            return redirect()->back()->withErrors($validator)->withInput();
            }
          
            $suplier_requirments = new SuplierEnquiry();
            $suplier_requirments->user_id = $request->user_id;
            $suplier_requirments->name=$request->name;
            $suplier_requirments->email=$request->email;
            $suplier_requirments->mobile=$request->mobile;
            $suplier_requirments->location=$request->location;
            $suplier_requirments->requirements=$request->requirements;              
            $suplier_requirments->save();

            $to_name = $request->name;
            $email = $request->email;
            $mobile = $request->mobile;
            $location = $request->location;
            $requirements = $request->requirements;
            $to_email = 'anu.kumari@webmobriltechnologies.com';
            $sup_email = $request->supplier_email;

           
            $body = 'B2b : | Sign Up Supplier Page Message.';
            $data = array('name' => $to_name,'email' => $email ,'mobile' => $mobile, 'location' => $location, 'requirements' => $requirements, 'body' => $body);
            \Mail::send('emails.thanksmail.suplier_product_thanks', $data, function($message) use ($to_name, $to_email,$sup_email) {
              $message->to($to_email, $to_name)
                       ->cc([$sup_email])
                      ->subject(' Product Enquiry Mail Message.');
            });


            Session::flash('message',"Your Enquiry Has Been Submitted Successfully.");
            return redirect()->back();

        }catch(\Exception $e){
            Session::flash('message',$e->getMessage());
            return redirect()->back();
    }
    }


    public function supplierContact(Request $request)
      {
        try{
              $validator = Validator::make($request->all(), [
                'name' => 'required|min:3|max:16',
                'email'=>'required|email|unique:users,email',
                'mobile' =>'required|numeric|unique:users,mobile|regex:/[0-9]{10}/',
                'location' =>'required', 
                'requirements'=>'required',  
                
            ]);
   
            if($validator->fails())
            {
            return redirect()->back()->withErrors($validator)->withInput();
            }
          
            $contact_data = new SuplierEnquiry();
            $contact_data->user_id = $request->user_id;
            $contact_data->name = $request->name;
            $contact_data->email = $request->email;
            $contact_data->mobile = $request->mobile;
            $contact_data->location = $request->location;
            $contact_data->requirements = $request->requirements;
            $contact_data->save();

            $to_name = $request->name;
            $email = $request->email;
            $mobile = $request->mobile;
            $location = $request->location;
            $requirements = $request->requirements;
            $to_email = 'anu.kumari@webmobriltechnologies.com';
            $sup_email = $request->supplier_email;

           
            $body = 'B2b : | Sign Up Supplier Page Message.';
            $data = array('name' => $to_name,'email' => $email ,'mobile' => $mobile, 'location' => $location, 'requirements' => $requirements, 'body' => $body);
            \Mail::send('emails.thanksmail.suplier_product_thanks', $data, function($message) use ($to_name, $to_email,$sup_email) {
              $message->to($to_email, $to_name)
                       ->cc([$sup_email])
                      ->subject(' Product Enquiry Mail Message.');
            });

            Session::flash('message',"Your Enquiry Has Been Submitted Successfully.");
            return redirect()->back();

        }catch(\Exception $e){
            Session::flash('message',$e->getMessage());
            return redirect()->back();
    }
    }
     
   
 
    public function editbuyer(Request $request,$id )
    {
    try{
 
    $user = User::query()->where(['id' => $id])->first();          
    return view('front.edit-user-profile')->withUser($user);
    }catch(Exception $e)
    {
    return redirect()->back()->withErrors($e->getMessage());
    }
    }




    public function updateBuyerProfile(Request $request ,$id)
    {
    try
    {
    if($request->has('personal_information')){
    $rules = array(
    'name' => 'required|regex:/^[\pL\s\-]+$/u',
    'email' => 'required|email|unique:users,email,'.$id,
    'designation' => 'required|regex:/^[\pL\s\-]+$/u',
    'mobile' =>  'required|numeric|regex:/[0-9]{10}/|unique:users,mobile,'.$id,
    //'alternate_number' =>  'required|numeric|regex:/[0-9]{10}/',
    'alternate_email' => 'required|email|unique:users,email,',
    );
    }
   if($request->has('address_information')){
    $rules = array(
    'pin' => 'required|numeric',
    'state' => 'required',
    'house_no' => 'required',
    'locality' => 'required',
    'city' =>  'required',
    'country'=> 'required',
    'area' => 'required',
    'landmark' =>  'required',
    );

    }

    if($request->has('company_information')){
    $rules = array(
    'company_name' => 'required',
    'gstin' => 'required',
    'website' => 'required',
    );
    }

    if($request->has('bank_account_details_information')){
    $rules = array(
    'ifcs_code' => 'required|',
    'bank_name' => 'required',
    'account_no' => 'required',
    'account_type' => 'required',
     );

    }
    $validator = Validator::make($request->all(), $rules);
    if ($validator->fails()) {
        return redirect()->back()->withErrors($validator);
    }

    $user = User::findOrfail($request->id);
   /* dd(6565);*/
    if($request->has('personal_information')){
    $user->name = $request->name;
    $user->designation = $request->designation;
    $user->mobile = $request->mobile;
    $user->email = $request->email;
    $user->alternate_number=$request->alternate_number;
    $user->alternate_email = $request->alternate_email;
 
    if ($request->hasFile('user_image')) 
    {
    $image = $request->file('user_image');
    $name = $image->getClientOriginalName();
    $destinationFolder = "public/images/buyer/";
    if(file_exists($destinationFolder.$name))
    {
    unlink($destinationFolder.$name);
    }
    $image->move($destinationFolder,$name);  
    $image ="public/images/buyer/".$name;
    $user->user_image=$image;
    } 
    }
       
    if($request->has('address_information')){
    $user->pin = $request->pin;
    $user->state = $request->state;
    $user->house_no = $request->house_no;
    $user->locality = $request->locality;
    $user->city = $request->city;
    $user->country = $request->country;
    $user->area=$request->area;
    $user->landmark = $request->landmark;
    }
   if($request->has('company_information')){
    $user->company_name = $request->company_name;
    $user->gstin = $request->gstin;
    $user->website = $request->website;
    }

    if($request->has('bank_account_details_information')){
    $user->ifcs_code = $request->ifcs_code;
    $user->bank_name = $request->bank_name;
    $user->account_no = $request->account_no;
    $user->account_type=$request->account_type;
    }
    $user->save();
    return Redirect::to('/edit-user-profile/{id}')->withMessage("User Updated Successfully.");
    }catch(Exception $e){
    return redirect()->back()
        ->withErrors($e->getMessage());

    }
    }
    
    public function userProfile()
    {
 

     $informations = User::query()->where('type',3)->get();

      return view('front.user-profile')->withInformations($informations);
      }


    public function profile1()
    {
 
    return view('front.profile1');
    }

    public function notification()
    {
 
    return view('front.notification');
    }


    /*Buyer Requirments*/


    public function addBuyerRequirment(Request $request)
      {
        try{
            $validator = Validator::make($request->all(), [
               'product_name1'  =>'required',
                'description' =>'required',
                'quantity' =>'required',
                'order' =>'required',
                'name1' => 'required|min:3|max:16',
                'email1'=>'required|email|unique:users,email',
                'mobile' =>'required|numeric|regex:/[0-9]{10}/',
                'addresss' =>'required', 
                'requirements'=>'required',
            ]);
   
            if($validator->fails())
            {
              
            return redirect()->back()->withErrors($validator)->withInput();
            }
              dd(23436666123);    
           $buyer_requirments = new BuyerRequirement();
            $buyer_requirments->product_name1=$request->product_name1;
            $buyer_requirments->description=$request->description;
            $buyer_requirments->quantity=$request->quantity;
            $buyer_requirments->order=$request->order;
            $buyer_requirments->email1=$request->email1;
            $buyer_requirments->name1=$request->name1;
            $buyer_requirments->addresss=$request->addresss;              
            $buyer_requirments->mobile=$request->mobile;          
            $buyer_requirments->save();

          Session::flash('massage',"Quick Enquiry Form Submitted Successfully");
            return redirect()->back();

        }catch(\Exception $e){
            // dd(3);
            Session::flash('massage',$e->getMessage());
            return redirect()->back();

    }
      
      }



    

    public function addQuickForm(Request $request)
      {
        try{
            $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:16',
            'phone' =>'required|numeric|regex:/[0-9]{10}/',
            'email'=>'required|email|unique:users,email',  
            'company_name' =>'required',
            'product_name'=>'required',
            'quality'  =>'required',
            'address' =>'required',  
            ]);
   
            if($validator->fails())
            {
              
            return redirect()->back()->withErrors($validator)->withInput();
            }
               
            $quickforms = new Form();
            $quickforms->name=$request->name;
            $quickforms->company_name=$request->company_name;
            $quickforms->phone=$request->phone;
            $quickforms->email=$request->email;
            $quickforms->product_name=$request->product_name;
            $quickforms->quality=$request->quality;
            $quickforms->address=$request->address;
            $quickforms->save();

          Session::flash('massage',"Quick Enquiry Form Submitted Successfully");
            return redirect()->back();

        }catch(\Exception $e){
            // dd(3);
            Session::flash('massage',$e->getMessage());
            return redirect()->back();

    }
      
      }

    

 /*   public function searchCategory(Request $request)
    {
    
        try{
            
        $searchRequest = isset($request->keyword) && $request->keyword ? $request->keyword : "";
                
      $pro_data = Product::query()
        ->select('products.*','users.name','users.company_name','users.mobile','users.address')
        ->where('products.status','1')
        ->where('title', 'LIKE', "%$searchRequest%")
        ->with('product_images')
        ->leftJoin("users", "users.id", "=", "products.user_id")
        ->get()->toArray();



       if(!$pro_data){
        return redirect()->back();
      }

       return view('front.search',compact('pro_data'));
            }catch(Exception $e){

                return response()->json();
            }
        
    }
*/



public function search(Request $request)
{

    if($request->ajax())
    {

    $output="";
   // $products=Product::where('title','LIKE','%'.$request->search."%")->get();

    $a = $request->search; 
    /*$products = Product::with('user')
            ->where('title','LIKE','%'.$request->search."%")
            ->orWhereHas('User', function ($query) use ($a){
           $query->where('company_name', 'LIKE','%'.$a."%");
        })->get();*/



    //     $products = Product::with(['user' => function($q) use($a) {
    //     $q->orWhere('company_name', 'LIKE','%'.$a."%");
    // }])->where('title','LIKE','%'.$request->search."%")->groupBy('title')->get();
        //sreturn $products;
$products= new Product;
$users=new User;
$products=$products->where('title','LIKE','%'.$request->search."%")->groupBy('title')->get();
$users=$users->where('company_name','LIKE','%'.$request->search."%")->groupBy('company_name')->get();

    if($products)
    {

    foreach ($products as $key => $product) {

    $output.='<li><a href="'.URL::to('product/'.$product->slug).'">'.$product->title.'</a></li>';
    }
    
    }

     if($users){
      
        foreach($users as $user){
            $output.='<li><a href="'.URL::to('/'.$user->slug).'">'.$user->company_name.'</a></li>';
        }
     }    

    return Response($output);    


    }
}






 

    public function storeNeedHelp(Request $request)
      {
        try{
              $validator = Validator::make($request->all(), [
                'name' => 'required|min:3|max:16',
                'email'=>'required|email|unique:users,email',
                'phone' =>'required|numeric|unique:users,mobile|regex:/[0-9]{10}/',
                'massage' =>'required',   
                
            ]);
   
            if($validator->fails())
            {
            return redirect()->back()->withErrors($validator)->withInput();
            }
          
        $need_help = new ContactForm();
        $need_help->name=$request->name;
        $need_help->email=$request->email;
        $need_help->phone=$request->phone;
        $need_help->massage=$request->massage;          
        $need_help->save();

            Session::flash('message',"Need Help Form Submitted Successfully");
            return redirect()->back();

        }catch(\Exception $e){
            Session::flash('message',$e->getMessage());
            return redirect()->back();
    }
    }


    public function productmodal(Request $request)
    {

    $product_page   = Product::with('main_category')->with('category')->with('subcategory')->where('slug',$request->slug)->where('status','1')->with('product_images')->first();
       // return json_encode($product_page);
      return view('front.product_modal',compact('product_page'));

    }


  

    public function recommendedVideo()
    {
    $video_recommended =Video::query()->with('user')->where('status',1)->where('is_recommended',1)->get();
    return view('front.recommended-video')->with('video_recommended',$video_recommended);
    }  

    public function trendingVideo()
    {
    $trendingvideo =Video::query()->with('user')->where('status',1)->where('is_trending',1)->get();
    return view('front.trending-video')->with('trendingvideo',$trendingvideo);
    }  


     public function blog(){

    return view('front.blog');
    }

    public function blogInfo(){

    return view('front.blog-info');
    }

    public function career(){

    $careers=Career::all();    

    return view('front.career')->withcareers($careers);
    }


    public function careerForm(Request $request)
      {
        try{
            $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:16',
            'email'=>'required|email|unique:users,email',  
            'phone'=>'required|numeric|regex:/[0-9]{10}/',
            'location'=>'required',
            'message'=>'required',
            ]);
   
            if($validator->fails())
            {
              
            return redirect()->back()->withErrors($validator)->withInput();
            }
            

            $career = new CareerForm();
            $career->name=$request->name;
            $career->email=$request->email;
            $career->phone=$request->phone;
            $career->location=$request->location;
            $career->message=$request->message;        
            $career->save();

          Session::flash('message',"Career Form Submitted Successfully");
            return redirect()->back();

        }catch(\Exception $e){
            // dd(3);
            Session::flash('message',$e->getMessage());
            return redirect()->back();

    }
      
      }


    

    public function successStory()
    {
      $stories=SuccessStory::all();

    return view('front.success-story')->withStories($stories);
    }

    public function package(){

    return view('front.package');
    }

    public function complaint(){

    return view('front.complaint');
    }

    
    public function addComplaint(Request $request)
      {
        try{
            $validator = Validator::make($request->all(), [
            'company_name'=>'required|min:5|max:20',
            'name' => 'required|min:3|max:20',
            'complain_title' =>'required',
            'email'=>'required|email|unique:users,email',  
            'contact_no'=>'required|numeric|regex:/[0-9]{10}/',
            'description'=>'required',  
            ]);
   
            if($validator->fails())
            {
              
            return redirect()->back()->withErrors($validator)->withInput();
            }
            

        $complaint=new Complaint();       
        $complaint->company_name=$request->company_name;
        $complaint->name=$request->name;
        $complaint->complain_title=$request->complain_title;
        $complaint->email=$request->email;
        $complaint->contact_no=$request->contact_no;
        $complaint->description=$request->description;
        $complaint->save();

          Session::flash('message',"Complaint Form Submitted Successfully");
            return redirect()->back();

        }catch(\Exception $e){
            // dd(3);
            Session::flash('message',$e->getMessage());
            return redirect()->back();

    }
      
     } 




    public function packageForm(Request $request)
      {
        try{
            $validator = Validator::make($request->all(), [
                
            'name' => 'required|min:3|max:16',
            'email'=>'required|email|unique:users,email',  
            'phone'=>'required|numeric|regex:/[0-9]{10}/',
            'company_name'=>'required',
            'address'=>'required',  
            ]);
   
            if($validator->fails())
            {
              
            return redirect()->back()->withErrors($validator)->withInput();
            }
            

        $packageform=new PackageForm();            
        $packageform->name=$request->name;
        $packageform->email=$request->email;
        $packageform->phone=$request->phone;
        $packageform->company_name=$request->company_name;
        $packageform->address=$request->address;
        $packageform->save();

          Session::flash('message',"Package Form Submitted Successfully");
            return redirect()->back();

        }catch(\Exception $e){
            // dd(3);
            Session::flash('message',$e->getMessage());
            return redirect()->back();
    }
    }  


      


    public function pressRelease(){

    $press=PressRelease::query()->where('status',1)->get();
    $pressrelease=PressRelease::query()->where('status',0)->get();

    return view('front.press-release')->withPress($press)->withPressrelease($pressrelease);
    }


    public function hottProducts(){

    return view('front.hot-products');
    }
    
    public function thankYou()
    {
    return view('front.thank-you');    
    }


    public function alllCategory()
    {
     return view('front.all-category'); 
    }

    public function productPage()
    {
     return view('front.product_page'); 
    }

    public function hotProduct()
    {
    $products=Product::query()->with('firstImage')->orderBy('id','desc')->limit('20')->get();
     return view('front.hot-products')->withProducts($products); 
    }

    public function trendingProducts()
    {
    $product=Product::query()->with('firstImage')->orderBy('id','desc')->limit('20')->get(); 
    return view('front.trending-products')->withProduct($product);
    }

     public function hotSeller()
    {
    $hotseller = Video::query()->with('user')->where('status',1)->limit(20)->where('is_trending',1)->get();
     return view('front.hot-seller')->withhotseller($hotseller); 
    }
}



