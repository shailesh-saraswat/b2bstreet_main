<?php

namespace App\Http\Controllers\Front;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\AddUserRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Facades\Mail;


class CategoryController extends Controller
{

    public function index()
    {
        $categories = Category::query()->where('type' , "!=",1)->get();

        return view('admin.category.category_list')->withUsers($categories);
    }


}
