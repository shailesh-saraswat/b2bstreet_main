<?php

namespace App\Http\Controllers\Suplier;

use App\Models\User;
use App\Models\SuplierEnquiry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Exception;

class SuplierController extends Controller
{
//boot
    public function profile(){

        $registeredUsers = User::query()->where(['verified' => 1, 'status' => 1])->get();
       

        $countRegisteredUsers = isset($registeredUsers) && $registeredUsers != null ? count($registeredUsers) : 0;
      

        return view('sup_admin/profile')
                ->withCountRegisteredUsers($countRegisteredUsers);

    }

    public function changePasswordView(){

        return view('sup_admin/change_password');

    }

       public function changePassword(Request $request)
    {

        try {

            $rules = array(
                'password' => 'required',
                'confirm_password' => 'required|same:password'
            );

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);

            } else {

                $user = auth()->user();

                if ($user) {

                    $user->fill([
                        'password' => Hash::make($request->password)
                    ])->save();

                    $message = "Password updated successfully";
                    return redirect()->back()->withMessage($message);
                }
               
            }

        } catch (Exception $e) {

            return redirect()->back()->withErrors($e->getMessage());
        }
    }




    public function getProfile(){

        $user = auth()->user();

        return view('sup_admin.update_profile')->withUser($user);

    }

    public function updateProfile(Request $request){

        $rules = array(
            'name' => 'required',
            'profile_image' => 'sometimes|mimes:jpg,jpeg,png,gif'
        );

        $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
}
        else{

            $user = auth()->user();
            $user->name = $request->input('name');

            if ($request->hasFile('profile_image')) {

                try {

                    $profile_image = $request->file('profile_image');

                    $name = $profile_image->getClientOriginalName();

                    $destinationFolder = public_path('/profile_pictures/admin-' . $user->id . '/');

                    // Delete folder
                    if(file_exists($destinationFolder.$name)){

                        unlink($destinationFolder.$name);
                    }

                    $profile_image->move($destinationFolder, $name);
                    $user->profile_image = env('APP_URL'). "/public/profile_pictures/admin-".$user->id.'/'.$name;

                } catch (\Exception $e) {
                    
                    return redirect()->back()->withErrors($e->getMessage());
                }
            }

            $user->update();

            return Redirect::to('sup_admin/profile');
        }


    }


    public function list()
    {
     $enquirys = SuplierEnquiry::where('user_id',auth()->user()->id)->get();

     return view('sup_admin.enquiry.enquiry_list')->withEnquirys($enquirys);
   
    }
    public function deleteEnquiry($id){

        $enquirys =SuplierEnquiry::findOrfail($id);

        $enquirys->delete();
        $message ="Buyer From Deleted Successfully!";
        return redirect()->back()->withMessage($message);
     }


   
}
