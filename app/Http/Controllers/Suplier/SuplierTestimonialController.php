<?php

namespace App\Http\Controllers\Suplier;

use Illuminate\Http\Request;
use App\Models\SuplierTestimonial;
use App\Http\Controllers\Controller;
use Exception;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Validator;
use Str;

class SuplierTestimonialController extends Controller
{
     public function list()
    {
        $testimonials = SuplierTestimonial::all()->where('user_id',auth()->user()->id);

        return view('sup_admin.Testimonial.testimonial_list')->withTestimonials($testimonials);
    }


    public function addFormTestimonial()
    {
        return view('sup_admin.Testimonial.add_testimonial');
    }

  
  public function addTestimonial(Request $request)
    {
      
      try{
     
      $rules=array(
       'title' => 'required',
       'description' => 'required',
       'image' => 'mimes:jpeg,jpg,png,gif|required|max:10000',
       'status' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
        return redirect()->back()->withErrors($validator)->withInput();
        }


    $testimonial = new SuplierTestimonial();
    $testimonial->user_id = $request->user_id;
    $testimonial->title = $request->title;
    $testimonial->description = $request->description;
    $testimonial->status = $request->status;
    if ($request->hasFile('image')) 
    {

    $image = $request->file('image');

    $name1 = $image->getClientOriginalName();
    $destinationFolder = "public/images/testimonial/";
    if(file_exists($destinationFolder.$name1))
    {
    unlink($destinationFolder.$name1);
    }
    $image->move($destinationFolder,$name1);  
    $image ="public/images/testimonial/".$name1;
    $testimonial->image=$image;

    } 

   
     $testimonial->save();
            
    return Redirect::to('sup_admin/testimonial_list')->withMessage("Banner Created Successfully."); 
            
    }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
    }


     public function editTestimonial(Request $request, $id){
        try{
           
           $testimonial = SuplierTestimonial::query()->where(['id' => $id])->first();
           
           return view('sup_admin.Testimonial.update_testimonial')->withTestimonial($testimonial);
            
          }catch(Exception $e){

            return redirect()->back()
            ->withErrors($e->getMessage());
          }
        }


        public function updateTestimonial(Request $request){

            
            try{ 
                
                $testimonial = SuplierTestimonial::findOrfail($request->id);
                $testimonial->user_id = $request->user_id;
                $testimonial->title = $request->title;
                $testimonial->description = $request->description;
                $testimonial->status = $request->status;
             
               if ($request->hasFile('image')) 
               {

               $image = $request->file('image');

               $name1 = $image->getClientOriginalName();
               $destinationFolder = "public/images/testimonial/";
               if(file_exists($destinationFolder.$name1))
               {
               unlink($destinationFolder.$name1);
               }
               $image->move($destinationFolder,$name1);  
               $image ="public/images/testimonial/".$name1;
               $testimonial->image=$image;
               }

               $testimonial->save();
           
             return Redirect::to('sup_admin/testimonial_list')->withMessage("Testimonial Updated Successfully."); 
            
             }catch(Exception $e){

                return redirect()->back()
                    ->withErrors($e->getMessage());
            }
                
    } 





     public function deleteTestimonial($id){

    $testimonial = SuplierTestimonial::findOrfail($id);
    $testimonial->delete();

     $message = "Testimonial Deleted Successfully!";
        return redirect()->back()->withMessage($message);
  }
}
