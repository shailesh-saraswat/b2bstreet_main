<?php

namespace App\Http\Controllers\Suplier;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Models\Network;
use Validator;

class NetworkController extends Controller
{
    public function list()
    {
      
      $networks = Network::all()->where('user_id',auth()->user()->id);

      return view('sup_admin.network.network_list')->withNetworks( $networks);

    }


    public function addFormNetwork(){

     return view('sup_admin.network.add_network');
    }

    public function addNetwork(Request $request){

        try{

            $rules = array(

            'title' => '',
            'description' =>'',
            'image' =>'mimes:jpeg,jpg,png,gif|max:10000',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) 
            {
                
            return redirect()->back()->withErrors($validator)->withInput();
            }

            $network = new Network();
            $network->user_id = $request->user_id;
            $network->title = $request->title;
            $network->description = $request->description;
            if ($request->hasFile('image')) 
            {

            $image =$request->file('image');
            $name =$image->getClientOriginalName();
            $destination = "public/images/gallery/";
            if (file_exists($destination.$name)) 
            {
                unlink($destination.$name);
            }
            $image->move($destination,$name); 
            $image = "public/images/gallery/".$name;
            $network->image=$image;
            }
            
            $network->save();

            return Redirect::to('sup_admin/network_list')->withMessage("Our Network Created Successfully.");

            }catch(Exception $e)
            {
            return redirect()->back()->withErrors($e->getMessage());
           }
        }


        public function editNetwork(Request $request , $id){

            try{

            
            $network = Network::query()->where(['id' => $id])->first();
          
           return view('sup_admin.network.update_network')->withNetwork($network);

            }catch(Exception $e)
            {
              return redirect()->back()->withErrors($e->getMessage());
            }
        }

        public function updateNetwork(Request $request){

            try{

            $network = Network::findOrfail($request->id);
            $network->user_id = $request->user_id;
            $network->title = $request->title;
            $network->description = $request->description;
            if ($request->hasFile('image')) 
            {

            $image =$request->file('image');
            $name =$image->getClientOriginalName();
            $destination = "public/images/gallery/";
            if (file_exists($destination.$name)) 
            {
                unlink($destination.$name);
            }
            $image->move($destination,$name); 
            $image = "public/images/gallery/".$name;
            $network->image=$image;
            }

            $network->save();

                return Redirect::to('sup_admin/network_list')->withMessage("Our Network Updated Successfully.");
            }catch(Exception $e){

                return redirect()->back()->withErrors($e->getMessage());
            }
        }


        public function deleteNetwork($id)
        {
            $network=Network::findOrfail($id);
            $network->delete();

            $message = "Network Deleted Successfully!";
        return redirect()->back()->withMessage($message); 
        }  


}


