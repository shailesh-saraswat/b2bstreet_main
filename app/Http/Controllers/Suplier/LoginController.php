<?php

namespace App\Http\Controllers\Suplier;

use App\Models\User;
use App\Models\SuplierEnquiry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;


class LoginController extends Controller
{
    public function leads(){

         try{

        $anus = SuplierEnquiry::where('user_id',auth()->user()->id)->get(); 
        $countAnus = isset($anus) ? count($anus) : 0;

        return view('sup_admin.suplier_dashboard')
                ->withCountAnus($countAnus);
               

        }catch(Exception $e){

            return redirect()->back()
                ->withErrors($e->getMessage());
        }
    }

    public function packageList(){
         
    $packages = User::all();
       
    return view('sup_admin.user.package_list')->withPackages($packages);

    }

      public function loginPage(){
        if(Auth::user()){
          return Redirect::to('sup_admin/suplier_dashboard');  
        }

        return view('sup_admin.auth.login');

    }

     public function logout(Request $request) {

        Auth::logout();
        Session::flush();

        return Redirect::to('/sup_admin')->withMessage('Logged out successfully.');
    }



    public function adminRedirect(){

        $supplier = User::query()->where(['id' => 2])->first();
     // $decrypt= decrypt($supplier->password);  
// dd($decrypt);
      $userdata = array(
                'email' => $supplier->email,
                'password' => $supplier->supplierpassword,
            );
          if (Auth::attempt($userdata,true)) {
            //echo Auth()->user()->id;
            //die('if');
                 // Auth::logout();
                if($supplier->type==1){
                    Session::forget('previoususer');
                    // Auth::logout();
                    //  dd('if==');
                    // Session::put('previoususer', 'admin');
                    // dd(Session::get('previoususer'));
                    $data = array('admin'=>'admin');
                    return Redirect::to('/admin/supplier_list');
                }
                              
              
            } else {
                die('else');
                return Redirect::to('/admin/supplier_list');
            }
  }

}
