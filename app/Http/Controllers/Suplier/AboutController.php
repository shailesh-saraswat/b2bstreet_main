<?php

namespace App\Http\Controllers\Suplier;

use Illuminate\Http\Request;
use App\Models\SuplierAbout;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Exception;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Validator;

class AboutController extends Controller
{
    public function list()
    {

    $abouts = SuplierAbout::all()->where('user_id',auth()->user()->id);

     return view('sup_admin.about.about_list')->withAbouts($abouts);

   
    }


    public function addAboutForm()
    {
       return view('sup_admin.about.add_about');  
    }


    public function addAbout(Request $request)
    {
      
      try{
     
      $rules=array(
       'title' => 'required',
       'description' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
        return redirect()->back()->withErrors($validator)->withInput();
        }

    $about = new SuplierAbout();
    $about->user_id = $request->user_id;
    $about->title = $request->title;
    $about->description = $request->description;
  
     $about->save();
            
    return Redirect::to('sup_admin/about_list')->withMessage("About Created Successfully."); 
            
    }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
    }

    public function editAbout(Request $request, $id)
     {
        
        try{
           
           $about = SuplierAbout::query()->where(['id' => $id])->first();
        
           return view('sup_admin.about.update_about')->withAbout($about);
            
          }catch(Exception $e){

            return redirect()->back()
            ->withErrors($e->getMessage());
          }
        }

    public function updateAbouts(Request $request){
  
    try{
     
      $rules=array(
       'title' => 'required',
       'description' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
        return redirect()->back()->withErrors($validator)->withInput();
        }

    $about = SuplierAbout::findOrfail($request->id); 
    $about->user_id = $request->user_id;
    $about->title = $request->title;
    $about->description = $request->description;
  
     $about->save();
            
    return Redirect::to('sup_admin/about_list')->withMessage("About Updated Successfully."); 
            
    }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }

    }

    public function deleteAbout($id){

     $about = SuplierAbout::findOrfail($id);
     $about->delete();

     $message = "About Deleted Successfully!";
        return redirect()->back()->withMessage($message);

    }

}
