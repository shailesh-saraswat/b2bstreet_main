<?php

namespace App\Http\Controllers\Suplier;

use Illuminate\Http\Request;
use App\Models\SuplierBanner;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Validator;

class SuplierBannerController extends Controller
{
    public function list()
    {

    $banners = SuplierBanner::all()->where('user_id',auth()->user()->id);

     return view('sup_admin.banner.banner_list')->withBanners($banners);

   
    }


    public function addBannerForm()
    {
       return view('sup_admin.banner.add_banner');  
    }


    public function addBanner(Request $request)
    {
      
      try{
     
      $rules=array(
       'title' => 'required',
       'name' => 'required',
       'address' => 'required',
       'mobile' =>  '',
       'email' => 'required',
       'website' => '',
       'image' => 'mimes:jpeg,jpg,png,gif|required|max:10000',
      // 'video' => 'mimes:mpeg,ogg,mp4,webm,3gp,mov,flv,avi,wmv,ts|max:100040|required',
       'status' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
        return redirect()->back()->withErrors($validator)->withInput();
        }


    $banner = new SuplierBanner();
    $banner->user_id = $request->user_id;
    $banner->title = $request->title;
    $banner->name = $request->name;
    $banner->address = $request->address;
    $banner->mobile = $request->mobile;
    $banner->email = $request->email;
    $banner->website = $request->website;
    $banner->status = $request->status;
    if ($request->hasFile('image')) 
    {

    $video_image = $request->file('image');

    $name = $video_image->getClientOriginalName();
    $destinationFolder = "public/images/banner/";
   
    if(file_exists($destinationFolder.$name))
    {
    unlink($destinationFolder.$name);
    }
    $video_image->move($destinationFolder,$name);  
    $video_image ="public/images/banner/".$name;
    $banner->image=$video_image;

    } 

    if ($request->hasFile('video')) 
    {

    $video_image = $request->file('video');

    $name = $video_image->getClientOriginalName();
    $destinationFolder = "public/images/banner/";
    if(file_exists($destinationFolder.$name))
    {
    unlink($destinationFolder.$name);
    }
    $video_image->move($destinationFolder,$name);  
    $video_image = "public/images/banner/".$name;
    $banner->video=$video_image;

    } 

   //return $banner;
     $banner->save();
            
    return Redirect::to('sup_admin/banner_list')->withMessage("Banner Created Successfully."); 
            
    }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
    }


     public function editbanner(Request $request, $id)
     {
        
        try{
           
           $banner = SuplierBanner::query()->where(['id' => $id])->first();
        
           return view('sup_admin.banner.update_banner')->withBanner($banner);
            
          }catch(Exception $e){

            return redirect()->back()
            ->withErrors($e->getMessage());
          }
        }


        public function updateBanner(Request $request)
    {
      
      try{
     
      $rules=array(
       'name' => '',
       'address' => '',
       'mobile' =>  '',
       'email' => '',
       'website' => '',
       'image' => '',
      // 'video' => 'mimes:mpeg,ogg,mp4,webm,3gp,mov,flv,avi,wmv,ts|max:100040|required',
       'status' => '',
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
        return redirect()->back()->withErrors($validator)->withInput();
        }

    $banner = SuplierBanner::findOrfail($request->id);
    $banner->user_id = $request->user_id;
    $banner->title = $request->title; 
    $banner->name = $request->name;
    $banner->address = $request->address;
    $banner->mobile = $request->mobile;
    $banner->email = $request->email;
    $banner->website = $request->website;
    $banner->status = $request->status;
    if ($request->hasFile('image')) 
    {

    $video_image = $request->file('image');

    $name = $video_image->getClientOriginalName();
    $destinationFolder = "public/images/banner/";
   
    if(file_exists($destinationFolder.$name))
    {
    unlink($destinationFolder.$name);
    }
    $video_image->move($destinationFolder,$name);  
    $video_image ="public/images/banner/".$name;
    $banner->image=$video_image;

    } 

    if ($request->hasFile('video')) 
    {

    $video_image = $request->file('video');

    $name = $video_image->getClientOriginalName();
    $destinationFolder = "public/images/banner/";
    if(file_exists($destinationFolder.$name))
    {
    unlink($destinationFolder.$name);
    }
    $video_image->move($destinationFolder,$name);  
    $video_image = "public/images/banner/".$name;
    $banner->video=$video_image;

    } 

   
     $banner->save();
            
    return Redirect::to('sup_admin/banner_list')->withMessage("Banner Updated Successfully."); 
            
    }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
    }

    public function deleteBanner($id){

     $video = SuplierBanner::findOrfail($id);
     $video->delete();

     $message = "Banner Deleted Successfully!";
        return redirect()->back()->withMessage($message);

    }
}
