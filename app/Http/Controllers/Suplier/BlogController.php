<?php

namespace App\Http\Controllers\Suplier;

use Illuminate\Http\Request;
use App\Models\Blog;
use App\Http\Controllers\Controller;
use Exception;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Validator;

class BlogController extends Controller
{
    public function list()
    {

    $blogs = Blog::all()->where('user_id',auth()->user()->id);

     return view('sup_admin.blog.blog_list')->withBlogs($blogs);

   
    }

     public function addBlogForm()
    {
       return view('sup_admin.blog.add_blog');  
    }


    public function addBlog(Request $request)
    {
      
      try{
     
      $rules=array(
       'title' => 'required',
       'description' => 'required',
       'user_name' => 'required',
       'date' => 'required',
       'image' => 'mimes:jpeg,jpg,png,gif|required|max:10000',
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
        return redirect()->back()->withErrors($validator)->withInput();
        }


    $blog = new Blog();
    $blog->user_id = $request->user_id;
    $blog->title = $request->title;
    $blog->description = $request->description;
    $blog->user_name = $request->user_name;
    $blog->date = $request->date;
    if ($request->hasFile('image')) 
    {

    $image = $request->file('image');

    $name = $image->getClientOriginalName();
    $destinationFolder = "public/images/blog/";
   
    if(file_exists($destinationFolder.$name))
    {
    unlink($destinationFolder.$name);
    }
    $image->move($destinationFolder,$name);  
    $image ="public/images/blog/".$name;
    $blog->image=$image;

    } 
     $blog->save();
            
    return Redirect::to('sup_admin/blog_list')->withMessage("Blog Created Successfully."); 
            
    }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
    }

        public function editBlog(Request $request, $id){
        try{
           
           $blog = Blog::query()->where(['id' => $id])->first();
          
           return view('sup_admin.blog.update_blog')->withBlog($blog);
            
          }catch(Exception $e){

            return redirect()->back()
            ->withErrors($e->getMessage());
          }
        }

    public function updateBlog(Request $request)
    {
      
      try{
     
      $rules=array(
       'title' => 'required',
       'description' => 'required',
       'user_name' => 'required',
       'date' => 'required',
       'image' => 'mimes:jpeg,jpg,png,gif|required|max:10000',
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
        return redirect()->back()->withErrors($validator)->withInput();
        }


    $blog = Blog::findOrfail($request->id); 
    $blog->user_id = $request->user_id;
    $blog->title = $request->title;
    $blog->description = $request->description;
    $blog->user_name = $request->user_name;
    $blog->date = $request->date;
    if ($request->hasFile('image')) 
    {

    $image = $request->file('image');

    $name = $image->getClientOriginalName();
    $destinationFolder = "public/images/blog/";
   
    if(file_exists($destinationFolder.$name))
    {
    unlink($destinationFolder.$name);
    }
    $image->move($destinationFolder,$name);  
    $image ="public/images/blog/".$name;
    $blog->image=$image;

    } 
     $blog->save();
            
    return Redirect::to('sup_admin/blog_list')->withMessage("Blog Updated Successfully."); 
            
    }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
    }

    public function deleteBlog($id)
    {
      
    $page = Blog::findOrfail($id);
    $page->delete();

    $message = "Blog Deleted Successfully!";
        return redirect()->back()->withMessage($message); 
    }

}


