<?php

namespace App\Http\Controllers\Suplier;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Models\Certificate;
use Validator;

class CertificateController extends Controller
{
    public function list()
    {
      
    $certificates = Certificate::all()->where('user_id',auth()->user()->id);

    return view('sup_admin.certificate.certificate_list')->withCertificates( $certificates);

    }


    public function addCertificateForm(){

     return view('sup_admin.certificate.add_certificate');
    }

    public function addCertificate(Request $request){

        try{

            $rules = array(

            'title' => '',
            'description' =>'',
            'image' =>'mimes:jpeg,jpg,png,gif|max:10000',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) 
            {
                
            return redirect()->back()->withErrors($validator)->withInput();
            }

            $certificate = new Certificate();
            $certificate->user_id = $request->user_id;
            $certificate->title = $request->title;
            $certificate->description = $request->description;
            if ($request->hasFile('image')) 
            {

            $image =$request->file('image');
            $name =$image->getClientOriginalName();
            $destination = "public/images/gallery/";
            if (file_exists($destination.$name)) 
            {
                unlink($destination.$name);
            }
            $image->move($destination,$name); 
            $image = "public/images/gallery/".$name;
            $certificate->image=$image;
            }
            
            $certificate->save();

            return Redirect::to('sup_admin/certificate_list')->withMessage("Certificate  Created Successfully.");

            }catch(Exception $e)
            {
            return redirect()->back()->withErrors($e->getMessage());
           }
        }


        public function editCertificate(Request $request , $id){

            try{

            
            $certificate = Certificate::query()->where(['id' => $id])->first();
          
           return view('sup_admin.certificate.update_certificate')->withCertificate($certificate);

            }catch(Exception $e)
            {
              return redirect()->back()->withErrors($e->getMessage());
            }
        }

        public function updateCertificate(Request $request){

            try{

            $certificate = Certificate::findOrfail($request->id);
            $certificate->user_id = $request->user_id;
            $certificate->title = $request->title;
            $certificate->description = $request->description;
            if ($request->hasFile('image')) 
            {

            $image =$request->file('image');
            $name =$image->getClientOriginalName();
            $destination = "public/images/gallery/";
            if (file_exists($destination.$name)) 
            {
                unlink($destination.$name);
            }
            $image->move($destination,$name); 
            $image = "public/images/gallery/".$name;
            $certificate->image=$image;
            }

            $certificate->save();

                return Redirect::to('sup_admin/certificate_list')->withMessage("Certificate Updated Successfully.");
            }catch(Exception $e){

                return redirect()->back()->withErrors($e->getMessage());
            }
        }


        public function deleteCertificate($id)
        {
            $certificate=Certificate::findOrfail($id);
            $certificate->delete();

            $message = "Certificate Deleted Successfully!";
        return redirect()->back()->withMessage($message); 
        }  

}
