<?php

namespace App\Http\Controllers\Suplier;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Str;
use Validator;

class SuplierUserController extends Controller
{
    public function list(){
         
    $users = User::query()->where('type', 3)->get();
       
    return view('sup_admin.user.user_list')->withUsers($users);

    }


    public function addUserForm(){

    return view('sup_admin.user.add_user');

    }
            

    public function addUser(request $request){

        if($request->isMethod('post')){

        try{

        $rules=array(
        'name'          => 'required',
        'email'         => 'required|email|unique:users',
        'mobile'        => 'required|min:6|max:15|unique:users',
        'password'      => 'required|min:6',
        'status'        => 'required',
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
        return redirect()->back()->withErrors($validator)->withInput();
        }

                $requestData = $request->all();

                $password = $request->password;

                $requestData['password'] = bcrypt($requestData['password']);

                $user = User::create($requestData);

                return Redirect::to('sup_admin/user-list')->withMessage("User Created Successfully."); 
            
            }catch(Exception $e){

                return redirect()->back()
                    ->withErrors($e->getMessage());
            }
        }

    }


    public function getUser(Request $request, $id)
    {
        try {
            
            $user = User::query()->where(['id' => $id])->first();

            return view('sup_admin.user.update_user')->withUser($user);
        
        }catch(Exception $e){

            return redirect()->back()
                    ->withErrors($e->getMessage());

        }
    }


    public function updateUser(Request $request){
     
        if($request->isMethod('post')){

            try{

                $rules=array(
                'name'          => 'required',
                'email'         => 'required|email|unique:users',
                'mobile'        => 'required|min:6|max:15|unique:users',
                'password'      => 'required|min:6',
                'status'        => 'required',
                );

                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails())
                {
                return redirect()->back()->withErrors($validator)->withInput();
                }

                $userId = $request->input('id');

                $user = User::query()->where('id', $userId)->first();
                
                $requestData = $request->all();
                $checkMobile = User::where(['mobile' => $requestData['mobile'], 'type' => 2])->where('id', '!=', $user->id)->first();

                if(isset($checkMobile)){
                    
                    return redirect()->back()
                        ->withErrors("Mobile Number already exists.");
                }

                $requestData['password'] = $requestData['password'] != null ? bcrypt($requestData['password']) : $user->password;

                $user->update($requestData);

                return Redirect::to('sup_admin/user-list')->withMessage("User Updated Successfully."); 
            
            }catch(Exception $e){

                return redirect()->back()
                    ->withErrors($e->getMessage());
            }
        }
    }    

    public function deleteUser($id){

        $user = User::findOrfail($id);
        $user->delete();

        $message = "User Deleted Successfully!";
        return redirect()->back()->withMessage($message);

    }
}
