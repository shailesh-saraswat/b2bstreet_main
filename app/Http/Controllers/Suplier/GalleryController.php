<?php

namespace App\Http\Controllers\Suplier;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Models\SuplierGallery;
use Validator;

class GalleryController extends Controller
{
    public function list()
    {
      
      $galleries = SuplierGallery::all()->where('user_id',auth()->user()->id);

      return view('sup_admin.gallery.gallery_list')->withGalleries( $galleries);

    }

    public function addFormGallery(){

     return view('sup_admin.gallery.add_gallery');
    }

    public function addGallery(Request $request){

        try{

            $rules = array(

            'title' => '',
            'description' =>'',
            'image' =>'mimes:jpeg,jpg,png,gif|max:10000',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) 
            {
                
            return redirect()->back()->withErrors($validator)->withInput();
            }

            $gallery = new SuplierGallery();
            $gallery->user_id = $request->user_id;
            $gallery->title = $request->title;
            $gallery->description = $request->description;
            if ($request->hasFile('image')) 
            {

            $gallery_image =$request->file('image');
            $name =$gallery_image->getClientOriginalName();
            $destination = "public/images/gallery/";
            if (file_exists($destination.$name)) 
            {
                unlink($destination.$name);
            }
            $gallery_image->move($destination,$name); 
            $gallery_image = "public/images/gallery/".$name;
            $gallery->image=$gallery_image;
            }
            
            $gallery->save();

            return Redirect::to('sup_admin/gallery_list')->withMessage("Gallery Created Successfully.");

            }catch(Exception $e)
            {
            return redirect()->back()->withErrors($e->getMessage());
           }
        }



        public function editGallery(Request $request , $id){

            try{

            
            $gallery = SuplierGallery::query()->where(['id' => $id])->first();
          
           return view('sup_admin.gallery.update_gallery')->withGallery($gallery);

            }catch(Exception $e)
            {
              return redirect()->back()->withErrors($e->getMessage());
            }
        }

        public function updateGallery(Request $request){

            try{

                $gallery = SuplierGallery::findOrfail($request->id);
                $gallery->user_id = $request->user_id;
                $gallery->title = $request->title;
                $gallery->description = $request->description;
                if ($request->hasFile('image')) 
            {

            $gallery_image =$request->file('image');
            $name =$gallery_image->getClientOriginalName();
            $destination = "public/images/gallery/";
            if (file_exists($destination.$name)) 
            {
                unlink($destination.$name);
            }
            $gallery_image->move($destination,$name); 
            $gallery_image = "public/images/gallery/".$name;
            $gallery->image=$gallery_image;
                }

                $gallery->save();

                return Redirect::to('sup_admin/gallery_list')->withMessage("Gallery Updated Successfully.");
            }catch(Exception $e){

                return redirect()->back()->withErrors($e->getMessage());
            }
        }



        public function deleteGallery($id)
        {
            $gallery=SuplierGallery::findOrfail($id);
            $gallery->delete();

            $message = "Gallery Deleted Successfully!";
        return redirect()->back()->withMessage($message); 
        }
}
