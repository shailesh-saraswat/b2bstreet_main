<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Models\SuccessStory;
use Validator;
use Exception;





class SuccessStoryController extends Controller
{
    public function list(){

        $stories = SuccessStory::all();
        
        return view('admin.success-story.success_story_list')->withStories($stories);
    }

    public function addstoryForm(){

        return view('admin.success-story.add_success_story');

    }


     public function addSuccessStory(Request $request)
    {
        
      try{
     
      $rules=array(
       'title' => 'required',
       'date'=>'required',
       'description' => 'required',
       'image' =>'mimes:jpeg,jpg,png,gif|max:10000',
       'status' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
        return redirect()->back()->withErrors($validator)->withInput();
        }

        $stories = new SuccessStory();
        $stories->title =$request->title;
        $stories->date =$request->date;
        $stories->description = $request->description;
        $stories->status = $request->status;
        if ($request->hasFile('image')) 
            {

            $gallery_image =$request->file('image');
            $name =$gallery_image->getClientOriginalName();
            $destination = "public/images/gallery/";
            if (file_exists($destination.$name)) 
            {
                unlink($destination.$name);
            }
            $gallery_image->move($destination,$name); 
            $gallery_image = "public/images/gallery/".$name;
            $stories->image=$gallery_image;
            }

        $stories->save();
            
        return Redirect::to('admin/success_story_list')->withMessage("Success Story Created Successfully."); 
            
        }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
    }

    public function editSuccessStory(Request $request, $id){
        try{
           
           $stories = SuccessStory::query()->where(['id' => $id])->first();
          
           return view('admin.success-story.update_success_story')->withStories($stories);
            
          }catch(Exception $e){

            return redirect()->back()
            ->withErrors($e->getMessage());
          }
        }


    public function UpdateSuccessStory(Request $request)
    {
        
      try{
     
      $rules=array(
       'title' => 'required',
       'date'=>'required',
       'description' => 'required',
       'image' =>'mimes:jpeg,jpg,png,gif|max:10000',
       'status' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
        return redirect()->back()->withErrors($validator)->withInput();
        }

        $stories = SuccessStory::findOrfail($request->id);
        $stories->title =$request->title;
        $stories->date =$request->date;
        $stories->description = $request->description;
        $stories->status = $request->status;
        if ($request->hasFile('image')) 
            {

            $gallery_image =$request->file('image');
            $name =$gallery_image->getClientOriginalName();
            $destination = "public/images/gallery/";
            if (file_exists($destination.$name)) 
            {
                unlink($destination.$name);
            }
            $gallery_image->move($destination,$name); 
            $gallery_image = "public/images/gallery/".$name;
            $stories->image=$gallery_image;
            }

        $stories->save();
            
        return Redirect::to('admin/success_story_list')->withMessage("Success Story Updated Successfully."); 
            
        }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
    }
    public function deleteSuccessStory($id){

    $stories = SuccessStory::findOrfail($id);
    $stories->delete();

     $message = "Success Story  Deleted Successfully!";
        return redirect()->back()->withMessage($message);
  }


}
