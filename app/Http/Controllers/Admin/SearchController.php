<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

class SearchController extends Controller
{


	public function index(Request $request)
	{

		$validation = Validator::make($request->all(), [
			'search'    => 'required'
		]);

		if ($validation->fails()) {

			return redirect()->back()
				->withErrors($validation);
		}

		else{

			$searchRequest = $request->input('search');

			$users = User::query()
					->where('type', '=', 2)
					->where('is_admin', '=', 0)
                    ->where('name', 'LIKE', "%$searchRequest%")
                    ->orWhere('address',  'LIKE', "%$searchRequest%")
                    ->orWhere('email',  'LIKE', "%$searchRequest%")
					->get();

            return view('admin.user.user_list')->withUsers($users);

		}


	}

}
