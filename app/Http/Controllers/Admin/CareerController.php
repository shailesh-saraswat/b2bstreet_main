<?php

namespace App\Http\Controllers\Admin;

use App\Models\Career;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\AddCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use Illuminate\Support\Facades\Mail;
use Validator;

class CareerController extends Controller
{

   public function list()
   {
   $careers=Career::all();
   return view('admin.career.career_list')->withCareers($careers);
  }

    public function careerForm()
    {
        return view('admin.career.add_career');
    }

    public function addCareer(Request $request)
    {
      
      try{
     
      $rules=array(
       'title' => 'required',
       'description' => 'required',
       'content' => 'required',
       'short_content' => 'required',
       'status' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
        return redirect()->back()->withErrors($validator)->withInput();
        }


    $career = new Career();
    $career->title = $request->title;
    $career->description = $request->description;
    $career->content = $request->content;
    $career->short_content = $request->short_content;
    $career->status = $request->status;
    $career->save();
            
    return Redirect::to('admin/career_list')->withMessage("Career Created Successfully."); 
            
    }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
    }


    public function editCareer(Request $request,$id)
    {
        try
        {
        $career=Career::query()->where(['id' => $id])->first();
        return view('admin.career.update_career')->withCareer($career);
    }catch(Exception $e){
        return redirect()->back()
        ->withErrors($e->getMessage());
    }
    }

    public function updateCareer(Request $request){
        try
        {
        $rules=array(
       'title' => 'required',
       'description' => 'required',
       'content' => 'required',
       'short_content' => 'required',
       'status' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
        return redirect()->back()->withErrors($validator)->withInput();
        }
        $career=Career::findOrfail($request->id);
        $career->title = $request->title;
        $career->description = $request->description;
        $career->content = $request->content;
        $career->short_content = $request->short_content;
        $career->status = $request->status;
        $career->save();
        return redirect::to('admin/career_list')->withMessage("Career Updated Successfully.");
        }catch(Exception $e){
            return redirect()->back()
            ->withErrors($e->getMessage());
        }
    }

    public function deleteCareer($id)
    {
        $career=Career::findOrfail($id);
        $career->delete();
        $message = "career  Deleted Successfully!";
        return redirect()->back()->withMessage($message);
    }
}
