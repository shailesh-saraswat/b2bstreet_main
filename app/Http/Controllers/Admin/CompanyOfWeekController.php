<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Models\CompanyOfWeek;
use App\Models\User;
use Validator;

class CompanyOfWeekController extends Controller
{
    public function list(){

      $companies = CompanyOfWeek::all();

      return view('admin.company-of-the-week.companyofweek_list')->withCompanies($companies);
    }


    public function addCompanyForm(){

        $user=User::all();

       return view('admin.company-of-the-week.add_companyofweek')->withUser($user);
}


    public function addCompanyOfWeek(Request $request){

        try{

            $rules = array(
            'description' => 'required',
            'video' => 'mimes:mpeg,ogg,mp4,webm,3gp,mov,flv,avi,wmv,ts|max:100040|required',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) 
            {
                
            return redirect()->back()->withErrors($validator)->withInput();
            }

            $company = new CompanyOfWeek();
            $company->user_id = $request->user_id;
            $company->description = $request->description;
            if ($request->hasFile('video')) 
            {

            $video_image = $request->file('video');

            $name = $video_image->getClientOriginalName();
            $destinationFolder = "public/images/video/";
            if(file_exists($destinationFolder.$name))
            {
            unlink($destinationFolder.$name);
            }
            $video_image->move($destinationFolder,$name);  
            $video_image = "public/images/video/".$name;
            $company->video=$video_image;

        } 
        
            $company->save();

            return Redirect::to('admin/companyofweek_list')->withMessage("Company of the Weeks Created Successfully.");

            }catch(Exception $e)
            {
            return redirect()->back()->withErrors($e->getMessage());
           }
        }


         public function editCompanyOfWeek(Request $request , $id){

            try{

            
            $company = CompanyOfWeek::query()->where(['id' => $id])->first();
            $user=User::all();
          
           return view('admin.company-of-the-week.update_companyofweek')->withCompany($company)->withUser($user);

            }catch(Exception $e)
            {
              return redirect()->back()->withErrors($e->getMessage());
            }
        }


        public function UpdateCompanyOfWeek(Request $request){

        try{

            $rules = array(

            'title' => '',
            'video' => 'mimes:mpeg,ogg,mp4,webm,3gp,mov,flv,avi,wmv,ts|max:100040|required',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) 
            {
                
            return redirect()->back()->withErrors($validator)->withInput();
            }
            $company = CompanyOfWeek::findOrfail($request->id);
            $company->user_id = $request->user_id;
            $company->description = $request->description;
            if ($request->hasFile('video')) 
            {

            $video_image = $request->file('video');

            $name = $video_image->getClientOriginalName();
            $destinationFolder = "public/images/video/";
            if(file_exists($destinationFolder.$name))
            {
            unlink($destinationFolder.$name);
            }
            $video_image->move($destinationFolder,$name);  
            $video_image = "public/images/video/".$name;
            $company->video=$video_image;

        } 
        
            $company->save();

            return Redirect::to('admin/companyofweek_list')->withMessage(" Company of the Weeks Updated Successfully.");

            }catch(Exception $e)
            {
            return redirect()->back()->withErrors($e->getMessage());
           }
        }

        public function deleteCompanyOfWeek($id)
        {
            $gallery=CompanyOfWeek::findOrfail($id);
            $gallery->delete();

            $message = "Company of the Weeks Deleted Successfully!";
        return redirect()->back()->withMessage($message); 
        }

}
