<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Supplier;
use App\Models\SuplierEnquiry;
use App\Models\User;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Validator;
use Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


class SupplierController extends Controller
{
    public function list()
    {

    $user=User::where('type',3)->get();    

    $suppliers = User::query()->where('type',2)->get();



   foreach($suppliers as $key => $val){
        //die('herre');
        $suppliers[$key]->user_id_count = SuplierEnquiry::where('user_id', $val->id)->count(); 
        if($val->id == '170'){
           // print_r($suppliers);die;    
        }
        
   }
 // die();

//print_r($buyers); die();
    /*echo "</pre>";
    print_r( $suppliers);*/

      return view('admin.supplier.supplier_list')->withSuppliers($suppliers)->withUser($user);

    }

     



    public function addSupplierForm()
    {
       return view('admin.supplier.add_supplier');
    }

    public function addSupplier(Request $request)
    {
      
      try{
     
      $rules=array(
       'name' => 'required',
       'company_name' => 'required',
       'mobile' => '',
       'email'=>'required',
       'password' => 'required | min:8|max:16',
       'address'=>'required',
       'image' => 'mimes:jpeg,jpg,png,gif|max:10000',
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
        return redirect()->back()->withErrors($validator)->withInput();
        }


    $supplier = new User();
    $supplier->name = $request->name;
    $supplier->company_name = $request->company_name;
    $supplier->slug = Str::slug($supplier->company_name, '-');
    $supplier->email = $request->email;
    $supplier->mobile = $request->mobile;
    $supplier->address = $request->address;
    $supplier->password =bcrypt ($request->password);
    $supplier->supplierpassword = $request->password;
    if ($request->hasFile('image')) 
    {

    $image = $request->file('image');

    $name = $image->getClientOriginalName();
    $destinationFolder = "public/images/";
    if(file_exists($destinationFolder.$name))
    {
    unlink($destinationFolder.$name);
    }
    $image->move($destinationFolder,$name);  
    $image ="public/images/".$name;
    $supplier->image=$image;

    } 

   
     $supplier->save();
            
    return Redirect::to('admin/supplier_list')->withMessage("Supplier Created Successfully."); 
            
    }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
    }


    public function editSupplier(Request $request, $id){
        try{
           
           $supplier = User::query()->where(['id' => $id])->first();
          
           return view('admin.supplier.update_supplier')->withSupplier($supplier);
            
          }catch(Exception $e){

            return redirect()->back()
            ->withErrors($e->getMessage());
          }
        }



        public function suppliertUpdate(Request $request)
    {
      
      try{
     
      $rules=array(
       'name' => 'required',
       'company_name' => 'required',
       'mobile' => '',
       'email'=>'required',
       'address'=>'required',
       'subscription'=>'',
       'package_name'=>'',
       'date'=>'',
       'amount'=>'',
       'viewer'=>'',
       'image' => 'mimes:jpeg,jpg,png,gif|max:10000',
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
        return redirect()->back()->withErrors($validator)->withInput();
        }


    $supplier = User::findOrfail($request->id);
    $supplier->name = $request->name;
    $supplier->company_name = $request->company_name;
    $supplier->slug = Str::slug($supplier->company_name, '-');
    $supplier->email = $request->email;
    $supplier->mobile = $request->mobile;
    $supplier->address = $request->address;
    $supplier->subscription = $request->subscription;
    $supplier->package_name = $request->package_name;
    $supplier->date = $request->date;
    $supplier->amount = $request->amount;
    $supplier->viewer = $request->viewer;
    if ($request->hasFile('image')) 
    {

    $image = $request->file('image');

    $name = $image->getClientOriginalName();
    $destinationFolder = "public/images/";
    if(file_exists($destinationFolder.$name))
    {
    unlink($destinationFolder.$name);
    }
    $image->move($destinationFolder,$name);  
    $image ="public/images/".$name;
    $supplier->image=$image;

    } 

   
     $supplier->save();
            
    return Redirect::to('admin/supplier_list')->withMessage("Supplier Updated Successfully."); 
            
    }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
    }

     public function deleteSupplier($id){

    $supplier = User::findOrfail($id);
    $supplier->delete();

     $message = "Supplier Deleted Successfully!";
        return redirect()->back()->withMessage($message);
  }

  public function buyersList()
    {
     
    $buyers =User::where('type',3)->get();

    return view('admin.supplier.buyer_enquiry_list')->withBuyers($buyers);
    }   





    




  public function supplierPannel($id)
  {
    $supplier = User::query()->where(['id' => $id])->first();
     // $decrypt= decrypt($supplier->password);  
// dd($decrypt);
      $userdata = array(
                'email' => $supplier->email,
                'password' => $supplier->supplierpassword,
            );
          if (Auth::attempt($userdata,true)) {
            //echo Auth()->user()->id;
            //die('if');
                 // Auth::logout();
                if($supplier->type==1){

                    // Auth::logout();
                      dd('if==');
                    // Session::put('previoususer', 'admin');
                    // dd(Session::get('previoususer'));
                    $data = array('admin'=>'admin');
                    return Redirect::to('/admin/supplier_list');
                }else{
                   //  dd('else==');
                     Session::put('previoususer', 'admin');
                   //  dd(Session::get('previoususer'));
                    $data = array('admin'=>'admin');

                      //return Redirect::to('/sup_admin/suplier_dashboard', ['previoususer' => 1]);
                      return Redirect::to('/sup_admin/suplier_dashboard');
               // return redirect()->route('Dashboard',  ['previoususer' => 1]);
                }
                              
              
            } else {
                die('else');
                return Redirect::to('/admin/supplier_list');
            }
        // dd($supplier); 
  }




 
}
