<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Page;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Validator;
use Str;

class PageController extends Controller
{
    
    public function list(){
        //dd(1);
        $pages = Page::all();
        //return $pages;

        return view('admin.page.page_list')->withPages($pages);
    }

    public function addForm()
    {
    $pageType = Page::getPageType();

    return view('admin.page.add_page',compact('pageType'));

    }

    public function addPage(Request $request){

     try{

       $rules=array(
       'title' => 'required',
       'name' => '',
       'content' => '',
       'short_content' => '',
       'image' => 'mimes:jpeg,jpg,png,gif|max:10000',
       'slug' => '',
       'status' => '',
       'type' => '',
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
        return redirect()->back()->withErrors($validator)->withInput();
        }

    $page = new Page();
    $page->title = $request->title;
    $page->name = $request->name;
    $page->content = $request->content;
    $page->short_content = $request->short_content;
    $page->status = $request->status;
    $page->type = $request->type;
    $page->slug = Str::slug($page->title, '-');
    if ($request->hasFile('image')) 
    {

    $image = $request->file('image');

    $name = $image->getClientOriginalName();
    $destinationFolder = "public/images/banner/";
    if(file_exists($destinationFolder.$name))
    {
    unlink($destinationFolder.$name);
    }
    $image->move($destinationFolder,$name);  
    $image ="public/images/banner/".$name;
    $page->image=$image;

    } 

   
     $page->save();
            
    return Redirect::to('admin/page_list')->withMessage("Page Created Successfully."); 
            
    }catch(Exception $e){

        return redirect()->back()->withErrors($e->getMessage());

    }
    }


        public function editPage(Request $request, $id){
        try{
           
           $page = Page::query()->where(['id' => $id])->first();
          
           return view('admin.page.update_page')->withPage($page);
            
          }catch(Exception $e){

            return redirect()->back()
            ->withErrors($e->getMessage());
          }
        }


    


    public function updatePage(Request $request){
    
    try{
     
    $page = Page::findOrfail($request->id); 
    $page->title = $request->title;
    $page->name = $request->name;
    $page->content = $request->content;
    $page->short_content = $request->short_content;
    $page->status = $request->status;
    $page->type = $request->type;
    $page->slug = Str::slug($page->title, '-');
    if ($request->hasFile('image')) 
    {

    $image = $request->file('image');

    $name = $image->getClientOriginalName();
    $destinationFolder = "public/images/banner/";
    if(file_exists($destinationFolder.$name))
    {
    unlink($destinationFolder.$name);
    }
    $image->move($destinationFolder,$name);  
    $image ="public/images/banner/".$name;
    $page->image=$image;

    } 
     $page->save();
            
    return Redirect::to('admin/page_list')->withMessage("Page Updated Successfully."); 
            
    }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
    }

    public function deletePage($id)
    {
      
    $page = Page::findOrfail($id);
    $page->delete();

    $message = "Page Deleted Successfully!";
        return redirect()->back()->withMessage($message); 
    }
    
}



