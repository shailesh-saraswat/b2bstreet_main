<?php

namespace App\Http\Controllers\Admin;

use App\Models\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Validator;
use App\Models\User;

class VideoController extends Controller
{
   
    public function list()
    {
      //  dd(234);
        $videos = Video::all();
        //dd($videos);
         return view('admin.video.video_list',compact('videos'));
    }


    public function createForm()
    {
        $user =User::groupBy('company_name')->get();
       return view('admin.video.add_video',compact('user'));  
    }


    public function addvideo(Request $request)
    {
        try{
     
       $rules=array(
      // 'title' => 'required',
       'description' => '',
       'image' => 'mimes:jpeg,jpg,png,gif|required|max:10000',
       'video' => 'mimes:mpeg,ogg,mp4,webm,3gp,mov,flv,avi,wmv,ts|max:100040|',
       'is_trending' => 'required',
       'is_recommended' => 'required',
       'status' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
        return redirect()->back()->withErrors($validator)->withInput();
        }


    $video = new Video();
    $video->user_id = $request->user_id;
   // $video->title = $request->title;
    $video->description = $request->description;
    $video->is_trending = $request->is_trending;
    $video->is_recommended = $request->is_recommended;
    $video->status = $request->status;
    if ($request->hasFile('image')) 
    {

    $video_image = $request->file('image');

    $name = $video_image->getClientOriginalName();
    $destinationFolder = "public/images/video/";
   
    if(file_exists($destinationFolder.$name))
    {
    unlink($destinationFolder.$name);
    }
    $video_image->move($destinationFolder,$name);  
    $video_image ="public/images/video/".$name;
    $video->image=$video_image;

    } 

    if ($request->hasFile('video')) 
    {

    $video_image = $request->file('video');

    $name = $video_image->getClientOriginalName();
    $destinationFolder = "public/images/video/";
    if(file_exists($destinationFolder.$name))
    {
    unlink($destinationFolder.$name);
    }
    $video_image->move($destinationFolder,$name);  
    $video_image = "public/images/video/".$name;
    $video->video=$video_image;

    } 

     $video->save();
            
    return Redirect::to('admin/video_list')->withMessage("Video Created Successfully."); 
            
    }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
    }
    

    public function editVideo(Request $request, $id)
    {
        
        try{
           $user = User::all();
         $video = Video::query()->where(['id' => $id])->with('user')->first();
        
           return view('admin.video.update_video',compact('user'))->withVideo($video);
            
          }catch(Exception $e){

            return redirect()->back()
            ->withErrors($e->getMessage());
          }
        }


   
    public function updateVideo(Request $request){

    {
      try{
     
    $video = Video::findOrfail($request->id); 
    //$video->title = $request->title;
    $video->description = $request->description;
    $video->is_trending = $request->is_trending;
    $video->is_recommended = $request->is_recommended;
    $video->status = $request->status;
    if ($request->hasFile('image')) 
    {

    $video_image = $request->file('image');

    $name = $video_image->getClientOriginalName();
    $destinationFolder = "public/images/video/";
    if(file_exists($destinationFolder.$name))
    {
    unlink($destinationFolder.$name);
    }
    $video_image->move($destinationFolder,$name);  
    $video_image = "public/images/video/".$name;
    $video->image=$video_image;

    }

    if ($request->hasFile('video')) 
    {

    $video_image = $request->file('video');

    $name = $video_image->getClientOriginalName();
    $destinationFolder = "public/images/video/";
    if(file_exists($destinationFolder.$name))
    {
    unlink($destinationFolder.$name);
    }
    $video_image->move($destinationFolder,$name);  
    $video_image = "public/images/video/".$name;
    $video->video=$video_image;

    } 

     $video->save(); 
            
    return Redirect::to('admin/video_list')->withMessage("Video Updated Successfully."); 
            
    }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
    }
    }

    public function deleteVideo($id){

     $video = Video::findOrfail($id);
     $video->delete();

     $message = "Video Deleted Successfully!";
        return redirect()->back()->withMessage($message);

    }
}
