<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\PressRelease;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Validator;
use Exception;

class PressReleaseController extends Controller
{
    public function list(){
        $press=PressRelease::all();
        return view('admin.press-release.press_release_list')->withPress($press);
    }

    public function pressReleaseForm(){

     return view('admin.press-release.add_press_release');
    }


    public function addPressRelease(Request $request){
        try{

            $rules=array(
            'title'=>'required',
            'date'=>'required',
            'short_content'=>'required',
            'content'=>'required',
            'image'=>'mimes:jpeg,jpg,png,gif|max:10000',
            'status'=>'required',
            );

            $validator=Validator::make($request->all(),$rules);
            if ($validator->fails()) {
               return redirect()->back()->withErrors($validator)->withInput();
            }
    
        $press=new PressRelease();
        $press->title=$request->title;
        $press->date=$request->date;
        $press->short_content=$request->short_content;
        $press->content=$request->content;
        $press->status=$request->status;
        if ($request->hasFile('image')) 
            {

            $gallery_image =$request->file('image');
            $name =$gallery_image->getClientOriginalName();
            $destination = "public/images/gallery/";
            if (file_exists($destination.$name)) 
            {
                unlink($destination.$name);
            }
            $gallery_image->move($destination,$name); 
            $gallery_image = "public/images/gallery/".$name;
            $press->image=$gallery_image;
            }

        $press->save();
            
        return Redirect::to('admin/press_release_list')->withMessage("Press Release Created Successfully."); 
            
        }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
    }

    public function editPressRelease(Request $request,$id)
    {
    $press=PressRelease::query()->where(['id'=> $id])->first();
    return view('admin.press-release.update_press_release')->withPress($press);
    }


    public function UpdatePressRelease(Request $request){
    try{

            $rules=array(
            'title'=>'required',
            'date'=>'required',
            'short_content'=>'required',
            'content'=>'required',
            'image'=>'mimes:jpeg,jpg,png,gif|max:10000',
            'status'=>'required',
            );

            $validator=Validator::make($request->all(),$rules);
            if ($validator->fails()) {
               return redirect()->back()->withErrors($validator)->withInput();
            }
    
        $press=PressRelease::findOrfail($request->id);
        $press->title=$request->title;
        $press->date=$request->date;
        $press->short_content=$request->short_content;
        $press->content=$request->content;
        $press->status=$request->status;
        if ($request->hasFile('image')) 
            {

            $gallery_image =$request->file('image');
            $name =$gallery_image->getClientOriginalName();
            $destination = "public/images/gallery/";
            if (file_exists($destination.$name)) 
            {
                unlink($destination.$name);
            }
            $gallery_image->move($destination,$name); 
            $gallery_image = "public/images/gallery/".$name;
            $press->image=$gallery_image;
            }

        $press->save();
            
        return Redirect::to('admin/press_release_list')->withMessage("Press Release Updated Successfully."); 
            
        }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
    }

    public function deletePressRelease($id){

        $press=PressRelease::findOrfail($id);
        $press->delete();
         $message = "Press Release Deleted Successfully!";
        return redirect()->back()->withMessage($message); 

    }
}
