<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Models\SubCategory;
use App\Models\Maincategory;
use App\Models\Category;
use App\Models\Product;
use App\Models\User;
use Str;
use Validator;
use App\Models\ProductImage;

class ProductController extends Controller
{
    public function list(){

        $products = Product::with('main_category')->get();

        $products = Product::with('category')->get();
        $products = Product::with('subcategory')->get();
        $products = Product::all();

        $product_image=ProductImage::all();
       // return($product_image);

        return view('admin.products.products_lists')->withProducts($products)->withProduct_image($product_image);
    }

    public function addProductForm(){

        $maincategories = Maincategory::all();

        $categories = Category::all();

        $subcategories = SubCategory::all();

        $users = User::where('type',2)->get();

        return view('admin.products.add_products',compact('maincategories','categories','subcategories','users'));

    }


    public function addProduct(Request $request)
    {
    try{
    
    $rules=array(
    'user_id' => '',
    'maincategory_id' => 'required',
    'categories_id' => 'required',
    'subcategories_id' => 'required',    
    'title' => 'required',
    'product_image' => '',
    'price' =>'',
    'description' =>'',
    'quality' =>'',
    'packaging_type' =>'',
    'storage_tips' =>'',
    'packaging' =>'',
    'product_image' =>'',
    'status'=>'',
    );

    $validator = Validator::make($request->all(), $rules);
    if ($validator->fails())
    {
    return redirect()->back()->withErrors($validator)->withInput();
    }

    $product = new Product();
    $product->user_id = $request->user_id;
    $product->maincategory_id = $request->maincategory_id;
    $product->categories_id = $request->categories_id;
    $product->subcategories_id = $request->subcategories_id;
    $product->title = $request->title;
    $product->slug = Str::slug($product->title, '-');
    $product->price = $request->price;
    $product->description = $request->description;
    $product->quality = $request->quality;
    $product->packaging_type = $request->packaging_type;
    $product->storage_tips = $request->storage_tips;
    $product->packaging = $request->packaging;
    $product->status = $request->status;
    $product->save();
    if ($request->hasFile('product_image')) 
    {

     $this->productgallery($product->id,$request->all());
     return Redirect::to('admin/products_lists')->withMessage("Product Created Successfully.");
    } 
      
    }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
    }


    public function productgallery($id,$input) {
  //   dd($input['product_image']);
   
   if(null!=$input['product_image']){

     foreach($input['product_image'] as $thumbnail){
        $gallery=new ProductImage;
        $name = $thumbnail->getClientOriginalName();
        $destinationFolder = public_path('/images');
        if(file_exists($destinationFolder.$name))
           {
           unlink($destinationFolder.$name);
           }
        $thumbnail->move($destinationFolder, $name);
        $data = "/public/images/" . $name;
        $gallery->product_image = $data;
        $gallery->products_id=$id;
        $gallery->save();
         
    }
}
}
    public function editProduct(Request $request, $id){

        try{
           
           $product = Product::query()->where(['id' => $id])->first();
           //$product = Product::with('product_images')->get();
           $categories=Category::where('id',$product->categories_id)->get();
           $subcategories=SubCategory::where('id',$product->subcategories_id)->get();
           $users = User::all();
        
           return view('admin.products.update_products')->withProduct($product)->with('categories',$categories)->with('subcategories',$subcategories)->withUsers($users);
            
          }catch(Exception $e){

            return redirect()->back()
            ->withErrors($e->getMessage());
          }

    }


    public function productUpdate(Request $request)
    {

    try{
     
    $rules=array(
    'user_id' => '',
    'maincategory_id' => 'required',
    'categories_id' => 'required',
    'subcategories_id' => 'required',    
    'title' => 'required',
    'product_image' => '',
    'price' =>'',
    'description' =>'',
    'quality' =>'',
    'packaging_type' =>'',
    'storage_tips' =>'',
    'packaging' =>'',
    'product_image' =>'',
    'status'=>'',
    );

    $validator = Validator::make($request->all(), $rules);
    if ($validator->fails())
    {
    return redirect()->back()->withErrors($validator)->withInput();
    }

    $product = Product::findOrfail($request->id);
    $product->user_id = $request->user_id;
    $product->maincategory_id = $request->maincategory_id;
    $product->categories_id = $request->categories_id;
    $product->subcategories_id = $request->subcategories_id;
    $product->title = $request->title;
    $product->slug = Str::slug($product->title, '-');
    $product->price = $request->price;
    $product->description = $request->description;
    $product->quality = $request->quality;
    $product->packaging_type = $request->packaging_type;
    $product->storage_tips = $request->storage_tips;
    $product->packaging = $request->packaging;
    $product->status = $request->status;
    $product->save();
    if ($request->hasFile('product_image')) 
    {
 
      $this->upproductgallery($product->id,$request->all());
        return Redirect::to('admin/products_lists')->withMessage("Product Updated Successfully.");  
    }
    //
   // return($product);
   /* $this->productgallery($product->id,$request->all()); */       
   
    
      
    }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
    }


     public function upproductgallery($id,$input) {
  //   dd($input['product_image']);
   
   if(null!=$input['product_image']){

     foreach($input['product_image'] as $thumbnail){
        $gallery=new ProductImage;
        $name = $thumbnail->getClientOriginalName();
        $destinationFolder = public_path('/images');
        if(file_exists($destinationFolder.$name))
           {
           unlink($destinationFolder.$name);
           }
        $thumbnail->move($destinationFolder, $name);
        $data = "/public/images/" . $name;
        $gallery->product_image = $data;
        $gallery->products_id=$id;
        $gallery->save();
         
    }
}
}

  
   

    public function deleteProduct($id){

     $product = Product::findOrfail($id);
     $product->delete();

     $message = "Product Deleted Successfully!";
        return redirect()->back()->withMessage($message);

    }


    public function deleteProductImages($id){

     $product = ProductImage::findOrfail($id);
     $product->delete();

     $message = "Product Image Deleted Successfully!";
        return redirect()->back()->withMessage($message);

    }




    public function getCategoryByName(){
      /*  print_r($_GET);*/

        $maincategory_id=$_GET['maincategory_id'];
        
        $html="<option value='' >Select Category</option>";
        if($maincategory_id !=""){   

        $sessions=Category::where('maincategory_id',$maincategory_id)->get();
        if(count($sessions) > 0){
                foreach ($sessions as  $value) {
                   $html.="<option value='".$value->id."'>".$value->name."</option>";
                }
            }else{
                $html.="<option value='' disabled='disabled' >Menu has no Category</option>";
            }

        }else{
           $html.="<option value='' disabled='disabled' >Menu has no Category</option>"; 
        }
        return response()->json(array('html'=>$html));
 
}


 public function getSubCategoryByName(){
      /*  print_r($_GET);*/

        $categories_id=$_GET['categories_id'];
        
        $html="<option value='' >Select Category</option>";
        if($categories_id !=""){   

        $sessions=SubCategory::where('categories_id',$categories_id)->get();
        if(count($sessions) > 0){
                foreach ($sessions as  $value) {
                   $html.="<option value='".$value->id."'>".$value->name."</option>";
                }
            }else{
                $html.="<option value='' disabled='disabled' >Menu has no Category</option>";
            }

        }else{
           $html.="<option value='' disabled='disabled' >Menu has no Category</option>"; 
        }
        return response()->json(array('html'=>$html));
 
}
            
}
