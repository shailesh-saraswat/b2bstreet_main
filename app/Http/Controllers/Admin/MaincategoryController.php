<?php

namespace App\Http\Controllers\Admin;

use App\Models\Maincategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use App\Http\Requests\AddMaincategoryRequest;
use App\Http\Requests\UpdateMaincategoryRequest;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;


class MaincategoryController extends Controller
{
    
    public function list(){

       $maincategories = Maincategory::all();
        
        return view('admin.maincategory.maincategory_list')->withMaincategories($maincategories);
    }


    public function addmaincategoryForm(){

         
        return view('admin.maincategory.add_maincategory');
    }

    public function addMaincategary(AddMaincategoryRequest $request){

        if($request->isMethod('post')){

            try{
                
                $maincategary = new Maincategory;
                $maincategary->name = $request->name;
                $maincategary->status = $request->status;
                if ($request->hasFile('image')) 
                {
                $category_image = $request->file('image');
                $name1 = $category_image->getClientOriginalName();
                $destinationFolder1 = "public/images/category/";
                if(file_exists($destinationFolder1.$name1))
                {
                unlink($destinationFolder1.$name1);
                }
                $category_image->move($destinationFolder1,$name1);  
                $category_image ="public/images/category/".$name1;
                $maincategary->image=$category_image;

                } 

                $maincategary->save();
            
                return Redirect::to('admin/maincategory_list')->withMessage("Main Category Created Successfully."); 
            
            }catch(Exception $e){

                return redirect()->back()
                    ->withErrors($e->getMessage());
            }
        }
    }
  
 
public function getMaincategory(Request $request, $id)
    {
        try {
            
           $maincategory = Maincategory::query()->where(['id' => $id])->first();

            return view('admin.maincategory.update_maincategory',compact('maincategory'));
        
        }catch(Exception $e){

            return redirect()->back()
                    ->withErrors($e->getMessage());

        }
    }


  public function updateMaincategory(UpdateMaincategoryRequest $request){  
        
            try{ 
                
                $maincategary = Maincategory::findOrfail($request->id);
                $maincategary->name = $request->name;
                $maincategary->status = $request->status;
                if ($request->hasFile('image')) 
                {
                $category_image = $request->file('image');
                $name1 = $category_image->getClientOriginalName();
                $destinationFolder1 = "public/images/category/";
                if(file_exists($destinationFolder1.$name1))
                {
                unlink($destinationFolder1.$name1);
                }
                $category_image->move($destinationFolder1,$name1);  
                $category_image ="public/images/category/".$name1;
                $maincategary->image=$category_image;
                } 
               // return $maincategary;
             $maincategary->update();
           
             return Redirect::to('admin/maincategory_list')->withMessage("Main Category Updated Successfully."); 
            
             }catch(Exception $e){

                return redirect()->back()
                    ->withErrors($e->getMessage());
            }
                
    }



  public function deleteMaincategory($id){

    $maincategary = Maincategory::findOrfail($id);
    $maincategary->delete();

    $message = "Main Category Deleted Successfully!";
        return redirect()->back()->withMessage($message);
  }




}
