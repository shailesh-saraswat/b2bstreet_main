<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Exception;

class AdminController extends Controller
{
//boot
    public function profile(){

        $registeredUsers = User::query()->where(['verified' => 1, 'status' => 1])->get();
       

        $countRegisteredUsers = isset($registeredUsers) && $registeredUsers != null ? count($registeredUsers) : 0;
      

        return view('admin/profile')
                ->withCountRegisteredUsers($countRegisteredUsers);

    }

    public function changePasswordView(){

        return view('admin/change_password');

    }

       public function changePassword(Request $request)
    {

        try {

            $rules = array(
                'password' => 'required',
                'confirm_password' => 'required|same:password'
            );

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);

            } else {

                $user = auth()->user();

                if ($user) {

                    $user->fill([
                        'password' => Hash::make($request->password)
                    ])->save();

                    $message = "Password updated successfully";
                    return redirect()->back()->withMessage($message);
                }
               
            }

        } catch (Exception $e) {

            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function getProfile(){

        $user = auth()->user();

        return view('admin.update_profile')->withUser($user);

    }

    public function updateProfile(Request $request){

        $rules = array(
            'name' => 'required',
            'profile_image' => 'sometimes|mimes:jpg,jpeg,png,gif'
        );

        $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
}
        else{

            $user = auth()->user();

            $user->name = $request->input('name');

            if ($request->hasFile('profile_image')) {

                try {

                    $profile_image = $request->file('profile_image');

                    $name = $profile_image->getClientOriginalName();

                    $destinationFolder = public_path('/profile_pictures/admin-' . $user->id . '/');

                    // Delete folder
                    if(file_exists($destinationFolder.$name)){

                        unlink($destinationFolder.$name);
                    }

                    $profile_image->move($destinationFolder, $name);
                    $user->profile_image = env('APP_URL'). "/public/profile_pictures/admin-".$user->id.'/'.$name;

                } catch (\Exception $e) {
                    
                    return redirect()->back()->withErrors($e->getMessage());
                }
            }

            $user->update();

            return Redirect::to('admin/profile');
        }


    }


   

}
