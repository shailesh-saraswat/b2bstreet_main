<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Ourteam;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Validator;
use Str;

class OurteamController extends Controller
{
    
    public function list(){

        $ourteams = Ourteam::all();

        return view('admin.ourteam.ourteam_list')->withOurteams($ourteams);
    }

    public function addForm(){
    
       return view('admin.ourteam.add_ourteam');

    }

    public function addOurteam(Request $request){

     try{

       $rules=array(
       'name' => 'required',
       'designation' => 'required',
       'designation' => 'required',
       'image' => 'mimes:jpeg,jpg,png,gif|max:10000',
       'status' => '',
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
        return redirect()->back()->withErrors($validator)->withInput();
        }

    $ourteam = new Ourteam();
    $ourteam->name = $request->name;
    $ourteam->designation = $request->designation;
    $ourteam->description = $request->description;
    $ourteam->status = $request->status;
    if ($request->hasFile('image')) 
    {

    $image = $request->file('image');

    $name = $image->getClientOriginalName();
    $destinationFolder = "public/images/team/";
    if(file_exists($destinationFolder.$name))
    {
    unlink($destinationFolder.$name);
    }
    $image->move($destinationFolder,$name);  
    $image ="public/images/team/".$name;
    $ourteam->image=$image;

    } 

   
     $ourteam->save();
            
    return Redirect::to('admin/ourteam_list')->withMessage("Our Team Created Successfully."); 
            
    }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
    }


        public function editOurteam(Request $request, $id){
        try{
           
           $ourteam = Ourteam::query()->where(['id' => $id])->first();
          
           return view('admin.ourteam.update_ourteam')->withourteam($ourteam);
            
          }catch(Exception $e){

            return redirect()->back()
            ->withErrors($e->getMessage());
          }
        }
  


    public function updateOurteam(Request $request){
    
    try{
     
    $ourteam = Ourteam::findOrfail($request->id); 
    $ourteam->name = $request->name;
    $ourteam->designation = $request->designation;
    $ourteam->description = $request->description;
    $ourteam->status = $request->status;
    if ($request->hasFile('image')) 
    {

    $image = $request->file('image');

    $name = $image->getClientOriginalName();
    $destinationFolder = "public/images/team/";
    if(file_exists($destinationFolder.$name))
    {
    unlink($destinationFolder.$name);
    }
    $image->move($destinationFolder,$name);  
    $image ="public/images/team/".$name;
    $ourteam->image=$image;

    } 
     $ourteam->save();
            
    return Redirect::to('admin/ourteam_list')->withMessage("Our Team Updated Successfully."); 
            
    }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
    }

    public function deleteOurteam($id)
    {
      
    $ourteam = Ourteam::findOrfail($id);
    $ourteam->delete();

    $message = "Our Team Deleted Successfully!";
        return redirect()->back()->withMessage($message); 
    }
    
}



