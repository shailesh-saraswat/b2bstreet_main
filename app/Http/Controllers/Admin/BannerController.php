<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Banner;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Validator;

class BannerController extends Controller

{

    public function list()
    {

    $banners = Banner::all();

     return view('admin.banner.banner_list')->withBanners($banners);

   
    }


    public function addBannerForm()
    {
       return view('admin.banner.add_banner');
    }


    public function addBanner(Request $request)
    {
      
      try{
     
      $rules=array(
       'title' => 'required',
       'description' => 'required',
       'banner_image' => 'mimes:jpeg,jpg,png,gif|required|max:10000',
       'status' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
        return redirect()->back()->withErrors($validator)->withInput();
        }


    $banner = new Banner();
    $banner->title = $request->title;
    $banner->description = $request->description;
    $banner->status = $request->status;
    if ($request->hasFile('banner_image')) 
    {

    $image = $request->file('banner_image');

    $name = $image->getClientOriginalName();
    $destinationFolder = "public/images/banner/";
    if(file_exists($destinationFolder.$name))
    {
    unlink($destinationFolder.$name);
    }
    $image->move($destinationFolder,$name);  
    $image ="public/images/banner/".$name;
    $banner->banner_image=$image;

    } 

   
     $banner->save();
            
    return Redirect::to('admin/banner_list')->withMessage("Banner Created Successfully."); 
            
    }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
    }

   public function editbanner(Request $request, $id){
        try{
           
           $banner = Banner::query()->where(['id' => $id])->first();
          
           return view('admin.banner.update_banner')->withBanner($banner);
            
          }catch(Exception $e){

            return redirect()->back()
            ->withErrors($e->getMessage());
          }
        }


    
    public function updateBanner(Request $request)
    {
    try{
     
    $banner = Banner::findOrfail($request->id); 
    $banner->title = $request->title;
    $banner->description = $request->description;
    $banner->status = $request->status;
    if ($request->hasFile('banner_image')) 
    {

    $image = $request->file('banner_image');

    $name = $image->getClientOriginalName();
    $destinationFolder = "public/images/banner/";
    if(file_exists($destinationFolder.$name))
    {
    unlink($destinationFolder.$name);
    }
    $image->move($destinationFolder,$name);  
    $image ="public/images/banner/".$name;
    $banner->banner_image=$image;

    } 

     $banner->save();
            
    return Redirect::to('admin/banner_list')->withMessage("Banner Updated Successfully."); 
            
    }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
    }


    public function deleteBanner($id)
    {
      
    $banner = Banner::findOrfail($id);
    $banner->delete();

    $message = "Banner Deleted Successfully!";
        return redirect()->back()->withMessage($message); 
    }
}
