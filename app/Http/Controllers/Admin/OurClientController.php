<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\OurClient;
use App\Http\Controllers\Controller;
use Exception;
use Str;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;

class OurClientController extends Controller
{
    
    public function list()
    {
     
     $clients = OurClient::all();

     return view('admin.our-client.our_client_list')->withClients($clients);

    }

   

     public function addForm()
    {
       return view('admin.our-client.add_our_client');
    }

    
    public function addOurClient(Request $request){

        try{

            $rules=array(
    
            'image' => 'mimes:jpeg,jpg,png,gif|max:10000',
            'status'=> '',
            );

           $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
        return redirect()->back()->withErrors($validator)->withInput();
        }

            $clients = new OurClient();
            $clients->status =$request->status;
            if ($request->hasFile('image')) 
           {

            $image = $request->file('image');

            $name = $image->getClientOriginalName();
            $destination = "public/images/clients/";
            if(file_exists($destination.$name))
            {
            unlink($destination.$name);
            }
            $image->move($destination,$name);  
            $image ="public/images/clients/".$name;
            $clients->image=$image;
        }

          $clients->save();

        return Redirect:: to('admin/our_client_list')->withMessage("Our Clients Created Successfully.");

        }catch (Exception $e){
        
        return redirect()->back()->withErrors($e->getMessage());
    }
}


   
    public function editOurClient(Request $request, $id){

        try{

            $clients = OurClient::query()->where(['id' => $id])->first();

            return view('admin.our-client.update_our_client')->withClients($clients);

            }catch(Exception $e){

            return redirect()->back()
            ->withErrors($e->getMessage());
        }
    }


    public function updateOurClient(Request $request){
    
    try{
     
    $clients = OurClient::findOrfail($request->id); 
    $clients->status = $request->status;
    if ($request->hasFile('image')) 
    {

    $image = $request->file('image');

    $name = $image->getClientOriginalName();
    $destination = "public/images/banner/";
    if(file_exists($destination.$name))
    {
    unlink($destination.$name);
    }
    $image->move($destination,$name);  
    $image ="public/images/banner/".$name;
    $clients->image=$image;

    } 
     $clients->save();
            
    return Redirect::to('admin/our_client_list')->withMessage("Our Client Updated Successfully."); 
            
    }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
    }


     public function deleteOurClient($id)
    {
      
    $clients = OurClient::findOrfail($id);
    $clients->delete();

    $message = "Our Client Deleted Successfully!";
        return redirect()->back()->withMessage($message); 
    }
}