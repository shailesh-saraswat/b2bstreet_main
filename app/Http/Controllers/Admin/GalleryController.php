<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Models\Gallery;
use Validator;
use Exception;

class GalleryController extends Controller
{
    public function list()
    {
      
      $galleries = Gallery::all();

      return view('admin.gallery.gallery_list')->withGalleries( $galleries);

    }

    public function addFormGallery(){

     return view('admin.gallery.add_gallery');
    }

    public function addGallery(Request $request){

        try{

            $rules = array(

            'title' => '',
            'content' =>'',
            'image' =>'mimes:jpeg,jpg,png,gif|max:10000',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) 
            {
                
            return redirect()->back()->withErrors($validator)->withInput();
            }

            $gallery = new Gallery();
            $gallery->title = $request->title;
            $gallery->content = $request->content;
            if ($request->hasFile('image')) 
            {

            $gallery_image =$request->file('image');
            $name =$gallery_image->getClientOriginalName();
            $destination = "public/images/gallery/";
            if (file_exists($destination.$name)) 
            {
                unlink($destination.$name);
            }
            $gallery_image->move($destination,$name); 
            $gallery_image = "public/images/gallery/".$name;
            $gallery->image=$gallery_image;
            }
            
            $gallery->save();

            return Redirect::to('admin/gallery_list')->withMessage("Gallery Created Successfully.");

            }catch(Exception $e)
            {
            return redirect()->back()->withErrors($e->getMessage());
           }
        }



        public function editGallery(Request $request , $id){

            try{

            
            $gallery = Gallery::query()->where(['id' => $id])->first();
          
           return view('admin.gallery.update_gallery')->withGallery($gallery);

            }catch(Exception $e)
            {
              return redirect()->back()->withErrors($e->getMessage());
            }
        }

        public function updateGallery(Request $request){

            try{

                $gallery = Gallery::findOrfail($request->id);
                $gallery->title = $request->title;
                $gallery->content = $request->content;
                if ($request->hasFile('image')) 
            {

            $gallery_image =$request->file('image');
            $name =$gallery_image->getClientOriginalName();
            $destination = "public/images/gallery/";
            if (file_exists($destination.$name)) 
            {
                unlink($destination.$name);
            }
            $gallery_image->move($destination,$name); 
            $gallery_image = "public/images/gallery/".$name;
            $gallery->image=$gallery_image;
                }

                $gallery->save();

                return Redirect::to('admin/gallery_list')->withMessage("Gallery Updated Successfully.");
            }catch(Exception $e){

                return redirect()->back()->withErrors($e->getMessage());
            }
        }



        public function deleteGallery($id)
        {
            $gallery=Gallery::findOrfail($id);
            $gallery->delete();

            $message = "Gallery Deleted Successfully!";
        return redirect()->back()->withMessage($message); 
        }

    }


