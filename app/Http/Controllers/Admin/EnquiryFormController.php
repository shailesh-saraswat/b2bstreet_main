<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\EnquiryForm;
use App\Models\ContactForm;
use App\Models\ServiceForm;
use App\Models\BuyerRequirement;
use App\Models\Form;
use App\Models\Complaint;
use App\Models\CareerForm;
use App\Models\ProductDetailContact;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;

class EnquiryFormController extends Controller
{
    
    public function list()
    {

        $enquiryforms = EnquiryForm::all();
        
        return view('admin.enquiry-form.enquiry_form_list')->withEnquiryforms($enquiryforms);
    }


    public function delete($id)
    {
      
    $enquiryforms = EnquiryForm::findOrfail($id);
    $enquiryforms->delete();

    $message = "Enquiry Form Deleted Successfully!";
        return redirect()->back()->withMessage($message); 
    }


    /*Contact Form Code*/

     public function contactList()
     {

        $contacts = ContactForm::all();

        return view('admin.contact.contact_form_list')->withContacts($contacts);
     }
    
     
     public function ContactDelete($id){

        $contacts =ContactForm::findOrfail($id);

        $contacts->delete();
        $message ="Contact Form Deleted Successfully!";
        return redirect()->back()->withMessage($message);
     }



     /*Quick Form Code*/

     public function QuickList()
     {

        $quickforms = Form::all();

        return view('admin.enquiry-form.quickinquiry_form_list')->withQuickforms($quickforms);
     }
         
     public function QuickDelete($id){

        $quickforms =Form::findOrfail($id);

        $quickforms->delete();
        $message ="Quick Form Deleted Successfully!";
        return redirect()->back()->withMessage($message);
     }


     public function notify()
     {

        $notifys = Form::query()->where('notify',0)->get();

        return view('admin.enquiry-form.notify')->withNotifys($notifys);
     }


     public function editQuick(Request $request, $id){
        try{
           
           $notify = Form::query()->where(['id' => $id])->first();

         $notify->notify=1;
         $notify->save();
           return view('admin.enquiry-form.view_notify')->withNotify($notify);
            
          }catch(Exception $e){

            return redirect()->back()
            ->withErrors($e->getMessage());
          }
        }

/*  BuyerRequirement*/

     public function buyerRequirementlist()
     {
     
     $buyers = BuyerRequirement::all();

     return view('admin.buyer-requirements.buyer_requirement_lists')
     ->withBuyers($buyers);

    }


      public function deleteBuyerRequirement($id){

        $buyers =BuyerRequirement::findOrfail($id);

        $buyers->delete();
        $message ="Buyer Requirement Deleted Successfully!";
        return redirect()->back()->withMessage($message);
     }


     /*  Supplier Contact From*/

     public function supplierContactlist()
     {
     
      $suppliers = ProductDetailContact::with('user')->get();

     return view('admin.supplier-contact.supplier_contact_lists')
     ->withSuppliers($suppliers);

    }
    public function deletesupplierContact($id){

        $suppliers =ProductDetailContact::findOrfail($id);

        $suppliers->delete();
        $message ="From Deleted Successfully!";
        return redirect()->back()->withMessage($message);
     }

    /*  Complaint Form*/
     
     public function complaintList()
     {
     $complaint=Complaint::all();
     return view('admin.enquiry-form.complaint_list')->withComplaint($complaint);
     }
     
     public function complaintDelete($id)
     {
     $complaint =Complaint::findOrfail($id);
     $complaint->delete();
     $message="Complaint Form Deleted Successfully!";
     return redirect()->back()->withMessage($message);
} 

/*  Career Form*/
     
     public function careerList()
     {
     $career=CareerForm::all();
     return view('admin.enquiry-form.carrer_form_list')->withCareer($career);
     }
     
     public function carrerDelete($id)
     {
     $career =CareerForm::findOrfail($id);
     $career->delete();
     $message="Career Form Deleted Successfully!";
     return redirect()->back()->withMessage($message);
}     
}




