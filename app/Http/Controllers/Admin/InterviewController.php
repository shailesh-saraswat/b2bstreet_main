<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Interview;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Validator;

class InterviewController extends Controller
{
   
    public function list()
    {
      $interviews = Interview::all();


       return view('admin.interview.interview_list')->with('interviews',$interviews);
    }

    public function addForm()
    {
        return view('admin.interview.add_interview');
    }

    public function addInterview(Request $request)
    {
        
      try{
     
      $rules=array(
       'title' => 'required',
       'description' => '',
       'video' => 'mimes:mpeg,ogg,mp4,webm,3gp,mov,flv,avi,wmv,ts|max:100040|required',
       'status' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
        return redirect()->back()->withErrors($validator)->withInput();
        }

        $interview = new Interview();
        $interview->title =$request->title;
        $interview->description = $request->description;
        $interview->status = $request->status;
        if ($request->hasFile('video')) 
        {

        $interview_video = $request->file('video');

        $name = $interview_video->getClientOriginalName();
        $destinationFolder = "public/images/video/";
        if(file_exists($destinationFolder.$name))
        {
        unlink($destinationFolder.$name);
        }
        $interview_video->move($destinationFolder,$name);  
        $interview_video = "public/images/video/".$name;
        $interview->video=$interview_video;
        }

        $interview->save();
            
        return Redirect::to('admin/interview_list')->withMessage("Interview Created Successfully."); 
            
        }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
    }
 
   public function editInterview(Request $request, $id){

    try{

        $interview = Interview::query()->where(['id' => $id])->first();
         return view('admin.interview.update_interview')->withInterview($interview);
        }catch(Exception $e){

        return redirect()->back()
        ->withErrors($e->getMessage());
          }
        }


    public function updateInterview(Request $request){

    try{
     
    $interview = Interview::findOrfail($request->id); 
    $interview->title = $request->title;
    $interview->description = $request->description;
    $interview->status = $request->status;
    if ($request->hasFile('video')) 
        {

        $interview_video = $request->file('video');

        $name = $interview_video->getClientOriginalName();
        $destinationFolder = "public/images/video/";
        if(file_exists($destinationFolder.$name))
        {
        unlink($destinationFolder.$name);
        }
        $interview_video->move($destinationFolder,$name);  
        $interview_video = "public/images/video/".$name;
        $interview->video=$interview_video;
        }

      $interview->save();
            
    return Redirect::to('admin/interview_list')->withMessage("Interview Updated Successfully."); 
            
    }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }

    }  

    public function delete($id)
    {
    $interview = Interview::findOrfail($id);
    $interview->delete();

    $message = "Interview Deleted Successfully!";
        return redirect()->back()->withMessage($message); 
    }
    }
