<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\EnquiryForm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Models\BuyerRequirement;
use App\Models\ProductDetailContact;
use App\Models\Form;

class LoginController extends Controller
{


    public function index(){

         try{
         
        $buyers = User::all()->where('type',3);
        $suppliers = User::all()->where('type', 2);
        $enquiryforms = EnquiryForm::all();
        $leads = BuyerRequirement::all();
        $sellerform = ProductDetailContact::all();
        $quickforms = Form::all();
        $notify = Form::query()->where('notify',0)->get();

        $countSuppliers = isset($suppliers) ? count($suppliers) : 0;
        $countBuyers = isset($buyers) ? count($buyers) : 0;
        $countEnquiryForm = isset($enquiryforms) ? count($enquiryforms) : 0;
        $countLeads = isset($leads) ? count($leads) : 0;
        $countSellerform = isset($sellerform) ? count($sellerform) : 0;
        $countQuickforms = isset($quickforms) ? count($quickforms) : 0;
        $notifyforms = isset($notify) ? count($notify) : 0;
       
        return view('admin.admin_dashboard')
                ->withcountBuyers($countBuyers)
                ->withCountEnquiryForm($countEnquiryForm)
                ->withCountSuppliers($countSuppliers)
                ->withCountLeads($countLeads)
                ->withCountSellerform($countSellerform)
                ->withCountQuickforms($countQuickforms)
                ->with('notifyforms',$notifyforms);


        }catch(Exception $e){

            return redirect()->back()
                ->withErrors($e->getMessage());
        }
}

//        public function list(){

//          try{
         
//         $enquiryforms = EnquiryForm::all();
//         $countEnquiryforms = isset($enquiryforms) ? count($enquiryforms) : 0;
       
//         return view('admin.admin_dashboard')
//                 ->withCountEnquiryforms($countEnquiryforms);

//         }catch(Exception $e){

//             return redirect()->back()
//                 ->withErrors($e->getMessage());
//         }
// }



    public function loginPage(){
        if(Auth::user()){
          return Redirect::to('admin/admin_dashboard');  
        }

        return view('admin.auth.login');

    }



    public function login(Request $request){
        try{
        // return "test";

        $rules = array(
            'email' => 'required|email',
            'password' => 'required|'
        );

        /*return $rules; */
        $remember_me = $request->has('remember') ? true : false;

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('/')
                ->withErrors("Email and Password is required to login.")
                ->withInput(Input::except('password'));

        } else {

            $userdata = array(
                'email' => $request->email,
                'password' => $request->password,
            );
         
            if (Auth::attempt($userdata, $remember_me)) {
                
                if(auth()->user()->type > 2 ){

                    Auth::logout();
                    return Redirect::to('/admin')
                        ->withErrors("Forbidden! You do not have permission to access this route.");
                }
                if(auth()->user()->type ==1){
                    return Redirect::to('admin/admin_dashboard');
                }
               if(auth()->user()->type ==2){
                    return Redirect::to('sup_admin/suplier_dashboard');
                }
            } else {

                return Redirect::to('/admin')
                    ->withErrors("The credentials do not match with our records.");

            }
        
         }
     }catch(Exception $e){

            return redirect()->back()
                ->withErrors($e->getMessage());
        }
    }

    public function logout(Request $request) {

        Auth::logout();

        return Redirect::to('/admin')->withMessage('Logged out successfully.');
    }

}





