<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use Str;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\AddSubcategoryRequest;
use App\Http\Requests\UpdateSubcategoryRequest;
use Illuminate\Support\Facades\Mail;
use App\Models\SubCategory;
use App\Models\Maincategory;
use App\Models\Category;



class SubcategoryController extends Controller
{
  
    public function list(){

        $subcategories = SubCategory::with('main_category')->get();

        $subcategories = SubCategory::with('category')->get();
        
        return view('admin.subcategory.subcategory_list')->withSubcategories($subcategories);
    }

    public function addsubcategoryForm(){

         $maincategories = Maincategory::all();

         $categories = Category::all();

        return view('admin.subcategory.add_subcategory',compact('maincategories','categories'));

    }
   

    public function addsubcategory(AddSubcategoryRequest $request){

        if($request->isMethod('post')){

            try{
                // return $request->all();
                $subcategory = new SubCategory;
                $subcategory->name = $request->name;
                $subcategory->description = $request->description;
                $subcategory->status = $request->status;
                $subcategory->maincategory_id = $request->maincategory_id;
                $subcategory->categories_id = $request->categories_id;
                $subcategory->slug = Str::slug($subcategory->name, '-');
                if ($request->hasFile('image')) 
                {
                   
                    $subcategory_image = $request->file('image');
                    $name1 = $subcategory_image->getClientOriginalName();
                    $destinationFolder1 = "assets/category/";
                        if(file_exists($destinationFolder1.$name1))
                        {
                            unlink($destinationFolder1.$name1);
                        }
                    $subcategory_image->move($destinationFolder1,$name1);  
                    $subcategory_image = url('/') ."/assets/category/".$name1;
                    $subcategory->image=$subcategory_image;

                } 
                // return $subcategory;

               $subcategory->save();

                return Redirect::to('admin/subcategory_list')->withMessage("Subcategory Created Successfully."); 
            
               }catch(Exception $e){

                return redirect()->back()
                    ->withErrors($e->getMessage());
            }
        }

    }


  public function editSubCategory(Request $request, $id){
        try{
           
           $subcategory = SubCategory::query()->where(['id' => $id])->first();
        
           return view('admin.subcategory.update_subcategory')->withSubcategory($subcategory);
            
          }catch(Exception $e){

            return redirect()->back()
            ->withErrors($e->getMessage());
          }
        }


   public function updateSubCategory(UpdateSubcategoryRequest $request){
     
            try{ 
                
                $subcategory = SubCategory::findOrfail($request->id);
                $subcategory->name = $request->name;
                $subcategory->description = $request->description;
                $subcategory->status = $request->status;
                $subcategory->slug = Str::slug($subcategory->name, '-');
             
                if ($request->hasFile('image')) 
                {
                 
                $subcategory_image = $request->file('image');
                $name = $subcategory_image->getClientOriginalName();
                $destinationFolder = "assets/category/";
                if(file_exists($destinationFolder.$name))
                {
                unlink($destinationFolder.$name);
                }
                $subcategory_image->move($destinationFolder,$name);  
                $subcategory_image = url('/') ."/assets/category/".$name;
                $subcategory->image=$subcategory_image;

                } 

               $subcategory->save();

                return Redirect::to('admin/subcategory_list')->withMessage("Sub Category Updated Successfully."); 
            
            }catch(Exception $e){

                return redirect()->back()
                    ->withErrors($e->getMessage());
            }
    } 




     public function deleteSubCategory($id){

     $subcategory = SubCategory::findOrfail($id);
     $subcategory->delete();

     $message = "Sub Category Deleted Successfully!";
        return redirect()->back()->withMessage($message);

    }


    public function getCategoryByName(){
      /*  print_r($_GET);*/

        $maincategory_id=$_GET['maincategory_id'];
        
        $html="<option value='' >Select Category</option>";
        if($maincategory_id !=""){   

        $sessions=Category::where('maincategory_id',$maincategory_id)->get();
        if(count($sessions) > 0){
                foreach ($sessions as  $value) {
                   $html.="<option value='".$value->id."'>".$value->name."</option>";
                }
            }else{
                $html.="<option value='' disabled='disabled' >Menu has no Category</option>";
            }

        }else{
           $html.="<option value='' disabled='disabled' >Menu has no Category</option>"; 
        }
        return response()->json(array('html'=>$html));
 
}

}







