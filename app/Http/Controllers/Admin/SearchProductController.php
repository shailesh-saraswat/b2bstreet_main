<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Models\SearchProduct;
use Illuminate\Support\Facades\Validator;

class SearchProductController extends Controller
{
    public function list(){

        $search= SearchProduct::all();

        return view('admin.search.search_list')->withSearch($search);
       
    }


public function search(Request $request)
{
if($request->ajax())
{
$output="";
$products=DB::table('search_products')->where('title','LIKE','%'.$request->search."%")->get();
if($products)
{
foreach ($products as $key => $product) {
$output.='<tr>'.
'<td>'.$product->id.'</td>'.
'<td>'.$product->title.'</td>'.
'<td>'.$product->description.'</td>'.
'<td>'.$product->price.'</td>'.
'</tr>';
}
return Response($output);
}
}
}
}
