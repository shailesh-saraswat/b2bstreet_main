<?php

namespace App\Http\Controllers\Admin;
use App\Models\Smartblock;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Validator;

use Illuminate\Http\Request;

class SmartblockController extends Controller
{
   
   public function list() {

    $smartblocks = Smartblock::all();

    return view('admin.smartblock.smartblock_list')->withSmartblocks($smartblocks);
   }
   
   public function addForm(){

      return view('admin.smartblock.add_smartblock');
   }

   public function addSmartblock(Request $request){

      try
      {
         $rules = array(

          'title' => 'required',
          'content' => 'required',
          'image' => 'mimes:jpeg,jpg,png,gif|max:10000',
          'status' => 'required',
         );

         $validator = Validator::make($request->all(), $rules);
         if ($validator->fails())
         {
         return redirect()->back()->withErrors($validator)->withInput();
         }

         $smartblock = new Smartblock;
         $smartblock->title =$request->title;
         $smartblock->content =$request->content;
         $smartblock->status =$request->status;
         if ($request->hasFile('image')) 
         {
         $block_image = $request->file('image');

        $name = $block_image->getClientOriginalName();
        $destinationFolder = "public/images/";
        if(file_exists($destinationFolder.$name))
        {
        unlink($destinationFolder.$name);
        }
        $block_image->move($destinationFolder,$name);  
        $block_image ="public/images/".$name;
        $smartblock->image=$block_image;
        }

        $smartblock->save();

        return Redirect::to('admin/smartblock_list')->withMessage("Smart block Created Successfully."); 
            
        }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());
         }

      }


      public function editSmartblock(Request $request , $id){

         try
         {
         $smartblock = Smartblock::query()->where(['id' => $id])->first();

         return view('admin.smartblock.update_smartblock')->withSmartblock($smartblock);

          }catch(Exception $e){

         return redirect()->back()
            ->withErrors($e->getMessage());
         }
      }

      public function updateSmartblock(Request $request){

         try
         {

         $smartblock = Smartblock::findOrfail($request->id);
         $smartblock->title = $request->title;
         $smartblock->content = $request->content;
         $smartblock->status = $request->status;
         if ($request->hasFile('image')) 
         {
         $block_image = $request->file('image');

        $name = $block_image->getClientOriginalName();
        $destinationFolder = "public/images/";
        if(file_exists($destinationFolder.$name))
        {
        unlink($destinationFolder.$name);
        }
        $block_image->move($destinationFolder,$name);  
        $block_image = "public/images/".$name;
        $smartblock->image=$block_image;
        }

        $smartblock->save();

        return Redirect::to('admin/smartblock_list')->withMessage("Smart Block Updated Successfully."); 
            
    }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());
      }
      }


      public function deleteSmartblock($id){

         $smartblock = Smartblock::findOrfail($id);
         $smartblock->delete();

         $message = "Smart Block Deleted Successfully!";
        return redirect()->back()->withMessage($message); 
          
      }
}
