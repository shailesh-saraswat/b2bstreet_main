<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\AddUserRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Facades\Mail;
use App\Models\SubCategory;
use App\Models\Maincategory;
use App\Models\Category;
use App\Models\ProductImage;
use Str;
use Validator;
class UsersController extends Controller
{
     public function index(){
         
        $users = User::query()->where('type',3)->get();
       
        return view('admin.user.buyer-list')->withUsers($users);

    }

    public function addUserForm(){

        return view('admin.user.add-buyer');

    }



    public function addUser(Request $request){
 
      try{
     
      $rules=array(
       'name' => 'required',
       'company_name' => 'required',
       'mobile' => 'required',
       'company_name'=>'required',
       'status'=>'required',
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
        return redirect()->back()->withErrors($validator)->withInput();
        }


    $supplier = new User();
    $supplier->name = $request->name;
    $supplier->email = $request->email;
    $supplier->company_name = $request->company_name;
    $supplier->slug = Str::slug($supplier->company_name, '-');
    $supplier->mobile = $request->mobile;
    $supplier->status = $request->status;
    
     $supplier->save();
            
    return Redirect::to('admin/buyer-list')->withMessage("Buyer Created Successfully."); 
            
    }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
    }


    public function getUser(Request $request, $id)
    {
        try {
            
            $user = User::query()->where(['id' => $id])->first();

            return view('admin.user.update-buyer')->withUser($user);
        
        }catch(Exception $e){

            return redirect()->back()
                    ->withErrors($e->getMessage());

        }
    }


   public function updateUser(UpdateUserRequest $request){
     
        if($request->isMethod('post')){

            try{

                $userId = $request->input('id');

                $user = User::query()->where('id', $userId)->first();
               
                $requestData = $request->all();
                $checkMobile = User::where(['mobile' => $requestData['mobile'], 'type' => 3])->where('id', '!=', $user->id)->first();

                if(isset($checkMobile)){
                    
                    return redirect()->back()
                        ->withErrors("Mobile Number already exists.");
                }

                $requestData['password'] = $requestData['password'] != null ? bcrypt($requestData['password']) : $user->password;

                $user->update($requestData);

                return Redirect::to('admin/buyer-list')->withMessage("Buyer Updated Successfully."); 
            
            }catch(Exception $e){

                return redirect()->back()
                    ->withErrors($e->getMessage());
            }
        }
    }    

    public function deleteUser($id){

        $user = User::findOrfail($id);
        $user->delete();

        $message = "Buyer Deleted Successfully!";
        return redirect()->back()->withMessage($message);

    }

    /*Manage Products */

    public function productList(){
         
        $products = Product::all();
       
        return view('admin.product.list',compact('products'));

    }

    public function getProduct(Request $request, $id){

        try{
           $product = Product::query()->where(['id' => $id])->first();
        $maincategories = Maincategory::all();

        $categories = Category::where('maincategory_id',$product->maincategory_id)->get();

        $subcategories = SubCategory::where('categories_id',$product->categories_id)->get();

        
           return view('admin.product.edit',compact('product','maincategories','categories','subcategories'));
            
          }catch(Exception $e){

            return redirect()->back()
            ->withErrors($e->getMessage());
          }

    }

    public function updateProduct(Request $request){

    
    if($request->isMethod('post')){

            try{
            // return $request->all();
                $product = Product::findOrfail($request->id);
              
                $product->maincategory_id = $request->maincategory_id;
                $product->categories_id = $request->categories_id;
                $product->subcategories_id = $request->subcategories_id;
                $product->title = $request->title;
                $product->price = $request->price;
                $product->description = $request->description;
                 $product->slug = Str::slug($product->title, '-');
                /*$product->quality = $request->quality;
                $product->packaging_type = $request->packaging_type;
                $product->storage_tips = $request->storage_tips;
                $product->packaging = $request->packaging;*/

                $product->status = $request->status;
                  if ($request->hasFile('product_image')) 
             {
                $product->save();
              $this->productgallery($product->id,$request->all());
                return Redirect::to('admin/product-list')->withMessage("Products Updated Successfully."); 
    
            }
            }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());


          }

    }
}
public function addProductForm(){

        $maincategories = Maincategory::all();

        $categories = Category::all();

        $subcategories = SubCategory::all();

        return view('admin.product.add',compact('maincategories','categories','subcategories'));

    }


    public function addProduct(Request $request){

        if($request->isMethod('post')){

            try{
                // return $request->all();
                $product = new Product;
               
                $product->id = $request->id;
                $product->maincategory_id = $request->maincategory_id;
                $product->categories_id = $request->categories_id;
                $product->subcategories_id = $request->subcategories_id;
                $product->title = $request->title;
                $product->price = $request->price;
                $product->description = $request->description;
                  $product->slug = Str::slug($product->title, '-');
               /* $product->quality = $request->quality;
                $product->packaging_type = $request->packaging_type;
                $product->storage_tips = $request->storage_tips;
                $product->packaging = $request->packaging;*/

                $product->status = $request->status;

                 if ($request->hasFile('product_image')) 
             {
             
    
            
            $product->save();
           // $this->productgallery($product->id,$request->hasFile('product_image'));
            $this->productgallery($product->id,$request->all());
            return Redirect::to('admin/product-list')->withMessage("Products Created Successfully."); 

           

            $product->save();

            return Redirect::to('admin/products-list')->withMessage("Products Created Successfully."); 


               }
            }catch(Exception $e){

            return redirect()->back()
            ->withErrors($e->getMessage());
            }

    }
}
 public function productgallery($id,$input) {
  //   dd($input['product_image']);
   
   if(null!=$input['product_image']){

     foreach($input['product_image'] as $thumbnail){
        $gallery=new ProductImage;
        $name = $thumbnail->getClientOriginalName();
        $destinationFolder = public_path('/images');
        if(file_exists($destinationFolder.$name))
           {
           unlink($destinationFolder.$name);
           }
        $thumbnail->move($destinationFolder, $name);
        $data = "/public/images/" . $name;
        $gallery->product_image = $data;
        $gallery->products_id=$id;
        $gallery->save();
         
    }
}
}

    public function deleteProduct($id){

     $product = Product::findOrfail($id);
     $product->delete();

     $message = "Product Deleted Successfully!";
        return redirect()->back()->withMessage($message);

    }

}
