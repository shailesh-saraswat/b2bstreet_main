<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Page;
use App\Models\SubCategory;
use App\Models\Maincategory;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
   public function boot()
    {


        if (\Schema::hasTable('maincategories')) {

           $maincategories=Maincategory::query()->limit(6)->get();

            view()->share('maincategories',$maincategories);
        }

         if (\Schema::hasTable('pages')) {
           $corporate_pages=Page::where('type','2')->where('status','1')->get();
           view()->share('corporate_pages',$corporate_pages);
        }
        

        
    }
}
