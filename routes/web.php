<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


require_once __DIR__.'/backend/admin.php';
require_once __DIR__.'/backend/suplier.php';

            
/*Route::get('/', function () {
    return view('front.index');
});*/


 Route::post('/sup_admin/login', ['uses' =>'Front\CommonController@login'])->name("admin.suplier_login");
Route::any('sup_admin/logout','Front\CommonController@logout')->name("SupAdmin.Logout");


Route::group([
    'prefix' => 'sup_admin',
    'as' => 'Front.',
   'middleware' => ['suplier']
   
], function () {

 // Route::get('/suplier_dashboard', 'Front\CommonController@suplierDashboard')->name('Dashboard');
  /*Route::any('/profile', 'Suplier\AdminController@profile')->name('Profile');
  Route::any('/get-profile', 'Suplier\AdminController@getProfile')->name('Get.Profile');
  Route::any('/update-profile', 'Suplier\AdminController@updateProfile')->name('Update.Profile');
  Route::any('/change-password', 'Suplier\AdminController@changePasswordView')->name('Change.Password.Form');
  Route::post('/change-password', 'Suplier\AdminController@changePassword')->name('Change.Password');*/





});


Route::get('/user-profile','Front\CommonController@userProfile')->name('Front.UserProfile');
Route::get('/all-category','Front\CommonController@alllCategory')->name('Front.AllCategory');

//Route::get('/login','Front\CommonController@loginFront')->name('Front.login');
Route::get('/login','Front\CommonController@loginPage')->name('Front.login');
Route::post('/login','Front\CommonController@storeLogin')->name('Front.login');

Route::post('/loginSuplier','Front\CommonController@storeLogin')->name('suplier_login');
Route::post('/loginBuyer','Front\CommonController@loginBuyer')->name('buyer_login');

Route::post('/loginSuplier','Front\CommonController@storeLogin')->name('suplier_login');
Route::post('/loginBuyer','Front\CommonController@loginBuyer')->name('buyer_login');
Route::get('/','Front\CommonController@index')->name('Front.Index');
Route::get('/pages/{slug}','Front\CommonController@getPage');
/*Route::get('/pages/{slug}','Front\CommonController@getPage');*/

Route::get('/change_password', 'Front\CommonController@changePassword')->name('Front.changePassword');
Route::post('/change_password', 'Front\CommonController@resetPassword')->name('Front.resetPassword');
Route::get('/forgot_password','Front\CommonController@forgotPassword')->name('Front.forgot_password');
Route::post('/forgot_password', 'Front\CommonController@storePassword')->name('Front.forgotPass');

Route::get('/password/{token}', 'Front\CommonController@reset_password_page')->name('Front.reset_password_page');

Route::get('/redirect', 'Front\SocialAuthFacebookController@redirect');
Route::get('/callback', 'Front\SocialAuthFacebookController@callback');


Route::get('/normal_pages/{slug}','Front\CommonController@getNormalPage');
Route::get('/products/{slug}','Front\CommonController@getSubCategory');

########################### Product Lists################################

Route::get('product/{slug}','Front\CommonController@productList');

############################### End #####################################


Route::post('/products/{slug}','Front\CommonController@addServiceForm')->name('Front.Add.ServiceForm');
Route::post('/products/{slug}','Front\CommonController@addBuyForm')->name('Front.Add.BuyForm');



Route::post('/store','Front\CommonController@addQuickForm')->name('Front.Quick.form');

Route::get('/contact','Front\CommonController@Contact')->name('Front.Contact');
Route::post('/contact','Front\CommonController@addContactForm')->name('Front.Add.form');


Route::get('/gallery','Front\CommonController@gallery')->name('Front.Gallery');
Route::get('/pages/{slug}', 'Front\CommonController@about')->name('Front.About');


Route::get('/gallery','Front\CommonController@gallery')->name('Front.Gallery');
Route::get('/pages/{slug}', 'Front\CommonController@about')->name('Front.About');

// Route::get('/product-details/{slug}','Front\CommonController@productDetails')->name('Front.Product Details');

 

Route::get('/sign_up','Front\CommonController@sign_up')->name('Front.Signup');
Route::post('/sign_up_sup','Front\CommonController@suplierRegister')->name('Front.suplierRegister');
Route::post('/sign_up/{section}','Front\CommonController@storebuyer')->name('Front.storebuyer');

/*Route::get('/sign_up/{section}','Front\CommonController@sign_up')->name('Front.Signup');
Route::post('/sign_up','Front\CommonController@storebuyer')->name('Front.Add.Buyer.sign_up');*/

Route::get('/registration','Front\CommonController@registration')->name('Front.Registration');


//Route::post('/change_password', 'Front\CommonController@changePassword')->name('Front.changPass');
//Route::post('/password', 'Front\CommonController@resetPassword')->name('Front.resetPassword');
//Route::get('/password/{token}', 'Front\CommonController@reset_password_page')->name('Front.reset_password_page');
Route::get('/need_help','Front\CommonController@needHelp')->name('Front.need_help');
Route::post('/need_help','Front\CommonController@storeNeedHelp')->name('Front.store.need_help');
Route::get('/msme-interviews','Front\CommonController@msmeInterviews')->name('Front.msme-interviews');
Route::get('/covid-19','Front\CommonController@covid')->name('Front.Covid-19');

Route::get('/get_state_by_id' , 'Front\CommonController@getStateByName');

//Route::get('/product-details','Front\CommonController@productDetails')->name('Front.productDetails');

Route::post('/add-supplier-contact','Front\CommonController@supplierContact')->name('Front.productSunil');



/*Enquiry Form Route*/

Route::post('/add_enquiry_form' , 'Front\CommonController@addEnquiryForm')->name('Add.EnquiryForm');



Route::any('/suplier-profile' , 'Front\CommonController@addSuplierRequirment')->name('Front.StoresuplierProfile');



Route::get('/profile1','Front\CommonController@profile1')->name('Front.Profile1');

Route::get('/notification','Front\CommonController@notification')->name('Front.Notification');



/*Buyer Requirments*/

Route::any('/add_buyer_requirment','Front\CommonController@addBuyerRequirment')->name('Add.BuyerRequirment'); 
Route::any('/edit-user-profile','Front\CommonController@editUserProfile')->name('Front.Edit.UserProfile');
/*Route::any('/searchCategory','Front\CommonController@searchCategory')->name('Front.Allsearch');*/


//Route::get('/','Front\CommonController@searchList');
Route::any('/search1','Front\CommonController@search');





/*Buyer Requirments*/
Route::get('/notification','Front\CommonController@notification')->name('Front.Notification');
Route::any('/add_buyer_requirment','Front\CommonController@addBuyerRequirment')->name('Add.BuyerRequirment'); 

//Route::get('/edit-user-profile/{id}','Front\CommonController@editUserProfile')->name('Front.Edit.UserProfile');

Route::get('/edit-user-profile/{id}' ,'Front\CommonController@editbuyer')->name('Front.Edit.UserProfile');

Route::post('/edit-user-profile/{id}','Front\CommonController@updateBuyerProfile')->name('Front.Edit.UserProfile');

Route::post('/productmodal','Front\CommonController@productmodal');



/*Route::get('/edit-user-profile/{id}' , 'Front\CommonController@editBuyerProfile')->name('Edit.BuyerProfile');*/
/*Route::post('/edit-user-profile' , 'Admin\CommonController@updateBuyerProfile');*/

Route::any('/hello','Front\CommonController@hello')->name('Front.hello');
Route::get('/recommended-video','Front\CommonController@recommendedVideo')->name('Front.recommendedVideo');
Route::get('/trending-video','Front\CommonController@trendingVideo')->name('Front.trendingVideo');
Route::get('/category/{slug}','Front\CommonController@categoryProduct')->name('Front.category');
Route::get('/package','Front\CommonController@package')->name('Front.Package');
Route::post('/add_package','Front\CommonController@packageForm')->name('Front.Add.Package');

Route::any('/blog','Front\CommonController@blog')->name('Front.Blog');
Route::any('/blog-info','Front\CommonController@blogInfo')->name('Front.BlogInfo');
Route::get('/career','Front\CommonController@career')->name('Front.Career');
Route::post('/career','Front\CommonController@careerForm')->name('Front.Add.Career');
Route::any('/press-release','Front\CommonController@pressRelease')->name('Front.pressRelease');
Route::get('/success-story','Front\CommonController@successStory')->name('Front.successStory');
Route::get('/thank-you', 'Front\CommonController@thankYou')->name('Front.thank-you');
Route::get('/product_page', 'Front\CommonController@productPage')->name('Front.productPage');
Route::get('/hot-products', 'Front\CommonController@hotProduct')->name('Front.hotProduct');
Route::get('/trending-products', 'Front\CommonController@trendingProducts')->name('Front.TrendingProducts');
Route::get('/hot-seller', 'Front\CommonController@hotSeller')->name('Front.hotSeller');
Route::get('/complaint','Front\CommonController@complaint')->name('Front.Complaint');
Route::post('/complaint','Front\CommonController@addComplaint')->name('Front.Add.Complaint');



Route::get('/{id}' , 'Front\CommonController@suplierProfileData')->name('Front.suplierProfile');

Route::get('/{id}/{slug}','Front\CommonController@productDetails')->name('Front.productDetails');










