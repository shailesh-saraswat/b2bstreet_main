<?php

use Illuminate\Support\Facades\Route;


Route::any('/admin', ['uses' => 'Admin\LoginController@loginPage']);

Route::post('/admin/login', ['uses' =>'Admin\LoginController@login'])->name("admin_login");
Route::any('admin/logout','Admin\LoginController@logout')->name("Admin.Logout");


Route::group([
    'prefix' => 'admin',
    'as' => 'Admin.',
    'middleware' => ['admin']
   
   
], function () {

  Route::get('/admin_dashboard', 'Admin\LoginController@index')->name('Dashboard');
  Route::any('/profile', 'Admin\AdminController@profile')->name('Profile');
  Route::any('/get-profile', 'Admin\AdminController@getProfile')->name('Get.Profile');
  Route::any('/update-profile', 'Admin\AdminController@updateProfile')->name('Update.Profile');
  Route::any('/change-password', 'Admin\AdminController@changePasswordView')->name('Change.Password.Form');
  Route::post('/change-password', 'Admin\AdminController@changePassword')->name('Change.Password');


/*Manage user*/

Route::any('/buyer-list','Admin\UsersController@index')->name('Buyers.List');
Route::get('/add-buyer' ,'Admin\UsersController@addUserForm' )->name('Add.Buyer.Form');
Route::post('/add-buyer' , 'Admin\UsersController@addUser')->name('Add.Buyer');
Route::get('/delete-buyer/{id}' , 'Admin\UsersController@deleteUser')->name('delete-buyer');
Route::get('/edit-buyer/{id}' , 'Admin\UsersController@getUser')->name('Get.User');
Route::post('/update-buyer', 'Admin\UsersController@updateUser')->name('Update.User');

/*Route::any('/buyer-list','Admin\UsersController@index')->name('Buyer.List');
Route::get('/add-user' ,'Admin\UsersController@addUserForm' )->name('Add.User.Form');
Route::post('/add-user' , 'Admin\UsersController@addUser')->name('Add.User');
Route::get('/delete-user/{id}' , 'Admin\UsersController@deleteUser')->name('Delete.User');
Route::get('/edit-user/{id}' , 'Admin\UsersController@getUser')->name('Get.User');
Route::post('/update-user', 'Admin\UsersController@updateUser')->name('Update.User');*/


/*Manage Main Category*/

Route::get('/maincategory_list' , 'Admin\MaincategoryController@list')->name('maincategory_list');
Route::get('/add_maincategory', 'Admin\MaincategoryController@addmaincategoryForm')->name('Add.Maincategory.Form');
Route::post('/add_maincategory' , 'Admin\MaincategoryController@addMaincategary')->name('Add.Maincategory');
Route::get('/delete_maincategory/{id}' , 'Admin\MaincategoryController@deleteMaincategory')->name('Delete.Maincategory');
Route::get('/update/{id}' , 'Admin\MaincategoryController@getMaincategory')->name('Store.Maincategory');
Route::post('/update_maincategory', 'Admin\MaincategoryController@updateMaincategory')->name('Update.Maincategory');




/*Manage Category*/

Route::get('/category_list' , 'Admin\CategoryController@index')->name('category_list');
Route::get('/add_category' , 'Admin\CategoryController@addcategoryForm')->name('Add.Category.Form');
Route::post('/add_category' , 'Admin\CategoryController@addCategory')->name('Add.Category');
Route::get('/delete_category/{id}' , 'Admin\CategoryController@deleteCategory')->name('Delete.Category');
Route::get('/edit_category/{id}' , 'Admin\CategoryController@editcategory');
Route::post('/update_category', 'Admin\CategoryController@updateCategory')->name('Update.Category');
Route::get('/get_image', 'Admin\CategoryController@imageUpload')->name('Get.Profile');



/*Manage Sub Category*/


Route::get('/subcategory_list' , 'Admin\SubcategoryController@list')->name('subcategory_list');
Route::get('/add_subcategory' , 'Admin\SubcategoryController@addsubcategoryForm')->name('Add.Subcategory.Form');
Route::post('/add_subcategory' , 'Admin\SubcategoryController@addsubcategory')->name('Add.Category');
Route::get('/delete_subcategory/{id}' , 'Admin\SubcategoryController@deleteSubCategory')->name('Delete.Subcategory');
Route::get('/edit_subcategory/{id}' , 'Admin\SubcategoryController@editSubCategory')->name('Edit.Subcategory');
Route::post('/update_subcategory', 'Admin\SubcategoryController@updateSubCategory')->name('Update.Subcategory');
Route::get('/get_category_by_menu_id' , 'Admin\SubcategoryController@getCategoryByName');



/*index route function*/

Route::get('/video_list' , 'Admin\VideoController@list')->name('Video.Lists');
Route::get('/add_video' , 'Admin\VideoController@createForm')->name('Add.Video.Form');
Route::post('/add_video' , 'Admin\VideoController@addvideo')->name('Add.Video');
Route::get('/edit_video/{id}' , 'Admin\VideoController@editVideo')->name('Edit.Video');
Route::post('/update_video' , 'Admin\VideoController@updateVideo')->name('Update.Video');
Route::get('/delete_video/{id}' , 'Admin\VideoController@deleteVideo')->name('Delete.Video');

/*Banner route function*/

Route::get('/banner_list' , 'Admin\BannerController@list')->name('Banner.Lists');
Route::get('/add_banner' , 'Admin\BannerController@addBannerForm')->name('Add.Banner.Form');
Route::post('/add_banner' ,'Admin\BannerController@addBanner')->name('Add.Banner');
Route::get('/edit_banner/{id}' ,'Admin\BannerController@editbanner')->name('Edit.Banner');
Route::post('/update_banner' ,'Admin\BannerController@updateBanner')->name('Update.Banner');
Route::get('/delete_banner/{id}' , 'Admin\BannerController@deleteBanner')->name('Delete.Banner');


/*MSME Interviews route function*/

Route::get('/interview_list' , 'Admin\InterviewController@list')->name('Interview.Lists');
Route::get('/add_interview' , 'Admin\InterviewController@addForm')->name('Add.Interview.Form');
Route::post('/add_interview' , 'Admin\InterviewController@addInterview')->name('Add.Interview');
Route::get('/edit_interview/{id}' , 'Admin\InterviewController@editInterview')->name('Edit.Interview');
Route::post('/update_interview' , 'Admin\InterviewController@updateInterview')->name('Update.Interview');
Route::get('/delete_interview/{id}' , 'Admin\InterviewController@delete')->name('Delete.Interview');



/*MSME Interviews route function*/

Route::get('/testimonial_list' , 'Admin\TestimonialController@list')->name('Testimonial.Lists');
Route::get('/add_testimonial' , 'Admin\TestimonialController@addForm')->name('Add.Testimonial.Form');
Route::post('/add_testimonial' , 'Admin\TestimonialController@addTestimonial')->name('Add.Testimonial');
Route::get('/edit_testimonial/{id}' , 'Admin\TestimonialController@editTestimonial')->name('Edit.Testimonial');
Route::post('/update_testimonial' , 'Admin\TestimonialController@updateTestimonial')->name('Update.Testimonial');
Route::get('/delete_testimonial/{id}' , 'Admin\TestimonialController@deleteTestimonial')->name('Delete.Testimonial');


/*Smart Lock function*/

Route::get('/smartblock_list' , 'Admin\SmartblockController@list')->name('Smartblock.Lists');
Route::get('/add_smartblock' , 'Admin\SmartblockController@addForm')->name('Add.Smartblock.Form');
Route::post('/add_smartblock' ,'Admin\SmartblockController@addSmartblock')->name('Add.Smartblock');
Route::get('/edit_smartblock/{id}' , 'Admin\SmartblockController@editSmartblock')->name('Edit.Smartblock');
Route::post('/update_smartblock' , 'Admin\SmartblockController@updateSmartblock')->name('Update.Smartblock');
Route::get('/delete_smartblock/{id}' , 'Admin\SmartblockController@deleteSmartblock')->name('Delete.Smartblock');


/*Smart Lock Route function*/

Route::get('/ourteam_list' , 'Admin\OurteamController@list')->name('Ourteam.Lists');
Route::get('/add_ourteam' , 'Admin\OurteamController@addForm')->name('Add.Ourteam.Form');
Route::post('/add_ourteam' , 'Admin\OurteamController@addOurteam')->name('Add.Ourteam');
Route::get('/edit_ourteam/{id}' , 'Admin\OurteamController@editOurteam')->name('Edit.Ourteam');
Route::post('/update_ourteam' , 'Admin\OurteamController@updateOurteam')->name('Update.Ourteam');
Route::get('/delete_ourteam/{id}' , 'Admin\OurteamController@deleteOurteam')->name('Delete.Ourteam');


/*Pages Route function*/

Route::get('/page_list' , 'Admin\PageController@list')->name('Page.Lists');
Route::get('/add_page' , 'Admin\PageController@addForm')->name('Add.Page.Form');
Route::post('/add_page' , 'Admin\PageController@addPage')->name('Add.Page');
Route::get('/edit_page/{id}' , 'Admin\PageController@editPage')->name('Edit.Page');
Route::post('/update_page' , 'Admin\PageController@updatePage')->name('Update.Page');
Route::get('/delete_page/{id}' , 'Admin\PageController@deletePage')->name('Delete.Page]');


/*Pages About function*/ 

Route::get('/our_client_list' , 'Admin\OurClientController@list')->name('OurClient.Lists');
Route::get('/add_our_client' , 'Admin\OurClientController@addForm')->name('Add.OurClient.Form');
Route::post('/add_our_client' , 'Admin\OurClientController@addOurClient')->name('Add.OurClient');
Route::get('/edit_our_client/{id}' , 'Admin\OurClientController@editOurClient')->name('Edit.OurClient');
Route::post('/update_our_client' , 'Admin\OurClientController@updateOurClient')->name('Update.OurClient');
Route::get('/delete_our_client/{id}' , 'Admin\OurClientController@deleteOurClient')->name('Delete.OurClient');


/*Gallery Route function*/ 

Route::get('/gallery_list' , 'Admin\GalleryController@list')->name('Gallery.Lists');
Route::get('/add_gallery' , 'Admin\GalleryController@addFormGallery')->name('Gallery.Add.Form');
Route::post('/add_gallery' , 'Admin\GalleryController@addGallery')->name('Add.Gallery');
Route::get('/edit_gallery/{id}' , 'Admin\GalleryController@editGallery')->name('Edit.Gallery');
Route::post('/update_gallery' , 'Admin\GalleryController@updateGallery')->name('Update.Gallery');
Route::get('/delete_gallery/{id}' , 'Admin\GalleryController@deleteGallery')->name('Delete.Gallery');


/*<=================Forms Route===============>*/

   /*Enquiry Form Routs*/

Route::get('/enquiry_form_list' , 'Admin\EnquiryFormController@list')->name('EnquiryForm.Lists');
Route::get('/delete_enquiry_form/{id}' , 'Admin\EnquiryFormController@delete')->name('Delete.EnquiryForm');

/*Contact Form Routes*/

Route::get('/complaint_list' , 'Admin\EnquiryFormController@complaintList')->name('Complaint.Lists');
Route::get('/delete_complaint/{id}' , 'Admin\EnquiryFormController@complaintDelete')->name('Delete.Complaint');


/*Contact Form Routs*/

Route::get('/contact_form_list' , 'Admin\EnquiryFormController@contactList')->name('ContactForm.Lists');
Route::get('/delete_contact_form/{id}' , 'Admin\EnquiryFormController@ContactDelete')->name('Delete.ContactForm');


/*Quick Form Routs*/

Route::get('/Quick_form_list' , 'Admin\EnquiryFormController@QuickList')->name('QuicktForm.Lists');
Route::get('/delete_quick_form/{id}' , 'Admin\EnquiryFormController@QuickDelete')->name('Delete.QuickForm');
Route::get('/notify' , 'Admin\EnquiryFormController@notify')->name('Notify.Lists');


/*Career Form Routes*/

Route::get('/carrer_form_list' , 'Admin\EnquiryFormController@careerList')->name('Carrer.Lists');
Route::get('/carrer/{id}' , 'Admin\EnquiryFormController@carrerDelete')->name('Delete.Carrer');




/*Buyer Requirment Route function*/ 

Route::get('/buyer_requirement_lists' , 'Admin\EnquiryFormController@buyerRequirementlist')->name('BuyerRequirement.Lists');

Route::get('/delete_buyer_requirement/{id}' , 'Admin\EnquiryFormController@deleteBuyerRequirement')->name('Delete.BuyerRequirement');


/*Supplier Contact Form Route function*/ 

Route::get('/supplier_contact_lists' , 'Admin\EnquiryFormController@supplierContactlist')->name('SupplierContactForm.Lists');
Route::get('/delete_supplier_contact/{id}' , 'Admin\EnquiryFormController@deletesupplierContact')->name('Delete.SupplierContactForm');


/*Products Route function*/

Route::get('/products_lists' , 'Admin\ProductController@list')->name('Products.Lists');
Route::get('/add_products' , 'Admin\ProductController@addProductForm')->name('Add.Products.Form');
Route::post('/add_products' , 'Admin\ProductController@addProduct')->name('Add.Product');
Route::get('/edit_products/{id}' , 'Admin\ProductController@editProduct')->name('Edit.Products');
Route::post('/update_products', 'Admin\ProductController@productUpdate')->name('Update.Product');
Route::get('/delete_products/{id}' , 'Admin\ProductController@deleteProduct')->name('Delete.Products');
Route::get('/delete_product_images/{id}' , 'Admin\ProductController@deleteProductImages')->name('Delete.ProductImages');


Route::get('/get_category_by_menu_id' , 'Admin\ProductController@getCategoryByName');
Route::get('/get_subcategory_by_id' , 'Admin\ProductController@getSubCategoryByName');


/*Supplier Route*/

Route::any('/supplier_list','Admin\SupplierController@list')->name('Suppliers.List');
Route::get('/add_supplier' ,'Admin\SupplierController@addSupplierForm' )->name('Add.Supplier');
Route::post('/add_supplier' , 'Admin\SupplierController@addSupplier')->name('Add.Supplier');
Route::get('/edit_supplier/{id}' , 'Admin\SupplierController@editSupplier')->name('Edit.Supplier');
Route::post('/update_supplier', 'Admin\SupplierController@suppliertUpdate')->name('Update.Supplier');
Route::get('/delete_supplier/{id}' , 'Admin\SupplierController@deleteSupplier')->name('Delete.Supplier');
Route::get('/supplier_pannel/{id}' , 'Admin\SupplierController@supplierPannel')->name('supplierPannel');


/*Company of the weeks Route*/

Route::any('/companyofweek_list','Admin\CompanyOfWeekController@list')->name('CompanyOfWeek.Lists');
Route::get('/add_companyofweek' ,'Admin\CompanyOfWeekController@addCompanyForm' )->name('Add.CompanyOfWeek.Form');
Route::post('/add_companyofweek' , 'Admin\CompanyOfWeekController@addCompanyOfWeek')->name('Add.CompanyOfWeek');
Route::get('/edit_companyofweek/{id}' , 'Admin\CompanyOfWeekController@editCompanyOfWeek')->name('Edit.CompanyOfWeek');
Route::post('/update_companyofweek', 'Admin\CompanyOfWeekController@UpdateCompanyOfWeek')->name('Update.CompanyOfWeek');
Route::get('/delete_companyofweek/{id}' , 'Admin\CompanyOfWeekController@deleteCompanyOfWeek')->name('Delete.CompanyOfWeek');


Route::get('/view_notify/{id}' , 'Admin\EnquiryFormController@editQuick')->name('Edit.editQuick');
//Route::post('/update_supplier', 'Admin\SupplierController@suppliertUpdate')->name('Update.Supplier');



/*Success Story Route*/

Route::any('/success_story_list','Admin\SuccessStoryController@list')->name('SuccessStory.Lists');
Route::get('/add_success_story' ,'Admin\SuccessStoryController@addstoryForm' )->name('Add.SuccessStory.Form');
Route::post('/add_success_story' , 'Admin\SuccessStoryController@addSuccessStory')->name('Add.SuccessStory');
Route::get('/edit_success_story/{id}' , 'Admin\SuccessStoryController@editSuccessStory')->name('Edit.SuccessStory');
Route::post('/update_success_story', 'Admin\SuccessStoryController@UpdateSuccessStory')->name('Update.SuccessStory');
Route::get('/delete_success_story/{id}' , 'Admin\SuccessStoryController@deleteSuccessStory')->name('Delete.SuccessStory');

/*Success Story Route*/

Route::any('/press_release_list','Admin\PressReleaseController@list')->name('PressRelease.Lists');
Route::get('/add_press_release' ,'Admin\PressReleaseController@pressReleaseForm' )->name('Add.PressRelease.Form');
Route::post('/add_press_release' , 'Admin\PressReleaseController@addPressRelease')->name('Add.PressRelease');
Route::get('/edit_press_release/{id}' , 'Admin\PressReleaseController@editPressRelease')->name('Edit.PressRelease');
Route::post('/update_press_release', 'Admin\PressReleaseController@UpdatePressRelease')->name('Update.PressRelease');
Route::get('/delete_press_release/{id}' , 'Admin\PressReleaseController@deletePressRelease')->name('Delete.PressRelease');

/*Career Route*/

Route::any('/career_list','Admin\CareerController@list')->name('Career.Lists');
Route::get('/add_career' ,'Admin\CareerController@careerForm' )->name('Add.Career.Form');
Route::post('/add_career' , 'Admin\CareerController@addCareer')->name('Add.Career');
Route::get('/edit_career/{id}' , 'Admin\CareerController@editCareer')->name('Edit.Career');
Route::post('/update_career', 'Admin\CareerController@UpdateCareer')->name('Update.Career');
Route::get('/delete_career/{id}' , 'Admin\CareerController@deleteCareer')->name('Delete.Career');
Route::get('/delete_career/{id}' , 'Admin\CareerController@deleteCareer')->name('Delete.Career');



Route::get('/search_list','Admin\SearchProductController@list')->name('Search.Lists');
Route::get('/search','Admin\SearchProductController@search');

});



