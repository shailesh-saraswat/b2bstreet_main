<?php

use Illuminate\Support\Facades\Route;


Route::any('/sup_admin', ['uses' => 'Suplier\LoginController@loginPage']);
Route::post('/sup_admin/login', ['uses' =>'Suplier\LoginController@login'])->name("suplier_login");
Route::any('sup_admin/logout','Suplier\LoginController@logout')->name("SupAdmin.Logout");


Route::group([
    'prefix' => 'sup_admin',
    'as' => 'Suplier.',
   

], function () {

  Route::get('/suplier_dashboard', 'Suplier\LoginController@index')->name('Dashboard');
  Route::any('/profile', 'Suplier\SuplierController@profile')->name('Profile');
  Route::any('/get-profile', 'Suplier\SuplierController@getProfile')->name('Get.Profile');
  Route::any('/update-profile', 'Suplier\SuplierController@updateProfile')->name('Update.Profile');
  Route::any('/change-password', 'Suplier\SuplierController@changePasswordView')->name('Change.Password.Form');
  Route::post('/change-password', 'Suplier\SuplierController@changePassword')->name('Change.Password');
  Route::get('/suplier_dashboard', 'Suplier\LoginController@leads')->name('Dashboard');
  Route::get('/package_list', 'Suplier\LoginController@packageList')->name('Dashboard');

  /*Manage user*/

Route::any('/user-list','Suplier\SuplierUserController@list')->name('Users.List');
Route::get('/add-user' ,'Suplier\SuplierUserController@addUserForm' )->name('Add.User.Form');
Route::post('/add-user' , 'Suplier\SuplierUserController@addUser')->name('Add.User');
Route::get('/delete-user/{id}' , 'Suplier\SuplierUserController@deleteUser')->name('Delete.User');
Route::get('/edit-user/{id}' , 'Suplier\SuplierUserController@getUser')->name('Get.User');
Route::post('/update-user', 'Suplier\SuplierUserController@updateUser')->name('Update.User');


/*Products Route function*/

Route::get('/products_lists' , 'Suplier\ProductController@list')->name('Products.Lists');
Route::get('/add_products' , 'Suplier\ProductController@addProductForm')->name('Add.Products.Form');
Route::post('/add_products' , 'Suplier\ProductController@addProduct')->name('Add.Product');
Route::get('/edit_products/{id}' , 'Suplier\ProductController@editProduct')->name('Edit.Products');
Route::post('/update_products', 'Suplier\ProductController@updateProduct')->name('update_products');
//Route::post('/update_products', 'Suplier\ProductController@productUpdateGallery');
Route::get('/delete_products/{id}' , 'Suplier\ProductController@deleteProduct')->name('Delete.Products');
Route::get('/delete_product_images/{id}' , 'Suplier\ProductController@deleteProductImages')->name('Delete.ProductImages');

/*Video Route function*/

Route::get('/video_lists' , 'Suplier\ProductController@videoList')->name('Video.Lists');
Route::get('/add_video' , 'Suplier\ProductController@addVideoForm')->name('Add.Video.Form');
Route::post('/add_video' , 'Suplier\ProductController@addVideo')->name('Add.Video');
Route::get('/edit_video/{id}' , 'Suplier\ProductController@editVideo')->name('Edit.Video');
Route::post('/update_video', 'Suplier\ProductController@updateVideo')->name('update_video');


Route::get('/delete_video/{id}' , 'Suplier\ProductController@deleteVideo')->name('Delete.Video');




Route::get('/get_category_by_menu_id' , 'Suplier\ProductController@getCategoryByName');
Route::get('/get_subcategory_by_id' , 'Suplier\ProductController@getSubCategoryByName');



/*Banner route function*/

Route::get('/banner_list' , 'Suplier\SuplierBannerController@list')->name('Banner.Lists');
Route::get('/add_banner' , 'Suplier\SuplierBannerController@addBannerForm')->name('Add.Banner.Form');
Route::post('/add_banner' ,'Suplier\SuplierBannerController@addBanner')->name('add_banner');
Route::get('/edit_banner/{id}' ,'Suplier\SuplierBannerController@editbanner')->name('edit_banner');
Route::post('/update_banner' ,'Suplier\SuplierBannerController@updateBanner')->name('update_banner');
Route::get('/delete_banner/{id}' , 'Suplier\SuplierBannerController@deleteBanner')->name('delete_banner');

/*<========About Us=======>*/

Route::get('/about_list' , 'Suplier\AboutController@list')->name('About.Lists');
Route::get('/add_about' , 'Suplier\AboutController@addAboutForm')->name('Add.About.Form');
Route::post('/add_about' ,'Suplier\AboutController@addAbout')->name('Add.About');
Route::get('/edit_about/{id}' ,'Suplier\AboutController@editAbout')->name('Edit.About');
Route::post('/update_about' ,'Suplier\AboutController@updateAbouts')->name('update_about');
Route::get('/delete_about/{id}' , 'Suplier\AboutController@deleteAbout')->name('Delete.About');

/*<========Blog=======>*/

Route::get('/blog_list' , 'Suplier\BlogController@list')->name('Blog.Lists');
Route::get('/add_blog' , 'Suplier\BlogController@addBlogForm')->name('Add.Blog.Form');
Route::post('/add_blog' ,'Suplier\BlogController@addBlog')->name('Add.Blog');
Route::get('/edit_blog/{id}' ,'Suplier\BlogController@editBlog')->name('Edit.Blog');
Route::post('/update_blog' ,'Suplier\BlogController@updateBlog')->name('Update.Blog');
Route::get('/delete_blog/{id}' , 'Suplier\BlogController@deleteBlog')->name('Delete.Blog');

/*<========Testimonial=======>*/

Route::get('/testimonial_list' , 'Suplier\SuplierTestimonialController@list')->name('Testimonial.Lists');
Route::get('/add_testimonial' , 'Suplier\SuplierTestimonialController@addFormTestimonial')->name('Add.Testimonial.Form');
Route::post('/add_testimonial' ,'Suplier\SuplierTestimonialController@addTestimonial')->name('Add.Testimonial');
Route::get('/edit_testimonialt/{id}' ,'Suplier\SuplierTestimonialController@editTestimonial')->name('Edit.Testimonial');
Route::post('/update_testimonial' ,'Suplier\SuplierTestimonialController@updateTestimonial')->name('Update.Testimonial');
Route::get('/delete_testimonial/{id}' , 'Suplier\SuplierTestimonialController@deleteTestimonial')->name('Delete.Testimonial');

/*<========Gallery=======>*/

Route::get('/gallery_list' , 'Suplier\GalleryController@list')->name('Gallery.Lists');
Route::get('/add_gallery' , 'Suplier\GalleryController@addFormGallery')->name('Add.Gallery.Form');
Route::post('/add_gallery' ,'Suplier\GalleryController@addGallery')->name('Add.Gallery');
Route::get('/edit_gallery/{id}' ,'Suplier\GalleryController@editGallery')->name('Edit.Gallery');
Route::post('/update_gallery' ,'Suplier\GalleryController@updateGallery')->name('Update.Gallery');
Route::get('/delete_gallery/{id}' , 'Suplier\GalleryController@deleteGallery')->name('Delete.Gallery');

/*<========Our network=======>*/

Route::get('/network_list' , 'Suplier\NetworkController@list')->name('Network.Lists');
Route::get('/add_network' , 'Suplier\NetworkController@addFormNetwork')->name('Add.Network.Form');
Route::post('/add_network' ,'Suplier\NetworkController@addNetwork')->name('Add.Network');
Route::get('/edit_network/{id}' ,'Suplier\NetworkController@editNetwork')->name('Edit.Network');
Route::post('/update_network' ,'Suplier\NetworkController@updateNetwork')->name('Update.Network');
Route::get('/delete_network/{id}' , 'Suplier\NetworkController@deleteNetwork')->name('Delete.Network');


/*<======== Certificate =======>*/

Route::get('/certificate_list' , 'Suplier\CertificateController@list')->name('Certificate.Lists');
Route::get('/add_certificate' , 'Suplier\CertificateController@addCertificateForm')->name('Add.Certificate.Form');
Route::post('/add_certificate' ,'Suplier\CertificateController@addCertificate')->name('Add.Certificate');
Route::get('/edit_certificate/{id}' ,'Suplier\CertificateController@editCertificate')->name('edit_certificate');
Route::post('/update_certificate' ,'Suplier\CertificateController@updateCertificate')->name('Update.Certificate');
Route::get('/delete_certificate/{id}' , 'Suplier\CertificateController@deleteCertificate')->name('Delete.Certificate');

/*<======== Enquiry Form =======>*/

Route::get('/enquiry_list' , 'Suplier\SuplierController@list')->name('Enquiry.Lists');
Route::get('/delete_enquiry/{id}' , 'Suplier\SuplierController@deleteEnquiry')->name('Delete.Enquiry');


Route::get('/admin_redirect' , 'Suplier\LoginController@adminRedirect');

});



