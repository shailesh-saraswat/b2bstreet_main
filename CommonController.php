<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Maincategory;
use App\Models\SubCategory;
use App\Models\Category;
use App\Models\Banner;
use App\Models\Video;
use App\Models\Testimonial;
use App\Models\Interview;
use App\Models\Ourteam;
use App\Models\Page;
use App\Models\Smartblock;
use App\Models\EnquiryForm;
use App\Models\OurClient;
use\App\Models\Gallery;
use App\Models\ContactForm;
use App\Models\Form;
use App\Models\ServiceForm;
use App\Models\User;
use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use DB;
use Illuminate\Support\Facades\Hash;
use Session;
use App\Models\BuyerRequirement;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use App\Mail\ForgotPassword;
use Carbon\Carbon;
use Response;
use App\Models\SuplierBanner;
use App\Models\SuplierAbout;
use App\Models\SuplierGallery;


class CommonController extends Controller
{

   public function index(Request $request)
    {
      
      $banner=Banner::query()->where('status',1)->first();
      $categories = Category::query()->where('status',1)->limit(16)->get()->toArray();
      $cat = Category::query()->where('status',1)->limit(10)->get()->toArray();
      $video_trending = Video::query()->with('user')->where('status',1)->where('is_trending',1)->get();
      $video_recommended = Video::query()->with('user')->where('status',1)->where('is_recommended',1)->get();
      $testimonials = Testimonial::all();
      $interviews = Interview::all();
      $smartblocks=Smartblock::query()->where('status',0)->get();
      $products=Product::query()->with('firstImage')->orderBy('id','desc')->limit('15')->get();
      $user = User::all();
      $medic = SubCategory::groupBy('categories_id')->where('maincategory_id',2)->get();
      $products_data = Product::select('title')->get();
       
       foreach($products_data as $key=>$list)
       {
        $pData[] = $list->title;
       }
      
        

      return view('front.index')->with('categories',$categories)->with('banner',$banner)->with('video_trending', $video_trending)->with('video_recommended', $video_recommended)->with('testimonials',$testimonials)->with('interviews',$interviews)->with('smartblocks',$smartblocks)->with('products',$products)->with('user',$user)->with('cat',$cat)->with('medic',$medic)->with('produc',$pData);    
      }

    public function suplierProfile($id)
    { 
    $banners = SuplierBanner::where('user_id',$id)->limit(1)->get();
    $abouts = SuplierAbout::where('user_id',$id)->limit(1)->get();
    $galleries = SuplierGallery::where('user_id',$id)->get();
    $networks = Network::where('user_id',$id)->get();
    $testimonials = SuplierTestimonial::where('user_id',$id)->get();
    $certificates = Certificate::where('user_id',$id)->get();
    $blogs = Blog::where('user_id',$id)->get();
    $products = Product::where('user_id',$id)->get();
    $product_img = ProductImage::where('products_id',$id)->get();

    return view('front.suplier-profile')->withBanners($banners)->withAbouts($abouts)->withGalleries($galleries)->withNetworks($networks)->withTestimonials($testimonials)->withCertificates($certificates)->withBlogs($blogs)->withProducts($products)->withProductImages($product_img);
    }
      public function getPage($slug)
      {
      
      $page=Page::where('slug',$slug)->first();

      if(!$page){
        return redirect()->back();
      }
      return view('front.page',compact('page'));
      }



      public function getSubCategory($slug)
      {

        
      $products=SubCategory::where('slug',$slug)->first();
     
       $product_page = Product::with('main_category')->with('category')->with('subcategory')->where('subcategories_id',$products->id)->get();
     
    $data = SubCategory::where('name','!=',$slug)->where('categories_id',$products->categories_id)->get();
     
    

     
      
      return view('front.products',compact('products','product_page','data'));
      }



     public function about($slug)
     {
      $page = Page::where('slug',$slug)->first();
  
      $view="front.page";

      $clients = OurClient::all();
       
      $ourteams = OurTeam::all();
      $smarts=Smartblock::query()->where('status',1)->get();
          
      if($slug=='about-us')
      {
         $view="front.about";
      }
         
      return view($view,compact('page','ourteams'))->withClients($clients)->with('smarts',$smarts);
      }
 

      public function addEnquiryForm(Request $request)
      {
         // return $request->all();

        $enquiry = new EnquiryForm();
        $enquiry->page_id = $request->input('page_id');
        $enquiry->name = $request->name;
        $enquiry->company_name = $request->company_name;
        $enquiry->phone = $request->phone;
        $enquiry->email = $request->email;
        $enquiry->product_name = $request->product_name;
        $enquiry->qty = $request->qty;
        $enquiry->address = $request->address;
         
        $enquiry->save();

        return Redirect()->back()->withMessage("Your Enquiry Form has been Successfully");

      }
      

   


     public function Contact()
     {

      return view('front.contact');
     }
      
      public function addContactForm(Request $request)
      {
         try{

            $rules=array(
            
           'name' =>'required',
           'email' => 'required|email|unique:users,email,',
           'subject' =>'required',
           'massage' =>'required'
            
            );

            $validator = Validator::make($request->all(),$rules);
            if($validator->fails())
            {
               return redirect()->back()->withErrors($validator)->withInput();
            }

         $contact = new ContactForm();
         $contact->name = $request->name;
         $contact->email =$request->email;
         $contact->subject =$request->subject;
         $contact->massage =$request->massage;

         $contact->save();

         Session::flash('reg_message', "Your Contact Form has been Successfully.");
           return Redirect()->back();
         }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
      
      }



      public function addQuickForm(Request $request){

     
         try{

            $rules=array(
             
             'name'  =>'required',
             'company_name' =>'required',
             'phone' =>  'required|numeric|unique:users,mobile|regex:/[0-9]{10}/',
             'email' => 'required|email|unique:users,email,',
             'product_name'=>'required',
             'quality'  =>'required',
             'address' =>'required',

            );

             $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);

            }
            //return $request;
            $quickforms = new Form();
            $quickforms->name=$request->name;
            $quickforms->company_name=$request->company_name;
            $quickforms->phone=$request->phone;
            $quickforms->email=$request->email;
            $quickforms->product_name=$request->product_name;
            $quickforms->quality=$request->quality;
            $quickforms->address=$request->address;

            $quickforms->save();

             Session::flash('reg_message', "Your Quick Enquiry Form has been Successfully.");
                return Redirect()->back();
         }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
        
   }
   
   

   public function addServiceForm(Request $request){

     try{

            $rules=array(
             
             'name'  =>'required',
             'company_name' =>'required',
             'phone' => 'required',
             'email' => 'required',
             'comment' =>'required',

            );

             $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);

            }
            //return $request;
            $serviceform = new ServiceForm();
            $serviceform->name=$request->name;
            $serviceform->company_name=$request->company_name;
            $serviceform->phone=$request->phone;
            $serviceform->email=$request->email;           
          $serviceform->comment=$request->comment;
          //return $serviceform;
            $serviceform->save();

            return Redirect()->back()->withMessage("Your Service Enquiry Form has been Successfully");

         }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }

   }

      public function gallery()
      {

      $galleries = Gallery::all();

      return view('front.gallery')->withGalleries($galleries);
      }
    
      public function msmeInterviews()
      {

      return view('front.msme-interviews');
      } 

   public function sign_up()
    {
 
       return view('front.sign_up');
    }


    public function store(Request $request)
    {
      
      try{
     
     $rules = array(
                'name' => 'required|regex:/^[\pL\s\-]+$/u',
                'email' => 'required|email|unique:users,email,',
                'password' => 'required | min:8|max:16',
                'confirm_password' => 'required|same:password',
                'mobile' =>  'required|numeric|unique:users,mobile|regex:/[0-9]{10}/',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            } else {


    $user = new User();
    $user->name = $request->name;
    $user->type = $request->type;
    $user->email = $request->email;
    $user->mobile = $request->mobile;
    $user->password = bcrypt($request->password);
    $user->gst = $request->gst;
    $user->address = $request->address;
    $user->company_name = $request->company_name;
   // $user->doc_file = $request->doc_file;
    if ($request->hasFile('doc_file')) 
    {

    $image = $request->file('doc_file');

    $name = $image->getClientOriginalName();
    $destinationFolder = "public/images/doc_file/";
    if(file_exists($destinationFolder.$name))
    {
    unlink($destinationFolder.$name);
    }
    $image->move($destinationFolder,$name);  
    $image ="public/images/doc_file/".$name;
    $user->doc_file=$image;

    } 

     $user->save();
            Session::flash('reg_message', "User Registered Successfully.");
                return Redirect()->back();
    // return Redirect()->back()->withMessage("Your Users has been Register Successfully");
            
    }
    }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
    }


    public function storebuyer(Request $request)
    {
      
      try{
         $rules = array(
                'name' => 'required|regex:/^[\pL\s\-]+$/u',
                'email' => 'required|email|unique:users,email,',
                'password' => 'required | min:8|max:16',
                'confirm_password' => 'required|same:password',
                'mobile' =>  'required|numeric|unique:users,mobile|regex:/[0-9]{10}/',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            } else {
     
    $user = new User();
    $user->name = $request->name;
    $user->type = $request->type;
    $user->email = $request->email;
    $user->mobile = $request->mobile;
    $user->password = bcrypt($request->password);
    
     $user->save();
            Session::flash('reg_message', "User Registered Successfully.");
                return Redirect()->back();
     //return Redirect()->back()->withMessage("Your Users has been Register Successfully");
            
    }
    }catch(Exception $e){

    return redirect()->back()
            ->withErrors($e->getMessage());

    }
    }




     public function loginBuyer(Request $request){
         try{
        //return "test1";

        $rules = array(
           'email' => 'required|email',
           'password' => 'required|'
        );

        /*return $rules; */
        $remember_me = $request->has('remember') ? true : false;

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('/login')
                ->withErrors("Email and Password is required to login.")
                ;

        } else {

            $userdata = array(
                'email' => $request->email,
                'password' => $request->password,
            );
         
            if (Auth::attempt($userdata, $remember_me)) {
                
                if(auth()->user()->type!=3 ){

                    Auth::logout();
                    return Redirect::to('/login')
                        ->withErrors("Forbidden! You do not have permission to access this route.");
                }
                
                    return Redirect::to('/user-profile');
              
            } else {

                return Redirect::to('/login')
                    ->withErrors("The credentials do not match with our records.");

            }
        
         }
     }catch(Exception $e){

            return redirect()->back()
                ->withErrors($e->getMessage());
        }
    }






    public function loginFront()
    {
 
       return view('front.login');
    }


    public function loginPage()
    {

     if(Auth::user()){
          return Redirect::to('sup_admin/suplier_dashboard');  
    }

       return view('front.login');
       
    }

     public function storeLogin(Request $request){
        try{
        //return "test1";

        $rules = array(
           'email' => 'required|email',
           'password' => 'required|'
        );

        /*return $rules; */
        $remember_me = $request->has('remember') ? true : false;

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('/login')
                ->withErrors("Email and Password is required to login.")
                ->withInput();

        } else {

            $userdata = array(
                'email' => $request->email,
                'password' => $request->password,
            );
         
            if (Auth::attempt($userdata, $remember_me)) {
                
                if(auth()->user()->type!=2 ){

                    Auth::logout();
                    return Redirect::to('/login')
                        ->withErrors("Forbidden! You do not have permission to access this route.");
                }
                
                    return Redirect::to('sup_admin/suplier_dashboard');
              
            } else {

                return Redirect::to('/login')
                    ->withErrors("The credentials do not match with our records.");

            }
        
         }
     }catch(Exception $e){

            return redirect()->back()
                ->withErrors($e->getMessage());
        }
    }



        public function supIndex(){

         try{
         
        $users = User::all()->whereNOtIn('type',['1','2'] );
        $countUsers = isset($users) ? count($users) : 0;
        $countEnquiryForm = isset($enquiryforms) ? count($enquiryforms) : 0;
       
        return view('sup_admin.suplier_dashboard')
                ->withCountUsers($countUsers)
                ->withCountEnquiryForm($countEnquiryForm);

        }catch(Exception $e){

            return redirect()->back()
                ->withErrors($e->getMessage());
        }
      }

      public function logout(Request $request) {

        Auth::logout();

        return Redirect::to('/login')->withMessage('Logged out successfully.');
      }
   /**
     * forgot password page.
     *
     * @param   : none
     * @return  : none
    */

      public function forgotPassword()
      {

       return view('front.forgot_password');
      
      } 
      

   /*     public function changePassword($token, Request $request)
    {
        $tokenData = \DB::table('password_resets')
            ->where('token', $token)->first();
        if ($tokenData) {
            $email = $request->email;
            return view('reset_password', compact('token', 'email'));
        } else {
            Session::flash('message', "Token not valid or may be expired please try again.");
            return redirect('/login');
        }
    }*/
     public function reset_password_page($token, Request $request)
    {
        
//dd($request);
        $tokenData = \DB::table('password_resets')
            ->where('token', $token)->first();

        if ($tokenData) {
            $email = $request->email;
           
            return view('front.change_password', compact('token', 'email'));
        } else {
            Session::flash('message', "Token not valid or may be expired please try again.");
            return redirect('/login');
        }
    }

    public function resetPassword(Request $request)
    {
        //Validate input
        $validator = Validator::make($request->all(), [
            'password' => 'required',
        ]);

        //check if payload is valid before moving on
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $password = $request->password;
        
      $email = base64_decode($request->email);

        $user = User::where('email',$email)->first();
      
        if ($user) {
            $user->password = bcrypt($password);
            $user->update(); //or $user->save();
            \DB::table('password_resets')->where('email', $user->email)
                ->delete();
            Session::flash('message', 'Password reset successfully');
            return redirect('/login');
        } else {
            Session::flash('message', 'Network error please try again.');
            return redirect('/login');
        }
    }

    public function forgotPass(Request $request)
    {
       
        try {
            $rules = array(
                'email' => 'required|email',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            } else {
                $user = User::where('email', $request->email)->first();
        
                if ($user) {
                    $token = Str::random(60);
                   
                    $link = url('/') . '/password/' . $token . '?email=' . base64_encode($user->email);
                    \DB::table('password_resets')->insert([
                        'email' => $request->email,
                        'token' => $token,
                        'created_at' => Carbon::now()
                    ]);
                    Mail::to($user->email)->send(new ForgotPassword($user->name, $link));
                    Session::flash('reg_message', "Password reset link sent to your email id successfully.");
                return redirect('/login');
                } else {
                   
                    Session::flash('message', "Email id not exist please try with registered mail id.");
                    return redirect('/forgot_password');
                }
            }
        } catch (QueryException $ex) {
            Session::flash('message', $ex->getMessage());
            return redirect('/forgot_password');
        }
    }

      public function needHelp()
      {
       return view('front.need_help');
      
      }  
  

      public function covid()
      {
      return view('front.covid-19');
      }  


    public function allCategory()
    {
 
      $categories = Category::all();
$subCategories = SubCategory::query()->with('category')->get();

     return view('front.all-category')->with('categories',$categories)->with('subCategories',$subCategories); 
    }

     public function productDetails($slug)
      {
         try{
            $products=Product::where('slug',$slug)->with('product_images')->first();

            //return  $products;
      if(!$products){
        return redirect()->back();
      }
           return view('front.product-details',compact('products'));
            
          }catch(Exception $e){

            return redirect()->back()
            ->withErrors($e->getMessage());
          }
      
      }


    public function editUserProfile()
    {
 
    return view('front.edit-user-profile');
    }

    public function userProfile()
    {
 

     $informations = User::query()->where('type',3)->get();

      return view('front.user-profile')->withInformations($informations);
      }


    public function profile1()
    {
 
    return view('front.profile1');
    }

    public function notification()
    {
 
    return view('front.notification');
    }


    /*Buyer Requirments*/

    public function addBuyerRequirment(Request $request){

     
         try{

            $rules=array(
             
             'product_name'  =>'required',
             'description' =>'required',
             'quantity' =>'required',
             'order' =>'required',
             'email' => 'required|email|unique:users,email,',
             'phone' =>  'required|numeric|unique:users,mobile|regex:/[0-9]{10}/',
             'terms_conditions'=>'',
             'whatsapp'=>'',

            );

             $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);

            }
            //return $request;
            $buyer_requirments = new BuyerRequirement();
            $buyer_requirments->product_name=$request->product_name;
            $buyer_requirments->description=$request->description;
            $buyer_requirments->quantity=$request->quantity;
            $buyer_requirments->order=$request->order;
            $buyer_requirments->email=$request->email;            
            $buyer_requirments->phone=$request->phone;           
            $buyer_requirments->save();

            
             Session::flash('reg_message', "Buyer has been Successfully.");
                return Redirect()->back();
         }catch(Exception $e){

        return redirect()->back()
            ->withErrors($e->getMessage());

    }
        
   }
     public function searchCategory(Request $request)
    {
        //return $request;
        try{
            
        $searchRequest = isset($request->keyword) && $request->keyword ? $request->keyword : "";
                
      $pro_data = Product::query()
        ->select('products.*','users.name','users.company_name','users.mobile','users.address')
        ->where('title', 'LIKE', "%$searchRequest%")
        ->with('product_images')
        ->leftJoin("users", "users.id", "=", "products.user_id")
        ->get()->toArray();



       if(!$pro_data){
        return redirect()->back();
      }

  //return $pro_data;
     // foreach($pro_data as $pro){
     //    if($pro['product_images']){
     //        foreach($pro['product_images'] as $key =>$image){
     //            echo $image['product_image'];
     //        }
     //    }
       
     // }
     // die;
        // if(isset($data) && !empty($data)){

       // dd($products);

       return view('front.search',compact('pro_data'));


       /*  if(isset($data)){
            
            $result['code'] = 200;
            $result['error'] = true;
            $result['message'] = 'Your Search Store list of grocery';
            $result['result'] = $data;                
            return response()->json($result);
            }
            else{
             
            $result['code'] = 404;
            $result['error'] = true;
            $result['message'] = 'No store found using the entered keyword!';
            $result['result'] = $nodata;                
            return response()->json($result);
            }*/    

            }catch(Exception $e){

                return response()->json();
            }


        
    }
}



