<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $user = new \App\Models\User();
        $user->name = "Admin";
        $user->email = "admin@b2bstreets.com";
        $user->mobile = "919999999998";
        $user->password = bcrypt('admin@123');
        $user->type = 1;
        $user->is_admin = 1;
        $user->status = 1;
        $user->save();
        

        $user = new \App\Models\User();
        $user->name = "User";
        $user->email = "user@gmail.com";
        $user->mobile = "919999999999";
        $user->password = bcrypt('user@123');
        $user->type = 2;
        $user->is_admin = 0;
        $user->status = 1;
        $user->save();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
