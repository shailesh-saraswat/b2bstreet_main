<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('email', 191)->unique();
            $table->string('mobile', 191)->unique();
            $table->integer('otp')->nullable();
            $table->tinyInteger('verified')->default(0);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->default('');
            $table->integer('type')->default(2);
            $table->string('profile_image')->nullable();
            $table->tinyInteger('is_subscribed')->default(0);
            $table->string('expires_at')->nullable();
            $table->string('device_type')->nullable();
            $table->string('device_token')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('is_admin')->default(0);
            $table->string('gst')->nullable();
            $table->string('address')->nullable();
            $table->string('company_name')->nullable();
            $table->string('doc_file')->nullable();
            $table->string('user_image')->nullable();
            $table->string('designation')->nullable();
            $table->integer('alternate_number')->nullable();
            $table->string('alternate_email')->nullable();
            $table->integer('pin')->nullable();
            $table->string('state')->nullable();
            $table->string('house_no')->nullable();
            $table->string('locality')->nullable();
            $table->string('city')->nullable();
            $table->string('country')->nullable();
            $table->string('area')->nullable();
            $table->string('landmark')->nullable();
            $table->string('website')->nullable();
            $table->string('gstin')->nullable();
            $table->string('ifcs_code')->nullable();
            $table->string('bank_name')->nullable();
            $table->integer('account_no')->nullable();
            $table->string('account_type')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
