<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStoresIdToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
           $table->string('designation')->nullable();
            $table->integer('alternate_number')->nullable();
            $table->string('alternate_email')->nullable();
            $table->integer('pin')->nullable();
            $table->string('state')->nullable();
            $table->string('house_no')->nullable();
            $table->string('locality')->nullable();
            $table->string('city')->nullable();
            $table->string('country')->nullable();
            $table->string('area')->nullable();
            $table->string('landmark')->nullable();
            $table->string('website')->nullable();
            $table->string('gstin')->nullable();
            $table->string('ifcs_code')->nullable();
            $table->string('bank_name')->nullable();
            $table->integer('account_no')->nullable();
            $table->string('account_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
