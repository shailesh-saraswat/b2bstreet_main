<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('form_name')->nullable();
            $table->string('name')->nullable();
            $table->string('company_name')->nullable();
            $table->string('email', 191)->unique();
            $table->string('phone', 191)->unique();
            $table->string('product_name')->nullable();
            $table->string('quality')->nullable();
            $table->string('address')->nullable();
            $table->string('query')->nullable();
            $table->integer('type')->default(2);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forms');
    }
}
